{**
 * WIKINDX : Bibliographic Management system.
 * @link https://wikindx.sourceforge.io/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2017 Mark Grimshaw-Aagaard <sirfragalot@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 *}
{if isset($pagingList)}
{if $pagingList != false}
<div class="contentPaging">
	{"&nbsp;&nbsp;|&nbsp;&nbsp;"|implode:$pagingList}
</div>
{/if}
{/if}