{**
 * WIKINDX : Bibliographic Management system.
 * @link https://wikindx.sourceforge.io/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2017 Mark Grimshaw-Aagaard <sirfragalot@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 *}

<!-- begin content_file_list.tpl -->
<div class="contentFileList">
{section loop=$fileList name=rows}
<div class="{cycle values="alternate1,alternate2"}">
	{if isset($fileList[rows])}{$fileList[rows]}{/if}
	{if isset($fileListIds[rows])}{$fileListIds[rows]}{/if}
</div>
{/section}
</div>
<!-- end content_file_list.tpl -->