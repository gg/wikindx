/**********************************************************************************
 WIKINDX : Bibliographic Management system.
 @link http://wikindx.sourceforge.net/ The WIKINDX SourceForge project
 @author The WIKINDX Team
 @copyright 2019 Mark Grimshaw-Aagaard <sirfragalot@users.sourceforge.net>
 @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
**********************************************************************************/

/**
* template.js
*
* This file provides the opportunity to alter the CSS display dependent upon certain browsers and to define various pop-up windows.
* template.js is loaded from header.tpl.
* initTemplate() is called in the onLoad function of the body tag in header.tpl.
* The minimum template.js should look like:

function initTemplate()
{
}
*/

var agt=navigator.userAgent.toLowerCase();
var is_nav  = ((agt.indexOf('mozilla')!=-1) && (agt.indexOf('spoofer')==-1)
                && (agt.indexOf('compatible') == -1) && (agt.indexOf('opera')==-1)
                && (agt.indexOf('webtv')==-1) && (agt.indexOf('hotjava')==-1));
var is_ie     = ((agt.indexOf("msie") != -1) && (agt.indexOf("opera") == -1));
var is_opera = (agt.indexOf("opera") != -1);
var is_win   = ((agt.indexOf("win")!=-1) || (agt.indexOf("16bit")!=-1));
var is_mac    = (agt.indexOf("mac")!=-1);
var is_safari = (agt.indexOf("safari") != -1);
var is_konqueror = (agt.indexOf("konqueror") != -1);
var is_gecko = (agt.indexOf("gecko") != -1);

/* ===== common JavaScript functions ===== */
/* ===== Code that should remain here although you can edit various parameters (window sizes, for example). ===== */
