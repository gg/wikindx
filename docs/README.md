---
title: Wikindx Readme
author: 
 - Mark Grimshaw-Aagaard
 - The WIKINDX Team 2019
 - <sirfragalot@users.sourceforge.net>
date: 5th october 2019
lang: en
stylesheet: default.css
css: templates/default.css
---

I originally started developing this as a means to help organise my PhD
research  by catalogueing  bibliographic notations,  references, quotes
and thoughts on a computer via a  program that was not tied to a single
operating system (like similar and expensively commercial software) and
that could be accessed from anywhere on the web (unlike other systems).
Additionally, I wanted a quick way to search, for example, for all
quotes and thoughts referencing a keyword or to be able to automatically
reformat bibliographies to different style guides.

As this is  a system designed to  run on any web server,  I thought its
use could be expanded to groups of researchers who could all contribute
to  and  read  the index. This  concept is  very  similar to  a Wiki
Encyclopaedia  where anyone can add or edit entries in an  on-line
encyclopaedia.

Since the original ideas, various others have been implemented such as a
wide variety of import and export options and, importantly, the
incorportation of a WYSIWYG word processor that can insert citations and
(re)format them with a few clicks of the mouse.  This was powerful
enough for me to write my entire PhD thesis in. (v4 removed this feature
to a plug-in rather than being a core feature.)

Developed under the Creative Commons [CC-BY-NC-SA 2.0] license (since
v5.1, [GPL 2.0] before that), the project homepage can be found at:
<https://sourceforge.net/projects/wikindx/> and the required
files/updates and a variety of enhancements (language localizations and
plug-ins) are freely available there.


# Support, requirements, and compatibility

Wikindx is a web application written in PHP tested on:

 * Linux (Debian), Mac, Windows (and is intended to be OS independent)
 * [Apache] >= 2.x or [nginx] >= 1.11 (or any web Server able to run PHP
   scripts)

Support is provided through SourceForge's project tracking tools: [forum],
[bug tracker], [mailing list].

The developpement style is [Trunk-Based], but sometimes an important
development is the subject of an ephemeral branch.

The version system is not a branch system with long term support for
each one.  Only the trunk gets new features, security and bug fixes that
are not backported.  These developments are made available to the public
at each release of a new version.

The versions are numbered for the history and semi-automatic update
system of the data and the database (each change is applied between your
version and the target version).  However, always read the UPGRADE.txt
file for the steps to be done by hand when upgrading.

So we recommend that you regularly update to the latest version from the
tarball version available in the [SourceForge File] section, especially
if your wikindx is hosted on the web.

If you prefer an installation from a source management client, __we
strongly recommand__ that you use the __stable__ branch on a
__production__ server.

The __trunk__ branch (for developers and testers) can be broken at any
(and for a long) time and damage your database, while the __stable__
branch has been debugged and validated.  __stable__ contains the same
code as the __latest version__ delivered.


## Extensions compatibility

Wikindx, the core application, and officials extensions are developped
together: templates (themes), styles (bibliographic styles), languages
(gettext locales) and PHP plugins.

Wikindx comes with pre-installed languages and styles.  Only the default
template is pre-installed and no plugins are pre-installed.

All extensions are available on [SourceForge File] section for a manual
instllation or via the component update system embeded in Wikindx.

Each official extension is released with a new version of the
application and only for the last version.

For reasons of immaturity of the system of extensions it is recommended
to contribute to the official developpement team so that the extensions
are always compatible with the latest version. If you create your own
extensions it is not guaranteed that they will work on a later version
of Wikindx.


## PHP & MySQL versions

Wikindx         [PHP]             [MySQL]  [MariaDB]
--------------- ----------------- -------- ---------
5.9.0 to latest >= 5.6 and <= 7.4 >= 5.7.5 >= 10.2
5.7.2 to 5.8.2  >= 5.6 and <= 7.3 >= 5.7.5 >= 10.2
5.7.0 to 5.7.1  >= 5.6 and <= 7.2 >= 5.7.5 >= 10.2
5.3.1 to 5.3.2  >= 5.5 and <= 7.1 >= 5.7.5 >= 10.2
5.2.1 to 5.2.2  >= 5.5 and <= 5.6 >= 5.7.5 >= 10.2
5.2.0           >= 5.1 and <= 5.6 >= 5.7.5 >= 10.2
4.2.0 to 4.2.2  >= 5.1 and <= ??? >= 4.1   >= 10.1
4.0.0 to 4.0.5  => 5.1 and <= ??? ???      >= 10.1
3.8.1           >= 4.3 and <= ??? ???      --

PHP support is tested for the core, extentions, and third party software
included. For security purpose we recommend you use an [officialy
supported PHP version] of the PHP Team. Wikindx can support older
versions to facilitate the migration of an installation but are not
intended to support a large number of versions.

Wikindx does not use advanced features of MySQL / MariaDB which should
allow to install it without problems on a recent version. However, there
is no guarantee that MySQL will change its default options to stricter
uses as in the past. If you encounter a problem let us know it.

Support for another database engine is not considered, and support is
limited to the [mysqli] driver.

Minimal versions are strictly understood for decent support of UTF-8 and
runtime-checked.

## Browser compatibility

The  client  accesses  the  database via  a  standard  (graphical)  web
browser. Wikindx  makes use of the tinyMCE editor  (v2.0.4) and this
could limit the web browsers that can be used. A migration project to
CKEditor is underway to address these limitations.

Generally, though, if you use the following, you should be OK:

 * Internet Explorer >= v9
 * Edge >= v10
 * Firefox >= v12
 * Chrome >= v4
 * Safari >= v10
 * Opera >= v12
 * [WebKit based browsers]

You must enable JavaScript execution, otherwise important features such
as searching or creating resources will not work.


# Contributing

We are happy to welcome new contributors.  You can join us to contribute
to the creation of official extensions or the development of Wikindx.

There is a lot to do, not just development. Contact us!


## Localisation (l10n)

We are looking for translators. If you want to see Wikindx in your
native language do not hesitate to join us on [Transifex].

If you only want to report an error without committing yourself, post it
on the [forum] or the wikindx-developers [mailing list].


# Credits

Wikindx includes and uses several free creations. I thank the authors
for their work:

 * [FatCow], Free Icon Set (CC BY 3.0)
 * [TinyMCE], JavaScript WYSIWYG HTML Editor (LGPL 2.1)
 * [TinyMCE Compressor PHP] (LGPL 3.0)
 * [Smarty], PHP templating system (LGPL 3.0)
 * [SmartyMenu], menu plugin for Smarty (LGPL 2.1)
 * [PdfToText], PHP library to extract data from a PDF (LGPL 3.0)
 * [PHPMailer], PHP email sending library (LGPL 2.1)
 * [CKEditor], JavaScript WYSIWYG HTML Editor (LGPL 2.1)
 * [json2.js], JSON in JavaScript (Public Domain)
 * [jpGraph], Graph creating library (QPL 1.0)


[CC-BY-NC-SA 2.0]: <https://creativecommons.org/licenses/by-nc-sa/2.0/>
[GPL 2.0]: <https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html>

[PHP]: <https://secure.php.net/>
[MySQL]: <https://www.mysql.com/fr/>
[MariaDB]: <https://mariadb.org/>
[mysqli]: <https://www.php.net/manual/fr/book.mysqli.php>
[Apache]: <https://httpd.apache.org/>
[nginx]: <https://www.nginx.com/>

[Trunk-Based]: <https://trunkbaseddevelopment.com/>
[SourceForge File]: <https://sourceforge.net/projects/wikindx/files/>
[FatCow]: <https://www.fatcow.com>
[TinyMCE]: <https://www.tiny.cloud/>
[TinyMCE Compressor PHP]: <https://github.com/hakjoon/hak_tinymce/tree/master/tinymce_compressor_php>
[Smarty]: <https://www.smarty.net>
[SmartyMenu]: <http://www.phpinsider.com/php/code/SmartyMenu>
[PdfToText]: <https://github.com/christian-vigh-phpclasses/PdfToText>
[PHPMailer]: <https://github.com/PHPMailer/PHPMailer>
[CKEditor]: <https://ckeditor.com/>
[json2.js]: <https://github.com/douglascrockford/JSON-js>
[jpGraph]: <https://jpgraph.net/>

[WebKit based browsers]: <https://en.wikipedia.org/wiki/List_of_web_browsers#WebKit-based>
[officialy supported PHP version]: <https://www.php.net/supported-versions.php>
[Transifex]: <https://www.transifex.com/saulery/wikindx/dashboard/>

[forum]: <https://sourceforge.net/p/wikindx/discussion/>
[bug tracker]: <https://sourceforge.net/p/wikindx/support-requests/>
[mailing list]: <https://sourceforge.net/p/wikindx/mailman/>

