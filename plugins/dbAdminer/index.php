<?php
/**
 * WIKINDX : Bibliographic Management system.
 * @link https://wikindx.sourceforge.io/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2019 Stéphane Aulery <lkppo@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 */
 
/**
 * dbAdminer class.
 *
 * Wikindx custom wrapper for adminer
 *
 * v1.0 2019
 */

/**
* Import initial configuration and initialize the web server
*/
include_once("core/startup/WEBSERVERCONFIG.php");


class dbAdminer_MODULE
{
    private $pluginmessages;
    private $coremessages;
    private $errors;
    private $config;
    private $session;
    private $vars;
    
    public $authorize;
    public $menus;

// Constructor.
// $menuInit is TRUE if called from MENU.php
	public function __construct($menuInit = FALSE)
	{
		include_once("core/messages/PLUGINMESSAGES.php");
		$this->pluginmessages = new PLUGINMESSAGES('dbAdminer', 'dbAdminerMessages');
		$this->coremessages = FACTORY_MESSAGES::getInstance();
		$this->errors = FACTORY_ERRORS::getInstance();
		include_once(__DIR__ . DIRECTORY_SEPARATOR .  "config.php");
		$this->config = new dbAdminer_CONFIG();
		$this->session = FACTORY_SESSION::getInstance();
		$this->authorize = $this->config->authorize;
		if($menuInit)
		{
			$this->makeMenu($this->config->menus);
			return; // Need do nothing more as this is simply menu initialisation.
		}

		$authorize = FACTORY_AUTHORIZE::getInstance();
		if(!$authorize->isPluginExecutionAuthorised($this->authorize)) // not authorised
			FACTORY_CLOSENOMENU::getInstance(); // die

		$this->vars = GLOBALS::getVars();
    }

// Make the menus
	private function makeMenu($menuArray)
	{
	    $this->menus = array(
	        $menuArray[0] => array($this->pluginmessages->text('menu') => "init")
	    );
	}
	
// This is the initial method called from the menu item.
	public function init()
	{
		return $this->display();
	}
	
	
	public function display($message = FALSE)
	{
		GLOBALS::setTplVar("heading", $this->pluginmessages->text("heading"));
		
		GLOBALS::addTplVar("content",  
        	HTML\p(HTML\a(
        	    "link",
        	    $this->pluginmessages->text("openlink"),
        	    __DIR__ . DIRECTORY_SEPARATOR . "adminer.php",
        	    "_blank"
        	))
    	);
	}
}
