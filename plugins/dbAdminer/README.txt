********************************************************************************
**                                 db Adminer                                 **
**                               WIKINDX module                               **
********************************************************************************


NB. this module is compatible with WIKINDX v5 and up.

Wikindx custom wrapper for Adminer (4.7.3) with a selection of tested plugins for Adminer.

Unzip this file (with any directory structure) into plugins/dbAdminer/.
Thus, plugins/dbAdminer/index.php etc.

NB: the FillLoginForm class is customized to always fill the login form with the db configuration of Wikindx.

Adminer website: https://www.adminer.org/

********************************************************************************

CHANGELOG:

v1.0, 2019
1. Initial release.

--
Stéphane Aulery 2019
