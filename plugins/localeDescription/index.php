<?php
/*****
*	localDescription class.
*
*	v1.3
*
* Store and make available localized versions of the front page description depending on the current language
* localization the user is using.
*
*
*****/

/**
* Import initial configuration and initialize the web server
*/
include_once("core/startup/WEBSERVERCONFIG.php");


class localeDescription_MODULE
{
private $pluginmessages;
private $coremessages;
private $session;

public $authorize;
public $menus;

// Constructor.
// $menuInit is TRUE if called from MENU.php
	public function __construct($menuInit = FALSE)
	{
		$this->coremessages = FACTORY_MESSAGES::getInstance();
		include_once("core/messages/PLUGINMESSAGES.php");
		$this->pluginmessages = new PLUGINMESSAGES('localeDescription', 'localeDescriptionMessages');
		include_once(__DIR__ . DIRECTORY_SEPARATOR . "config.php");
		$this->config = new localeDescription_CONFIG();
		$this->authorize = $this->config->authorize;
		$this->session = FACTORY_SESSION::getInstance();
		if($menuInit) // portion of constructor used for menu initialisation
		{
			$this->makeMenu($this->config->menus);
			return; // need do nothing more.
		}

		$authorize = FACTORY_AUTHORIZE::getInstance();
		if(!$authorize->isPluginExecutionAuthorised($this->authorize)) // not authorised
			FACTORY_CLOSENOMENU::getInstance(); // die

		GLOBALS::setTplVar('heading', $this->pluginmessages->text('heading'));
}
// Make the menus
	private function makeMenu($menuArray)
	{
		$this->menus = array($menuArray[0] => array($this->pluginmessages->text('menu') => "init"));
	}
// This is the initial method called from the menu item.
	public function init()
	{
		return $this->display();
	}
// display
	public function display($message = FALSE)
	{
		if($message)
			$pString = $message;
		else
			$pString = '';
		$pString .= FORM\formHeader("localeDescription_edit");
		$pString .= HTML\p($this->pluginmessages->text("text1"));
		$languages = \UTILS\getLocalizedLanguagesList();
		if((sizeof($languages) == 1) && array_key_exists(WIKINDX_LANGUAGE_DEFAULT, $languages))
		{
			GLOBALS::addTplVar('content', HTML\p($this->pluginmessages->text("onlyEnglish"), "error", "center"));
			FACTORY_CLOSE::getInstance();
		}
		unset($languages[WIKINDX_LANGUAGE_DEFAULT]);
		$size = sizeof($languages) > 5 ? 5 : sizeof($languages);
		$pString .= HTML\p(FORM\selectFBoxValue($this->pluginmessages->text("choose"), "language",
			$languages, $size));
		$pString .= HTML\p(FORM\formSubmit($this->coremessages->text("submit", "Proceed")));
		$pString .= FORM\formEnd();
		GLOBALS::addTplVar('content', $pString);
	}
// edit
	public function edit()
	{
		$db = FACTORY_DB::getInstance();
		$vars = GLOBALS::getVars();
		$co = FACTORY_CONFIGDBSTRUCTURE::getInstance();
		if(!array_key_exists('language', $vars))
		{
			$this->display(HTML\p($this->pluginmessages->text("missingLanguage"), "error", "center"));
			FACTORY_CLOSE::getInstance();
		}
		$field = 'configDescription_' . $vars['language'];
		$db->formatConditions(array('configName' => $field));
		if($input = $db->fetchOne($db->select('config', 'configText')))
			$input = HTML\dbToFormTidy($input);
		$original = HTML\dbToHtmlTidy($co->getOne('configDescription'));
		$pString = HTML\p(HTML\strong($this->pluginmessages->text('original')));
		$pString .= HTML\p($original);
		$pString .= HTML\hr();
		$pString .= HTML\p($this->pluginmessages->text("text2"));
		$tinymce = FACTORY_LOADTINYMCE::getInstance();
		$pString .= FORM\formHeader("localeDescription_write");
		$pString .= FORM\hidden('language', $vars['language']);
		$pString .= $tinymce->loadMinimalTextarea(array('description'), TRUE);
		$pString .= HTML\p(FORM\textareaInput(HTML\strong($vars['language']), "description", $input, 75, 20));
		$pString .= HTML\p(FORM\formSubmit($this->coremessages->text("submit", "Submit")));
		$pString .= FORM\formEnd();
		GLOBALS::addTplVar('content', $pString);
	}
// write
	public function write()
	{
		$db = FACTORY_DB::getInstance();
		$vars = GLOBALS::getVars();
		$field = 'configDescription_' . $vars['language'];
		$db->formatConditions(array('configName' => $field));
		$resultSet = $db->select('config', '*');
		$exists = $db->numRows($resultSet);
		if(!array_key_exists('description', $vars) || !trim($vars['description'])) // delete row if it exists in table
		{
			if($exists)
			{
				$db->formatConditions(array('configName' => $field));
				$db->delete('config');
			}
			$this->display(HTML\p($this->pluginmessages->text("success", $vars['language']), "success", "center"));
			FACTORY_CLOSE::getInstance();
		}
// something to write
		if($exists)
		{
			$db->formatConditions(array('configName' => $field));
			$db->update('config', array('configText' => trim($vars['description'])));
		}
		else
			$db->insert('config', array('configName', 'configText'), array($field, trim($vars['description'])));
		$this->display(HTML\p($this->pluginmessages->text("success", $vars['language']), "success", "center"));
	}
}
