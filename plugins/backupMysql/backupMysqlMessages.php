<?php
/**
 * WIKINDX : Bibliographic Management system.
 * @link https://wikindx.sourceforge.io/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2017 Mark Grimshaw-Aagaard <sirfragalot@users.sourceforge.net>
 * @copyright 2019 Stéphane Aulery <lkppo@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 */

class backupMysqlMessages
{
    public $text = array();

	public function __construct()
	{
	    $domain = mb_strtolower(basename(__DIR__));
	    
		$this->text = array(
			"menu" => dgettext($domain, "Backup Database"),
			"heading" => dgettext($domain, "Backup Database"),
			"backup" => dgettext($domain, "Backup"),
			"noWrite" => dgettext($domain, "plugins/backupMysql/dumps/ is not writeable by the web server user.  It currently has the permissions: ###"),
			"deleted" => dgettext($domain, "Files deleted"),
		);
	}
}
