/**********************************************************************************
 WIKINDX : Bibliographic Management system.
 @link http://wikindx.sourceforge.net/ The WIKINDX SourceForge project
 @author The WIKINDX Team
 @copyright 2019 Mark Grimshaw-Aagaard <sirfragalot@users.sourceforge.net>
 @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
**********************************************************************************/

/**
* Javascript functions for plugins/chooseLanguage
*
* @version 1.0
* @date June 2012
* @author Mark Grimshaw-Aagaard
*/

function chooseLanguageChangeLanguage(language)
{
    window.location='index.php?action=chooseLanguage_resetLanguage&language=' + language;
}