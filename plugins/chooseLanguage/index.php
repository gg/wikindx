<?php
/**
 * WIKINDX : Bibliographic Management system.
 * @link https://wikindx.sourceforge.io/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2017-2019 Mark Grimshaw-Aagaard <sirfragalot@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 */

/*
* Inline plugin: choose a new language.
*/

/**
* Import initial configuration and initialize the web server
*/
include_once("core/startup/WEBSERVERCONFIG.php");


class chooseLanguage_MODULE
{

public $authorize;
public $menus;

// constructor
	public function __construct($menuInit = FALSE)
	{
		include_once(__DIR__ . DIRECTORY_SEPARATOR . "config.php");
		$localconfig = new chooseLanguage_CONFIG();
		$this->authorize = $localconfig->authorize;
		GLOBALS::setTplVar($localconfig->container, $this->display());
		if($menuInit) // portion of constructor used for menu initialisation
		{
			return; // need do nothing more.
		}
		$authorize = FACTORY_AUTHORIZE::getInstance();
		if(!$authorize->isPluginExecutionAuthorised($this->authorize)) // not authorised
			FACTORY_CLOSENOMENU::getInstance(); // die

	}
	private function display()
	{
		$session = FACTORY_SESSION::getInstance();
		$this->config = FACTORY_CONFIG::getInstance();
		
		// For the graphical interface, add the "auto" value that allows to say that the language is chosen by the browser.
		$LanguageNeutralChoice = "auto";
		$languages[$LanguageNeutralChoice] = "Auto";
		$languages = array_merge($languages, \UTILS\getLocalizedLanguagesList());

		$display = "";

		if(count($languages) > 1) {
			$display .= HTML\jsInlineExternal($this->config->WIKINDX_BASE_URL . '/' . WIKINDX_DIR_PLUGINS . '/chooseLanguage/chooseLanguage.js');
			$js = 'onchange="javascript:chooseLanguageChangeLanguage(this.value);"';
			if(array_key_exists($session->getVar("setup_Language", $LanguageNeutralChoice), $languages))
				$display .= FORM\selectedBoxValue(FALSE, "Language", $languages, $session->getVar("setup_Language", $LanguageNeutralChoice), 1, FALSE, $js);
			else
				$display .= FORM\selectFBoxValue(FALSE, "Language", $languages, 1, FALSE, $js);
		}

		return $display;
	}
	public function resetLanguage()
	{
		$vars = GLOBALS::getVars();
		if(array_key_exists('language', $vars))
			$language = $vars['language'];
		$session = FACTORY_SESSION::getInstance();
		$session->setVar("setup_Language", $language);
		header("Location: index.php");
	}
}
