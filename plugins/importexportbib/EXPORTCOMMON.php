<?php
/**
 * WIKINDX : Bibliographic Management system.
 * @link https://wikindx.sourceforge.io/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2017-2019 Mark Grimshaw-Aagaard <sirfragalot@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 */

/*
* EXPORT COMMON class
*
* Provides methods common to the export scripts.
* @author Mark Grimshaw-Aagaard
*/
class EXPORTCOMMON
{
private $db;
private $session;
private $messages;
private $errors;
private $config;
public $filesDir;
public $fullFileName = FALSE;
public $fileName;
public $context;
public $fp;
private $cite;

// Constructor
	public function __construct($outputType = 'plain')
	{
		$this->db = FACTORY_DB::getInstance();
		$this->session = FACTORY_SESSION::getInstance();
		$this->coremessages = FACTORY_MESSAGES::getInstance();
		$this->errors = FACTORY_ERRORS::getInstance();
		$this->config = FACTORY_CONFIG::getInstance();
		$this->cite = FACTORY_CITE::getInstance($outputType);
		$this->filesDir = $this->config->WIKINDX_FILES_DIR;
// Perform some system admin
		FILE\tidyFiles();
	}
/**
* Create a file name
*/
	public function createFileName($string, $extension)
	{
		return list($this->fileName, $this->fullFileName) = FILE\createFileName($this->filesDir, $string, $extension);
	}
/**
* Open/create a file
*/
	public function openFile($string = FALSE, $extension, $mode = 'w')
	{
		if(!$string)
			$string = uniqid();
		$this->context = stream_context_create();
		list($this->fileName, $this->fullFileName) = FILE\createFileName($this->filesDir, $string, $extension);
		$fullFileName = $this->fullFileName;
		if(!$this->fp = fopen($fullFileName, $mode, FALSE, $this->context))
			return FALSE;
		else
		    return TRUE;
	}
/**
* Close a file
*/
	public function closeFile()
	{
	    if ($this->fp) fclose($this->fp);
	}
/**
* Write/append to a file creating it if necessary
*/
	public function writeToFile($string)
	{
		if(!$this->fullFileName) // file not yet created and $fp not yet opened
		{
			list($this->fileName, $this->fullFileName) = $this->createFileName($string, '.bib');
			if(!$this->fullFileName)
				return FALSE;
			$fullFileName = $this->fullFileName;
			$this->context = stream_context_create();
			if($this->fp = fopen($fullFileName, 'w', FALSE, $this->context))
			{
				if(fwrite($this->fp, $string) === FALSE)
					return FALSE;
			}
			else
				return FALSE;
		}
		else if(fwrite($this->fp, $string) === FALSE) // appending because $this->fp has not been closed yet
			return FALSE;
		else
		    return TRUE;
	}
/**
* writeFilenameToSession - add filename to session array
*
* @author Mark Grimshaw-Aagaard
* @param string $fileName
*/
	public function writeFilenameToSession($fileName)
	{
		if($sessVar = $this->session->getVar('fileExports'))
			$sessArray = unserialize($sessVar);
		else
			$sessArray = array();
		if(array_search($fileName, $sessArray) === FALSE)
		{
			$sessArray[] = $fileName;
			$this->session->setVar('fileExports', serialize($sessArray));
		}
	}
// get the SQL either from the basket or from the last view
	public function getSQL()
	{
		$stmt = FACTORY_SQLSTATEMENTS::getInstance();
		if($this->session->getVar('exportBasket'))
		{
			$tempAllIds = $this->session->getVar('list_AllIds');
			$tempListStmt = $this->session->getVar('sql_ListStmt');
			$this->session->setVar('list_AllIds', base64_encode($this->session->getVar('basket_List')));
			include_once('core/modules/basket/BASKET.php');
			$basket = new BASKET();
			$sqlEncoded = base64_encode(serialize(array($basket->returnBasketSql(FALSE, 'creator'))));
			$this->session->setVar('list_AllIds', $tempAllIds);
			$this->session->setVar('sql_ListStmt', $tempListStmt);
			return $sqlEncoded;
		}
		else
		{
			$this->db->DisableFullGroupBySqlMode();
			$sql = $stmt->getExportSql();
			$this->db->EnableFullGroupBySqlMode();
			return $sql;
		}
	}
// Check if there any custom fields in the database and provide options to map these to bibtex fields
	public function getCustomFields()
	{
		if($this->db->tableIsEmpty('resource_custom'))
			return FALSE;
		$this->db->leftJoin('resource_custom', 'resourcecustomCustomId', 'customId');
		$recordset = $this->db->select('custom', array('resourcecustomCustomId', 'customLabel'));
		while($row = $this->db->fetchRow($recordset))
		{
			if($row['resourcecustomCustomId'])
				$customLabels[$row['resourcecustomCustomId']] = stripslashes($row['customLabel']);
		}
		if(!isset($customLabels) || empty($customLabels))
			return FALSE;
		$customLabels = array_unique($customLabels);
		$pString = HTML\p(HTML\strong($this->coremessages->text("misc", "customFieldMap")) .
			' ' . $this->coremessages->text("misc", "customFieldMap2"));
		foreach($customLabels as $id => $label)
		{
			$text = $this->session->getVar("export_Map_$id");
			$pString .= HTML\p(FORM\textInput($label, "Map_$id", $text));
		}
		return $pString;
	}
/**
* formatName - format one name depending on the export type
*
* @author Mark Grimshaw-Aagaard
* @param assocArray $creatorRow
* @param string $exportType
* @return string
*/
	public function formatName($creatorRow, $exportType)
	{
		$surname = $firstname = $initials = '';
// WIKINDX stores Jr., IV etc. at end of surname...
		if($creatorRow['creatorSurname'])
		{
			if($creatorRow['creatorPrefix'])
				$surname = stripslashes($creatorRow['creatorPrefix']) . " " .
				stripslashes($creatorRow['creatorSurname']);
			else
				$surname = stripslashes($creatorRow['creatorSurname']);
		}
		if($creatorRow['creatorFirstname'])
			$firstname = stripslashes($creatorRow['creatorFirstname']);
		if($creatorRow['creatorInitials'])
		{
			if(($exportType == 'endnoteTabbed') || ($exportType == 'endnoteXml'))
				$initials = implode(' ', UTF8::mb_explode(' ', stripslashes($creatorRow['creatorInitials'])));
			else if($exportType == 'ris')
				$initials = implode('.', UTF8::mb_explode(' ', stripslashes($creatorRow['creatorInitials']))) . ".";
			else if($exportType == 'bibtex')
				$initials = implode('. ', UTF8::mb_explode(' ', stripslashes($creatorRow['creatorInitials']))) . ".";
		}
		if($exportType == 'ris')
		{
			if($firstname && $initials)
				return $surname . ',' . $firstname . ',' . $initials;
			else if($firstname)
				return $surname . ',' . $firstname;
			else if($initials)
				return $surname . ',' . $initials;
		}
		else if(($exportType == 'endnoteTabbed') || ($exportType == 'endnoteXml'))
		{
			if($firstname && $initials)
				return $surname . ',' . $firstname . ' ' . $initials;
			else if($firstname)
				return $surname . ',' . $firstname;
			else if($initials)
				return $surname . ',' . $initials;
		}
		else if($exportType == 'bibtex')
		{
			if(preg_match("/(.*)(Sr\.|jr\.)/ui", $surname, $matches))
				$surname = trim($matches[1]) . ", " . trim($matches[2]);
			if(preg_match("/(.*)\s(I|II|III|IV|V|VI|VII|VIII|IX|X)$/u", $surname, $matches))
				$surname = trim($matches[1]) . ", " . trim($matches[2]);
			if($firstname && $initials)

				return $surname . ", " . $firstname . ' ' . $initials;
			else if($firstname)
				return $surname . ", " . $firstname;
			else if($initials)
				return $surname . ", " . $initials;
		}
		return $surname; // if all else fails
	}
/**
* titleFormat - format and return the resource title from the supplied SQL $row
*
* @author Mark Grimshaw-Aagaard
* @param assocArray $row
* @param boolean $bibtex
* @return string
*/
	public function titleFormat($row, $bibtex = FALSE)
	{
// For book_chapter, 'title' is bibtex 'chapter' and 'collectionTitle' is bibtex 'title'
		if($bibtex && ($row['resourceType'] == 'book_chapter'))
			return stripslashes($row['collectionTitle']);
		$noSort = $row['resourceNoSort'] ? stripslashes($row['resourceNoSort']) . ' ' : FALSE;
		if($row['resourceSubtitle'])
			$string = $noSort . stripslashes($row['resourceTitle']) . ": " . stripslashes($row['resourceSubtitle']);
		else
			$string = $noSort . stripslashes($row['resourceTitle']);
		if($bibtex)
			return $string;
// If !bibtex, remove any braces that have been inserted to maintain case of characters - only required for resource title
		return preg_replace("/[{|}]/u", "", $string);
	}
/**
* grabNote - grab note from WKX_resource_note and strip (optionally) multiple whitespace
*
* @author Mark Grimshaw-Aagaard
* @param assocArray $row
* @param boolean $exportType
* @return string
*/
	public function grabNote($row, $exportType)
	{
		if($row['resourcetextNote'])
		{
			if(($exportType == 'ris') || ($exportType == 'endnoteTabbed'))
				$text = $this->cite->parseCitations(stripslashes($row['resourcetextNote']), $exportType, FALSE);
			else
				$text = stripslashes($row['resourcetextNote']);
// replace all whitespace (TABS, CR, \n etc.) with single space.
			if(($exportType == 'ris') || ($exportType == 'endnoteTabbed') || ($exportType == 'endnoteXml'))
				return preg_replace("/\s/u", " ", $text);
// For bibtex, ensure first letter is capitalized
			if($exportType == 'bibtex')
				return UTF8::utf8_ucfirst($text);
			return $text;
		}
		return '';
	}
/**
* grabAbstract - abstract from WKX_resource_abstract and strip (optionally) multiple whitespace
*
* @author Mark Grimshaw-Aagaard
* @param assocArray $row
* @param boolean $exportType
* @return string
*/
	public function grabAbstract($row, $exportType)
	{
		if($row['resourcetextAbstract'])
		{
			if(($exportType == 'ris') || ($exportType == 'endnoteTabbed'))
				$text = $this->cite->parseCitations(stripslashes($row['resourcetextAbstract']), $exportType, FALSE);
			else
				$text = stripslashes($row['resourcetextAbstract']);
// replace all whitespace (TABS, CR, \n etc.) with single space.
			if(($exportType == 'ris') || ($exportType == 'endnoteTabbed') || ($exportType == 'endnoteXml'))
				return preg_replace("/\s/u", " ", $text);
// For bibtex, ensure first letter is capitalized
			if($exportType == 'bibtex')
				return UTF8::utf8_ucfirst($text);
			return $text;
		}
		return '';
	}
/**
* pageFormat - return formatted pageStart and pageEnd with different delimiters
*
* @author Mark Grimshaw-Aagaard
* @param assocArray $row
* @param string $exportType
* @return string
*/
	public function pageFormat($row, $exportType)
	{
		$page = FALSE;
		if($row['resourcepagePageStart'])
		{
			if(($exportType == 'endnoteTabbed') || ($exportType == 'endnoteXml'))
				$page = stripslashes($row['resourcepagePageStart']);
			else if($exportType == 'ris')
				$page = 'SP  - ' . stripslashes($row['resourcepagePageStart']);
			else if($exportType == 'bibtex')
				$page = stripslashes($row['resourcepagePageStart']);
		}
		if($row['resourcepagePageEnd'])
		{
			if(($exportType == 'endnoteTabbed') || ($exportType == 'endnoteXml'))
				$page .= '-' . stripslashes($row['resourcepagePageEnd']);
			else if($exportType == 'ris')
				$page .= CR. 'EP  - ' . stripslashes($row['resourcepagePageEnd']);
			else if($exportType == 'bibtex')
				$page .= '--' . stripslashes($row['resourcepagePageEnd']);
		}
		if($page)
			return $page;
		return '';
	}
/**
* keywordFormat - return formatted keywords with different delimiters
*
* @author Mark Grimshaw-Aagaard
* @param assocArray $row
* @param string $exportType
* @return string
*/
	public function keywordFormat($row, $exportType)
	{
		$this->db->formatConditions(array('resourcekeywordResourceId' => $row['resourceId']));
		$this->db->leftJoin('keyword', 'keywordId', 'resourcekeywordKeywordId');
		$recordset = $this->db->select('resource_keyword', 'keywordKeyword');
		if($this->db->numRows($recordset))
		{
			while($kw = $this->db->fetchRow($recordset))
				$k[] = stripslashes($kw['keywordKeyword']);
			if($exportType == 'endnoteTabbed') // tabbed file
				return join(";", $k);
			else if($exportType == 'endnoteXml') // XML
			{
				return $k;
			}
			else if($exportType == 'ris')
			{
// asterisk (character 42) is not allowed in the author, keywords, or periodical name fields - replace with '#'
				foreach($k as $key => $value)
					$k[$key] = 'KW  - ' . preg_replace("/\*/u", "#", $value);
				return join(CR, $k);
			}
			else if($exportType == 'bibtex')
				return join(",", $k);
		}
		return '';
	}
/**
* Format resource according to bibliographic style when exporting to a formatted export format (HTML or RTF for example)
*
* @param array $resources
* @param string $exportType
* @param string $param1 Option parameter (default is FALSE)
* @param string $param1 Option parameter (default is FALSE)
* @param string $writeToFile Option write each row to file rather than returning a concatenated string (default is FALSE)
* @return mixed $pString or boolean (TRUE = success writing to file, FALSE = failure writing to file)
*/
	public function formatResources(&$rows, $exportType, $param1 = FALSE, $param2 = FALSE, $writeToFile = FALSE)
	{
		$bibStyle = FACTORY_BIBSTYLE::getInstance($exportType);
		$creators = array();
		$pString = '';
		$resultSet = $this->getCreators(array_keys($rows));
		while($cRow = $this->db->fetchRow($resultSet))
		{
			$creators[$cRow['resourcecreatorResourceId']][$cRow['resourcecreatorRole']][] = $cRow['creatorId'];
			$array = array(
					'surname' => $cRow['surname'],
					'firstname' => $cRow['firstname'],
					'initials' => $cRow['initials'],
					'prefix' => $cRow['prefix'],
					'creatorId' => $cRow['creatorId'],
				);
			$bibStyle->creators[$cRow['creatorId']] = array_map(array($bibStyle, "removeSlashes"), $array);
		}
		unset($cRow);
		if($exportType == 'html')
		{
			foreach($rows as $rId => $row)
			{
				if(empty($creators) || !array_key_exists($rId, $creators) || empty($creators[$rId]))
				{
					for($index = 1; $index <= 5; $index++)
						$row["creator$index"] = ''; // need empty fields for BIBSTYLE
				}
				else
				{
					for($index = 1; $index <=5; $index++)
					{
						if(array_key_exists($index, $creators[$rId]))
							$row["creator$index"] = join(',', $creators[$rId][$index]);
						else
							$row["creator$index"] = '';
					}
				}
				$hyperlink = $param2 ? "&nbsp;&nbsp;" . HTML\a('link', "[$param1]", $param2 . $rId) : $param2;
				$string = HTML\p($bibStyle->process($row, FALSE, FALSE) . $hyperlink) . LF;
				if($writeToFile)
				{
					if(!$this->fp || !fwrite($this->fp, $string))
						return FALSE;
				}
				else
					$pString .= $string;
			}
		}
		if($pString)
			return $pString;
		else
			return TRUE;
	}
/**
* Get SQL resultset for creator details before formatting resources
*
* @param array $resourceIds
* @return object SQL resultset
*/
	private function getCreators($resourceIds)
	{
		$this->db->formatConditionsOneField($resourceIds, 'resourcecreatorResourceId');
		$this->db->leftJoin('creator', 'creatorId', 'resourcecreatorCreatorId');
		$this->db->orderBy('resourcecreatorResourceId', TRUE, FALSE);
		$this->db->ascDesc = $this->db->asc;
		$this->db->orderBy('resourcecreatorRole', TRUE, FALSE);
		$this->db->orderBy('resourcecreatorOrder', TRUE, FALSE);
		return $this->db->select('resource_creator', array('resourcecreatorResourceId', array('creatorSurname' => 'surname'),
			array('creatorFirstname' => 'firstname'), array('creatorInitials' => 'initials'), array('creatorPrefix' => 'prefix'),
			'creatorId', 'resourcecreatorRole'));
	}
/**
* set user/group ID conditions for ideas
*
* @return boolean
*/
	public function setIdeasCondition()
	{
		if($userId = $this->session->getVar('setup_UserId'))
		{
			$this->db->formatConditions(array('usergroupsusersUserId' => $userId));
			$this->db->formatConditions($this->db->formatFields('usergroupsusersGroupId') . $this->db->equal .
				$this->db->formatFields('resourcemetadataPrivate'));
			$subSql = $this->db->selectNoExecute('user_groups_users', 'usergroupsusersId', FALSE, TRUE, TRUE);
			$subject = $this->db->formatFields('resourcemetadataPrivate') . $this->db->notEqual . $this->db->tidyInput('N')
				. $this->db->and .
				$this->db->formatFields('resourcemetadataPrivate') . $this->db->notEqual . $this->db->tidyInput('Y');
			$case1 = $this->db->caseWhen($subject, FALSE, $subSql, FALSE, FALSE);
			$subject = $this->db->formatFields('resourcemetadataPrivate') . $this->db->equal . $this->db->tidyInput('Y');
			$result = $this->db->formatFields('resourcemetadataAddUserId') . $this->db->equal . $this->db->tidyInput($userId);
			$case2 = $this->db->caseWhen($subject, FALSE, $result, FALSE, FALSE);
			$subject = $this->db->formatFields('resourcemetadataPrivate') . $this->db->equal . $this->db->tidyInput('N');
			$result = $this->db->tidyInput(1);
			$case3 = $this->db->caseWhen($subject, FALSE, $result, FALSE, FALSE);
			$this->db->formatConditions($case1 . $this->db->or . $case2 . $this->db->or . $case3);
			$this->db->formatConditions(array('resourcemetadataType' => 'i'));
			return TRUE;
		}
		return FALSE;
	}
}
