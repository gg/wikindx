********************************************************************************
**                                Debug Tools                                 **
**                               WIKINDX module                               **
********************************************************************************


NB. this module is compatible with WIKINDX v5 and up.

Some tools for debugging Wikindx.

Unzip this file (with any directory structure) into plugins/debugTools/.
Thus, plugins/debugTools/index.php etc.

********************************************************************************

CHANGELOG:

v1.0, 2019
1. Initial release.

--
Stéphane Aulery 2019.
