********************************************************************************
**                                 Adminstyle                                 **
**                               WIKINDX module                               **
********************************************************************************


NB. this module is compatible with WIKINDX v5 and up.

Create and edit bibliographic/citations styles.

The module registers itself in admin and is only for registered WIKINDX users although this can be changed.

Unzip this file (with any directory structure) into plugins/adminstyle/.
Thus, plugins/adminstyle/index.php etc.

********************************************************************************

CHANGELOG:

v1.10, 2019
1. Removed doubled text on preview citation. This makes it a little clearer to see what is going on.
2. Corrected an error in the bibliography preview whereby it was not possible to remove the publicationYear from the reference.
3. Fix an error previewing the first bibliographic template.
4. In style previews, add a 'RESET' field to the 'Disable fields' select box.
5. Some improvements to the display.
6. Debugged the handling of non-Latin/accented characters during citation and bibliography previews.
7. Corrected a bug in displaying '<' and '>' characters.

v1.9
1. Fix a fclose() bug.

v1.8
1. Plugin now compatible with WIKINDX v5.x
2. Fix to the preview of bibliographic and footnote citations for books and book articles.
3. Added the option to add custom fields to bibliographic styles.
4. Fix any memory leaks due to an oversight fclose().

v1.7
1. As per wikindx v4.2.2, season names (e.g. Spring) can now be added to resources requiring them (e.g. journal article);
   user defined seasons in the style editor have been added to reflect this change.

v1.6
1. Fixed a bug in the packaging of the plugin zip file -- the folder name is now correct.

v1.5
1. Fixed a bug when previewing in-text citations

v1.4
1. Bibliography templates and in-text citations can now be previewed.

v1.3
1. Plugin compatible only with WIKINDX v4.2.x

v1.2
1. Fix to packing of the download file (again).

v1.1
1. Fix to packing of the download file.

v1.0
1. Initial release.

--
Mark Grimshaw-Aagaard 2019.