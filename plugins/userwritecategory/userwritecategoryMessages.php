<?php
/**
 * WIKINDX : Bibliographic Management system.
 * @link https://wikindx.sourceforge.io/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2019 Mark Grimshaw-Aagaard <sirfragalot@users.sourceforge.net>
 * @copyright 2019 Stéphane Aulery <lkppo@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 */

/*****
*	USERWRITECATEGORY plugin -- English messages.
*
*****/
class userwritecategoryMessages
{
    public $text = array();
    
	public function __construct()
	{
	    $domain = mb_strtolower(basename(__DIR__));
	    
        $this->text = array(
// translators: Menu items
            "uwcSub" => dgettext($domain, "Administer Categories..."),
            "uwcCategories" => dgettext($domain, "Categories"),
            "uwcSubcategories" => dgettext($domain, "Subcategories"),
        );
	}
}
