<?php
/**
 * WIKINDX : Bibliographic Management system.
 * @link https://wikindx.sourceforge.io/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2019 Mark Grimshaw-Aagaard <sirfragalot@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 */

class visualize_CONFIG
{
    public $menus = array('plugin1');
    public $authorize = 2;
    public $wikindxVersion = 5.8;
// image dimensions (pixels)
	public $width = 1200;
	public $height = 600;
}
