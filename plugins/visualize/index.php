<?php
/*****
*	visualize class.
*
*	v1.1 2019
*
*	Visualize
*
*	Create various visualizations from WIKINDX data.
*
*	Makes use of JpGraph: https://jpgraph.net/
*
*****/

/**
* Import initial configuration and initialize the web server
*/
include_once("core/startup/WEBSERVERCONFIG.php");


class visualize_MODULE
{
private $pluginmessages;
private $coremessages;
private $errors;
private $config;
private $wConfig;
private $session;
private $vars;
private $db;
private $xAxisMetadata;
private $xAxis = array();
private $yAxis = array();
private $colour;

public $authorize;
public $menus;

// Constructor.
// $menuInit is TRUE if called from MENU.php
	public function __construct($menuInit = FALSE)
	{
		include_once("core/messages/PLUGINMESSAGES.php");
		$this->coremessages = FACTORY_MESSAGES::getInstance();
		$this->pluginmessages = new PLUGINMESSAGES('visualize', 'visualizeMessages');
		$this->errors = FACTORY_ERRORS::getInstance();
		include_once(__DIR__ . DIRECTORY_SEPARATOR . "config.php");
		$this->config = new visualize_CONFIG();
		$this->session = FACTORY_SESSION::getInstance();
		$this->db = FACTORY_DB::getInstance();
		$this->authorize = $this->config->authorize;
		if($menuInit)
		{
			$this->makeMenu($this->config->menus);
			return; // Need do nothing more as this is simply menu initialisation.
		}
		$authorize = FACTORY_AUTHORIZE::getInstance();
		if(!$authorize->isPluginExecutionAuthorised($this->authorize)) // not authorised
			FACTORY_CLOSENOMENU::getInstance(); // die
		$this->wConfig = FACTORY_CONFIG::getInstance();
		$this->vars = GLOBALS::getVars();
		GLOBALS::setTplVar('heading', $this->pluginmessages->text('heading'));
}
// Make the menus
	private function makeMenu($menuArray)
	{
		$this->menus = array($menuArray[0] => array($this->pluginmessages->text('menu') => "init"));
	}
// This is the initial method called from the menu item.
// Present options
	public function init($message = FALSE)
	{
		if($message)
			$pString = $message;
		else
			$pString = '';
		$pString .= FORM\formHeader("visualize_visualize");
		$pString .= HTML\tableStart('generalTable borderStyleSolid');
		$pString .= HTML\trStart();
		$pString .= HTML\td($this->chooseOptions());
		$pString .= HTML\trEnd();
		$pString .= HTML\trStart();
		$js = "onClick=\"visualizePopUp(); return false\"";
		$td = HTML\p(FORM\formSubmit($this->pluginmessages->text('visualize'), FALSE, $js));
		$pString .= HTML\td($td);
		$pString .= HTML\trEnd();
		$pString .= HTML\tableEnd();
		$pString .= FORM\formEnd();
		\AJAX\loadJavascript($this->wConfig->WIKINDX_BASE_URL . '/' . WIKINDX_DIR_PLUGINS . '/visualize/visualize.js');
		GLOBALS::addTplVar('content', $pString);
	}
// Display options for the x and y axes
	private function chooseOptions()
	{
		$yAxisTypes = $this->yAxisOptions();
		$xAxisTypes = $this->xAxisOptions();
		$jScript = 'index.php?action=visualize_visualizeOptions';
		$jsonArray[] = array(
			'startFunction'	=>	'triggerFromSelect',
			'script' => "$jScript",
			'triggerField' => 'yAxis',
			'targetDiv' => 'xAxis',
			);
		$js = AJAX\jActionForm('onchange', $jsonArray);
		$pString = HTML\tableStart('generalTable borderStyleSolid');
		$pString .= HTML\trStart();
		reset($yAxisTypes);
		$firstKey = key($yAxisTypes);
		$selected = $this->session->getVar('visualize_YAxis') ? $this->session->getVar('visualize_YAxis') : $firstKey;
		$pString .= HTML\td(FORM\selectedBoxValue($this->pluginmessages->text("yAxis"), "yAxis",
			$yAxisTypes, $selected, 1, FALSE, $js));
		reset($xAxisTypes);
		$firstKey = key($xAxisTypes);
		$selected = $this->session->getVar('visualize_XAxis') ? $this->session->getVar('visualize_XAxis') : $firstKey;
		$pString .= HTML\td(FORM\selectedBoxValue($this->pluginmessages->text("xAxis"), "xAxis",
			$xAxisTypes, $selected, 6));
		$selected = $this->session->getVar('visualize_MaxXAxis') ? $this->session->getVar('visualize_MaxXAxis') : -1;
		$hint = \HTML\aBrowse('green', '', $this->coremessages->text("hint", "hint"), '#', "", $this->pluginmessages->text("maxXAxisLimit"));
		$pString .= HTML\td(FORM\textInput($this->pluginmessages->text("maxXAxis"), "maxXAxis", $selected, 3, 3) . BR . \HTML\span($hint, 'hint'));
		$pString .= HTML\td($this->choosePlot());
		$pString .= HTML\trEnd();
		$pString .= HTML\tableEnd();
		return $pString;
	}
// Choose the type of plot
	private function choosePlot()
	{
		$plots = array(
						'line' => $this->pluginmessages->text('line'),
						'bar' => $this->pluginmessages->text('bar'),
						'barLine' => $this->pluginmessages->text('barLine'),
						'scatter' => $this->pluginmessages->text('scatter'),
						'scatterLine' => $this->pluginmessages->text('scatterLine'),
						'balloon' => $this->pluginmessages->text('balloon'),
					);
		reset($plots);
		$firstKey = key($plots);
		$selected = $this->session->getVar('visualize_Plot') ? $this->session->getVar('visualize_Plot') : $firstKey;
		return HTML\td(FORM\selectedBoxValue($this->pluginmessages->text("plotType"), "plot",
			$plots, $selected, 6));
	}
// Carry out the visualization
	public function visualize()
	{
		include_once('jpgraph/jpgraph.php');
		if(!$this->validate())
			$this->badInput($this->pluginmessages->text('inputMissing'));
		$this->xAxisMetadata = $this->getXAxisMetadata();
		$this->getData();
		if(array_key_exists('maxXAxis', $this->vars) && ($this->vars['maxXAxis'] > 0))
			$scale = $this->vars['maxXAxis'];
		else
			$scale = FALSE;
		if($scale && (sizeOf($this->xAxis) > $scale))
		{
			if(!array_key_exists('start', $this->vars) || !$this->vars['start'])
			{
				$xAxis = array_slice($this->xAxis, 0, $scale);
				$yAxis = array_slice($this->yAxis, 0, $scale);
			}
			else
			{
				$xAxis = array_slice($this->xAxis, $this->vars['start'], $scale);
				$yAxis = array_slice($this->yAxis, $this->vars['start'], $scale);
			}
		}
		else
		{
			$xAxis = $this->xAxis;
			$yAxis = $this->yAxis;
		}
 // Create a graph instance
		$graph = new Graph($this->config->width, $this->config->height);
// Specify what scale we want to use
		$scales = $this->xAxisMetadata[$this->vars['xAxis']]['xScale'] . $this->xAxisMetadata[$this->vars['xAxis']]['yScale'];
		$graph->SetScale($scales);
		$graph->SetMargin(80,20,60,$this->xAxisMetadata[$this->vars['xAxis']]['xAxisMargin']);
		$graph->SetTickDensity(TICKD_DENSE, TICKD_VERYSPARSE);
// Setup a title for the graph
		$graph->title->Set($this->pluginmessages->text($this->vars['yAxis']) . '/' . $this->pluginmessages->text($this->vars['xAxis']));
// Setup titles and X-axis labels
		$graph->xaxis->SetLabelAlign('center','top');
		$graph->xaxis->SetTitlemargin($this->xAxisMetadata[$this->vars['xAxis']]['xAxisTitleMargin']);
		$graph->yaxis->SetTitlemargin(50);
		$graph->xaxis->title->Set($this->pluginmessages->text($this->vars['xAxis']));
		$graph->xaxis->SetTickLabels($xAxis);
		$graph->xaxis->SetLabelAngle($this->xAxisMetadata[$this->vars['xAxis']]['xAxisAngle']);
// Setup Y-axis title
		$graph->yaxis->title->Set($this->pluginmessages->text($this->vars['yAxis']));
		if($this->vars['plot'] == 'bar')
			$this->barPlot($yAxis, $graph);
		else if($this->vars['plot'] == 'barLine')
		{
			$this->barPlot($yAxis, $graph);
			$this->linePlot($yAxis, $graph, FALSE);
		}
		else if($this->vars['plot'] == 'scatter')
			$this->scatterPlot($yAxis, $graph);
		else if($this->vars['plot'] == 'scatterLine')
			$this->scatterPlot($yAxis, $graph, TRUE);
		else if($this->vars['plot'] == 'balloon')
			$this->balloonPlot($yAxis, $graph);
		else // 'line'
			$this->linePlot($yAxis, $graph);
// Add the plot to the graph
		$this->display($graph);
	}
// Do a bar plot
	private function barPlot($yAxis, $graph)
	{
		include_once('jpgraph/jpgraph_bar.php');
		$plot = new BarPlot($yAxis);
		$graph->Add($plot);
		$plot->SetFillColor(array('red','blue','green'));
		$plot->SetShadow('teal', 2, 2);
		$plot->value->SetFormat('%d');
		$plot->value->Show();
		$graph->yaxis->scale->SetGrace(5);
		$plot->SetValuePos('top');
	}
// Do a line plot
	private function linePlot($yAxis, $graph, $showValue = TRUE)
	{
		include_once('jpgraph/jpgraph_line.php');
		$plot = new LinePlot($yAxis);
		$graph->Add($plot);
		if($showValue)
		{
			$graph->yaxis->scale->SetGrace(5);
			$plot->value->SetFormat('%d');
			$plot->value->Show();
		}
		$plot->SetColor('red');
	}
// Do a scatter plot
	private function scatterPlot($yAxis, $graph, $line = FALSE)
	{
		include_once('jpgraph/jpgraph_scatter.php');
		$plot = new ScatterPlot($yAxis);
		if($line)
			$plot->SetLinkPoints();
		$plot->mark->SetType(MARK_FILLEDCIRCLE);
		$plot->SetColor('red');
		$graph->Add($plot);
	}
// Do a balloon plot
	private function balloonPlot($yAxis, $graph)
	{
		include_once('jpgraph/jpgraph_scatter.php');
		$plot = new ScatterPlot($yAxis);
// Use a lot of grace to get large scales
		$graph->yaxis->scale->SetGrace(10);
		$plot->mark->SetType(MARK_FILLEDCIRCLE);
		$plot->value->SetFormat('%d');
		$plot->value->Show();
		$plot->value->SetFont(FF_FONT1,FS_BOLD);
		$this->colour = new COLOR(100);
		$plot->mark->SetCallback(array($this, "balloonCallback"));
		$graph->Add($plot);
	}
	public function balloonCallback($aVal)
	{
// This callback will adjust the fill color and size of
// the datapoint according to the data value according to
//		$min = min($this->yAxis);
		$max = max($this->yAxis);
/*		$diff = $max - $min;
		if($aVal < ($diff / 3))
			$c = "blue";
		elseif($aVal < (2 * ($diff / 3)))
			$c = "green";
		else
			$c="red";
*/
// Print the size between 5 and 100
		$value = floor((($aVal / 3) / $max) * 100);
		$size = floor((($aVal / 3) / $max) * 95) + 5;
		if(!$value)
			++$value;
		return array($size, "", $this->colour->sequence[$value]);
//		return array(floor($aVal / 3), "", $c);
	}
// Display the graph
	private function display($graph)
	{
		$filesDir = $this->wConfig->WIKINDX_FILE_PATH ? $this->wConfig->WIKINDX_FILE_PATH : WIKINDX_DIR_FILES;
		$file = $filesDir . DIRECTORY_SEPARATOR . 'jpGraph' . uniqId() . '.png';
		$graph->Stroke($file);
		$pString = HTML\img($file, $this->config->width, $this->config->height);
		$size = sizeOf($this->xAxis);
		if(array_key_exists('maxXAxis', $this->vars) && ($this->vars['maxXAxis'] > 0))
		{
			$p = $this->links($size);
			$p .= FORM\formEnd();
			$pString .= \HTML\p($p);
		}
		$pString .= HTML\p(\FORM\closePopup($this->coremessages->text("misc", "closePopup")), "right");
		GLOBALS::addTplVar('content', $pString);
		FACTORY_CLOSEPOPUP::getInstance();
	}
	private function links($size)
	{
		$previous = $next = FALSE;
		if($this->vars['maxXAxis'] == -1)
			$this->vars['maxXAxis'] = $size;
		$links = htmlentities("&yAxis=" . $this->vars['yAxis']) .
				htmlentities("&xAxis=" . $this->vars['xAxis']) .
				htmlentities("&maxXAxis=" . $this->vars['maxXAxis']) .
				htmlentities("&plot=" . $this->vars['plot']);
		$icons = FACTORY_LOADICONS::getInstance();
		$icons->next = $this->pluginmessages->text('next');
		$icons->previous = $this->pluginmessages->text('previous');
		$icons->setupIcons();
		if(array_key_exists('start', $this->vars) && $this->vars['start'])
		{
			$previousStart = $this->vars['start'] - $this->vars['maxXAxis'];
			$nextStart = $this->vars['maxXAxis'] + $this->vars['start'];
			$previous = \HTML\a($icons->previousLink, $icons->previous,
				"index.php?action=visualize_visualize" . htmlentities("&start=" . $previousStart) . $links);
			if(($this->vars['start'] + $this->vars['maxXAxis']) < $size)
				$next = \HTML\a($icons->nextLink, $icons->next,
					"index.php?action=visualize_visualize" . htmlentities("&start=" . $nextStart) . $links);
		}
		else if($this->vars['maxXAxis'] < $size)
			$next = \HTML\a($icons->nextLink, $icons->next,
				"index.php?action=visualize_visualize" . htmlentities("&start=" . $this->vars['maxXAxis']) . $links);
		if($previous && $next)
			return "$previous&nbsp;&nbsp;$next";
		else if($previous)
			return "$previous";
		else
			return "$next";
	}
	private function getData()
	{
		if($this->xAxisMetadata[$this->vars['xAxis']]['countField'])
			$yAxisField = $this->xAxisMetadata[$this->vars['xAxis']]['countField'];
		else
			$yAxisField = $this->vars['xAxis'];
		if($this->xAxisMetadata[$this->vars['xAxis']]['sql'])
			$recordSet = $this->db->query($this->xAxisMetadata[$this->vars['xAxis']]['sql']);
		else
		{
	    	$this->db->orderBy($yAxisField);
	    	$recordSet = $this->db->selectCount($this->xAxisMetadata[$this->vars['xAxis']]['table'], $yAxisField);
	    }
    	if(!$this->db->numRows($recordSet))
        	throw new JpGraphException($this->pluginmessages->text('noData'));
		while($row = $this->db->fetchRow($recordSet))
		{
			if($this->xAxisMetadata[$this->vars['xAxis']]['isNumeric'])
			{
				if(is_numeric($row[$yAxisField]))
				{
					if($this->xAxisMetadata[$this->vars['xAxis']]['messagesArray'])
						$this->xAxis[] = $this->coremessages->text($this->xAxisMetadata[$this->vars['xAxis']]['messagesArray'],
							$row[$yAxisField]);
					else
						$this->xAxis[] = $row[$yAxisField];
					$this->yAxis[] = $row['count'];
				}
			}
			else
			{
				if($this->xAxisMetadata[$this->vars['xAxis']]['messagesArray'])
					$this->xAxis[] = $this->coremessages->text($this->xAxisMetadata[$this->vars['xAxis']]['messagesArray'],
						$row[$yAxisField]);
				else if($this->xAxisMetadata[$this->vars['xAxis']]['labelField'])
					$this->xAxis[] = $row[$this->xAxisMetadata[$this->vars['xAxis']]['labelField']];
				else
					$this->xAxis[] = $row[$yAxisField];
				$this->yAxis[] = $row['count'];
			}
    	}
	}
// Get tables as per the field
	private function getXAxisMetadata()
	{
		return array(
					'resourceyearYear1' => array('table' => 'resource_year',
												'isNumeric' => TRUE,
												'xAxisMargin' => 100,
												'xAxisTitleMargin' => 55,
												'xAxisAngle' => 45,
												'xScale'	=>	'text',
												'yScale'	=>	'int',
												'messagesArray' => FALSE,
												'sql' => FALSE,
												'countField'	=>	FALSE,
												'labelField'	=>	FALSE,
												),
					'resourceType'	=>	array('table' => 'resource',
												'isNumeric' => FALSE,
												'xAxisMargin' => 200,
												'xAxisTitleMargin' => 170,
												'xAxisAngle' => 90,
												'xScale'	=>	'text',
												'yScale'	=>	'int',
												'messagesArray' => 'resourceType',
												'sql' => FALSE,
												'countField'	=>	FALSE,
												'labelField'	=>	FALSE,
												),
					'keywordKeyword' =>	array('table' => FALSE,
												'isNumeric' => FALSE,
												'xAxisMargin' => 200,
												'xAxisTitleMargin' => 170,
												'xAxisAngle' => 90,
												'xScale'	=>	'text',
												'yScale'	=>	'int',
												'messagesArray' => FALSE,
												'sql' => "SELECT `resourcekeywordKeywordId`, COUNT(`resourcekeywordKeywordId`) AS count, `keywordKeyword` FROM wkx_resource_keyword LEFT OUTER JOIN wkx_keyword ON `keywordId` = `resourcekeywordKeywordId` WHERE (`resourcekeywordResourceId` IS NOT NULL) AND (`keywordKeyword` IS NOT NULL) GROUP BY `resourcekeywordKeywordId`, `keywordKeyword` ORDER BY REPLACE( REPLACE(`keywordKeyword`, '{', ''), '}', '') ASC",
												'countField'	=>	FALSE,
												'labelField'	=>	FALSE,
												),
					'categoryCategory' =>	array('table' => FALSE,
												'isNumeric' => FALSE,
												'xAxisMargin' => 200,
												'xAxisTitleMargin' => 160,
												'xAxisAngle' => 90,
												'xScale'	=>	'text',
												'yScale'	=>	'int',
												'messagesArray' => FALSE,
												'sql' => "SELECT `categoryId`, `categoryCategory`, `count` FROM (SELECT `resourcecategoryCategoryId`, COUNT(`resourcecategoryCategoryId`) AS count FROM wkx_resource_category WHERE (`resourcecategoryCategoryId` IS NOT NULL) GROUP BY `resourcecategoryCategoryId`) AS wkx_t LEFT OUTER JOIN wkx_category ON `categoryId` = `resourcecategoryCategoryId` ORDER BY REPLACE( REPLACE(`categoryCategory`, '{', ''), '}', '') ASC",
												'countField'	=>	FALSE,
												'labelField'	=>	FALSE,
												),
					'journal' =>	array('table' => FALSE,
												'isNumeric' => FALSE,
												'xAxisMargin' => 300,
												'xAxisTitleMargin' => 260,
												'xAxisAngle' => 90,
												'xScale'	=>	'text',
												'yScale'	=>	'int',
												'messagesArray' => FALSE,
												'sql' => "SELECT `collectionId`, COUNT(`collectionId`) AS count , `resourcemiscCollection`, `collectionType`, `collectionTitle` FROM wkx_resource_misc LEFT OUTER JOIN wkx_collection ON `collectionId` = `resourcemiscCollection` WHERE (`collectionType` = 'journal') AND (`collectionId` IS NOT NULL) GROUP BY `collectionId`, `resourcemiscCollection`, `collectionType`, `collectionTitle` ORDER BY REPLACE( REPLACE(`collectionTitle`, '{', ''), '}', '') ASC",
												'countField'	=>	'collectionId',
												'labelField'	=>	'collectionTitle',
												),
					'proceedings' =>	array('table' => FALSE,
												'isNumeric' => FALSE,
												'xAxisMargin' => 300,
												'xAxisTitleMargin' => 260,
												'xAxisAngle' => 90,
												'xScale'	=>	'text',
												'yScale'	=>	'int',
												'messagesArray' => FALSE,
												'sql' => "SELECT `collectionId`, COUNT(`collectionId`) AS count , `resourcemiscCollection`, `collectionType`, `collectionTitle` FROM wkx_resource_misc LEFT OUTER JOIN wkx_collection ON `collectionId` = `resourcemiscCollection` WHERE (`collectionType` = 'proceedings') AND (`collectionId` IS NOT NULL) GROUP BY `collectionId`, `resourcemiscCollection`, `collectionType`, `collectionTitle` ORDER BY REPLACE( REPLACE(`collectionTitle`, '{', ''), '}', '') ASC",
												'countField'	=>	'collectionId',
												'labelField'	=>	'collectionTitle',
												),
				);
	}
// Choose options for the Y axis
	private function yAxisOptions()
	{
		return array(
					'numResources' =>	$this->pluginmessages->text('numResources'),
				);
	}
// Choose options for the X axis
	private function xAxisOptions()
	{
		return array(
					'resourceType' =>	$this->pluginmessages->text('resourceType'),
					'resourceyearYear1' =>	$this->pluginmessages->text('resourceyearYear1'),
					'keywordKeyword' =>	$this->pluginmessages->text('keywordKeyword'),
					'categoryCategory' =>	$this->pluginmessages->text('categoryCategory'),
					'journal' =>	$this->pluginmessages->text('journal'),
					'proceedings' =>	$this->pluginmessages->text('proceedings'),
				);
	}
// Validate input and store in session
	private function validate()
	{
		if(array_key_exists('yAxis', $this->vars) && $this->vars['yAxis'])
			$this->session->setVar('visualize_YAxis', $this->vars['yAxis']);
		else
			return FALSE;
		if(array_key_exists('xAxis', $this->vars) && $this->vars['xAxis'])
			$this->session->setVar('visualize_XAxis', $this->vars['xAxis']);
		else
			return FALSE;
		if(array_key_exists('plot', $this->vars) && $this->vars['plot'])
			$this->session->setVar('visualize_Plot', $this->vars['plot']);
		else
			return FALSE;
		if(array_key_exists('maxXAxis', $this->vars) && $this->vars['maxXAxis'])
		{
			if(!preg_match("#^(-[0-9]{1,}|[0-9]{1,})$#", $this->vars['maxXAxis'])) // need to check either '-1' or a positive integer
				return FALSE;
			$this->session->setVar('visualize_MaxXAxis', $this->vars['maxXAxis']);
		}
		else
			return FALSE;
		return TRUE;
	}
// bad Input function
	private function badInput($error)
	{
		$pString = HTML\p($error, 'error');
		$pString .= HTML\p(\FORM\closePopup($this->coremessages->text("misc", "closePopup")), "left");
		GLOBALS::addTplVar('content', $pString);
		FACTORY_CLOSEPOPUP::getInstance();
	}
}
/**
* From https://stackoverflow.com/questions/1211705/paint-me-a-rainbow and
* https://stackoverflow.com/questions/3597417/php-hsv-to-rgb-formula-comprehension#3642787
*/
class COLOR
{
    public $sequence = array();
/**
* constructor fills $sequence with a list of colours as long as the $count param
*/
    public function __construct($count, $s = .5, $l = .5)
    {
    	$index = 1;
        for($h = 0; $h <= .85; $h += .85/$count)    //.85 is pretty much in the middle of the violet spectrum
            $this->sequence[$index++] = '#' . color::hexHSLtoRGB($h, $s, $l);
    }
/**
* From https://stackoverflow.com/questions/1211705/paint-me-a-rainbow and
* https://stackoverflow.com/questions/3597417/php-hsv-to-rgb-formula-comprehension#3642787
*/
    public static function HSLtoRGB($h, $s, $l)
    {
        $r = $l;
        $g = $l;
        $b = $l;
        $v = ($l <= 0.5) ? ($l * (1.0 + $s)) : (l + $s - l * $s);
        if ($v > 0)
        {
              $m;
              $sv;
              $sextant;
              $fract;
              $vsf;
              $mid1;
              $mid2;
              $m = $l + $l - $v;
              $sv = ($v - $m ) / $v;
              $h *= 6.0;
              $sextant = floor($h);
              $fract = $h - $sextant;
              $vsf = $v * $sv * $fract;
              $mid1 = $m + $vsf;
              $mid2 = $v - $vsf;
              switch ($sextant)
              {
                    case 0:
                          $r = $v;
                          $g = $mid1;
                          $b = $m;
                          break;
                    case 1:
                          $r = $mid2;
                          $g = $v;
                          $b = $m;
                          break;
                    case 2:
                          $r = $m;
                          $g = $v;
                          $b = $mid1;
                          break;
                    case 3:
                          $r = $m;
                          $g = $mid2;
                          $b = $v;
                          break;
                    case 4:
                          $r = $mid1;
                          $g = $m;
                          $b = $v;
                          break;
                    case 5:
                          $r = $v;
                          $g = $m;
                          $b = $mid2;
                          break;
              }
        }
        return array('r' => floor($r * 255.0),
                    'g' => floor($g * 255.0),
                    'b' => floor($b * 255.0)
                    );
    }
//return a hex code from hsv values
    public static function hexHSLtoRGB($h, $s, $l)
    {
        $rgb = self::HSLtoRGB($h, $s, $l);
        $hex = base_convert($rgb['r'], 10, 16) . base_convert($rgb['g'], 10, 16) . base_convert($rgb['b'], 10, 16);
        return $hex;
    }
}
