<?php
/**
 * WIKINDX : Bibliographic Management system.
 * @link https://wikindx.sourceforge.io/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2019 Mark Grimshaw-Aagaard <sirfragalot@users.sourceforge.net>
 * @copyright 2019 Stéphane Aulery <lkppo@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 */

class visualizeMessages
{
    public $text = array();
    
	public function __construct()
	{
	    $domain = mb_strtolower(basename(__DIR__));
	    
		$this->text = array(
			"menu" => dgettext($domain, "Visualize"),
			"heading" => dgettext($domain, "Visualize"),
			"submit" => dgettext($domain, "Visualize"),
			"noData" => dgettext($domain, "No data in the database of selected type."),
			"yAxis" => dgettext($domain, "Y axis"),
			"xAxis" => dgettext($domain, "X axis"),
			"maxXAxis" => dgettext($domain, "Maximum no. items on the X axis"),
			"maxXAxisLimit" => dgettext($domain, "-1 is unlimited"),
			"numResources" => dgettext($domain, "No. resources"),
			"resourceType" => dgettext($domain, "Resource type"),
			"resourceyearYear1" => dgettext($domain, "Publication year"),
            "keywordKeyword" => dgettext($domain, "Keyword"),
            "categoryCategory" => dgettext($domain, "Category"),
            "journal" => dgettext($domain, "Journal"),
			"proceedings"	=>	dgettext($domain, "Proceedings"),
            "inputMissing" => dgettext($domain, "Missing input"),
            "next" => dgettext($domain, "next"),
            "previous" => dgettext($domain, "previous"),
            "plotType" => dgettext($domain, "Type of plot"),
            "line" => dgettext($domain, "Line plot"),
            "bar" => dgettext($domain, "Bar plot"),
            "barLine" => dgettext($domain, "Combined bar/line plot"),
            "scatter" => dgettext($domain, "Scatter plot"),
            "scatterLine" => dgettext($domain, "Scatter plot with line"),
            "balloon" => dgettext($domain, "Balloon plot"),
            "visualize"	=>	dgettext($domain, "Visualize"),
		);
	}
}
