********************************************************************************
**                                 Visualize                                  **
**                               WIKINDX module                               **
********************************************************************************


NB. this module is compatible with WIKINDX v5.8.3 and up.
Results may be unexpected if used with a lower version.

Create various visualizations from WIKINDX data.

Makes use of JpGraph: https://jpgraph.net/

The module registers itself in the 'plugin1' menu.

Unzip this file (with any directory structure) into plugins/visualize/.
Thus, plugins/visualize/index.php etc.

********************************************************************************

CHANGELOG:

v1.1 ~ October 2019
1.  Initial release

--
Mark Grimshaw-Aagaard 2019.