# Wikindx's Español Translation ressource.
# Copyright (C) 2019, Mark Grimshaw-Aagaard <sirfragalot@users.sourceforge.net>.
# This file is distributed under the same license as the localedescription package.
#
# ### [Translators]##############################################################
# Mark Grimshaw-Aagaard <sirfragalot@users.sourceforge.net>, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: localedescription\n"
"Report-Msgid-Bugs-To: sirfragalot@users.sourceforge.net\n"
"POT-Creation-Date: 2019-10-24 21:09+0200\n"
"PO-Revision-Date: 2019-09-27 01:50+0200\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: plugins\localeDescription\localeDescriptionMessages.php:20
msgid "Localize Front Page"
msgstr ""

#: plugins\localeDescription\localeDescriptionMessages.php:21
msgid "Localize Front Page Description"
msgstr ""

#: plugins\localeDescription\localeDescriptionMessages.php:22
msgid ""
"Here, you can set an alternate front page description for each localization "
"language you have installed.\n"
"\t\t\t\t\t\t Alternate descriptions are stored in the config_ database as "
"configDescription_xx where 'xx' is the localization\n"
"\t\t\t\t\t\t folder name. The original description, set in Admin|Configure, "
"is displayed whenever the user changes to a localization\n"
"\t\t\t\t\t\t language for which there is no alternate front page description."
msgstr ""

#: plugins\localeDescription\localeDescriptionMessages.php:26
msgid ""
"If you empty the description and submit, you are removing the database field."
msgstr ""

#: plugins\localeDescription\localeDescriptionMessages.php:27
msgid "Choose localization"
msgstr ""

#: plugins\localeDescription\localeDescriptionMessages.php:28
msgid "Original Admin|Configure description"
msgstr ""

#: plugins\localeDescription\localeDescriptionMessages.php:29
msgid "Missing language"
msgstr ""

#: plugins\localeDescription\localeDescriptionMessages.php:30
msgid "Successfully edited ###"
msgstr ""

#: plugins\localeDescription\localeDescriptionMessages.php:31
msgid ""
"Only English is available on this WIKINDX – set the front page description "
"via the Admin|Configure interface"
msgstr ""
