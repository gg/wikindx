# Wikindx's Русский Translation ressource.
# Copyright (C) 2019, Mark Grimshaw-Aagaard <sirfragalot@users.sourceforge.net>.
# This file is distributed under the same license as the importexportbib package.
#
# ### [Translators]##############################################################
# Mark Grimshaw-Aagaard <sirfragalot@users.sourceforge.net>, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: importexportbib\n"
"Report-Msgid-Bugs-To: sirfragalot@users.sourceforge.net\n"
"POT-Creation-Date: 2019-10-24 21:09+0200\n"
"PO-Revision-Date: 2019-09-27 01:50+0200\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#: plugins\importexportbib\importexportbibMessages.php:20
msgid "Import & Export..."
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:21
msgid "Import Endnote"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:22
msgid "Import Endnote XML Bibliography"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:23
#: plugins\importexportbib\importexportbibMessages.php:24
msgid "Import PubMed"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:25
msgid "Bibutils"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:26
msgid "Exp. RTF..."
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:27
msgid "Exp. BibTeX..."
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:28
msgid "Exp. HTML..."
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:29
msgid "Exp. RIS..."
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:30
msgid "Exp. Endnote..."
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:31
msgid "Basket"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:32
msgid "Last List"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:33
#: plugins\importexportbib\importexportbibMessages.php:41
msgid "Exported Files"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:34
#: plugins\importexportbib\importexportbibMessages.php:40
msgid "Export Ideas"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:35
msgid "Export Rich Text Format"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:36
msgid "Export BibTeX"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:37
msgid "Export HTML"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:38
msgid "Export RIS"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:39
msgid "Export Endnote"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:42
msgid "Bibutils Conversion"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:43
msgid "Input Type"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:44
msgid "Output Type"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:45
msgid "Input File"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:46
#: plugins\importexportbib\importexportbibMessages.php:164
msgid "Output File"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:47
msgid ""
"The Bibutils plugin uses C binaries written by Chris Putnam at ###. Most "
"conversions use the MODS XML\n"
"\t\t\tintermediate format."
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:49
msgid "No Bibutils programs found in ###"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:50
msgid "No input type specified"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:51
msgid "No output type specified"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:52
msgid "Missing input file"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:53
#: plugins\importexportbib\importexportbibMessages.php:166
msgid "FAILED to convert (###)"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:54
msgid "Conversion options for intermediate MODS XML"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:55
msgid "IGNORE"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:56
msgid ""
"Encode unicode characters directly in the file rather than as XML entities"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:57
msgid "Don't put citation key in the MODS id field"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:58
msgid "Don't split titles into TITLE/SUBTITLE pairs"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:59
msgid "Do not covert latex character combinations"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:60
msgid "Unicode in, unicode out"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:61
#: plugins\importexportbib\importexportbibMessages.php:163
msgid "Successfully converted ###"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:62
msgid ""
"You may import Endnote XML bibliographies (.xml files) here. Large files may "
"take some time so if\n"
"\t\t\t\tWIKINDX senses that php.ini's 'max_execution_time' variable is about "
"to be exceeded, it will start importing the bibliography in chunks.\n"
"\t\t\t\tIf there is a date field, the format should be either dd/mm/yyyy or "
"yyyy/mm/dd and yyyy will override any year field in the record.\n"
"\t\t\t\tIf you have custom fields in your import file, create custom fields "
"first in the WIKINDX database (the Admin menu) so that you can then map\n"
"\t\t\t\tthe import custom fields."
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:67
msgid ""
"You may export to Rich Text Format your most recent bibliography list. Large "
"numbers of resources might\n"
"\t\t\t\ttake some time and memory so you might need to adjust php.ini."
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:69
msgid "Category"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:70
msgid ""
"All WIKINDX resources belong to at least one category which you chose here.  "
"The category(s) a resource belongs to can always be edited later."
msgstr ""

#. importDuplicates For file imports, allow duplicates?
#: plugins\importexportbib\importexportbibMessages.php:72
msgid "Import duplicates:"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:73
msgid "Ignore keywords:"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:74
msgid ""
"You may store Endnote fields that WIKINDX does not use so that any resources "
"later exported to Endnote can include this original unchanged data. Store "
"unused fields:"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:75
msgid "File is empty"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:76
msgid "No. resources added: ###"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:77
msgid "No. resources discarded (duplicates or no titles): ###"
msgstr ""

#. invalidField1 If non-standard import fields are found in the input file, invite the user to map these fields to wikindx fields
#: plugins\importexportbib\importexportbibMessages.php:79
msgid ""
"Unknown fields have been found. You may map these fields to WIKINDX fields -- "
"no duplicate mapping is allowed."
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:80
msgid ""
"Where an unknown field is mapped to a WIKINDX field that would normally be "
"automatically mapped to a standard input field, the unknown field mapping "
"takes precedence."
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:81
msgid "Import File"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:82
msgid "Tag this import so you can do a mass select or delete later"
msgstr ""

#. executionTimeExceeded With large imports that would go over php.ini's max_execution time, WIKINDX splits the imports into chunks
#: plugins\importexportbib\importexportbibMessages.php:84
msgid ""
"'max_execution_time' (### seconds) in php.ini was about to be exceeded.  "
"WIKINDX is importing the bibliography in chunks."
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:85
msgid "No. resources added this chunk: ###"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:86
msgid "You have already imported that file"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:87
msgid "Import bibliography is empty"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:88
msgid "File upload error"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:89
msgid "You must create a list from the Search menu first"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:90
msgid "You have no ideas"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:91
msgid "Export all ideas"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:92
msgid "Export selected ideas"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:93
msgid "Data successfully exported"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:94
msgid "Data successfully imported"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:95
msgid "Contents of your temporary folder (newest first):"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:96
msgid "Directory is empty"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:97
msgid ""
"These files will be available for ### minutes or while you keep your browser "
"open (whichever is the shorter) so download and save them elsewhere "
"immediately"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:98
msgid "Bibliography exports:"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:99
msgid "Export bibliographic entries:"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:100
msgid "Font size"
msgstr ""

#. Font type e.g. 'Times New Roman', 'Courier'
#: plugins\importexportbib\importexportbibMessages.php:102
msgid "Font"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:103
msgid "Export abstract:"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:104
msgid "Export notes:"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:105
msgid "Export quotes:"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:106
msgid "Export paraphrases:"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:107
msgid "Export musings:"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:108
msgid "Export comments:"
msgstr ""

#. Number of indents (tabulation - TAB)
#: plugins\importexportbib\importexportbibMessages.php:110
msgid "Left indent"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:111
msgid "Right indent"
msgstr ""

#. 'CR' = carriage return (newlines)
#: plugins\importexportbib\importexportbibMessages.php:113
msgid "CR following"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:114
msgid "CR between"
msgstr ""

#. bold, italics, underline
#: plugins\importexportbib\importexportbibMessages.php:116
msgid "Text format"
msgstr ""

#. 'tag' = label given to each section (abstract, quotes, quote comments etc.)
#: plugins\importexportbib\importexportbibMessages.php:118
msgid "Tag"
msgstr ""

#. Some text or characters to visually divide resources in the RT output
#: plugins\importexportbib\importexportbibMessages.php:120
msgid "Divider between entries"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:121
msgid "CR after entries"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:122
msgid "Include ID no. (ISBN etc.)"
msgstr ""

#. Metadata export options
#: plugins\importexportbib\importexportbibMessages.php:124
msgid "If exporting quote and paraphrase comments or musings"
msgstr ""

#. These two are in a select box and follow on from the string above
#: plugins\importexportbib\importexportbibMessages.php:126
msgid "Export only mine"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:127
msgid "Export my data and all public data"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:128
msgid "Add resource's primary creator and publication year to metadata"
msgstr ""

#. Default text for labelling metadata in the RTF export
#: plugins\importexportbib\importexportbibMessages.php:130
msgid "QUOTES:"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:131
msgid "PARAPHRASES:"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:132
#: plugins\importexportbib\importexportbibMessages.php:133
msgid "COMMENTS:"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:134
msgid "MUSINGS:"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:135
msgid "ABSTRACT:"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:136
msgid "NOTES:"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:137
msgid "Include resource keywords"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:138
msgid "Include a hyperlink to the resource in this WIKINDX:"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:139
msgid "Endnote file type"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:140
msgid "Endnote XML"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:141
msgid "Endnote tabbed"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:142
msgid ""
"Search PubMed: please enter one or more fields.  Multiple items within a "
"field\n"
"\t\t\t\tshould be separated by a space.  Per NCBI policy, a maximum of 100 "
"search results can be obtained."
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:144
msgid "PubMed ID"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:145
msgid "All Fields"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:146
msgid "Author"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:147
msgid "First Author"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:148
msgid "Last Author"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:149
msgid "Title"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:150
msgid "Abstract"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:151
msgid "Year"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:152
msgid "Journal"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:153
msgid "Volume"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:154
msgid "Issue"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:155
msgid "Language"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:156
msgid "Limit search to the last n days"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:157
msgid "Max. Results"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:158
msgid "Import the PubMed import directly into WIKINDX"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:159
msgid "Please enter an integer up to 100 for Max. Results"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:160
msgid "Please enter one or more fields"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:161
#: plugins\importexportbib\importexportbibMessages.php:162
msgid "result"
msgid_plural "results"
msgstr[0] ""
msgstr[1] ""
msgstr[2] ""

#: plugins\importexportbib\importexportbibMessages.php:165
msgid "No Results Found"
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:167
msgid ""
"XML file is not compatible with Endnote v8 which is required for this import "
"script."
msgstr ""

#: plugins\importexportbib\importexportbibMessages.php:168
msgid "Successfully imported Endnote file."
msgstr ""
