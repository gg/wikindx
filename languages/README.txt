WIKINDX v5.x localizations

Various languages for WIKINDX v5.x.

Unzip into wikindx/languages/ then select in the Preferences menu.

These localizations depend on third parties for their creation and upkeep and so might not 
always be up-to-date. Any missing messages will automatically be replaced with the English version.

Download and install the localization plugin to compile and use your own localizations – please 
consider donating your localization to WIKINDX for others to use.
