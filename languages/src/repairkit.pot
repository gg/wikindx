# Wikindx's ### Translation ressource.
# Copyright (C) 2019, Mark Grimshaw-Aagaard <sirfragalot@users.sourceforge.net>.
# This file is distributed under the same license as the repairkit package.
#
# ### [Translators]##############################################################
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: repairkit\n"
"Report-Msgid-Bugs-To: sirfragalot@users.sourceforge.net\n"
"POT-Creation-Date: 2019-10-24 21:09+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. *
#. * Menu items
#.
#: plugins\repairkit\repairkitMessages.php:27
msgid "Repair kit..."
msgstr ""

#: plugins\repairkit\repairkitMessages.php:28
msgid "Fix chars"
msgstr ""

#: plugins\repairkit\repairkitMessages.php:29
msgid "Missing rows"
msgstr ""

#: plugins\repairkit\repairkitMessages.php:30
msgid "Fix totals"
msgstr ""

#: plugins\repairkit\repairkitMessages.php:31
msgid "Dump DB schema"
msgstr ""

#: plugins\repairkit\repairkitMessages.php:32
msgid "DB integrity"
msgstr ""

#: plugins\repairkit\repairkitMessages.php:33
msgid "Fix creators"
msgstr ""

#. *
#. * Headings
#.
#: plugins\repairkit\repairkitMessages.php:37
msgid "Fix Chars"
msgstr ""

#: plugins\repairkit\repairkitMessages.php:38
msgid "Missing Rows"
msgstr ""

#: plugins\repairkit\repairkitMessages.php:39
msgid "Fix Totals"
msgstr ""

#: plugins\repairkit\repairkitMessages.php:40
msgid "Dump Database Schema"
msgstr ""

#: plugins\repairkit\repairkitMessages.php:41
msgid "Database Integrity"
msgstr ""

#: plugins\repairkit\repairkitMessages.php:42
msgid "Fix Creators"
msgstr ""

#. *
#. * Text
#.
#: plugins\repairkit\repairkitMessages.php:46
msgid ""
"Depending on the size of your database, this operation can be memory "
"intensive.  The\n"
"\t\t\t\tplugin turns on error reporting so, if you see a memory error, adjust "
"memory in config.php and\n"
"\t\t\t\ttry again."
msgstr ""

#: plugins\repairkit\repairkitMessages.php:49
msgid "It is assumed you have backed up your database."
msgstr ""

#: plugins\repairkit\repairkitMessages.php:50
msgid ""
"When upgrading WIKINDX from v3.8.2 to v4.x, not all UTF-8-encoded database\n"
"\t\t\t\tfields are properly dealt with and you may see characters similar to "
"'ã¼' or 'ã¶' etc.\n"
"\t\t\t\tin the WIKINDX. Additionally, as WIKINDX is a system that can accept "
"input from many different\n"
"\t\t\t\tsources, character encoding can be corrupted right from the start. "
"This plugin will attempt to correct such anomalies.  These\n"
"\t\t\t\toperations are irreversible so ensure that you have backed up your "
"database. You will get an opportunity to confirm the fixing of data\n"
"\t\t\t\twhen you click on Submit."
msgstr ""

#: plugins\repairkit\repairkitMessages.php:56
msgid ""
"Sometimes rows required in other tables are not created when new resources "
"are added and this\n"
"\t\t\tcan lead to problems in searching etc.  If such missing rows are "
"identified, they will be created and filled with\n"
"\t\t\tdefault (usually NULL) data."
msgstr ""

#: plugins\repairkit\repairkitMessages.php:59
msgid ""
"Fix totals of resources, quotes, paraphrases and musings in the "
"database_summary table."
msgstr ""

#: plugins\repairkit\repairkitMessages.php:60
msgid "Fix various errors regarding creators."
msgstr ""

#: plugins\repairkit\repairkitMessages.php:61
msgid ""
"This should only be used by WIKINDX project admins when creating a file that "
"stores the current database and tables schemata just prior to a WIKINDX "
"release. This provides the means to check the user's database structure "
"against the release database structure and to effect any necessary repairs. "
"plugins/repairKit/databaseSchemata/ should be writable. If you are not a "
"WIKINDX project admin, proceed at your own risk."
msgstr ""

#: plugins\repairkit\repairkitMessages.php:62
msgid ""
"Ensure you have a fresh version ### database that has the official release "
"structure (a new, empty database, created when installing the release code, "
"is best) before clicking on OK."
msgstr ""

#: plugins\repairkit\repairkitMessages.php:63
msgid ""
"The database schema has been written to ###. You should add it to SVN before "
"release of a new WIKINDX version."
msgstr ""

#: plugins\repairkit\repairkitMessages.php:64
msgid ""
"Before checking the database integrity, you should ensure that your database "
"(currently version ###)"
msgstr ""

#: plugins\repairkit\repairkitMessages.php:65
msgid "matches the WIKINDX version (currently version ###)."
msgstr ""

#: plugins\repairkit\repairkitMessages.php:66
msgid "The database structure is correct and there is nothing to fix."
msgstr ""

#: plugins\repairkit\repairkitMessages.php:67
msgid ""
"The database structure differs to the final release database structure for "
"version ### (see report below). This could be because you have been updating "
"this database with pre-release SVN code that has upgraded the structure in "
"incremental steps. Click on OK to fix this. Note: this fix does NOT fix "
"missing tables or missing fields – it will only fix variances in engine, "
"collation, indices, field types, and null and default values. Remember, this "
"fix is a last resort for those cases where a production (rather than a test) "
"database has been mistakenly upgraded with SVN code. As always, backup your "
"database before proceeding."
msgstr ""

#: plugins\repairkit\repairkitMessages.php:68
msgid "Successfully fixed. ###"
msgstr ""

#: plugins\repairkit\repairkitMessages.php:69
msgid ": ### resources fixed."
msgstr ""

#: plugins\repairkit\repairkitMessages.php:70
msgid "Light UTF-8 fix"
msgstr ""

#: plugins\repairkit\repairkitMessages.php:71
msgid "Tough UTF-8 fix"
msgstr ""

#: plugins\repairkit\repairkitMessages.php:72
msgid ""
"Encode non-UTF-8 characters as UTF-8. This will fix most UTF-8 problems and "
"should be the fix you try first."
msgstr ""

#: plugins\repairkit\repairkitMessages.php:73
msgid ""
"Encode non-UTF-8 characters as UTF-8. If the light fix does not work, you "
"should try this as a last resort."
msgstr ""

#: plugins\repairkit\repairkitMessages.php:74
msgid ""
"The following items show the possibly fixed text that you can now write to "
"the database."
msgstr ""

#: plugins\repairkit\repairkitMessages.php:75
msgid "Nothing found to fix."
msgstr ""

#: plugins\repairkit\repairkitMessages.php:76
msgid "Fix all"
msgstr ""

#: plugins\repairkit\repairkitMessages.php:77
msgid "Fix selected"
msgstr ""

#: plugins\repairkit\repairkitMessages.php:78
msgid "Fix not selected"
msgstr ""

#: plugins\repairkit\repairkitMessages.php:79
msgid "Invalid or missing input"
msgstr ""

#: plugins\repairkit\repairkitMessages.php:80
msgid ""
"plugins/repairKit/databaseSchemata/ is not writeable by the web server user. "
"It currently has the permissions: ###"
msgstr ""

#: plugins\repairkit\repairkitMessages.php:81
msgid "Unable to write to file."
msgstr ""

#: plugins\repairkit\repairkitMessages.php:82
msgid "Unable to read file."
msgstr ""

#: plugins\repairkit\repairkitMessages.php:83
msgid "The database is missing the following tables and cannot be fixed:"
msgstr ""

#: plugins\repairkit\repairkitMessages.php:84
msgid "The database is missing the following fields and cannot be fixed:"
msgstr ""

#: plugins\repairkit\repairkitMessages.php:85
msgid ""
"There are invalid datetime field values ('0000-00-00 00:00:00') that must be "
"fixed."
msgstr ""
