# Wikindx's ### Translation ressource.
# Copyright (C) 2019, Mark Grimshaw-Aagaard <sirfragalot@users.sourceforge.net>.
# This file is distributed under the same license as the dbadminer package.
#
# ### [Translators]##############################################################
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: dbadminer\n"
"Report-Msgid-Bugs-To: sirfragalot@users.sourceforge.net\n"
"POT-Creation-Date: 2019-10-24 21:09+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: plugins\dbAdminer\dbAdminerMessages.php:19
#: plugins\dbAdminer\dbAdminerMessages.php:20
msgid "DB Adminer"
msgstr ""

#: plugins\dbAdminer\dbAdminerMessages.php:21
msgid "Open DB Adminer in a new window / tab"
msgstr ""
