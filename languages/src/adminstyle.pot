# Wikindx's ### Translation ressource.
# Copyright (C) 2019, Mark Grimshaw-Aagaard <sirfragalot@users.sourceforge.net>.
# This file is distributed under the same license as the adminstyle package.
#
# ### [Translators]##############################################################
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: adminstyle\n"
"Report-Msgid-Bugs-To: sirfragalot@users.sourceforge.net\n"
"POT-Creation-Date: 2019-10-24 21:09+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. *
#. * Menu items
#.
#: plugins\adminstyle\adminstyleMessages.php:27
msgid "Styles..."
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:28
msgid "Create Style"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:29
msgid "Copy Style"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:30
msgid "Edit Style"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:31
msgid "Short Name"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:32
msgid "Long Name"
msgstr ""

#. *
#. * General
#.
#: plugins\adminstyle\adminstyleMessages.php:36
msgid "Successfully edited style"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:37
msgid "Successfully added style"
msgstr ""

#. *
#. * Styles
#.
#: plugins\adminstyle\adminstyleMessages.php:41
msgid "Primary creator delimiters"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:42
msgid "Other creator delimiters"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:43
msgid "If only two creators"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:44
msgid "between"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:45
msgid "before last"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:46
msgid "Between first two creators"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:47
msgid "Between following creators"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:48
msgid "Primary creator style"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:49
msgid "Other creator styles"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:50
msgid "First"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:51
msgid "Others"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:52
msgid "Initials"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:53
msgid "First name"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:54
msgid "Full"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:55
msgid "Initial"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:56
msgid "Primary creator list abbreviation"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:57
msgid "Other creator list abbreviation"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:58
msgid "Full list"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:59
msgid "Limit list"
msgstr ""

#. translators: The next 3 surround form text boxes: "If xx or more creators, list the first xx and abbreviate with xx".  For example: "If 4 or more creators, list the first 1 and abbreviate with ,et. al"
#: plugins\adminstyle\adminstyleMessages.php:61
msgid "If"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:62
msgid "or more creators, list the first"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:63
msgid "and abbreviate with"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:64
msgid "Title capitalization"
msgstr ""

#. Title as entered with no changes to capitalization
#: plugins\adminstyle\adminstyleMessages.php:66
msgid "As entered"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:67
msgid "Available fields:"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:68
msgid "Available fields (Bibliography)"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:69
msgid "Available fields (Footnote)"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:70
msgid "Disable fields"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:71
msgid "RESET"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:72
msgid "Edition format"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:73
msgid "Month format"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:74
msgid "Date format"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:75
msgid "Day format"
msgstr ""

#. Add a leading zero to day if less than 10.
#: plugins\adminstyle\adminstyleMessages.php:77
msgid "Add leading zero"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:78
msgid "Page format"
msgstr ""

#. Length of film, broadcast etc.
#: plugins\adminstyle\adminstyleMessages.php:80
msgid "Running time format"
msgstr ""

#. When displaying a book that has no author but has an editor, do we put the editor in the position occupied by the author?
#: plugins\adminstyle\adminstyleMessages.php:82
msgid "Editor switch"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:83
msgid "For books with no author but an editor, put editor in author position"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:84
msgid "Yes"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:85
msgid "No"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:86
msgid "If 'Yes', replace editor field in style definitions with"
msgstr ""

#. Uppercase creator names?
#: plugins\adminstyle\adminstyleMessages.php:88
msgid "Uppercase all names"
msgstr ""

#. For repeated creator names in next bibliographic item
#: plugins\adminstyle\adminstyleMessages.php:90
msgid "For works immediately following by the same creators"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:91
msgid "Print the creator list"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:92
msgid "Do not print the creator list"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:93
msgid "Replace creator list with text below"
msgstr ""

#. Fallback formatting style when a specific resource type has none defined
#: plugins\adminstyle\adminstyleMessages.php:95
msgid "Fallback style"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:96
msgid "Bibliography Formatting"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:97
msgid "Italics"
msgstr ""

#. For user specific month naming
#: plugins\adminstyle\adminstyleMessages.php:99
msgid "Use month names defined below"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:100
msgid ""
"User-defined month names (all fields must be completed if selected above)"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:101
msgid ""
"User-defined seasons (all fields must be completed if month format above is "
"set to user-defined)"
msgstr ""

#. Date ranges for e.g. conferences
#: plugins\adminstyle\adminstyleMessages.php:103
msgid "Date range"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:104
msgid "Delimiter between start and end dates if day and month given"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:105
msgid "Delimiter between start and end dates if month only given"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:106
msgid "If start and end months are equal"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:107
msgid "Print both months"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:108
msgid "Print start month only"
msgstr ""

#. Different puncutation may be required if a month is given with no day.
#: plugins\adminstyle\adminstyleMessages.php:110
msgid "If a date has a month but no day"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:111
msgid "Use style definition unchanged"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:112
msgid "Replace date field in template with:"
msgstr ""

#. Don't translate 'date'
#: plugins\adminstyle\adminstyleMessages.php:114
msgid "(Use 'date' as the field)"
msgstr ""

#. Which language localization to use for ordinals and months.
#: plugins\adminstyle\adminstyleMessages.php:116
msgid "Localization"
msgstr ""

#. Re-write creator(s) portion of templates to handle styles such as DIN 1505.
#: plugins\adminstyle\adminstyleMessages.php:118
msgid "Split creator lists and add strings"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:119
msgid "Add string to first name in list"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:120
msgid "Add string to remaining names"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:121
msgid "Before name:"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:122
msgid "To each name:"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:123
msgid "Bibliography template"
msgstr ""

#. Don't translate 'pages'
#: plugins\adminstyle\adminstyleMessages.php:125
msgid "(Footnote template may also use 'pages')"
msgstr ""

#. Some general help for using templates displayed in the Admin|Style page
#: plugins\adminstyle\adminstyleMessages.php:127
msgid ""
"1. The three generic bibliography templates are required and will be used if "
"a displayed resource has no bibliographic template."
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:128
msgid ""
"2. The footnote templates are only required for those styles that use "
"footnotes for citations.  In all cases, the complete bibliography ('works "
"cited') for footnote styles, as well as for endnote and in-text styles, uses "
"the bibliography template."
msgstr ""

#. Don't translate 'citation'
#: plugins\adminstyle\adminstyleMessages.php:130
msgid ""
"3. For footnote citations, the 'citation' field above refers to the footnote "
"template or, if that does not exist, to the bibliography template or, if that "
"does not exist, to the fallback style."
msgstr ""

#. Don't translate 'pages'
#: plugins\adminstyle\adminstyleMessages.php:132
msgid ""
"4. The 'pages' field in the bibliography template refers to the complete "
"article page range; in the footnote template, it refers to the specific "
"citation page(s)."
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:133
msgid ""
"5. If you define a footnote template for a resource you must also define the "
"bibliography template for that resource."
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:134
msgid ""
"6. If a resource is missing the first field in the bibliography template you "
"may replace that field with the fields in the partial template (this allows a "
"reordering of the initial fields)."
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:135
msgid ""
"7. In the bibliography template for Book Chapter, the field 'title' refers to "
"the chapter number."
msgstr ""

#. For template previewing, allow the use to preview by turning various fields on and off.
#: plugins\adminstyle\adminstyleMessages.php:137
msgid "Preview with the following fields"
msgstr ""

#. Characters separating title and subtitle
#: plugins\adminstyle\adminstyleMessages.php:139
msgid "Title/subtitle separator"
msgstr ""

#. See "templateHelp6" above
#: plugins\adminstyle\adminstyleMessages.php:141
msgid "Partial template"
msgstr ""

#. Use the partial template to replace all of the bibliography template.
#: plugins\adminstyle\adminstyleMessages.php:143
msgid "Replace all of original template with partial template"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:144
msgid "Generic book-type"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:145
msgid "Generic article-type"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:146
msgid "Generic miscellaneous"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:147
msgid "Preview bibliography"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:148
msgid "Preview in-text citation"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:149
msgid "Preview footnote"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:150
msgid "ERROR"
msgstr ""

#. *
#. * Citation
#.
#. The displayed hyperlink next to the textarea form input
#: plugins\adminstyle\adminstyleMessages.php:155
msgid "Cite"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:156
msgid "Citation Formatting"
msgstr ""

#. In-text citation style as opposed to endnote style citations.
#: plugins\adminstyle\adminstyleMessages.php:158
msgid "In-text style"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:159
msgid "Endnote style"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:160
msgid "Footnote creators"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:161
msgid "Creator list abbreviation"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:162
msgid "Creator list abbreviation (subsequent appearances)"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:163
msgid "Creator delimiters"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:164
msgid "Creator style"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:165
msgid "Last name only"
msgstr ""

#. 'Last name only' is a choice in a select box and should not be translated
#: plugins\adminstyle\adminstyleMessages.php:167
msgid ""
"If 'Last name only', use initials to differentiate between creators with the "
"same surname"
msgstr ""

#. For consecutive citations by the same creator(s)
#: plugins\adminstyle\adminstyleMessages.php:169
msgid ""
"For consecutive citations by the same creator(s) use the following template:"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:170
msgid "and separate citations with:"
msgstr ""

#. The template is something like '(author|, year)' that the user is asked to enter
#: plugins\adminstyle\adminstyleMessages.php:172
msgid "Template"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:173
msgid "Separate consecutive citations with"
msgstr ""

#. Formatting of years
#: plugins\adminstyle\adminstyleMessages.php:175
msgid "Year format"
msgstr ""

#. Normal, superscript or subscript of citation
#: plugins\adminstyle\adminstyleMessages.php:177
msgid "Normal text"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:178
msgid "Superscript"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:179
msgid "Subscript"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:180
msgid "Parentheses or other characters enclosing the citation"
msgstr ""

#. Ambiguous citations
#: plugins\adminstyle\adminstyleMessages.php:182
msgid "Ambiguous citations"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:183
msgid "Use the following template"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:184
msgid "Add a letter after the year"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:185
msgid "Leave citation unchanged"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:186
msgid ""
"Use template below if a single citation is in the same sentence as the first "
"creator's surname"
msgstr ""

#. This follows on from sentence above....  Split the pages from the main citation placing the main citation immediately after the creator names in the text and the pages immediately following the quote.  e.g. if the citation is in the form: Grimshaw states:  "WIKINDX is wonderful" [cite]123:25[/cite], the result will be Grimshaw (2005) states:  "WIKINDX is wonderful" (p.25) rather than Grimshaw states:  "WIKINDX is wonderful" (2005, p.25).
#: plugins\adminstyle\adminstyleMessages.php:188
msgid ""
"and split the citation placing the main citation after the creator names and "
"the page number after the quote:"
msgstr ""

#. For endnote-style citations
#: plugins\adminstyle\adminstyleMessages.php:190
msgid "Format like bibliography"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:191
msgid "Format like in-text citations"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:192
msgid ""
"Replace consecutive citations for the same resource and the same page with "
"this template"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:193
msgid ""
"Replace consecutive citations for the same resource but a different page with "
"this template"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:194
msgid "Replace previously cited resources with this template"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:195
msgid "Format of the citation in the text"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:196
msgid "Format of the citation in the endnotes"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:197
msgid "Endnote/Footnote"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:198
msgid "Endnotes: incrementing"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:199
msgid "Endnotes: same ID for same resource"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:200
msgid "Footnotes: incrementing"
msgstr ""

#. Ordering of the appended bibliography for in-text citations and endnote-style citations using the same id number for each cited resource
#: plugins\adminstyle\adminstyleMessages.php:202
msgid "Bibliography ordering"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:203
msgid ""
"(For in-text citations, endnote-style citations using the same ID number and "
"bibliographies appended to papers using footnotes.)"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:204
msgid "Use this order for endnote-style citations using the same ID number:"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:205
msgid "1st. sort by"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:206
msgid "2nd. sort by"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:207
msgid "3rd. sort by"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:208
msgid "Ascending"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:209
msgid "Descending"
msgstr ""

#. For a particular resource type (personal communication for example), replace the in-text citation template with another template
#: plugins\adminstyle\adminstyleMessages.php:211
msgid "For in-text citations, replace the citation template with this template"
msgstr ""

#. Text preceeding and following citations e.g. (see Grimshaw 1999; Boulanger 2004 for example): 'see' is preText and 'for example' is postText
#: plugins\adminstyle\adminstyleMessages.php:213
msgid "Preliminary text"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:214
msgid "Following text"
msgstr ""

#. Formatting of the id number in the endnotes for endnote-style citations
#: plugins\adminstyle\adminstyleMessages.php:216
msgid "Parentheses or other characters enclosing the ID number"
msgstr ""

#. For subsequent citations from the same resource
#: plugins\adminstyle\adminstyleMessages.php:218
msgid ""
"For subsequent citations from the same resource use the following template:"
msgstr ""

#. This follows on from the text in 'subsequentCreator'
#: plugins\adminstyle\adminstyleMessages.php:220
msgid ""
"only if the sentence containing the citation has the creator surname, title "
"or shortTitle in it:"
msgstr ""

#. If no year for in-text citations, replace year field
#: plugins\adminstyle\adminstyleMessages.php:222
msgid "If no year, replace year field with the following"
msgstr ""

#. When compiling the appended bibliography for in-text citations, certain resources (e.g. APA personal communication) are not added.
#: plugins\adminstyle\adminstyleMessages.php:224
msgid "Do not add to the bibliography when cited:"
msgstr ""

#. When using endnote-style citations and defining templates using fields such as 'creator', 'pages', 'year' or 'title'.  Don't translate 'creator', 'pages' or 'citation'.
#: plugins\adminstyle\adminstyleMessages.php:226
msgid ""
"Fields are formatted as defined in in-text citation formatting above unless "
"using footnotes in which case the 'creator' field is defined below and the "
"'pages' field format is defined in the footnote template in the bibliography "
"section. If the 'citation' field is used, it should be by itself and it "
"refers to the bibliographic/footnote templates below."
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:227
msgid "Footnote template"
msgstr ""

#. Set the range of text within which the subsequent creator template (for in-text citations) is used
#: plugins\adminstyle\adminstyleMessages.php:229
msgid "Range within which subsequent citations are searched for"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:230
msgid "Entire text"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:231
msgid "Paragraph"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:232
msgid "Section"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:233
msgid ""
"Remove title and shortTitle fields from the citation if either of those "
"fields is in the same sentence:"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:234
msgid "First Creator"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:235
msgid "Title"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:236
msgid "Publication Year"
msgstr ""

#. *
#. * Hints
#.
#: plugins\adminstyle\adminstyleMessages.php:240
msgid "(No spaces)"
msgstr ""

#: plugins\adminstyle\adminstyleMessages.php:241
msgid "(Fields are case-sensitive)"
msgstr ""

#: plugins\adminstyle\help.php:30
msgid ""
"If you have WIKINDX admin rights, you can create and edit bibliographic "
"styles for on-the-fly formatting when displaying or exporting bibliographic "
"lists."
msgstr ""

#. translators: #linkSfWikindx# unchanged. This pattern is replaced by a link
#: plugins\adminstyle\help.php:33
msgid ""
"These styles are stored as XML files each within its own directory in the "
"styles/bibliography/ directory. This directory <strong>must</strong> be "
"writeable by everyone or at least the web server user (usually user "
"'nobody'). Additionally, when editing an existing style, the XML style file "
"within its named directory in the styles/bibliography/ directory <strong>must "
"also</strong> be writeable by everyone or the web server user. As new "
"bibliographic styles are created, the WIKINDX team will make these available "
"on the #linkSfWikindx# downloads site as plug-ins. Once you have a downloaded "
"file, simply unzip the contents to the styles/bibliography/ directory."
msgstr ""

#. translators: #linkSfWikindx# unchanged. This pattern is replaced by a link
#: plugins\adminstyle\help.php:36
msgid ""
"If you develop new styles yourself, you are strongly encouraged to contact "
"the WIKINDX developers at #linkSfWikindx# to make them available to other "
"users."
msgstr ""

#: plugins\adminstyle\help.php:37
msgid ""
"You can create a new style based on an existing one by copying the existing "
"style. To remove a style from the list available to your users, disable that "
"style in the Admin|Components menu."
msgstr ""

#: plugins\adminstyle\help.php:38
msgid ""
"Please note, to edit a style, you should do it from the same browser window "
"as you use to view a bibliographic list.  This is because, in order to save "
"processing the style definition file each time you list a bibliography, "
"WIKINDX will check to see if the style definition file has been edited and "
"therefore needs reprocessing. This information is stored in a PHP session "
"variable; each browser window has its own separate set of session variables "
"with no cross-interrogation available. If you edit the style definition file "
"from another browser window then you are unlikely to see changes when you "
"refresh your bibliographic list."
msgstr ""

#: plugins\adminstyle\help.php:42
msgid ""
"Each style has a set of options that define the heading style of titles, how "
"to display numbers and dates etc. and then a separate style definition for "
"each resource type that WIKINDX handles. The 'Short Name' is used by WIKINDX "
"as both the folder and file name and for this reason should not only be a "
"unique name within styles/bibliography/, but should also have no spaces or "
"any other characters that may cause confusion with your operating system (i."
"e. alphanumeric characters only). The 'Long Name' is the description of the "
"style that is displayed to WIKINDX users."
msgstr ""

#: plugins\adminstyle\help.php:43
msgid ""
"The 'Editor switch' requires special attention. Some bibliographic styles "
"require that for books and book chapters, where there exists an editor but no "
"author, the position occupied by the author is taken by the editor. If you "
"select 'Yes' here, you should then supply a replacement editor field. Please "
"note that if the switch occurs, the editor(s) formatting will be inherited "
"from the global settings you supplied for the author. See the examples below."
msgstr ""

#: plugins\adminstyle\help.php:44
msgid ""
"The three 'generic style' definitions are required and are used to display "
"any resource type for which there does not yet exist a style definition. This "
"allows you to build up your style definitions bit by bit.  Furthermore, some "
"bibliographic styles provide no formatting guidelines for particular types of "
"resource in which case the generic styles will provide some formatting for "
"those resources according to the general guidelines for that bibliographic "
"style. Each resource for which there is no style definition will fall back to "
"the chosen generic style. The generic styles try their best but if formatting "
"is strange for a particular resource type then you should explicitly define a "
"style definition for that type."
msgstr ""

#: plugins\adminstyle\help.php:45
msgid ""
"Each style definition has a range of available fields listed to the right of "
"each input box. These fields are <strong>case-sensitive</strong> and need not "
"all be used. However, with some of the more esoteric styles, the more "
"database fields that have been populated for each resource in the WIKINDX, "
"the more likely it is that the formatting will be correct."
msgstr ""

#: plugins\adminstyle\help.php:49
msgid "SYNTAX"
msgstr ""

#: plugins\adminstyle\help.php:50
msgid ""
"The style definition syntax uses a number of rules and special characters:"
msgstr ""

#: plugins\adminstyle\help.php:52
msgid "The character '|' separates fields from one another."
msgstr ""

#: plugins\adminstyle\help.php:53
msgid ""
"If a field does not exist or is blank in the database, none of the definition "
"for that field is printed."
msgstr ""

#: plugins\adminstyle\help.php:54
msgid ""
"<strong>Field names are case-sensitive</strong>&nbsp;and need not all be used."
msgstr ""

#: plugins\adminstyle\help.php:55
msgid ""
"Within a field, you can add any punctuation characters or phrases you like "
"before and after the field name."
msgstr ""

#: plugins\adminstyle\help.php:56
msgid ""
"Any word that you wish to be printed and that is the same (even a partial "
"word) as a field name should be enclosed in backticks '`'."
msgstr ""

#: plugins\adminstyle\help.php:57
msgid ""
"For creator lists (editors, revisers, directors etc.) and pages, alternative "
"singular and plural text can be specified with '^' (e.g. |^p.^pp.^pages| "
"would print the field 'pages' preceded by 'pp.' if there were multiple pages "
"or 'p.' if not)."
msgstr ""

#: plugins\adminstyle\help.php:58
msgid ""
"BBCode <code>[u]..[/u]</code>, <code>[i]..[/i]</code>, <code>[b]..[/b]</"
"code>, <code>[sup]..[/sup]</code> and <code>[sub]..[/sub]</code> can be used "
"to specify underline, italics, bold, superscript and subscript."
msgstr ""

#: plugins\adminstyle\help.php:59
msgid ""
"The character '%' enclosing any text or punctuation <em>before</em> the field "
"name states that that text or those characters will only be printed if the "
"<em>preceeding</em> field exists or is not blank in the database. The "
"character <code>'%'</code> enclosing any text or punctuation <em>after</em> "
"the field name states that that text or those characters will only be printed "
"if the <em>following</em> field exists or is not blank in the database. It is "
"optional to have a second pair in which case the construct should be read "
"<code>'if target field exists, then print this, else, if target field does "
"not exist, print that'</code>.  For example, <code>'%: %'</code> will print "
"<code>': '</code> if the target field exists else nothing if it doesn't while "
"<code>'%: %. %'</code> will print <code>': '</code> if the target field "
"exists else <code>'. '</code> if it does not."
msgstr ""

#: plugins\adminstyle\help.php:60
msgid ""
"Characters in fields that do not include a field name should be paired with "
"another set and together enclose a group of fields. If these special fields "
"are not paired unintended results may occur. These are intended to be used "
"for enclosing groups of fields in brackets where <em>at least</em> one of the "
"enclosed fields exists or is not blank in the database."
msgstr ""

#: plugins\adminstyle\help.php:61
msgid ""
"<p>The above two rules can combine to aid in defining particularly complex "
"bibliographic styles (see examples below). The pair </p>\n"
"\t\t<p><code>|%, %. %|xxxxx|xxxxx|%: %; %|</code></p>\n"
"\t\t<p>states that if at least one of the intervening fields exists, then the "
"comma and colon will be printed; if an intervening field does not exist, then "
"the full stop will be printed <em>only</em> if the <em>preceeding</em> field "
"exists (else nothing will be printed) and the semicolon will be printed "
"<em>only</em> if the <em>following</em> field exists (else nothing will be "
"printed).</p>"
msgstr ""

#: plugins\adminstyle\help.php:64
msgid ""
"If the final set of characters in the style definition is '|.' for example, "
"the '.' is taken as the ultimate punctuation printed at the very end."
msgstr ""

#: plugins\adminstyle\help.php:65
msgid ""
"<p>Fields can be printed or not dependent upon the existence of preceding or "
"subsequent fields. For example,</p>\n"
"\t\t<p><code>creator. |$shortTitle. $title. $|publicationYear.</code></p>\n"
"\t\t<p>would print the shortTitle field if the creator were populated "
"otherwise it prints the title field.</p>\n"
"\t\t<p><code>creator. |title. |#ISBN. ##|edition.</code></p>\n"
"\t\t<p>prints the ISBN field if edition exists otherwise it prints nothing.</"
"p>"
msgstr ""

#: plugins\adminstyle\help.php:70
msgid "Newlines may be added with the special string NEWLINE."
msgstr ""

#: plugins\adminstyle\help.php:73
msgid ""
"Tip: In most cases, you will find it easiest to attach punctuation and "
"spacing at the end of the preceding field rather than at the start of the "
"following field. This is especially the case with finite punctuation such as "
"full stops."
msgstr ""

#: plugins\adminstyle\help.php:77
msgid "EXAMPLES"
msgstr ""

#: plugins\adminstyle\help.php:79
#: plugins\adminstyle\help.php:95
#: plugins\adminstyle\help.php:104
#: plugins\adminstyle\help.php:111
msgid "<em>might produce:</em>"
msgstr ""

#: plugins\adminstyle\help.php:81
msgid ""
"<em>and, if there were no publisher location or edition entered for that "
"resource and only one page number given, it would produce:</em>"
msgstr ""

#: plugins\adminstyle\help.php:87
msgid "<em>might produce:</em></code>"
msgstr ""

#: plugins\adminstyle\help.php:89
msgid ""
"<em>and, if there were no publisher location or publication year entered for "
"that resource, it would produce:</em><br>"
msgstr ""

#: plugins\adminstyle\help.php:97
msgid ""
"<em>and, if there were no author entered for that resource and the "
"replacement editor field were 'editor ^ed.^eds.^ ', it would produce:</em>"
msgstr ""

#: plugins\adminstyle\help.php:102
msgid ""
"Consider the following (IEEE-type) generic style definition and what it does "
"with a resource type lacking certain fields:"
msgstr ""

#: plugins\adminstyle\help.php:106
msgid ""
"<em>and, when applied to a resource type with editor and edition fields:</em>"
msgstr ""

#: plugins\adminstyle\help.php:109
msgid ""
"Clearly there is a problem here, notably at the end of the resource title. "
"The solution is to use rule no. 10 above:"
msgstr ""

#: plugins\adminstyle\help.php:113
msgid "<em>and:</em>"
msgstr ""

#: plugins\adminstyle\help.php:115
msgid "Bibliographic styles requiring this complexity are few and far between."
msgstr ""

#: plugins\adminstyle\help.php:119
msgid ""
"If the value entered for the edition of a resource contains non-numeric "
"characters, then, despite having set the global setting for the edition "
"format to ordinal (3rd. etc.), no conversion will take place."
msgstr ""

#: plugins\adminstyle\help.php:120
msgid ""
"The formatting of the names, edition and page numbers and the capitalization "
"of the title depends on the global settings provided for your bibliographic "
"style."
msgstr ""
