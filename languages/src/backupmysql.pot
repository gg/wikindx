# Wikindx's ### Translation ressource.
# Copyright (C) 2019, Mark Grimshaw-Aagaard <sirfragalot@users.sourceforge.net>.
# This file is distributed under the same license as the backupmysql package.
#
# ### [Translators]##############################################################
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: backupmysql\n"
"Report-Msgid-Bugs-To: sirfragalot@users.sourceforge.net\n"
"POT-Creation-Date: 2019-10-24 21:09+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: plugins\backupMysql\backupMysqlMessages.php:20
#: plugins\backupMysql\backupMysqlMessages.php:21
msgid "Backup Database"
msgstr ""

#: plugins\backupMysql\backupMysqlMessages.php:22
msgid "Backup"
msgstr ""

#: plugins\backupMysql\backupMysqlMessages.php:23
msgid ""
"plugins/backupMysql/dumps/ is not writeable by the web server user.  It "
"currently has the permissions: ###"
msgstr ""

#: plugins\backupMysql\backupMysqlMessages.php:24
msgid "Files deleted"
msgstr ""
