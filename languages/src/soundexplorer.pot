# Wikindx's ### Translation ressource.
# Copyright (C) 2019, Mark Grimshaw-Aagaard <sirfragalot@users.sourceforge.net>.
# This file is distributed under the same license as the soundexplorer package.
#
# ### [Translators]##############################################################
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: soundexplorer\n"
"Report-Msgid-Bugs-To: sirfragalot@users.sourceforge.net\n"
"POT-Creation-Date: 2019-10-24 21:09+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. *
#. * Menu items
#.
#: plugins\soundExplorer\soundExplorerMessages.php:27
msgid "Sound Explorer"
msgstr ""

#: plugins\soundExplorer\soundExplorerMessages.php:28
msgid ""
"Sound Explorer will store searches then, at any time in the future, play a "
"sound if a resource matching\n"
"\t\t\t\ta past search is found in a current list operation. This works on the "
"principle of a serendipitous conjunction of past searches\n"
"\t\t\t\twith a current search result, hopefully allowing you to make new "
"creative connections by prodding at the boundaries of your\n"
"\t\t\t\tcurrent conceptual space.  But it is also useful in a multi-user\n"
"\t\t\t\tWIKINDX where it functions like a 'tripwire' signal -- the sound will "
"play if a new resource or metadata has been added by another\n"
"\t\t\t\tuser that matches a stored search (if the resource is being displayed "
"in a list). It is currently an experimental\n"
"\t\t\t\tprototype and has been tested in the Firefox, Safari, and Chrome web "
"browsers. Other browsers are untested."
msgstr ""

#: plugins\soundExplorer\soundExplorerMessages.php:35
msgid "Sound Explorer On"
msgstr ""

#: plugins\soundExplorer\soundExplorerMessages.php:36
msgid "Sound Explorer Off"
msgstr ""

#: plugins\soundExplorer\soundExplorerMessages.php:37
msgid "Search Label"
msgstr ""

#: plugins\soundExplorer\soundExplorerMessages.php:38
msgid "Sound"
msgstr ""

#: plugins\soundExplorer\soundExplorerMessages.php:39
msgid "Status"
msgstr ""

#: plugins\soundExplorer\soundExplorerMessages.php:40
msgid "Enabled"
msgstr ""

#: plugins\soundExplorer\soundExplorerMessages.php:41
msgid "Disabled"
msgstr ""

#: plugins\soundExplorer\soundExplorerMessages.php:42
msgid "New search"
msgstr ""

#: plugins\soundExplorer\soundExplorerMessages.php:43
msgid "Delete"
msgstr ""

#: plugins\soundExplorer\soundExplorerMessages.php:44
msgid "Notes"
msgstr ""

#: plugins\soundExplorer\soundExplorerMessages.php:45
msgid "Matched searches for current list"
msgstr ""

#: plugins\soundExplorer\soundExplorerMessages.php:46
msgid "Sound Explorer successfully configured"
msgstr ""

#: plugins\soundExplorer\soundExplorerMessages.php:47
msgid "Store the Sound Explorer search"
msgstr ""

#: plugins\soundExplorer\soundExplorerMessages.php:48
msgid "Sound Explorer search successfully stored"
msgstr ""

#: plugins\soundExplorer\soundExplorerMessages.php:49
msgid "Sound Explorer search successfully deleted"
msgstr ""
