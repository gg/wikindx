��    +      t  ;   �      �  #   �     �  ,   �       !        @     G     L  	   a     k     {     �     �     �     �     �  0   �  -   �          *     <     Q     b     h     ~     �     �  	   �     �     �     �     �     �  #   �     �     �  &     %   3     Y     f     �     �  t  �  .        6  D   =     �  '   �     �     �     �  
   �     �     	      	     1	     6	     M	  	   V	  E   `	  -   �	     �	     �	     �	     
     -
     3
     M
     S
     Y
     b
     n
     r
     ~
     �
     �
  1   �
     �
  #   �
  )   �
  %   (     N  '   b     �     �           !   '            "   &          *                                                         )         (   	              +          #                                      
      %                 $    (This value is hidden for security) Action All user session variables have been deleted App. Config. Application Configuration (in DB) Backup Both Browse by category:  Constants Constants Debug Cookies Cookies Debug ($_COOKIE) Core Debug tools... Decoded Delete Delete all session variables of the current user Destroy the whole session of the current user Environment Environment Debug Extension Extensions Extensions Debug Field Format<br>Raw/Decoded Id Keys Loaded Mandatory No Optional Raw Requirements of WIKINDX Server Server superglobal Debug ($_SERVER) Session Session Debug ($_SESSION) The Session variable have been deleted The whole session have been destroyed User Config. User Configuration (in DB) Value Values Yes Project-Id-Version: debugtools
Report-Msgid-Bugs-To: sirfragalot@users.sourceforge.net
POT-Creation-Date: 2019-10-24 21:09+0200
PO-Revision-Date: 2019-09-07 15:22+0200
Last-Translator: Automatically generated
Language-Team: fr
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 (Cette valeur est masquée pour la sécurité) Action Toutes les variables de la session utilisateur ont été supprimées Config. app. Configuration de l'application (en BDD) Backup Les deux Navigation par catégorie :  Constantes Débogage des constantes Cookies Débogage des cookies ($_COOKIE) Core Outils de débogage... Décodé Supprimer Supprimer toutes les variables de la session de l'utilisateur courant Détruire la session de l'utilisateur courant Environnement Débogage de l'environnement Extension Extensions Débogage des extensions Champ Format<br>Brut / Décodé Réf. Clefs Chargée Obligatoire Non Facultative Brut Exigences de WIKINDX Serveur Débogage des superglobales du serveur ($_SERVER) Session Débogage de la session ($_SESSION) La variable de session a été supprimée La session entière a été détruite Config. utilisateur Configuration de l'utilisateur (en BDD) Valeur Valeurs Oui 