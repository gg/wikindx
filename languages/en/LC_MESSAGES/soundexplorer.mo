��          �      <      �     �     �     �  !   �  
   �     �     �     	               1  *   C  )   n  &   �    �     �     �  z            �     �  !   �  
   �     �     �     �     �     �     �  *     )   <  &   f    �     �     �                                 	         
                                                           Delete Disabled Enabled Matched searches for current list New search Notes Search Label Sound Sound Explorer Sound Explorer Off Sound Explorer On Sound Explorer search successfully deleted Sound Explorer search successfully stored Sound Explorer successfully configured Sound Explorer will store searches then, at any time in the future, play a sound if a resource matching
				a past search is found in a current list operation. This works on the principle of a serendipitous conjunction of past searches
				with a current search result, hopefully allowing you to make new creative connections by prodding at the boundaries of your
				current conceptual space.  But it is also useful in a multi-user
				WIKINDX where it functions like a 'tripwire' signal -- the sound will play if a new resource or metadata has been added by another
				user that matches a stored search (if the resource is being displayed in a list). It is currently an experimental
				prototype and has been tested in the Firefox, Safari, and Chrome web browsers. Other browsers are untested. Status Store the Sound Explorer search Project-Id-Version: soundexplorer
Report-Msgid-Bugs-To: sirfragalot@users.sourceforge.net
POT-Creation-Date: 2019-10-24 21:09+0200
PO-Revision-Date: 2019-10-24 21:09+0200
Last-Translator: Automatically generated
Language-Team: none
Language: en
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Delete Disabled Enabled Matched searches for current list New search Notes Search Label Sound Sound Explorer Sound Explorer Off Sound Explorer On Sound Explorer search successfully deleted Sound Explorer search successfully stored Sound Explorer successfully configured Sound Explorer will store searches then, at any time in the future, play a sound if a resource matching
				a past search is found in a current list operation. This works on the principle of a serendipitous conjunction of past searches
				with a current search result, hopefully allowing you to make new creative connections by prodding at the boundaries of your
				current conceptual space.  But it is also useful in a multi-user
				WIKINDX where it functions like a 'tripwire' signal -- the sound will play if a new resource or metadata has been added by another
				user that matches a stored search (if the resource is being displayed in a list). It is currently an experimental
				prototype and has been tested in the Firefox, Safari, and Chrome web browsers. Other browsers are untested. Status Store the Sound Explorer search 