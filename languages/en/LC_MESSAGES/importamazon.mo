��          �      �       H     I  4   W  "   �  )   �  0   �  G   
  N   R     �     �     �     �  B        D  y  Q     �  4   �  "     )   1  0   [  G   �  N   �     #     1     M     h  B   �     �                                                    	   
          Amazon Import Enter the Amazon URL for the item you wish to import Import failed. Amazon reports: ### Invalid Amazon URL (unable to strip ISBN) Invalid Amazon URL (unable to strip domain name) Missing Amazon access key.  Please read modules/importAmazon/README.txt Missing Amazon secret access key.  Please read modules/importAmazon/README.txt Missing input Successfully added resource That title already exists. The resource is not a book You can only import from the British Amazon website (amazon.co.uk) https://.... Project-Id-Version: importamazon
Report-Msgid-Bugs-To: sirfragalot@users.sourceforge.net
POT-Creation-Date: 2019-10-24 21:09+0200
PO-Revision-Date: 2019-10-24 21:09+0200
Last-Translator: Automatically generated
Language-Team: none
Language: en
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Amazon Import Enter the Amazon URL for the item you wish to import Import failed. Amazon reports: ### Invalid Amazon URL (unable to strip ISBN) Invalid Amazon URL (unable to strip domain name) Missing Amazon access key.  Please read modules/importAmazon/README.txt Missing Amazon secret access key.  Please read modules/importAmazon/README.txt Missing input Successfully added resource That title already exists. The resource is not a book You can only import from the British Amazon website (amazon.co.uk) https://.... 