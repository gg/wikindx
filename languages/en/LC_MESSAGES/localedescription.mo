��    
      l      �       �      �   �    M   �     �          /  n   @  $   �     �  ~  �     k  �    M   '     u     �     �  n   �  $   )     N                        	      
               Choose localization Here, you can set an alternate front page description for each localization language you have installed.
						 Alternate descriptions are stored in the config_ database as configDescription_xx where 'xx' is the localization
						 folder name. The original description, set in Admin|Configure, is displayed whenever the user changes to a localization
						 language for which there is no alternate front page description. If you empty the description and submit, you are removing the database field. Localize Front Page Localize Front Page Description Missing language Only English is available on this WIKINDX – set the front page description via the Admin|Configure interface Original Admin|Configure description Successfully edited ### Project-Id-Version: localedescription
Report-Msgid-Bugs-To: sirfragalot@users.sourceforge.net
POT-Creation-Date: 2019-10-24 21:09+0200
PO-Revision-Date: 2019-10-24 21:09+0200
Last-Translator: Automatically generated
Language-Team: none
Language: en
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Choose localization Here, you can set an alternate front page description for each localization language you have installed.
						 Alternate descriptions are stored in the config_ database as configDescription_xx where 'xx' is the localization
						 folder name. The original description, set in Admin|Configure, is displayed whenever the user changes to a localization
						 language for which there is no alternate front page description. If you empty the description and submit, you are removing the database field. Localize Front Page Localize Front Page Description Missing language Only English is available on this WIKINDX – set the front page description via the Admin|Configure interface Original Admin|Configure description Successfully edited ### 