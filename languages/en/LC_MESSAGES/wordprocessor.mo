��    O      �  k         �  �  �  	   �     �     �  	   �     �  	   �  (   �            5        Q     j     �     �     �     �     �     �     �     �     �     �  
     	                  0     E     Y  	   `     j     w  4   �     �     �     �     �     �  
     	     	        #  
   (  
   3     >     T  -   c     �     �     �     �     �  
   �     	  !        @     M     e     |     �  
   �     �     �     �     �     �  A   �  #   ?     c     r     �     �     �  U   �  R     #   [       z  �  �    	   �     �       	          	   %  (   /     X     \  5   o     �     �     �     �     �     �     �               ,     <     L  
   U  	   `     j     q     �     �     �  	   �     �     �  4   �           &      ;      @      L   
   X   	   c   	   m      w   
   |   
   �      �      �   -   �      �      �      !     2!     F!  
   R!     ]!  !   r!     �!     �!     �!     �!     �!  
   �!     �!     �!     �!     "     8"  A   Q"  #   �"     �"     �"     �"     �"     �"  U   #  R   \#  #   �#     �#     ?      N      B         -   %   L          K   '         *       .                            F          )   &                         3                     8      >   A   C              5   +   !   ;   @   D   6                        "   #   1               9   
       /      I   	      :   G       O   $   4   2                        (   H   E   <         0   =   ,      J   M   7            
<h3>Word Processor</h3>
<p>The WYSISWYG Word Processor allows you to write your articles and papers entirely within WIKINDX and then export them to an external word processor such as OpenOffice or Word for final polishing if necessary.  When exporting, you can choose the citation style and any citations you have inserted will be formatted to that citation style with appended bibliography.  The Word Processor can handle footnote and endnote styles as well as in-text citation that is context sensitive.</p>
<ul>
<li>If you have existing papers from a WIKINDX v3.x installation, you must first import them before they can be used in this plugin. With a new WIKINDX v4 install, the conversion process from WIKINDX v3.8.2 will keep the record of papers in the v4 database but the actual papers will still be in the wikindx3/papers/ folder. You can manually copy them or use the Import Paper function.</li>
<li>Papers can later be backed up to external sources and imported back into the word processor using the Import Paper function.</li>
<li>If you save an existing paper with a new title, the default behaviour is that previous versions with the old title will be deleted unless you check the 'save new version' checkbox.</li>
<li>The title of a paper can only comprise uppercase/lowercase a-z letters, arabic numerals, spaces and underscores.</li>
<li>The wikindx4/plugins/wordProcessor/papers/ folder must be writeable by the web user.</li>
<li>If you have Image Magick installed on your server, the Export Paper function will attempt to convert images for use in a word processor. The Image Magick bin/ path can be set in the Word Processor's config.php file.</li>
<li>A horizontal line inserted in the editor will become a new section when exported (to Rich Text Format, for example). Some of the WIKINDX citation styles will format elements according to sections.</li>
<li><strong>Be careful</strong>: there is currently no autosave and no warning if you leave the editor without first saving.</li>
</ul>
		 1.5 lines Add Citation Add Footnote Add Image Add Link Add Table Add section break before appended paper: All All but first line Are you sure you wish to revert to the saved version? Bibliography indentation Bibliography line space Centre Columns Delete Paper Delete paper Delete paper(s):  Double Export & Save Paper Export and Save Export paper to Filename First line Font size Footer Footnote font size Footnote indentation Footnote line space Header Image URL Import Paper Import paper Indent quotations with at least this number of words Insert only the cite tag Keep quotation marks Left List Papers List papers Load paper New Paper New paper None Open Paper Open paper Page number alignment Page numbering Paper has now been loaded ready for appending Paper line space Paper not saved: invalid title Paper not saved: write error Paper not yet saved Paper saved Paper size Quotation line space Restart footnotes at each section Restore menu Revert to saved version Rich Text Format (RTF) Right Rows Save Paper Save new version Single Successfully deleted paper(s) Successfully imported paper Successfully saved paper The paper you are trying to import already exists in this wikindx There is no autosave or save prompt Title of Paper Try to use the SUWP anyway URL Word Processor Word Processor... You can download and backup papers if you wish. The newest files are displayed first. You can import a paper here which should be a plain file as backed up from WIKINDX You have already deleted that paper You have no papers Project-Id-Version: wordprocessor
Report-Msgid-Bugs-To: sirfragalot@users.sourceforge.net
POT-Creation-Date: 2019-10-24 21:09+0200
PO-Revision-Date: 2019-10-24 21:09+0200
Last-Translator: Automatically generated
Language-Team: none
Language: en
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 
<h3>Word Processor</h3>
<p>The WYSISWYG Word Processor allows you to write your articles and papers entirely within WIKINDX and then export them to an external word processor such as OpenOffice or Word for final polishing if necessary.  When exporting, you can choose the citation style and any citations you have inserted will be formatted to that citation style with appended bibliography.  The Word Processor can handle footnote and endnote styles as well as in-text citation that is context sensitive.</p>
<ul>
<li>If you have existing papers from a WIKINDX v3.x installation, you must first import them before they can be used in this plugin. With a new WIKINDX v4 install, the conversion process from WIKINDX v3.8.2 will keep the record of papers in the v4 database but the actual papers will still be in the wikindx3/papers/ folder. You can manually copy them or use the Import Paper function.</li>
<li>Papers can later be backed up to external sources and imported back into the word processor using the Import Paper function.</li>
<li>If you save an existing paper with a new title, the default behaviour is that previous versions with the old title will be deleted unless you check the 'save new version' checkbox.</li>
<li>The title of a paper can only comprise uppercase/lowercase a-z letters, arabic numerals, spaces and underscores.</li>
<li>The wikindx4/plugins/wordProcessor/papers/ folder must be writeable by the web user.</li>
<li>If you have Image Magick installed on your server, the Export Paper function will attempt to convert images for use in a word processor. The Image Magick bin/ path can be set in the Word Processor's config.php file.</li>
<li>A horizontal line inserted in the editor will become a new section when exported (to Rich Text Format, for example). Some of the WIKINDX citation styles will format elements according to sections.</li>
<li><strong>Be careful</strong>: there is currently no autosave and no warning if you leave the editor without first saving.</li>
</ul>
		 1.5 lines Add Citation Add Footnote Add Image Add Link Add Table Add section break before appended paper: All All but first line Are you sure you wish to revert to the saved version? Bibliography indentation Bibliography line space Centre Columns Delete Paper Delete paper Delete paper(s):  Double Export & Save Paper Export and Save Export paper to Filename First line Font size Footer Footnote font size Footnote indentation Footnote line space Header Image URL Import Paper Import paper Indent quotations with at least this number of words Insert only the cite tag Keep quotation marks Left List Papers List papers Load paper New Paper New paper None Open Paper Open paper Page number alignment Page numbering Paper has now been loaded ready for appending Paper line space Paper not saved: invalid title Paper not saved: write error Paper not yet saved Paper saved Paper size Quotation line space Restart footnotes at each section Restore menu Revert to saved version Rich Text Format (RTF) Right Rows Save Paper Save new version Single Successfully deleted paper(s) Successfully imported paper Successfully saved paper The paper you are trying to import already exists in this wikindx There is no autosave or save prompt Title of Paper Try to use the SUWP anyway URL Word Processor Word Processor... You can download and backup papers if you wish. The newest files are displayed first. You can import a paper here which should be a plain file as backed up from WIKINDX You have already deleted that paper You have no papers 