# Wikindx's English Translation ressource.
# Copyright (C) 2019, Mark Grimshaw-Aagaard <sirfragalot@users.sourceforge.net>.
# This file is distributed under the same license as the importexportbib package.
#
# ### [Translators]##############################################################
# Mark Grimshaw-Aagaard <sirfragalot@users.sourceforge.net>, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: importexportbib\n"
"Report-Msgid-Bugs-To: sirfragalot@users.sourceforge.net\n"
"POT-Creation-Date: 2019-10-24 21:09+0200\n"
"PO-Revision-Date: 2019-10-24 21:09+0200\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: en\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: plugins\importexportbib\importexportbibMessages.php:20
msgid "Import & Export..."
msgstr "Import & Export..."

#: plugins\importexportbib\importexportbibMessages.php:21
msgid "Import Endnote"
msgstr "Import Endnote"

#: plugins\importexportbib\importexportbibMessages.php:22
msgid "Import Endnote XML Bibliography"
msgstr "Import Endnote XML Bibliography"

#: plugins\importexportbib\importexportbibMessages.php:23
#: plugins\importexportbib\importexportbibMessages.php:24
msgid "Import PubMed"
msgstr "Import PubMed"

#: plugins\importexportbib\importexportbibMessages.php:25
msgid "Bibutils"
msgstr "Bibutils"

#: plugins\importexportbib\importexportbibMessages.php:26
msgid "Exp. RTF..."
msgstr "Exp. RTF..."

#: plugins\importexportbib\importexportbibMessages.php:27
msgid "Exp. BibTeX..."
msgstr "Exp. BibTeX..."

#: plugins\importexportbib\importexportbibMessages.php:28
msgid "Exp. HTML..."
msgstr "Exp. HTML..."

#: plugins\importexportbib\importexportbibMessages.php:29
msgid "Exp. RIS..."
msgstr "Exp. RIS..."

#: plugins\importexportbib\importexportbibMessages.php:30
msgid "Exp. Endnote..."
msgstr "Exp. Endnote..."

#: plugins\importexportbib\importexportbibMessages.php:31
msgid "Basket"
msgstr "Basket"

#: plugins\importexportbib\importexportbibMessages.php:32
msgid "Last List"
msgstr "Last List"

#: plugins\importexportbib\importexportbibMessages.php:33
#: plugins\importexportbib\importexportbibMessages.php:41
msgid "Exported Files"
msgstr "Exported Files"

#: plugins\importexportbib\importexportbibMessages.php:34
#: plugins\importexportbib\importexportbibMessages.php:40
msgid "Export Ideas"
msgstr "Export Ideas"

#: plugins\importexportbib\importexportbibMessages.php:35
msgid "Export Rich Text Format"
msgstr "Export Rich Text Format"

#: plugins\importexportbib\importexportbibMessages.php:36
msgid "Export BibTeX"
msgstr "Export BibTeX"

#: plugins\importexportbib\importexportbibMessages.php:37
msgid "Export HTML"
msgstr "Export HTML"

#: plugins\importexportbib\importexportbibMessages.php:38
msgid "Export RIS"
msgstr "Export RIS"

#: plugins\importexportbib\importexportbibMessages.php:39
msgid "Export Endnote"
msgstr "Export Endnote"

#: plugins\importexportbib\importexportbibMessages.php:42
msgid "Bibutils Conversion"
msgstr "Bibutils Conversion"

#: plugins\importexportbib\importexportbibMessages.php:43
msgid "Input Type"
msgstr "Input Type"

#: plugins\importexportbib\importexportbibMessages.php:44
msgid "Output Type"
msgstr "Output Type"

#: plugins\importexportbib\importexportbibMessages.php:45
msgid "Input File"
msgstr "Input File"

#: plugins\importexportbib\importexportbibMessages.php:46
#: plugins\importexportbib\importexportbibMessages.php:164
msgid "Output File"
msgstr "Output File"

#: plugins\importexportbib\importexportbibMessages.php:47
msgid ""
"The Bibutils plugin uses C binaries written by Chris Putnam at ###. Most "
"conversions use the MODS XML\n"
"\t\t\tintermediate format."
msgstr ""
"The Bibutils plugin uses C binaries written by Chris Putnam at ###. Most "
"conversions use the MODS XML\n"
"\t\t\tintermediate format."

#: plugins\importexportbib\importexportbibMessages.php:49
msgid "No Bibutils programs found in ###"
msgstr "No Bibutils programs found in ###"

#: plugins\importexportbib\importexportbibMessages.php:50
msgid "No input type specified"
msgstr "No input type specified"

#: plugins\importexportbib\importexportbibMessages.php:51
msgid "No output type specified"
msgstr "No output type specified"

#: plugins\importexportbib\importexportbibMessages.php:52
msgid "Missing input file"
msgstr "Missing input file"

#: plugins\importexportbib\importexportbibMessages.php:53
#: plugins\importexportbib\importexportbibMessages.php:166
msgid "FAILED to convert (###)"
msgstr "FAILED to convert (###)"

#: plugins\importexportbib\importexportbibMessages.php:54
msgid "Conversion options for intermediate MODS XML"
msgstr "Conversion options for intermediate MODS XML"

#: plugins\importexportbib\importexportbibMessages.php:55
msgid "IGNORE"
msgstr "IGNORE"

#: plugins\importexportbib\importexportbibMessages.php:56
msgid ""
"Encode unicode characters directly in the file rather than as XML entities"
msgstr ""
"Encode unicode characters directly in the file rather than as XML entities"

#: plugins\importexportbib\importexportbibMessages.php:57
msgid "Don't put citation key in the MODS id field"
msgstr "Don't put citation key in the MODS id field"

#: plugins\importexportbib\importexportbibMessages.php:58
msgid "Don't split titles into TITLE/SUBTITLE pairs"
msgstr "Don't split titles into TITLE/SUBTITLE pairs"

#: plugins\importexportbib\importexportbibMessages.php:59
msgid "Do not covert latex character combinations"
msgstr "Do not covert latex character combinations"

#: plugins\importexportbib\importexportbibMessages.php:60
msgid "Unicode in, unicode out"
msgstr "Unicode in, unicode out"

#: plugins\importexportbib\importexportbibMessages.php:61
#: plugins\importexportbib\importexportbibMessages.php:163
msgid "Successfully converted ###"
msgstr "Successfully converted ###"

#: plugins\importexportbib\importexportbibMessages.php:62
msgid ""
"You may import Endnote XML bibliographies (.xml files) here. Large files may "
"take some time so if\n"
"\t\t\t\tWIKINDX senses that php.ini's 'max_execution_time' variable is about "
"to be exceeded, it will start importing the bibliography in chunks.\n"
"\t\t\t\tIf there is a date field, the format should be either dd/mm/yyyy or "
"yyyy/mm/dd and yyyy will override any year field in the record.\n"
"\t\t\t\tIf you have custom fields in your import file, create custom fields "
"first in the WIKINDX database (the Admin menu) so that you can then map\n"
"\t\t\t\tthe import custom fields."
msgstr ""
"You may import Endnote XML bibliographies (.xml files) here. Large files may "
"take some time so if\n"
"\t\t\t\tWIKINDX senses that php.ini's 'max_execution_time' variable is about "
"to be exceeded, it will start importing the bibliography in chunks.\n"
"\t\t\t\tIf there is a date field, the format should be either dd/mm/yyyy or "
"yyyy/mm/dd and yyyy will override any year field in the record.\n"
"\t\t\t\tIf you have custom fields in your import file, create custom fields "
"first in the WIKINDX database (the Admin menu) so that you can then map\n"
"\t\t\t\tthe import custom fields."

#: plugins\importexportbib\importexportbibMessages.php:67
msgid ""
"You may export to Rich Text Format your most recent bibliography list. Large "
"numbers of resources might\n"
"\t\t\t\ttake some time and memory so you might need to adjust php.ini."
msgstr ""
"You may export to Rich Text Format your most recent bibliography list. Large "
"numbers of resources might\n"
"\t\t\t\ttake some time and memory so you might need to adjust php.ini."

#: plugins\importexportbib\importexportbibMessages.php:69
msgid "Category"
msgstr "Category"

#: plugins\importexportbib\importexportbibMessages.php:70
msgid ""
"All WIKINDX resources belong to at least one category which you chose here.  "
"The category(s) a resource belongs to can always be edited later."
msgstr ""
"All WIKINDX resources belong to at least one category which you chose here.  "
"The category(s) a resource belongs to can always be edited later."

#. importDuplicates For file imports, allow duplicates?
#: plugins\importexportbib\importexportbibMessages.php:72
msgid "Import duplicates:"
msgstr "Import duplicates:"

#: plugins\importexportbib\importexportbibMessages.php:73
msgid "Ignore keywords:"
msgstr "Ignore keywords:"

#: plugins\importexportbib\importexportbibMessages.php:74
msgid ""
"You may store Endnote fields that WIKINDX does not use so that any resources "
"later exported to Endnote can include this original unchanged data. Store "
"unused fields:"
msgstr ""
"You may store Endnote fields that WIKINDX does not use so that any resources "
"later exported to Endnote can include this original unchanged data. Store "
"unused fields:"

#: plugins\importexportbib\importexportbibMessages.php:75
msgid "File is empty"
msgstr "File is empty"

#: plugins\importexportbib\importexportbibMessages.php:76
msgid "No. resources added: ###"
msgstr "No. resources added: ###"

#: plugins\importexportbib\importexportbibMessages.php:77
msgid "No. resources discarded (duplicates or no titles): ###"
msgstr "No. resources discarded (duplicates or no titles): ###"

#. invalidField1 If non-standard import fields are found in the input file, invite the user to map these fields to wikindx fields
#: plugins\importexportbib\importexportbibMessages.php:79
msgid ""
"Unknown fields have been found. You may map these fields to WIKINDX fields -- "
"no duplicate mapping is allowed."
msgstr ""
"Unknown fields have been found. You may map these fields to WIKINDX fields -- "
"no duplicate mapping is allowed."

#: plugins\importexportbib\importexportbibMessages.php:80
msgid ""
"Where an unknown field is mapped to a WIKINDX field that would normally be "
"automatically mapped to a standard input field, the unknown field mapping "
"takes precedence."
msgstr ""
"Where an unknown field is mapped to a WIKINDX field that would normally be "
"automatically mapped to a standard input field, the unknown field mapping "
"takes precedence."

#: plugins\importexportbib\importexportbibMessages.php:81
msgid "Import File"
msgstr "Import File"

#: plugins\importexportbib\importexportbibMessages.php:82
msgid "Tag this import so you can do a mass select or delete later"
msgstr "Tag this import so you can do a mass select or delete later"

#. executionTimeExceeded With large imports that would go over php.ini's max_execution time, WIKINDX splits the imports into chunks
#: plugins\importexportbib\importexportbibMessages.php:84
msgid ""
"'max_execution_time' (### seconds) in php.ini was about to be exceeded.  "
"WIKINDX is importing the bibliography in chunks."
msgstr ""
"'max_execution_time' (### seconds) in php.ini was about to be exceeded.  "
"WIKINDX is importing the bibliography in chunks."

#: plugins\importexportbib\importexportbibMessages.php:85
msgid "No. resources added this chunk: ###"
msgstr "No. resources added this chunk: ###"

#: plugins\importexportbib\importexportbibMessages.php:86
msgid "You have already imported that file"
msgstr "You have already imported that file"

#: plugins\importexportbib\importexportbibMessages.php:87
msgid "Import bibliography is empty"
msgstr "Import bibliography is empty"

#: plugins\importexportbib\importexportbibMessages.php:88
msgid "File upload error"
msgstr "File upload error"

#: plugins\importexportbib\importexportbibMessages.php:89
msgid "You must create a list from the Search menu first"
msgstr "You must create a list from the Search menu first"

#: plugins\importexportbib\importexportbibMessages.php:90
msgid "You have no ideas"
msgstr "You have no ideas"

#: plugins\importexportbib\importexportbibMessages.php:91
msgid "Export all ideas"
msgstr "Export all ideas"

#: plugins\importexportbib\importexportbibMessages.php:92
msgid "Export selected ideas"
msgstr "Export selected ideas"

#: plugins\importexportbib\importexportbibMessages.php:93
msgid "Data successfully exported"
msgstr "Data successfully exported"

#: plugins\importexportbib\importexportbibMessages.php:94
msgid "Data successfully imported"
msgstr "Data successfully imported"

#: plugins\importexportbib\importexportbibMessages.php:95
msgid "Contents of your temporary folder (newest first):"
msgstr "Contents of your temporary folder (newest first):"

#: plugins\importexportbib\importexportbibMessages.php:96
msgid "Directory is empty"
msgstr "Directory is empty"

#: plugins\importexportbib\importexportbibMessages.php:97
msgid ""
"These files will be available for ### minutes or while you keep your browser "
"open (whichever is the shorter) so download and save them elsewhere "
"immediately"
msgstr ""
"These files will be available for ### minutes or while you keep your browser "
"open (whichever is the shorter) so download and save them elsewhere "
"immediately"

#: plugins\importexportbib\importexportbibMessages.php:98
msgid "Bibliography exports:"
msgstr "Bibliography exports:"

#: plugins\importexportbib\importexportbibMessages.php:99
msgid "Export bibliographic entries:"
msgstr "Export bibliographic entries:"

#: plugins\importexportbib\importexportbibMessages.php:100
msgid "Font size"
msgstr "Font size"

#. Font type e.g. 'Times New Roman', 'Courier'
#: plugins\importexportbib\importexportbibMessages.php:102
msgid "Font"
msgstr "Font"

#: plugins\importexportbib\importexportbibMessages.php:103
msgid "Export abstract:"
msgstr "Export abstract:"

#: plugins\importexportbib\importexportbibMessages.php:104
msgid "Export notes:"
msgstr "Export notes:"

#: plugins\importexportbib\importexportbibMessages.php:105
msgid "Export quotes:"
msgstr "Export quotes:"

#: plugins\importexportbib\importexportbibMessages.php:106
msgid "Export paraphrases:"
msgstr "Export paraphrases:"

#: plugins\importexportbib\importexportbibMessages.php:107
msgid "Export musings:"
msgstr "Export musings:"

#: plugins\importexportbib\importexportbibMessages.php:108
msgid "Export comments:"
msgstr "Export comments:"

#. Number of indents (tabulation - TAB)
#: plugins\importexportbib\importexportbibMessages.php:110
msgid "Left indent"
msgstr "Left indent"

#: plugins\importexportbib\importexportbibMessages.php:111
msgid "Right indent"
msgstr "Right indent"

#. 'CR' = carriage return (newlines)
#: plugins\importexportbib\importexportbibMessages.php:113
msgid "CR following"
msgstr "CR following"

#: plugins\importexportbib\importexportbibMessages.php:114
msgid "CR between"
msgstr "CR between"

#. bold, italics, underline
#: plugins\importexportbib\importexportbibMessages.php:116
msgid "Text format"
msgstr "Text format"

#. 'tag' = label given to each section (abstract, quotes, quote comments etc.)
#: plugins\importexportbib\importexportbibMessages.php:118
msgid "Tag"
msgstr "Tag"

#. Some text or characters to visually divide resources in the RT output
#: plugins\importexportbib\importexportbibMessages.php:120
msgid "Divider between entries"
msgstr "Divider between entries"

#: plugins\importexportbib\importexportbibMessages.php:121
msgid "CR after entries"
msgstr "CR after entries"

#: plugins\importexportbib\importexportbibMessages.php:122
msgid "Include ID no. (ISBN etc.)"
msgstr "Include ID no. (ISBN etc.)"

#. Metadata export options
#: plugins\importexportbib\importexportbibMessages.php:124
msgid "If exporting quote and paraphrase comments or musings"
msgstr "If exporting quote and paraphrase comments or musings"

#. These two are in a select box and follow on from the string above
#: plugins\importexportbib\importexportbibMessages.php:126
msgid "Export only mine"
msgstr "Export only mine"

#: plugins\importexportbib\importexportbibMessages.php:127
msgid "Export my data and all public data"
msgstr "Export my data and all public data"

#: plugins\importexportbib\importexportbibMessages.php:128
msgid "Add resource's primary creator and publication year to metadata"
msgstr "Add resource's primary creator and publication year to metadata"

#. Default text for labelling metadata in the RTF export
#: plugins\importexportbib\importexportbibMessages.php:130
msgid "QUOTES:"
msgstr "QUOTES:"

#: plugins\importexportbib\importexportbibMessages.php:131
msgid "PARAPHRASES:"
msgstr "PARAPHRASES:"

#: plugins\importexportbib\importexportbibMessages.php:132
#: plugins\importexportbib\importexportbibMessages.php:133
msgid "COMMENTS:"
msgstr "COMMENTS:"

#: plugins\importexportbib\importexportbibMessages.php:134
msgid "MUSINGS:"
msgstr "MUSINGS:"

#: plugins\importexportbib\importexportbibMessages.php:135
msgid "ABSTRACT:"
msgstr "ABSTRACT:"

#: plugins\importexportbib\importexportbibMessages.php:136
msgid "NOTES:"
msgstr "NOTES:"

#: plugins\importexportbib\importexportbibMessages.php:137
msgid "Include resource keywords"
msgstr "Include resource keywords"

#: plugins\importexportbib\importexportbibMessages.php:138
msgid "Include a hyperlink to the resource in this WIKINDX:"
msgstr "Include a hyperlink to the resource in this WIKINDX:"

#: plugins\importexportbib\importexportbibMessages.php:139
msgid "Endnote file type"
msgstr "Endnote file type"

#: plugins\importexportbib\importexportbibMessages.php:140
msgid "Endnote XML"
msgstr "Endnote XML"

#: plugins\importexportbib\importexportbibMessages.php:141
msgid "Endnote tabbed"
msgstr "Endnote tabbed"

#: plugins\importexportbib\importexportbibMessages.php:142
msgid ""
"Search PubMed: please enter one or more fields.  Multiple items within a "
"field\n"
"\t\t\t\tshould be separated by a space.  Per NCBI policy, a maximum of 100 "
"search results can be obtained."
msgstr ""
"Search PubMed: please enter one or more fields.  Multiple items within a "
"field\n"
"\t\t\t\tshould be separated by a space.  Per NCBI policy, a maximum of 100 "
"search results can be obtained."

#: plugins\importexportbib\importexportbibMessages.php:144
msgid "PubMed ID"
msgstr "PubMed ID"

#: plugins\importexportbib\importexportbibMessages.php:145
msgid "All Fields"
msgstr "All Fields"

#: plugins\importexportbib\importexportbibMessages.php:146
msgid "Author"
msgstr "Author"

#: plugins\importexportbib\importexportbibMessages.php:147
msgid "First Author"
msgstr "First Author"

#: plugins\importexportbib\importexportbibMessages.php:148
msgid "Last Author"
msgstr "Last Author"

#: plugins\importexportbib\importexportbibMessages.php:149
msgid "Title"
msgstr "Title"

#: plugins\importexportbib\importexportbibMessages.php:150
msgid "Abstract"
msgstr "Abstract"

#: plugins\importexportbib\importexportbibMessages.php:151
msgid "Year"
msgstr "Year"

#: plugins\importexportbib\importexportbibMessages.php:152
msgid "Journal"
msgstr "Journal"

#: plugins\importexportbib\importexportbibMessages.php:153
msgid "Volume"
msgstr "Volume"

#: plugins\importexportbib\importexportbibMessages.php:154
msgid "Issue"
msgstr "Issue"

#: plugins\importexportbib\importexportbibMessages.php:155
msgid "Language"
msgstr "Language"

#: plugins\importexportbib\importexportbibMessages.php:156
msgid "Limit search to the last n days"
msgstr "Limit search to the last n days"

#: plugins\importexportbib\importexportbibMessages.php:157
msgid "Max. Results"
msgstr "Max. Results"

#: plugins\importexportbib\importexportbibMessages.php:158
msgid "Import the PubMed import directly into WIKINDX"
msgstr "Import the PubMed import directly into WIKINDX"

#: plugins\importexportbib\importexportbibMessages.php:159
msgid "Please enter an integer up to 100 for Max. Results"
msgstr "Please enter an integer up to 100 for Max. Results"

#: plugins\importexportbib\importexportbibMessages.php:160
msgid "Please enter one or more fields"
msgstr "Please enter one or more fields"

#: plugins\importexportbib\importexportbibMessages.php:161
#: plugins\importexportbib\importexportbibMessages.php:162
msgid "result"
msgid_plural "results"
msgstr[0] "result"
msgstr[1] "results"

#: plugins\importexportbib\importexportbibMessages.php:165
msgid "No Results Found"
msgstr "No Results Found"

#: plugins\importexportbib\importexportbibMessages.php:167
msgid ""
"XML file is not compatible with Endnote v8 which is required for this import "
"script."
msgstr ""
"XML file is not compatible with Endnote v8 which is required for this import "
"script."

#: plugins\importexportbib\importexportbibMessages.php:168
msgid "Successfully imported Endnote file."
msgstr "Successfully imported Endnote file."
