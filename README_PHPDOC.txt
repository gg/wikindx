                         --o README_PHPDOC o--

 ---:::---:::---:::---:::---:::---:::---:::---:::---:::---:::---:::---

To compile the WIKINDX core/ programming manual, run phpDocumentor 3 inside wikindx/trunk/

Using the command:

   phpdoc

at the command line will automatically make use of the config file wikindx/trunk/phpdoc.xml

Documentation is saved to wikindx/trunk/docs/manual/


phpdoc is often capricious depending on the PHP environment installed. For a better experience
it is recommended to use the phar version of phpDocumentor 3.


Download it from the assets section of the last release of phpdoc at:

   https://github.com/phpDocumentor/phpDocumentor2/releases

Put the two files phpDocumentor.phar and phpDocumentor.phar.pubkey in the root directory of the site and run:

   php phpDocumentor.phar -c phpdoc.xml

The -c option is mandatory since a bug of phpdoc 3 prevents the implicit read of the config file.

 ---:::---:::---:::---:::---:::---:::---:::---:::---:::---:::---:::---

-- 
Mark Grimshaw-Aagaard
The WIKINDX Team 2019
sirfragalot@users.sourceforge.net