<?php
/*
Copyright Stéphane Aulery, 2019

<lkppo@users.sourceforge.net>

Ce logiciel est un programme informatique servant à préparer le code de
wikindx pour sa publication officiel.

Ce logiciel est régi par la licence CeCILL-B soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-B telle que diffusée par le CEA, le CNRS et l'INRIA
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme, le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard l'attention de l'utilisateur est attirée sur les risques
associés au chargement, à l'utilisation, à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant
donné sa spécificité de logiciel libre, qui peut le rendre complexe à
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant des connaissances informatiques approfondies. Les
utilisateurs sont donc invités à charger et tester l'adéquation du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
pris connaissance de la licence CeCILL-B, et que vous en avez accepté les
termes.
*/

include_once("core/startup/CONSTANTS.php");
include_once("core/file/FILE.php");

///////////////////////////////////////////////////////////////////////
/// Configuration
///////////////////////////////////////////////////////////////////////

$path_listlocales = implode(DIRECTORY_SEPARATOR, [__DIR__, "languages", "languages_localized.json"]);
$listlocales = json_decode(file_get_contents($path_listlocales), TRUE);

$dirroot = __DIR__;
$dirplugins = implode(DIRECTORY_SEPARATOR, [__DIR__, "plugins"]);
$dirsrc = implode(DIRECTORY_SEPARATOR, [__DIR__, "languages", "src"]);
$dirpo = implode(DIRECTORY_SEPARATOR, [__DIR__, "languages"]);
$execoutput = array();
$errorcode = 0;
$emailreport = "sirfragalot@users.sourceforge.net";
$excludedir = array(
    $dirplugins, // Plugins are treated as their own domain
    implode(DIRECTORY_SEPARATOR, [__DIR__, "attachments"]), // Data directory
    implode(DIRECTORY_SEPARATOR, [__DIR__, "attachments_cache"]), // Cache directory
    implode(DIRECTORY_SEPARATOR, [__DIR__, "dbschema"]), // Data directory
    implode(DIRECTORY_SEPARATOR, [__DIR__, "docs"]), // Data directory
    implode(DIRECTORY_SEPARATOR, [__DIR__, "files"]), // Data directory
    implode(DIRECTORY_SEPARATOR, [__DIR__, "images"]), // Data directory
    implode(DIRECTORY_SEPARATOR, [__DIR__, "styles"]), // Data directory
    implode(DIRECTORY_SEPARATOR, [__DIR__, "templates"]), // Data directory
    implode(DIRECTORY_SEPARATOR, [__DIR__, "tplcompilation"]), // Cache directory
    implode(DIRECTORY_SEPARATOR, [__DIR__, "core", "display", "Smarty"]), // Third party lib
    implode(DIRECTORY_SEPARATOR, [__DIR__, "core", "modules", "list", "PdfToText"]), // Third party lib
    implode(DIRECTORY_SEPARATOR, [__DIR__, "core", "smtp", "PHPMailer"]), // Third party lib
    implode(DIRECTORY_SEPARATOR, [__DIR__, "plugins", "visualize", "jpgraph"]), // Third party lib
    implode(DIRECTORY_SEPARATOR, [__DIR__, "plugins", "dbAdminer", "plugins"]), // Third party lib
);


///////////////////////////////////////////////////////////////////////
/// MAIN
///////////////////////////////////////////////////////////////////////


echo "Create missing locales folders\n";

if (!file_exists($dirsrc)) mkdir($dirsrc, 0777, TRUE);
if (!file_exists($dirpo)) mkdir($dirpo, 0777, TRUE);

foreach ($listlocales as $locale => $localeName)
{
    $dir = $dirpo . DIRECTORY_SEPARATOR . $locale . DIRECTORY_SEPARATOR . "LC_MESSAGES";
    if (!file_exists($dir))
    {
        echo " - MKDIR $dir\n";
        mkdir($dir, 0777, TRUE);
    }
}


echo "\n";
echo "Updating plugin tranlations\n";

$listDirDomain = array(
    $dirroot => array(WIKINDX_LANGUAGE_DOMAIN_DEFAULT),
    $dirplugins => FILE\dirInDirToArray($dirplugins),
);

foreach ($listDirDomain as $dir => $DirDomain)
{
    foreach ($DirDomain as $domain)
    {
        if ($dir == __DIR__)
        {
            echo " - Core $domain domain\n";
            $packagename = strtolower($domain);
            $inputdir = $dir;
        }
        else
        {
            echo " - Plugin $domain domain\n";
            $packagename = strtolower($domain);
            $inputdir = $dirplugins . DIRECTORY_SEPARATOR . $domain;
        }
        
        $phpfilelist = $dirsrc . DIRECTORY_SEPARATOR . $packagename . ".lst";
        $potfile = $dirsrc . DIRECTORY_SEPARATOR . $packagename . ".pot";
        
        echo "   - List all PHP files to $phpfilelist\n";
        
        if ($dir == __DIR__)
            saveListPHPfilesInDirectory($phpfilelist, $inputdir, $excludedir);
        else
            saveListPHPfilesInDirectory($phpfilelist, $inputdir);
            
        // Create missing templates for each domain
        echo "   - Extract all translatable strings in file " . $potfile . "\n";
        $potfile = $dirsrc . DIRECTORY_SEPARATOR . $packagename . ".pot";
        exec("xgettext -L PHP --from-code=UTF-8 -c -n -w 80 --sort-by-file --msgid-bugs-address=$emailreport --package-name=$packagename -o $potfile -f $phpfilelist");
        
        // Cleaning
        unlink($phpfilelist);
        
        // countinue only for plugins with translatable strings
        if (file_exists($potfile))
        {
            $potcontent = file_get_contents($potfile);
            $potcontent = str_replace("# SOME DESCRIPTIVE TITLE.", "# Wikindx's ### Translation ressource.", $potcontent);
            $potcontent = str_replace("# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER", "# Copyright (C) " . str_replace("-2019", "", "2019-" . date("Y")) . ", Mark Grimshaw-Aagaard <sirfragalot@users.sourceforge.net>.", $potcontent);
            $potcontent = str_replace("# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.", "#\n# ### [Translators]##############################################################\n# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.", $potcontent);
            $potcontent = str_replace("Content-Type: text/plain; charset=CHARSET", "Content-Type: text/plain; charset=UTF-8", $potcontent);
            $potcontent = str_replace(__DIR__ . DIRECTORY_SEPARATOR, "", $potcontent);
            file_put_contents($potfile, $potcontent);
            
            echo "   - Merge and compile translations:\n";
            foreach (FILE\dirInDirToArray($dirpo) as $locale)
            {
                // Skip folders that do not contain po files
                if ($locale == "src")
                    continue;
                
                echo "     - " . $locale . " : ";
                
                $basefile = $dirpo . DIRECTORY_SEPARATOR . $locale . DIRECTORY_SEPARATOR . "LC_MESSAGES" . DIRECTORY_SEPARATOR . $packagename;
                $tmpfile = $basefile . ".po.tmp";
                $pofile = $basefile . ".po";
                $mofile = $basefile . ".mo";
                
                // Delete all english translations. This forces gettext to regenerate an English catalog
                // whose translation does not need to be done without passing through fuzzy strings.
                if ($locale == WIKINDX_LANGUAGE_DEFAULT)
                {
                    if (file_exists($tmpfile)) unlink($tmpfile);
                    if (file_exists($pofile)) unlink($pofile);
                    if (file_exists($mofile)) unlink($mofile);
                }
                
                if (!file_exists($pofile))
                {
                    // Create a translation file
                    exec("msginit --no-translator --locale=$locale.UTF-8 -i $potfile -o $pofile", $execoutput, $errorcode);
                    abortOnError($errorcode);
                }
                
                if (file_exists($pofile))
                {
                    // Merge all translatable string changes with previous translations
                    exec("msgmerge -q --previous -w 80 --sort-by-file --lang=$locale -o $tmpfile $pofile $potfile", $execoutput, $errorcode);
                    abortOnError($errorcode, $errorcode);
                    copy($tmpfile, $pofile);
                    
                    // Cleaning
                    unlink($tmpfile);
                }
                
                if (file_exists($pofile))
                {
                    $pocontent = file_get_contents($pofile);
                    $pocontent = str_replace("# Wikindx's ### Translation ressource.", "# Wikindx's " . $listlocales[$locale] . " Translation ressource.", $pocontent);
                    $pocontent = str_replace(
                        "# Automatically generated, 2019.",
                        "# Mark Grimshaw-Aagaard <sirfragalot@users.sourceforge.net>, " . str_replace("-2019", "", "2019-" . date("Y")) . ".",
                        $pocontent
                    );
                    $pocontent = str_replace(
                        "# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.",
                        "# Mark Grimshaw-Aagaard <sirfragalot@users.sourceforge.net>, " . str_replace("-2019", "", "2019-" . date("Y")) . ".",
                        $pocontent
                    );
                    file_put_contents($pofile, $pocontent);
                }
                
                // Compile gettext catalog
                $execoutput = array();
                exec("msgfmt -v -o $mofile $pofile", $execoutput, $errorcode);
                abortOnError($errorcode, $errorcode);
            }
        }
        
        echo "\n";
    }
}


///////////////////////////////////////////////////////////////////////
/// Library
///////////////////////////////////////////////////////////////////////

function saveListPHPfilesInDirectory($filelist, $searchdir, $excludedir = array())
{
    file_put_contents($filelist, implode("\n", recursiveListPHPfilesInDirectory($searchdir, $excludedir)));
}

function recursiveListPHPfilesInDirectory($rootdir, $excludedir = NULL)
{
    $list = array();
    
    foreach (FILE\dirToArray($rootdir) as $p)
    {
        if (is_dir($rootdir . DIRECTORY_SEPARATOR . $p))
        {
            $process = true;
            if (count($excludedir) > 0)
            {
                foreach ($excludedir as $ed)
                {
                    if (mb_substr($rootdir . DIRECTORY_SEPARATOR . $p, 0, mb_strlen($ed)) == $ed)
                    {
                        $process = false;
                        break;
                    }
                }
            }
            
            if ($process)
            {
                $tmp = recursiveListPHPfilesInDirectory($rootdir . DIRECTORY_SEPARATOR . $p, $excludedir);
                $list = array_merge($list, $tmp);
            }
        }
        else if (matchExtension($p, ".php"))
        {
            $list[] = $rootdir . DIRECTORY_SEPARATOR . $p;
        }
    }
    
    return $list;
}

function matchExtension($filename, $ext)
{
    return (mb_strtolower(mb_substr($filename, - mb_strlen($ext))) == $ext);
}

function abortOnError($errorcode)
{
    if ($errorcode != 0) die("\n" . "The previous process exited with error code " . $errorcode . "\n");
}
