BIBLIOGRAPHIC STYLE:
Comicforschung-Bibliographie Stil (CFB)

INSTALLATION:
Place this folder into /styles/bibliography/

COMPILER:
Dr. Joachim Trinkwitz (2016), http://www.comicforschung.uni-bonn.de/

EDITOR:
Mark Grimshaw-Aagaard (2016)
