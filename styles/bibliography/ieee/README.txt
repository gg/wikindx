BIBLIOGRAPHIC STYLE:
Institute of Electrical and Electronics Engineers (IEEE)

INSTALLATION:
Place this folder into /styles/bibliography/

COMPILER:
Mark Grimshaw-Aagaard (2005)

EDITOR:
Frank Goergen (2005)