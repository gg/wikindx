/**********************************************************************************
WIKINDX: Bibliographic Management system.
Copyright (C)

Creative Commons
Creative Commons Legal Code
Attribution-NonCommercial-ShareAlike 2.0
THE WORK IS PROVIDED UNDER THE TERMS OF THIS CREATIVE COMMONS PUBLIC LICENSE ("CCPL" OR "LICENSE”) — docs/LICENSE.txt. 
THE WORK IS PROTECTED BY COPYRIGHT AND/OR OTHER APPLICABLE LAW. ANY USE OF THE WORK OTHER THAN AS AUTHORIZED UNDER 
THIS LICENSE OR COPYRIGHT LAW IS PROHIBITED.

BY EXERCISING ANY RIGHTS TO THE WORK PROVIDED HERE, YOU ACCEPT AND AGREE TO BE BOUND BY THE TERMS OF THIS LICENSE. 
THE LICENSOR GRANTS YOU THE RIGHTS CONTAINED HERE IN CONSIDERATION OF YOUR ACCEPTANCE OF SUCH TERMS AND CONDITIONS.

The WIKINDX Team 2016
sirfragalot@users.sourceforge.net

**********************************************************************************/

/**
* Javascript functions for core/modules/amin/ADMINPLUGINS
*
* Not AJAX, but uses the WIKINDX AJAX framework for simplicity and efficiency
*
* @version 1.1
* @date February 2013
* @author Mark Grimshaw-Aagaard
*/

function disablePlugin()
{
	coreSelectToSelect('pluginsDisabled', 'pluginsEnabled');
}
function enablePlugin()
{
	coreSelectToSelect('pluginsEnabled', 'pluginsDisabled');
}
function disableStyle()
{
	coreSelectToSelect('stylesDisabled', 'stylesEnabled');
}
function enableStyle()
{
	coreSelectToSelect('stylesEnabled', 'stylesDisabled');
}
function disableTemplate()
{
	coreSelectToSelect('templatesDisabled', 'templatesEnabled');
}
function enableTemplate()
{
	coreSelectToSelect('templatesEnabled', 'templatesDisabled');
}
/**
* On submit, select all options in both enabled and disabled select boxes -- this allows PHP to pick up those options
*/
function selectAll()
{
	selectAllPlugins();
	selectAllStyles();
	selectAllTemplates();
}
function selectAllPlugins()
{
	var objA = coreGetElementById('pluginsEnabled');
	var objB = coreGetElementById('pluginsDisabled');
	for(i = objA.options.length - 1; i >= 0; i--)
		objA.options[i].selected = true;
	for(i = objB.options.length - 1; i >= 0; i--)
		objB.options[i].selected = true;
}
function selectAllStyles()
{
	var objA = coreGetElementById('stylesEnabled');
	var objB = coreGetElementById('stylesDisabled');
	for(i = objA.options.length - 1; i >= 0; i--)
		objA.options[i].selected = true;
	for(i = objB.options.length - 1; i >= 0; i--)
		objB.options[i].selected = true;
}
function selectAllTemplates()
{
	var objA = coreGetElementById('templatesEnabled');
	var objB = coreGetElementById('templatesDisabled');
	for(i = objA.options.length - 1; i >= 0; i--)
		objA.options[i].selected = true;
	for(i = objB.options.length - 1; i >= 0; i--)
		objB.options[i].selected = true;
}
