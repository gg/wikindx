/**********************************************************************************
WIKINDX: Bibliographic Management system.
Copyright (C)

Creative Commons
Creative Commons Legal Code
Attribution-NonCommercial-ShareAlike 2.0
THE WORK IS PROVIDED UNDER THE TERMS OF THIS CREATIVE COMMONS PUBLIC LICENSE ("CCPL" OR "LICENSE”) — docs/LICENSE.txt. 
THE WORK IS PROTECTED BY COPYRIGHT AND/OR OTHER APPLICABLE LAW. ANY USE OF THE WORK OTHER THAN AS AUTHORIZED UNDER 
THIS LICENSE OR COPYRIGHT LAW IS PROHIBITED.

BY EXERCISING ANY RIGHTS TO THE WORK PROVIDED HERE, YOU ACCEPT AND AGREE TO BE BOUND BY THE TERMS OF THIS LICENSE. 
THE LICENSOR GRANTS YOU THE RIGHTS CONTAINED HERE IN CONSIDERATION OF YOUR ACCEPTANCE OF SUCH TERMS AND CONDITIONS.

The WIKINDX Team 2018
sirfragalot@users.sourceforge.net

**********************************************************************************/

/**
* Javascript functions for core/modules/admin/CONFIGURE.php
*
* @version 1
* @date April 2018
* @author Mark Grimshaw-Aagaard
*/

/**
* Transfer an option from the available resources selectbox to the discarded resources selectbox
*/
function selectResource()
{
	var target = 'configDeactivateResourceTypes';
	var source = 'activeResourceTypes';
	coreSelectToSelect(target, source);
}
/**
* Transfer an option from the disabled resources selectbox to the available resources selectbox
*/
function discardResource()
{
	var target = 'activeResourceTypes';
	var source = 'configDeactivateResourceTypes';
	coreSelectToSelect(target, source);
}
/**
* On submit, select all options in select boxes -- this allows PHP to pick up those options
*/
function selectAll()
{
	selectAllProcess('configDeactivateResourceTypes');
}
/**
* Select selected options
*/
function selectAllProcess(box)
{
	var element = box;
	var obj = coreGetElementById(element);
	if(obj == null)
		return;
	for(i = obj.options.length - 1; i >= 0; i--)
		obj.options[i].selected = true;
}