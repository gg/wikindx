<?php
/**
 * WIKINDX : Bibliographic Management system.
 * @link https://wikindx.sourceforge.io/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2018–2019 Mark Grimshaw-Aagaard <sirfragalot@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 */

/*****
*	ADMINCOMPONENTSUPDATE class.
*
*	Update of components including plugins, bibliography styles, languages, and templates.
*	Methods and properties called from ADMINCOMPONENTS.php
*****/
class ADMINCOMPONENTSUPDATE
{
private $vars;
private $errors;
private $messages;
private $success;
private $incompatible = array();
private $newPlugins = array();
private $updatePlugins = array();
private $pluginsDir = 'plugins';
private $newStyles = array();
private $updateStyles = array();
private $stylesDir = 'styles';
private $newTemplates = array();
private $updateTemplates = array();
private $templatesDir = 'templates';
private $newLanguages = array();
private $updateLanguages = array();
private $languagesDir = 'languages';
private $updateScript = 'https://wikindx.sourceforge.io/downloads/componentUpdate.php';
public $internetOn = TRUE;

	public function __construct()
	{
		$this->vars = GLOBALS::getVars();
		$this->config = FACTORY_CONFIG::getInstance();
		$this->errors = FACTORY_ERRORS::getInstance();
		$this->messages = FACTORY_MESSAGES::getInstance();
		$this->success = FACTORY_SUCCESS::getInstance();
		GLOBALS::setTplVar('heading', $this->messages->text("heading", "adminComponents"));
		if(!$this->ping())
			$this->internetOn = FALSE;
	}
// Check for incompatible but enabled plugins
	public function checkPlugins()
	{
		include_once("core/modules/LOADEXTERNALMODULES.php");
		$loadmodules = new LOADEXTERNALMODULES();
		$array = array();
		foreach (FILE\dirInDirToArray($loadmodules->moduleDirectory) as $dir)
		{
			$index = $loadmodules->moduleDirectory . $dir . DIRECTORY_SEPARATOR . 'index.php';
			$config = $loadmodules->moduleDirectory . $dir . DIRECTORY_SEPARATOR . 'config.php';
			$type = $loadmodules->moduleDirectory . $dir . DIRECTORY_SEPARATOR . 'plugintype.txt';
			if(file_exists($index) && file_exists($config) && file_exists($type))
			{
				if(!$loadmodules->checkVersion($loadmodules->moduleDirectory, $dir))
					$array[$dir] = $dir;
			}
		}
		return $array;
	}
// Check for new or updated styles in a new popup window
	public function stylesCheckDisplay()
	{
		$pString = '';
		$data = array(
			'method' => 'reportStyles'
			);
		$data = http_build_query($data, '', '&'); // NB do NOT use 'amp;' or the default (also 'amp;') as this is not decoded in $_GET
		$serverStyles = json_decode(utf8_encode(file_get_contents($this->updateScript . "?" . $data, FALSE)), JSON_OBJECT_AS_ARRAY);
		if(json_last_error() !== JSON_ERROR_NONE)
			die('JSON error: ' . json_last_error());
		if(!is_array($serverStyles))
			die($serverStyles); // if a string, then an error message
		$pString .= $this->displayUpdateStyles($serverStyles);
		$pString .= HTML\p(\FORM\closePopup($this->messages->text("misc", "closePopup")), "right");
		GLOBALS::addTplVar('content', $pString);
		FACTORY_CLOSEPOPUP::getInstance();
	}
// Check for new or updated languages in a new popup window
	public function languagesCheckDisplay()
	{
		$pString = '';
		$data = array(
			'method' => 'reportLanguages'
			);
		$data = http_build_query($data, '', '&'); // NB do NOT use 'amp;' or the default (also 'amp;') as this is not decoded in $_GET
		$serverLanguages = json_decode(utf8_encode(file_get_contents($this->updateScript . "?" . $data, FALSE)), JSON_OBJECT_AS_ARRAY);
		if(json_last_error() !== JSON_ERROR_NONE)
			die('JSON error: ' . json_last_error());
		if(!is_array($serverLanguages))
			die($serverLanguages); // if a string, then an error message
		$pString .= $this->displayUpdateLanguages($serverLanguages);
		$pString .= HTML\p(\FORM\closePopup($this->messages->text("misc", "closePopup")), "right");
		GLOBALS::addTplVar('content', $pString);
		FACTORY_CLOSEPOPUP::getInstance();
	}
// Check for new or updated templates in a new popup window
	public function templatesCheckDisplay()
	{
		$pString = '';
		$data = array(
			'method' => 'reportTemplates'
			);
		$data = http_build_query($data, '', '&'); // NB do NOT use 'amp;' or the default (also 'amp;') as this is not decoded in $_GET
		$serverTemplates = json_decode(utf8_encode(file_get_contents($this->updateScript . "?" . $data, FALSE)), JSON_OBJECT_AS_ARRAY);
		if(json_last_error() !== JSON_ERROR_NONE)
			die('JSON error: ' . json_last_error());
		if(!is_array($serverTemplates))
			die($serverTemplates); // if a string, then an error message
		$pString .= $this->displayUpdateTemplates($serverTemplates);
		$pString .= HTML\p(\FORM\closePopup($this->messages->text("misc", "closePopup")), "right");
		GLOBALS::addTplVar('content', $pString);
		FACTORY_CLOSEPOPUP::getInstance();
	}
// Check for new or updated plugins in a new popup window
	public function pluginsCheckDisplay()
	{
		$pString = '';
		$data = array(
			'method' => 'reportPlugins'
			);
		$data = http_build_query($data, '', '&'); // NB do NOT use 'amp;' or the default (also 'amp;') as this is not decoded in $_GET
		$serverPlugins = json_decode(utf8_encode(file_get_contents($this->updateScript . "?" . $data, FALSE)), JSON_OBJECT_AS_ARRAY);
		if(json_last_error() !== JSON_ERROR_NONE)
			die('JSON error: ' . json_last_error());
		if(!is_array($serverPlugins))
			die($serverPlugins); // if a string, then an error message
		$pString .= $this->displayUpdatePlugins($serverPlugins);
		$pString .= HTML\p(\FORM\closePopup($this->messages->text("misc", "closePopup")), "right");
		GLOBALS::addTplVar('content', $pString);
		FACTORY_CLOSEPOPUP::getInstance();
	}
// Parse server and local plugins to see what needs updating or if there are any new plugins
	private function displayUpdatePlugins($serverPlugins)
	{
		$localPlugins = $this->recursePlugins();
		list($enabled, $disabled) = $this->listPlugins();
//		print 'LOCAL: '; print_r($localPlugins); print BR;
		$update = $incompatible = $newConfig = array();
		foreach($serverPlugins as $pluginDir => $pluginArray)
		{
// First check if there are any new plugins
			if(!array_key_exists($pluginDir, $localPlugins))
				$this->newPlugins[$pluginDir] = $pluginArray['name'];
// Check for incompatible plugins that are enabled (those that are disabled are ignored)
			if(array_key_exists($pluginDir, $enabled))
			{
				if($pluginArray['wikindxVersion'] > $localPlugins[$pluginDir]['wikindxVersion'])
					$incompatible[$pluginDir] = $pluginArray['name'];
			}
// Check for any new version of plugins on the server of enabled plugins (those that are disabled are ignored)
			if(array_key_exists($pluginDir, $enabled))
			{
				$serverTimestamp = array_shift($pluginArray['timestamp']);
				$localTimestamp = array_shift($localPlugins[$pluginDir]['timestamp']);
				if(($serverTimestamp > $localTimestamp) && !array_key_exists($pluginDir, $incompatible))
					$update[$pluginDir] = $pluginArray['name'];
			}
// Having passed all checks, a local config.php file might have been edited after an updated plugin has been uploaded to the server.
// NB 1. There is no such thing (on *NIX) as file creation time. 
// NB 2. FTPing files to a server retains the last modified time from the source location.
// To check the config file, we must compare the names of their public properties (values can be different).
// Really, we're checking if there are extra config.php public properties on the server version – private properties cannot be read this
// way but all config.php properties should be public anyway..
			if(array_key_exists($pluginDir, $enabled))
			{
				foreach($pluginArray['configProperties'] as $property)
				{
					if(!array_key_exists($property, $localPlugins[$pluginDir]['configProperties']) && 
						 !array_key_exists($pluginDir, $update) && 
						 !array_key_exists($pluginDir, $incompatible))
						$newConfig[$pluginDir] = $pluginDir;
				}
			}
		}
		$this->updatePlugins = array_merge($update, $incompatible, $newConfig);
		return $this->displayUpdateForm($this->pluginsDir);
	}
// Parse server and local styles to see what needs updating or if there are any new styles
	private function displayUpdateStyles($serverStyles)
	{
		$localStyles = $this->recurseStyles();
		list($enabled, $disabled) = $this->listStyles();
		$update = $incompatible = $newConfig = array();
		foreach($serverStyles as $styleDir => $styleArray)
		{
// First check if there are any new plugins
			if(!array_key_exists($styleDir, $localStyles))
				$this->newStyles[$styleDir] = $styleArray['name'];
// Check for any new version of styles on the server of enabled styles (those that are disabled are ignored)
			if(array_key_exists($styleDir, $enabled))
			{
				$serverTimestamp = array_shift($styleArray['timestamp']);
				$localTimestamp = array_shift($localStyles[$styleDir]['timestamp']);
				if($serverTimestamp > $localTimestamp)
					$this->updateStyles[$styleDir] = $styleArray['name'];
			}
		}
		return $this->displayUpdateForm($this->stylesDir);
	}
// Parse server and local languages to see what needs updating or if there are any new styles
	private function displayUpdateLanguages($serverLanguages)
	{
		$localLanguages = $this->recurseLanguages();
		list($enabled, $disabled) = $this->listLanguages();
		$update = $incompatible = $newConfig = array();
		foreach($serverLanguages as $languageDir => $languageArray)
		{
// First check if there are any new plugins
			if(!array_key_exists($languageDir, $localLanguages))
				$this->newLanguages[$languageDir] = $languageArray['name'];
// Check for any new version of styles on the server of enabled styles (those that are disabled are ignored)
			if(array_key_exists($languageDir, $enabled))
			{
				$serverTimestamp = array_shift($languageArray['timestamp']);
				$localTimestamp = array_shift($localLanguages[$languageDir]['timestamp']);
				if($serverTimestamp > $localTimestamp)
					$this->updateLanguages[$languageDir] = $languageArray['name'];
			}
		}
		return $this->displayUpdateForm($this->languagesDir);
	}
// Parse server and local templates to see what needs updating or if there are any new styles
	private function displayUpdateTemplates($serverTemplates)
	{
		$localTemplates = $this->recurseTemplates();
		list($enabled, $disabled) = $this->listTemplates();
		$update = $incompatible = $newConfig = array();
		foreach($serverTemplates as $templateDir => $templateArray)
		{
// First check if there are any new plugins
			if(!array_key_exists($templateDir, $localTemplates))
				$this->newTemplates[$templateDir] = $templateArray['name'];
// Check for any new version of styles on the server of enabled styles (those that are disabled are ignored)
			if(array_key_exists($templateDir, $enabled))
			{
				$serverTimestamp = array_shift($templateArray['timestamp']);
				$localTimestamp = array_shift($localTemplates[$templateDir]['timestamp']);
				if($serverTimestamp > $localTimestamp)
					$this->updateTemplates[$templateDir] = $templateArray['name'];
			}
		}
		return $this->displayUpdateForm($this->templatesDir);
	}
// Display form for updating and adding components
	private function displayUpdateForm($serverDir)
	{
		switch($serverDir)
		{
			case 'plugins':
				$messageComponentsNewMenu = $this->messages->text('misc', 'pluginsNewMenu');
				$messageComponentsUpdateMenu = $this->messages->text('misc', 'pluginsUpdateMenu');
				$warningMessage = $this->errors->text('warning', 'noPlugins');
				$newComponents = $this->newPlugins;
				$updateComponents = $this->updatePlugins;
				$jsMethod = 'initPluginMenu';
				break;
			case 'styles':
				$messageComponentsNewMenu = $this->messages->text('misc', 'stylesNewMenu');
				$messageComponentsUpdateMenu = $this->messages->text('misc', 'stylesUpdateMenu');
				$warningMessage = $this->errors->text('warning', 'noStyles');
				$newComponents = $this->newStyles;
				$updateComponents = $this->updateStyles;
				$jsMethod = 'initStyleMenu';
				break;
			case 'languages':
				$messageComponentsNewMenu = $this->messages->text('misc', 'languagesNewMenu');
				$messageComponentsUpdateMenu = $this->messages->text('misc', 'languagesUpdateMenu');
				$warningMessage = $this->errors->text('warning', 'noLanguages');
				$newComponents = $this->newLanguages;
				$updateComponents = $this->updateLanguages;
				$jsMethod = 'initLanguageMenu';
				break;
			case 'templates':
				$messageComponentsNewMenu = $this->messages->text('misc', 'templatesNewMenu');
				$messageComponentsUpdateMenu = $this->messages->text('misc', 'templatesUpdateMenu');
				$warningMessage = $this->errors->text('warning', 'noTemplates');
				$newComponents = $this->newTemplates;
				$updateComponents = $this->updateTemplates;
				$jsMethod = 'initTemplateMenu';
				break;
			default:
				die('Broken switch');
		}
		if(empty($newComponents) && empty($updateComponents))
			return $warningMessage;
		$pString = \FORM\formHeader("admin_ADMINCOMPONENTSUPDATE_CORE");
		$pString .= \FORM\hidden("method", 'downloadComponents');
		$pString .= \FORM\hidden("serverDir", $serverDir);
		$jsonArray = array();
		$jScript = 'index.php?action=admin_ADMINCOMPONENTSUPDATE_CORE&method=' . $jsMethod;
		$jsonArray[] = array(
			'startFunction' => 'triggerFromMultiSelect',
			'script' => "$jScript",
			'triggerField' => 'displayReadme',
			'targetDiv' => 'divReadme',
			);
		$js = \AJAX\jActionForm('onchange', $jsonArray);
		$pString .= \HTML\tableStart('');
		$pString .= \HTML\trStart('generalTable');
		if(empty($newComponents))
			$pString .= \HTML\td('&nbsp;');
		else
			$pString .= \HTML\td(\FORM\selectFBoxValueMultiple($messageComponentsNewMenu, 'newComponents', $newComponents, 5) . BR .
				\HTML\span($this->messages->text("hint", "multiples"), 'hint'));
		if(empty($updateComponents))
			$pString .= \HTML\td('&nbsp;');
		else
			$pString .= \HTML\td(\FORM\selectFBoxValueMultiple($messageComponentsUpdateMenu, 'updateComponents', $updateComponents, 5) . BR .
				\HTML\span($this->messages->text("hint", "multiples"), 'hint'));
		$pString .= \HTML\trStart('generalTable');
		$pString .= \HTML\td(\FORM\formSubmit($this->messages->text("submit", "downloadInstall")), FALSE, 2);
		$pString .= \HTML\trEnd();
		$pString .= HTML\trEnd();
		$pString .= \HTML\trStart('generalTable');
		$mergeComponents = array_merge($newComponents, $updateComponents);
		$pString .= \HTML\td(\FORM\selectFBoxValue($this->messages->text('misc', 'displayReadme'), 
			'displayReadme', $mergeComponents, 5, FALSE, $js));
		$pString .= \HTML\td(\HTML\div('divReadme', $this->getReadmeDetails($mergeComponents, $serverDir)), 'left top width80percent');
		$pString .= \HTML\trEnd();
		$pString .= \HTML\tableEnd();
		$pString .= \FORM\formEnd();
		return $pString;
	}
/**
* AJAX-based DIV content creator for update of plugins
*/
	public function initPluginMenu()
	{
		$div = \HTML\div('divReadme', $this->getReadmeDetails(array_merge($this->newPlugins, $this->updatePlugins), $this->pluginsDir));
		GLOBALS::addTplVar('content', \AJAX\encode_jArray(array('innerHTML' => $div)));
		FACTORY_CLOSERAW::getInstance();
	}
/**
* AJAX-based DIV content creator for update of styles
*/
	public function initStyleMenu()
	{
		$div = \HTML\div('divReadme', $this->getReadmeDetails(array_merge($this->newStyles, $this->updateStyles), $this->stylesDir));
		GLOBALS::addTplVar('content', \AJAX\encode_jArray(array('innerHTML' => $div)));
		FACTORY_CLOSERAW::getInstance();
	}
/**
* AJAX-based DIV content creator for update of templates
*/
	public function initTemplateMenu()
	{
		$div = \HTML\div('divReadme', $this->getReadmeDetails(array_merge($this->newTemplates, $this->updateTemplates), $this->templatesDir));
		GLOBALS::addTplVar('content', \AJAX\encode_jArray(array('innerHTML' => $div)));
		FACTORY_CLOSERAW::getInstance();
	}
/**
* AJAX-based DIV content creator for update of languages
*/
	public function initLanguageMenu()
	{
		$div = \HTML\div('divReadme', $this->getReadmeDetails(array_merge($this->newLanguages, $this->updateLanguages), $this->languagesDir));
		GLOBALS::addTplVar('content', \AJAX\encode_jArray(array('innerHTML' => $div)));
		FACTORY_CLOSERAW::getInstance();
	}
// Download and write the components
	public function downloadComponents()
	{
//	print_r($this->vars);
// Just code for testing the principle of copying files (e.g. zip files) from server to local host. To be deleted . . .
/*		$dir = $this->config->WIKINDX_FILE_PATH ? $this->config->WIKINDX_FILE_PATH : WIKINDX_DIR_FILES;
		$fileName = uniqid();
		$filePath = $dir;
		if(!@copy('https://wikindx.sourceforge.io/downloads/plugins/newPlugin/description.txt', $filePath . DIRECTORY_SEPARATOR . $fileName))
		{
			$errors = error_get_last();
			$error = $this->errors->text('file', 'write', $errors['message']);
			die($error);
		}
		if(!@copy($filePath . DIRECTORY_SEPARATOR . $fileName, WIKINDX_DIR_TEMPLATES . DIRECTORY_SEPARATOR . $fileName))
		{
			$errors = error_get_last();
			$error = $this->errors->text('file', 'write', $errors['message']);
			die($error);
		}
*/
		$this->validate();
		$this->getFiles();
		$pString = \HTML\p('Move along folks. Nothing to see yet . . .');
		$pString .= HTML\p(\FORM\closePopup($this->messages->text("misc", "closePopup")), "left");
		GLOBALS::addTplVar('content', $pString);
		FACTORY_CLOSEPOPUP::getInstance();
	}
/**
* Get downloads and write to files directory
*/
	private function getFiles()
	{
		$filesDir = $this->config->WIKINDX_FILE_PATH ? $this->config->WIKINDX_FILE_PATH : WIKINDX_DIR_FILES;
		$filesDir .= DIRECTORY_SEPARATOR;
		if(array_key_exists('newComponents', $this->vars))
		{
			foreach($this->vars['newComponents'] as $sourceDirectory)
			{
//				$file = $this->zipDirectory($this->vars['serverDir'] . DIRECTORY_SEPARATOR . $sourceDirectory);
//				$newComponents[$serverDir] = $file;
			}
		}
/*		switch($serverDir)
		{
			case 'plugins':
				break;
			case 'styles':
				break;
			case 'languages':
				break;
			case 'templates':
				break;
			default:
				die('Broken switch');
		}
*/
	}
/**
* Get a zip file of the component directory from the remote server
*/
	private function zipDirectory($path)
	{
		$data = array(
			'method' => 'zipDirectory',
			'path'	=>	$path
			);
		$data = http_build_query($data, '', '&'); // NB do NOT use 'amp;' or the default (also 'amp;') as this is not decoded in $_GET
		$zipFile = json_decode(utf8_encode(file_get_contents($this->updateScript . "?" . $data, FALSE)), JSON_OBJECT_AS_ARRAY);
		if(json_last_error() !== JSON_ERROR_NONE)
			die('JSON error: ' . json_last_error());
		if(!is_array($zipFile))
			die($zipFile); // if a string, then an error message
		print_r($zipFile);
	}
/**
* Validate download input
*/
	private function validate()
	{
		if(!array_key_exists('serverDir', $this->vars) 
			|| (($this->vars['serverDir'] != $this->pluginsDir)
			&& ($this->vars['serverDir'] != $this->stylesDir)
			&& ($this->vars['serverDir'] != $this->languagesDir)
			&& ($this->vars['serverDir'] != $this->templatesDir))
			|| (!array_key_exists('newComponents', $this->vars) 
			&& !array_key_exists('updateComponents', $this->vars)))
		{
			$pString = $this->errors->text('inputError', 'missing');
			$pString .= HTML\p(\FORM\closePopup($this->messages->text("misc", "closePopup")), "left");
			GLOBALS::addTplVar('content', $pString);
			FACTORY_CLOSEPOPUP::getInstance();
		}
		return TRUE;
	}
/**
* Get readme files and put into DIV
*/
	private function getReadmeDetails($array, $topDir)
	{
		if(array_key_exists('ajaxReturn', $this->vars))
			$dir = $this->vars['ajaxReturn'];
		else // grab the first of the list
		{
			foreach($array as $dir => $null)
				break;
		}
		$path = $topDir  . DIRECTORY_SEPARATOR . $dir;
		$string = $pString = '';
		$data = array(
			'method' => 'getServerReadme',
			'path'	=>	$path
			);
		$data = http_build_query($data, '', '&'); // NB do NOT use 'amp;' or the default (also 'amp;') as this is not decoded in $_GET
		$returnArray = json_decode(utf8_encode(file_get_contents($this->updateScript . "?" . $data, FALSE)), JSON_OBJECT_AS_ARRAY);
		if(json_last_error() !== JSON_ERROR_NONE)
			die('JSON error: ' . json_last_error());
// if a string, then an error message
		else if(!is_array($returnArray))
			$pString = $this->messages->text('misc', 'noReadme');
		else
			$pString = HTML\nlToHtml(array_shift($returnArray));
		return $pString;
	}
// recurse the plugins directory and report various stats in order to check for updates etc.
	private function recursePlugins()
	{
		$pluginList = array();
		$topFolder = WIKINDX_DIR_PLUGINS;
		if($handle = opendir($topFolder))
		{
			$topFolder .= DIRECTORY_SEPARATOR;
			while(FALSE !== ($dir = readdir($handle)))
			{
				if(($dir != '.') && ($dir != '..') && is_dir($topFolder . $dir))
				{
					$index = $topFolder . $dir . DIRECTORY_SEPARATOR . 'index.php';
					$indexDis = $topFolder . $dir . DIRECTORY_SEPARATOR . 'index.php~';
					$config = $topFolder . $dir . DIRECTORY_SEPARATOR . 'config.php';
					$type = $topFolder . $dir . DIRECTORY_SEPARATOR . 'plugintype.txt';
					if((file_exists($index) || file_exists($indexDis)) && file_exists($config) && file_exists($type)) // Appears to be valid plugin
					{
// single member array of filename => UNIX timestamp
						$array = $this->getHighestFileTimestamp($topFolder . $dir . DIRECTORY_SEPARATOR);
						$pluginList[$dir]['timestamp'] = $array;
						include_once($config);
						$className = $dir . "_CONFIG";
						$class = new $className();
						$pluginList[$dir]['wikindxVersion'] = $class->wikindxVersion;
						$classVars = get_object_vars($class);
// Grab class properties (NB only public properties can be checked but all properties in config.php are likely to be public
						foreach($classVars as $property => $value)
							$pluginList[$dir]['configProperties'][$property] = $property;
					}
				}
			}
			closedir($handle);
		}
		else
			die('Cannot recurse plugins folder');
		return $pluginList;
	}
// recurse the styles directory and report various stats in order to check for updates etc.
	private function recurseStyles()
	{
		$styleList = array();
		$topFolder = WIKINDX_DIR_STYLES . DIRECTORY_SEPARATOR . 'bibliography';
		if($handle = opendir($topFolder))
		{
			$topFolder .= DIRECTORY_SEPARATOR;
			while(FALSE !== ($dir = readdir($handle)))
			{
				if(($dir != '.') && ($dir != '..') && is_dir($topFolder . $dir))
				{
					$xml = $topFolder . $dir . DIRECTORY_SEPARATOR . strtoupper($dir) . '.xml';
					$xmlDis = $xml . '~';
					$readme = $topFolder . $dir . DIRECTORY_SEPARATOR . 'README.txt';
					if((file_exists($xml) || file_exists($xmlDis)) && file_exists($readme)) // Appears to be valid style
					{
// single member array of filename => UNIX timestamp
						$array = $this->getHighestFileTimestamp($topFolder . $dir . DIRECTORY_SEPARATOR);
						$styleList[$dir]['timestamp'] = $array;
					}
				}
			}
			closedir($handle);
		}
		else
			die('Cannot recurse styles folder');
		return $styleList;
	}
// recurse the languages directory and report various stats in order to check for updates etc.
	private function recurseLanguages()
	{
		$languageList = array();
		$topFolder = WIKINDX_DIR_LANGUAGES;
		if($handle = opendir($topFolder))
		{
			$topFolder .= DIRECTORY_SEPARATOR;
			while(FALSE !== ($dir = readdir($handle)))
			{
				if(($dir != '.') && ($dir != '..') && is_dir($topFolder . $dir))
				{
					$array = $this->getHighestFileTimestamp($topFolder . $dir . DIRECTORY_SEPARATOR);
					$languageList[$dir]['timestamp'] = $array;
				}
			}
			closedir($handle);
		}
		else
			die('Cannot recurse languages folder');
		return $languageList;
	}
// recurse the templates directory and report various stats in order to check for updates etc.
	private function recurseTemplates()
	{
		$templateList = array();
		$topFolder = WIKINDX_DIR_TEMPLATES;
		if($handle = opendir($topFolder))
		{
			$topFolder .= DIRECTORY_SEPARATOR;
			while(FALSE !== ($dir = readdir($handle)))
			{
				if(($dir != '.') && ($dir != '..') && is_dir($topFolder . $dir))
				{
					$array = $this->getHighestFileTimestamp($topFolder . $dir . DIRECTORY_SEPARATOR);
					$templateList[$dir]['timestamp'] = $array;
				}
			}
			closedir($handle);
		}
		else
			die('Cannot recurse templates folder');
		return $templateList;
	}
// List plugins
	public function listPlugins()
	{
		$enabled = $disabled = array();
		$rootDir = WIKINDX_DIR_PLUGINS;
		foreach (FILE\dirInDirToArray($rootDir) as $dir)
		{
    		$filePath = $rootDir . DIRECTORY_SEPARATOR . $dir . DIRECTORY_SEPARATOR . 'index.php';
    		$descFilePath = $rootDir . DIRECTORY_SEPARATOR . $dir . DIRECTORY_SEPARATOR . 'description.txt';
    		if(file_exists($filePath))
    		{
    			if(file_exists($descFilePath))
    			{
    			    $fh = fopen($descFilePath, "r");
    				$string = fgets($fh);
    				$enabled[$dir] = $string;
    				fclose($fh);
    			}
    			else
    				$enabled[$dir] = $dir;
    		}
    		else if(file_exists($filePath . '~'))
    		{
    			if(file_exists($descFilePath))
    			{
    			    $fh = fopen($descFilePath, "r");
    				$string = fgets($fh);
    				$disabled[$dir] = $string;
    				fclose($fh);
    			}
    			else
    				$disabled[$dir] = $dir;
    		}
		}
		return array($enabled, $disabled);
	}
// List styles
	public function listStyles()
	{
		$enabled = $disabled = array();
		$rootDir = WIKINDX_DIR_STYLES . DIRECTORY_SEPARATOR . 'bibliography';
		foreach (FILE\dirInDirToArray($rootDir) as $dir)
		{
			$filePath = $rootDir . DIRECTORY_SEPARATOR . $dir . DIRECTORY_SEPARATOR . mb_strtoupper($dir) . ".xml";
			if(file_exists($filePath))
			{
			    $fh = fopen($filePath, "r");
				while(!feof($fh))
				{
					$line = stream_get_line($fh, 1000000, "". LF);
					if(preg_match("/<description>(.*)<\\/description>/Uui", $line, $matches))
					{
						$array[mb_strtoupper($dir)] = $matches[1];
						$enabled[$dir] = $matches[1];
						break;
					}
				}
				fclose($fh);
			}
			else if(file_exists($filePath . "~"))
			{
			    $fh = fopen($filePath . "~", "r");
				while(!feof($fh))
				{
					$line = stream_get_line($fh, 1000000, "". LF);
					if(preg_match("/<description>(.*)<\\/description>/Uui", $line, $matches))
					{
						$array[mb_strtoupper($dir)] = $matches[1];
						$disabled[$dir] = $matches[1];
						break;
					}
				}
				fclose($fh);
			}
		}
		return array($enabled, $disabled);
	}
// List templates
	public function listTemplates()
	{
		$enabled = $disabled = array();
		$rootDir = 'templates';
		foreach (FILE\dirInDirToArray($rootDir) as $dir)
		{
    		$filePath = $rootDir . DIRECTORY_SEPARATOR . $dir . DIRECTORY_SEPARATOR . 'description.txt';
    		if(file_exists($filePath))
    		{
    		    $fh = fopen($filePath, "r");
    			$enabled[$dir] = fgets($fh);
    			fclose($fh);
    		}
    		else if(file_exists($filePath . '~'))
    		{
    		    $fh = fopen($filePath . '~', "r");
    			$disabled[$dir] = fgets($fh);
    			fclose($fh);
    		}
		}
		return array($enabled, $disabled);
	}
// List languages
	public function listLanguages()
	{
		$enabled = $disabled = array();
		$rootDir = 'languages';
		foreach (FILE\dirInDirToArray($rootDir) as $dir)
		{
    		$filePath = $rootDir . DIRECTORY_SEPARATOR . $dir . DIRECTORY_SEPARATOR . 'description.txt';
    		if(file_exists($filePath))
    		{
    		    $fh = fopen($filePath, "r");
    			$enabled[$dir] = fgets($fh);
    			fclose($fh);
    		}
    		else if(file_exists($filePath . '~'))
    		{
    		    $fh = fopen($filePath . '~', "r");
    			$disabled[$dir] = fgets($fh);
    			fclose($fh);
    		}
		}
		return array($enabled, $disabled);
	}
// 	Recurse through directories getting filenames
	private function getAllFiles($directory, $recursive = TRUE) 
	{
    	$result = array();
    	$handle = opendir($directory);
    	while(FALSE !== ($datei = readdir($handle)))
    	{
       		if (($datei != '.') && ($datei != '..'))
  			{
				$file = $directory . $datei;
            	if (is_dir($file))
            	{
            		if($recursive)
                 		$result = array_merge($result, $this->getAllFiles($file . DIRECTORY_SEPARATOR));
            	}
            	else
              		$result[] = $file;
    		}
		}
		closedir($handle);
		return $result;
	}
// Grabe filename in nested directoreis with latest timestamp
	private function getHighestFileTimestamp($directory, $recursive = TRUE)
	{
		$allFiles = $this->getAllFiles($directory, $recursive);
		$highestKnown = 0;
		foreach($allFiles as $value)
		{
			$currentValue = filemtime($value);
			if($currentValue > $highestKnown)
			{
				$highestKnown = $currentValue;
				$newestFile = $value;
			}
     	}
     	return array($newestFile => $highestKnown);
}
// Check we have an Internet connection
// From: https://snipplr.com/view/69621/check-your-internet-connection-with-php/
// We use sourceforge.net as that is where the updates and new files are
	private function ping() 
	{
  		return (bool)fSockOpen("sourceforge.net", 80, $errno, $errstr, 10);
	}
}
