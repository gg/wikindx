<?php
/**
 * WIKINDX : Bibliographic Management system.
 * @link https://wikindx.sourceforge.io/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2017-2019 Mark Grimshaw-Aagaard <sirfragalot@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 */

/**
* LOADEXTERNALMODULES class for third-party plug-in modules in the WIKINDX_DIR_PLUGINS directory
*
*/
class LOADEXTERNALMODULES
{
public $moduleDirectory = WIKINDX_DIR_PLUGINS . DIRECTORY_SEPARATOR;
public static $moduleList = array();

	public function __construct()
	{

	}

	// Read root WIKINDX_DIR_PLUGINS directory for any available and validate modules
	// and return a list of them OR FALSE
	public function readPluginsDirectory()
	{
		if(empty(self::$moduleList))
		{
    		if($handle = opendir($this->moduleDirectory))
    		{
    			while(FALSE !== ($dir = readdir($handle)))
    			{
    				if($dir != '.' && $dir != '..' && is_dir($this->moduleDirectory . $dir))
    				{
    					$index = $this->moduleDirectory . $dir . '/index.php';
    					$config = $this->moduleDirectory . $dir . '/config.php';
    					$type = $this->moduleDirectory . $dir . '/plugintype.txt';

    					// NB: don't merge these tests because PHP hasn't specific order of evaluation
    					if (file_exists($index) && file_exists($config) && file_exists($type))
    						if ($this->checkVersion($this->moduleDirectory, $dir))
    							self::$moduleList[] = $dir;
    				}
    			}
    			closedir($handle);
    		}
		}
		if (empty(self::$moduleList))
			return FALSE;
		else
			return self::$moduleList;
	}

	// Check version compatiblility, if not, quietly exit
	public function checkVersion($moduleDirectory, $dir)
	{
		include_once($moduleDirectory . $dir . '/config.php');
		$class = $dir . "_CONFIG";
		if(!class_exists($class))
			return FALSE;
		$config = new $class();
		if(isset($config->wikindxVersion) && ($config->wikindxVersion == WIKINDX_PLUGIN_VERSION))
			return TRUE;
		return FALSE;
	}
}
