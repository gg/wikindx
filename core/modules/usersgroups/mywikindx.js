/**********************************************************************************
WIKINDX: Bibliographic Management system.
Copyright (C)

Creative Commons
Creative Commons Legal Code
Attribution-NonCommercial-ShareAlike 2.0
THE WORK IS PROVIDED UNDER THE TERMS OF THIS CREATIVE COMMONS PUBLIC LICENSE ("CCPL" OR "LICENSE”) — docs/LICENSE.txt. 
THE WORK IS PROTECTED BY COPYRIGHT AND/OR OTHER APPLICABLE LAW. ANY USE OF THE WORK OTHER THAN AS AUTHORIZED UNDER 
THIS LICENSE OR COPYRIGHT LAW IS PROHIBITED.

BY EXERCISING ANY RIGHTS TO THE WORK PROVIDED HERE, YOU ACCEPT AND AGREE TO BE BOUND BY THE TERMS OF THIS LICENSE. 
THE LICENSOR GRANTS YOU THE RIGHTS CONTAINED HERE IN CONSIDERATION OF YOUR ACCEPTANCE OF SUCH TERMS AND CONDITIONS.

The WIKINDX Team 2019
sirfragalot@users.sourceforge.net

**********************************************************************************/

/**
* Javascript functions for core/modules/usergroups/MYWIKINDX.php
*
* @version 1
* @date October 2019
* @author Mark Grimshaw-Aagaard
*/

function getMywikindxInput()
{
	var i;
	var selectItem = coreGetElementById('groupId');
	if(selectItem != null)
	{
		var i;
		for(i = 0, length = selectItem.length; i < length; i++)
		{
			if(selectItem.options[i].selected)
			{
				var groupId = selectItem.options[i].value;
				break;
			}
		}
	}
	else // no groups currently extant
		var groupId = 0;
	var radioItem = coreGetElementByName('method');
	for(i = 0, length = radioItem.length; i < length; i++)
	{
		if(radioItem[i].checked)
		{
			var method = radioItem[i].value;
			break;
		}
	}
	var options = '&method=' + method + '&groupId=' + groupId;
	coreOpenPopup('index.php?action=usersgroups_MYWIKINDXX_CORE' + options, 90)
}
function sendMywikindxInput()
{
	var type = coreGetElementById('method');
	if((type.value == 'createUserGroup') || (type.value == 'editUserGroup'))
	{
		var i;
		var select = coreGetElementById('addUsers');
		if(select != null)
		{
			var addUsers = new Array();
			for(i = 0, length = select.length; i < length; i++)
			{
				if(select.options[i].selected)
				{
					if(select.options[i].value != null)
						addUsers[i] = select.options[i].value;
				}
			}
		}
		else
			var addUsers = [0];
		addUsers = JSON.stringify(addUsers);
		var obj = coreGetElementById('title');
		var title = JSON.stringify(coreTrim(obj.value));
		var description = coreTrim(tinymce.EditorManager.get('description').getContent());
		description = description.replace(/^<p>/, '');
		description = description.replace(/<\/p>/, '');
		description = JSON.stringify(description);
		if(type.value == 'editUserGroup')
		{
			select = coreGetElementById('deleteUsers');
			if(select != null)
			{
				var deleteUsers = new Array();
				for(i = 0, length = select.length; i < length; i++)
				{
					if(select.options[i].selected)
					{
						if(select.options[i].value != null)
							deleteUsers[i] = select.options[i].value;
					}
				}
			}
			else
				var deleteUsers = [0];
			deleteUsers = JSON.stringify(deleteUsers);
			var groupId = coreGetElementById('groupId').value;
			var options = '&title=' + title + '&description=' + description + 
				'&addUsers=' + addUsers + '&deleteUsers=' + deleteUsers + '&groupId=' + groupId;
		}
		else
			var options = '&title=' + title + '&description=' + description + '&addUsers=' + addUsers;
	}
	else if(type.value == 'deleteUserGroup')
	{
		var groupId = coreGetElementById('groupId').value;
		var options = '&groupId=' + groupId;
	}
	else
	{
		alert('Missing type');
		return;
	}
	var method = type.value;
	var url = 'index.php?action=usersgroups_MYWIKINDXX_CORE&method=' + method + options;
	window.opener.location.href=url;
	window.close();
}
