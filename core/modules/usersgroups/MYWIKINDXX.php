<?php
/**
 * WIKINDX : Bibliographic Management system.
 * @link https://wikindx.sourceforge.io/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2018-2019 Mark Grimshaw-Aagaard <sirfragalot@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 */

/**
*	MYWIKINDX WIKINDX class
*
*/
class MYWIKINDXX
{
private $errors;
private $messages;
private $success;
private $session;
private $config;
private $user;
private $db;
private $vars;
private $badInput;
private $gatekeep;
private $errorString = FALSE;
private $userName = FALSE;
private $userNameDisplay = FALSE;

	public function __construct()
	{
		$this->errors = FACTORY_ERRORS::getInstance();
		$this->messages = FACTORY_MESSAGES::getInstance();
		$this->success = FACTORY_SUCCESS::getInstance();
		$this->config = FACTORY_CONFIG::getInstance();
		$this->session = FACTORY_SESSION::getInstance();
		$this->user = FACTORY_USER::getInstance();
		$this->db = FACTORY_DB::getInstance();
		$this->vars = GLOBALS::getVars();
		$this->badInput = FACTORY_BADINPUT::getInstance();
		$this->gatekeep = FACTORY_GATEKEEP::getInstance();
		$this->gatekeep->requireSuper = FALSE;
		$this->gatekeep->init();
		if($this->session->getVar("setup_UserId"))
		{
			$this->db->formatConditions(array('usersId' => $this->session->getVar("setup_UserId")));
			$recordset = $this->db->select('users', 'usersUsername');
			if(!$this->db->numRows($recordset))
				die($this->errors->text("dbError", "read"));
			$row = $this->db->fetchRow($recordset);
			$this->userName = $row['usersUsername'];
			$this->userNameDisplay = ': ' . $row['usersUsername'];
		}
	}
	public function init($message = FALSE)
	{
// Anything in the session takes precedence
		if(($messageIn = $this->session->getVar('mywikindx_Message')) && ($itemIn = $this->session->getVar('mywikindx_Item')))
		{
			$this->session->delVar('mywikindx_Message');
			$this->session->delVar('mywikindx_Item');
			$messageString = $messageIn;
			$item = $itemIn;
		}
		else if(is_array($message))
		{
			$messageString = $message[0];
			$item = $message[1];
		}
		else
		{
			$messageString = $message;
			$item = FALSE;
		}
		$configGroups = $this->getConfigGroups();
		if(empty($configGroups))
			return FALSE;
		$this->errorString = $messageString;
		include_once("core/modules/help/HELPMESSAGES.php");
		$help = new HELPMESSAGES();
		GLOBALS::setTplVar('help', $help->createLink('preferences'));
		if($this->session->getVar("setup_UserId"))
			GLOBALS::setTplVar('heading', $this->messages->text("heading", "myWikindx", $this->userNameDisplay));
		else
			GLOBALS::setTplVar('heading', $this->messages->text("heading", "myWikindx"));
		$jsonArray = array();
		$jScript = 'index.php?action=usersgroups_MYWIKINDXX_CORE&method=initConfigDiv';
		$jsonArray[] = array(
			'startFunction' => 'triggerFromMultiSelect',
			'script' => "$jScript",
			'triggerField' => 'configMenu',
			'targetDiv' => 'configDiv',
			);
		$js = \AJAX\jActionForm('onchange', $jsonArray);
		$pString = \HTML\tableStart('');
		$pString .= \HTML\trStart();
		if($item)
			$pString .= \HTML\td(\FORM\selectedBoxValue($this->messages->text('config',
				'options'), 'configMenu', $configGroups, $item, sizeOf($configGroups), FALSE, $js));
		else
			$pString .= \HTML\td(\FORM\selectFBoxValue($this->messages->text('config',
				'options'), 'configMenu', $configGroups, sizeOf($configGroups), FALSE, $js));
		$pString .= \HTML\td(\HTML\div('configDiv', $this->getConfigDetails($configGroups, $item)), 'left top width80percent');
		$pString .= \HTML\trEnd();
		$pString .= \HTML\tableEnd();
		$pString .= '<script type="text/javascript" src="' . $this->config->WIKINDX_BASE_URL . '/core/modules/usersgroups/mywikindx.js"></script>';
		GLOBALS::addTplVar('content', $pString);
	}
// create array of config menu items
	private function getConfigGroups()
	{
		$groups = array('resources' => $this->messages->text('config', 'resources'),
			'appearance' => $this->messages->text('config', 'appearance'),
		);
		if($this->session->getVar('setup_UserId') != $this->config->WIKINDX_RESTRICT_USERID)
		{
			if($this->session->issetVar("mywikindx_Email") && $this->config->WIKINDX_MAIL_SERVER)
				$groups['forget'] = $this->messages->text('config', 'forget');
			$groups['notification'] = $this->messages->text('config', 'notification');
		}
// Add user group administration only if there is more than one user.
		$resourceId = $this->db->select('users', 'usersId');
		if($this->db->numRows($resourceId) > 1)
			$groups['userGroups'] = $this->messages->text("user", "groups");
// Only for logged on users
		if($this->session->getVar('setup_UserId'))
		{
			$user = array('user' => $this->messages->text('user', 'user'));
			$groups = $user + $groups;
		}
		return $groups;
	}
/**
* Get config details for menu items and put into form elements
*/
	private function getConfigDetails($groups, $item = FALSE)
	{
		if(array_key_exists('ajaxReturn', $this->vars))
			$item = $this->vars['ajaxReturn'];
		else if(!$item) // grab the first of the list
		{
			foreach($groups as $item => $null)
				break;
		}
		if($item == 'user')
		{
			$password = FACTORY_PASSWORD::getInstance();
			$input = $this->session->getVar("setup_Username");
			list($formText, $jsString) = $password->createElements(FALSE);
			$pString = \FORM\formHeader("usersgroups_MYWIKINDXX_CORE", 'onsubmit="return checkForm(' . $jsString . ');"');
		}
		else if(($item == 'userGroups'))
		{
			$pString = \FORM\formHeader(FALSE);
			$pString .= \FORM\hidden("method", 'userGroupsConfigDisplay');
		}
		else
			$pString = \FORM\formHeader("usersgroups_MYWIKINDXX_CORE");
		$pString .= \FORM\hidden("selectItem", $item);
		switch ($item)
		{
			case 'user': // user configuration
				$pString .= $this->userConfigDisplay($formText);
				break;
			case 'resources': // resources display configuration
				$pString .= $this->resourcesConfigDisplay();
				break;
			case 'appearance': // appearance configuration
				$pString .= $this->appearanceConfigDisplay();
				break;
			case 'forget': // forgotten password configuration
				$pString .= $this->forgetConfigDisplay();
				break;
			case 'notification': // email notification configuration
				$pString .= $this->notificationConfigDisplay();
				break;
			case 'userGroups': // user groups configuration
				$pString .= $this->userGroupsConfigDisplay();
				break;
			default:
				$pString .= '';
				break;
		}
		if(($item == 'userGroups'))
			$pString .= \HTML\p(\FORM\formSubmitButton($this->messages->text("submit", "Proceed"), FALSE, 'onclick="return getMywikindxInput();"'));
		else
			$pString .= \HTML\p(\FORM\formSubmit($this->messages->text("submit", "Proceed")));
		$pString .= \FORM\formEnd();
		return $pString;
	}
/**
* AJAX-based DIV content creator for configuration of selected menu items
*/
	public function initConfigDiv()
	{
		$configGroups = $this->getConfigGroups();
		$div = \HTML\div('divMenu', $this->getConfigDetails($configGroups));
		GLOBALS::addTplVar('content', \AJAX\encode_jArray(array('innerHTML' => $div)));
		FACTORY_CLOSERAW::getInstance();
	}
// Display user config options
	private function userConfigDisplay($formText)
	{
		$userArray = array(array('usersEmail' => 'Email'), array('usersFullname' => 'Fullname'), array('usersCookie' => 'Cookie'));
		$this->db->formatConditions(array('usersId' => $this->session->getVar("setup_UserId")));
		$recordset = $this->db->select('users', $userArray);
		if(!$this->db->numRows($recordset))
			$this->badInputLoad($this->errors->text("dbError", "read"), 'user');
		$row = $this->db->fetchRow($recordset);
		$pString = $this->errorString;
		$pString .= \FORM\hidden("method", "userConfigEdit");
		$pString .= \FORM\hidden("uname", $this->userName);
		$pString .= \HTML\tableStart('generalTable borderStyleSolid left');
		$pString .= \HTML\trStart();
		$pString .= $formText;
		$pString .= \HTML\td('&nbsp;');
		$pString .= \HTML\trEnd();
		$pString .= \HTML\trStart();
		$pString .= \HTML\td('&nbsp;');
		$pString .= \HTML\td('&nbsp;');
		$pString .= \HTML\td('&nbsp;');
		$pString .= \HTML\trEnd();
		$pString .= \HTML\trStart();
		$pString .= \HTML\td(\FORM\textInput($this->messages->text("user", "email"),
			"email", $row["Email"], 30) . " " . \HTML\span('*', 'required'));
		$pString .= \HTML\td(\FORM\textInput($this->messages->text("user", "fullname"),
			"fullname", $row["Fullname"], 30));
		$cookie = $row["Cookie"] == 'Y' ? 'CHECKED' : FALSE;
		$pString .= \HTML\td(\FORM\checkbox($this->messages->text("user", "cookie"), "cookie", $cookie));
		$pString .= \HTML\trEnd();
		$pString .= \HTML\tableEnd();
		return $pString;
	}
// Edit user details
	public function userConfigEdit()
	{
		if(!trim($this->vars['password']) || !trim($this->vars['passwordConfirm']))
			$this->badInputLoad($this->errors->text("inputError", "missing"), 'user');
		if(trim($this->vars['password']) != trim($this->vars['passwordConfirm']))
			$this->badInputLoad($this->errors->text("inputError", "invalid"), 'user');
		if((!array_key_exists('email', $this->vars) || !trim($this->vars['email'])))
			$this->badInputLoad($this->errors->text("inputError", "missing"), 'user');
		$this->user->writeUser(FALSE); // FALSE = editing user
		include_once("core/modules/email/EMAIL.php");
		$emailClass = new EMAIL();
		if(!$emailClass->userEdit())
			$this->badInputLoad($this->errors->text("inputError", "mail", GLOBALS::getError()), 'user');
		$this->init(array($this->success->text("userEdit"), $this->vars['selectItem']));
		FACTORY_CLOSE::getInstance();
	}
// Display resources display options
	private function resourcesConfigDisplay()
	{
		$pString = $this->errorString;
		$pString .= \FORM\hidden("method", "resourcesConfigEdit");
		$pString .= \HTML\tableStart('generalTable borderStyleSolid left');
		$pString .= \HTML\trStart();
				$pString .= \HTML\td(\FORM\textInput($this->messages->text("config", "paging"), "Paging",
			$this->session->getVar("setup_Paging"), 5) . " " . \HTML\span('*', 'required') . BR .
			\HTML\span($this->messages->text("hint", "pagingLimit"), 'hint'));
		$pString .= \HTML\td(\FORM\textInput($this->messages->text("config", "maxPaging"), "PagingMaxLinks",
			$this->session->getVar("setup_PagingMaxLinks"), 5) . " " . \HTML\span('*', 'required') .
			BR . \HTML\span($this->messages->text("hint", "pagingMaxLinks"), 'hint'));
		if(!$this->session->getVar("setup_PagingTagCloud"))
			$this->session->setVar("setup_PagingTagCloud", 100);
		$pString .= \HTML\td(\FORM\textInput($this->messages->text("config", "pagingTagCloud"), "PagingTagCloud",
			$this->session->getVar("setup_PagingTagCloud"), 5) . " " . \HTML\span('*', 'required') .
			BR . \HTML\span($this->messages->text("hint", "pagingLimit"), 'hint'));
		$pString .= \HTML\td(\FORM\textInput($this->messages->text("config", "stringLimit"), "StringLimit",
			$this->session->getVar("setup_StringLimit"), 5) . " " . \HTML\span('*', 'required') . BR .
			\HTML\span($this->messages->text("hint", "pagingLimit"), 'hint'));
		$pString .= \HTML\trEnd();
		$pString .= \HTML\trStart();
		$pString .= \HTML\td('&nbsp;');
		$pString .= \HTML\td('&nbsp;');
		$pString .= \HTML\td('&nbsp;');
		$pString .= \HTML\td('&nbsp;');
		$pString .= \HTML\trEnd();
		$pString .= \HTML\trStart();
		$input = $this->session->getVar("setup_PagingStyle") == 'A' ? "CHECKED" : FALSE;
		$pString .= \HTML\td(\FORM\checkbox($this->messages->text("config", "pagingStyle"),
			"PagingStyle", $input));
		$input = $this->session->getVar("setup_UseWikindxKey") ? "CHECKED" : FALSE;
		$pString .= \HTML\td(\FORM\checkbox($this->messages->text("config", "useWikindxKey"),
			"UseWikindxKey", $input));
		$input = $this->session->getVar("setup_UseBibtexKey") ? "CHECKED" : FALSE;
		$pString .= \HTML\td(\FORM\checkbox($this->messages->text("config", "useBibtexKey"),
			"UseBibtexKey", $input));
		$pString .= \HTML\td('&nbsp;');
		$pString .= \HTML\trEnd();
		$pString .= \HTML\trStart();
		$pString .= \HTML\td('&nbsp;');
		$pString .= \HTML\td('&nbsp;');
		$pString .= \HTML\td('&nbsp;');
		$pString .= \HTML\td('&nbsp;');
		$pString .= \HTML\trEnd();
		$pString .= \HTML\trStart();
		$input = $this->session->getVar("setup_DisplayBibtexLink") ? "CHECKED" : FALSE;
		$pString .= \HTML\td(\FORM\checkbox($this->messages->text("config", "displayBibtexLink"),
			"DisplayBibtexLink", $input));
		$input = $this->session->getVar("setup_DisplayCmsLink") ? "CHECKED" : FALSE;
		$pString .= \HTML\td(\FORM\checkbox($this->messages->text("config", "displayCmsLink"),
			"DisplayCmsLink", $input));
		$input = $this->session->getVar("setup_Listlink") ? "CHECKED" : FALSE;
		$pString .= \HTML\td(\FORM\checkbox($this->messages->text("config", "listlink"), "Listlink", $input));
		$pString .= \HTML\td('&nbsp;');
		$pString .= \HTML\trEnd();
		$pString .= \HTML\tableEnd();
		return $pString;
	}
// Edit resource display details
	public function resourcesConfigEdit()
	{
// checkInput writes the session
		$this->checkResourcesInput();
// If this is a logged on user, write preferences to WKX_user_preferences
		if($this->session->getVar("setup_UserId"))
		{
			$user = FACTORY_USER::getInstance();
			$user->writePreferences($this->session->getVar("setup_UserId"));
		}
		$this->init(array($this->success->text("config"), $this->vars['selectItem']));
		FACTORY_CLOSE::getInstance();
	}
// Check resource display input
	function checkResourcesInput()
	{
		$required = array("Paging", "PagingMaxLinks", "StringLimit", "PagingTagCloud");
		foreach($required as $key)
		{
			if(!is_numeric($this->vars[$key]) || !is_int($this->vars[$key] + 0)) // cast to number
				$this->badInputLoad($this->errors->text("inputError", "nan", " ($key) "), 'resources');
			if(!array_key_exists($key, $this->vars) || !$this->vars[$key])
				$this->badInputLoad($this->errors->text("inputError", "missing", " ($key) "), 'resources');
			if(($key == 'PagingMaxLinks') && ($this->vars[$key] < 4))
				$this->vars[$key] = 11;
			else if($this->vars[$key] < 0)
				$this->vars[$key] = -1;
			$array[$key] = $this->vars[$key];
		}
// All input good - write to session
		$this->session->writeArray($array, "setup");
		if(array_key_exists("PagingStyle", $this->vars))
			$this->session->setVar("setup_PagingStyle", 'A');
		else
			$this->session->delVar("setup_PagingStyle");
		$this->session->delVar("sql_LastMulti"); // always reset in case of paging changes
		$this->session->delVar("sql_LastIdeaSearch"); // always reset in case of paging changes
		if(array_key_exists("UseWikindxKey", $this->vars))
			$this->session->setVar("setup_UseWikindxKey", TRUE);
		else
			$this->session->delVar("setup_UseWikindxKey");
		if(array_key_exists("UseBibtexKey", $this->vars))
			$this->session->setVar("setup_UseBibtexKey", TRUE);
		else
			$this->session->delVar("setup_UseBibtexKey");
		if(array_key_exists("DisplayBibtexLink", $this->vars))
			$this->session->setVar("setup_DisplayBibtexLink", TRUE);
		else
			$this->session->delVar("setup_DisplayBibtexLink");
		if(array_key_exists("DisplayCmsLink", $this->vars))
			$this->session->setVar("setup_DisplayCmsLink", TRUE);
		else
			$this->session->delVar("setup_DisplayCmsLink");
		if(array_key_exists("Listlink", $this->vars))
			$this->session->setVar("setup_Listlink", TRUE);
		else
			$this->session->delVar("setup_Listlink");
	}
// Display appearance config options
	public function appearanceConfigDisplay()
	{
		$pString = $this->errorString;
		$pString .= \FORM\hidden("method", "appearanceConfigEdit");
		$templates = FACTORY_TEMPLATE::getInstance()->loadDir();
		$pString .= \HTML\tableStart('generalTable borderStyleSolid left');
		$pString .= \HTML\trStart();
		$subTd = \HTML\tableStart();
		$subTd .= \HTML\trStart();
		if($this->session->getVar("setup_Template") && array_key_exists($this->session->getVar("setup_Template"), $templates))
			$subTd .= \HTML\td(\FORM\selectedBoxValue($this->messages->text("config", "template"),
				"Template", $templates, $this->session->getVar("setup_Template"), 4) . " " . \HTML\span('*', 'required'));
		else
			$subTd .= \HTML\td(\FORM\selectFBoxValue($this->messages->text("config", "template"),
				"Template", $templates, 4) . " " . \HTML\span('*', 'required'));
		$menus[0] = $this->messages->text("config", "templateMenu1");
		$menus[1] = $this->messages->text("config", "templateMenu2");
		$menus[2] = $this->messages->text("config", "templateMenu3");
		$subTd .= \HTML\td(\FORM\selectedBoxValue($this->messages->text("config", "templateMenu"),
			"TemplateMenu", $menus, $this->session->getVar("setup_TemplateMenu"), 3)
			 . " " . \HTML\span('*', 'required'));
		$subTd .= \HTML\trEnd();
		$subTd .= \HTML\tableEnd();
		$pString .= \HTML\td($subTd);
		$languages = \UTILS\getLocalizedLanguagesList();
		if($this->session->getVar("setup_Language") && array_key_exists($this->session->getVar("setup_Language"), $languages))
			$pString .= \HTML\td(\FORM\selectedBoxValue($this->messages->text("config", "language"),
				"Language", $languages, $this->session->getVar("setup_Language"))
				 . " " . \HTML\span('*', 'required'));
		else
			$pString .= \HTML\td(\FORM\selectFBoxValue($this->messages->text("config", "language"),
				"Language", $languages) . " " . \HTML\span('*', 'required'));
		$styles = \LOADSTYLE\loadDir();
		if($this->session->getVar("setup_Style") && array_key_exists($this->session->getVar("setup_Style"), $styles))
			$pString .= \HTML\td(\FORM\selectedBoxValue($this->messages->text("config", "style") , "Style",
				$styles, $this->session->getVar("setup_Style"), 4) . " " . \HTML\span('*', 'required'));
		else
			$pString .= \HTML\td(\FORM\selectFBoxValue($this->messages->text("config", "style") , "Style",
				$styles, 4) . " " . \HTML\span('*', 'required'));
		$pString .= \HTML\trEnd();
		$pString .= \HTML\tableEnd();
		return $pString;
	}
// Edit appearance display details
	public function appearanceConfigEdit()
	{
// checkInput writes the session
		$this->checkAppearanceInput();
// If this is a logged on user, write preferences to WKX_user_preferences
		if($this->session->getVar("setup_UserId"))
		{
			$user = FACTORY_USER::getInstance();
			$user->writePreferences($this->session->getVar("setup_UserId"));
		}
		$this->session->setVar('mywikindx_Message', $this->success->text("config"));
		$this->session->setVar('mywikindx_Item', $this->vars['selectItem']);
// need to use header() to ensure any change in appearance is immediately picked up.
		header("Location: index.php?action=usersgroups_MYWIKINDXX_CORE&method=init");
	}
// Check appearance display input
	function checkAppearanceInput()
	{
		$required = array("Language", "Template", "Style");
		foreach($required as $value)
		{
			if(!array_key_exists($value, $this->vars) || !$this->vars[$value])
				$this->badInputLoad($this->errors->text("inputError", "missing", " ($value) "), 'appearance');
			$array[$value] = $this->vars[$value];
		}
		if(!array_key_exists("TemplateMenu", $this->vars))
			$this->badInputLoad($this->errors->text("inputError", "missing", " (TemplateMenu) "), 'appearance');
		else
			$array['TemplateMenu'] = $this->vars['TemplateMenu'];
// All input good - write to session
		$this->session->writeArray($array, "setup");
	}
// Display forgotten password config options
	private function forgetConfigDisplay()
	{
		include_once('core/modules/usersgroups/FORGET.php');
		$forget = new FORGET();
		$pString = $this->errorString;
		$pString .= \FORM\hidden("method", "forgetConfigEdit");
		$pString .= \HTML\tableStart('generalTable borderStyleSolid left');
		$pString .= \HTML\trStart();
		$pString .= \HTML\td($forget->forgetSet());
		$pString .= \HTML\trEnd();
		$pString .= \HTML\tableEnd();
		return $pString;
	}
// Edit forgotten password details
	public function forgetConfigEdit()
	{
		include_once('core/modules/usersgroups/FORGET.php');
		$forget = new FORGET();
		list($success, $message) = $forget->forgetWrite();
		if($success === FALSE)
			$this->badInputLoad($message, 'forget');
		$this->init(array($this->success->text("forgetUpdate"), $this->vars['selectItem']));
		FACTORY_CLOSE::getInstance();
	}
// Email notification config options
	private function notificationConfigDisplay()
	{
		$pString = $this->errorString;
		$pString .= \FORM\hidden("method", "notificationConfigEdit");
		$pString .= \HTML\tableStart('generalTable borderStyleSolid left');
		$pString .= \HTML\trStart();
		if(!$this->session->getVar("mywikindx_Notify"))
		{
			$this->db->formatConditions(array('usersId' => $this->session->getVar('setup_UserId')));
			$recordset = $this->db->select('users', array('usersNotify', 'usersNotifyAddEdit',
				'usersNotifyThreshold', 'usersNotifyTimestamp', 'usersNotifyDigestThreshold'));
			$row = $this->db->fetchRow($recordset);
			$this->session->setVar("mywikindx_Notify", $row['usersNotify']);
			if($row['usersNotifyAddEdit'] == 'A')
			{
				$this->session->setVar("mywikindx_NotifyAdd", TRUE);
				$this->session->setVar("mywikindx_NotifyEdit", TRUE);
			}
			else if($row['usersNotifyAddEdit'] == 'N')
			{
				$this->session->setVar("mywikindx_NotifyAdd", TRUE);
				$this->session->delVar("mywikindx_NotifyEdit");
			}
			else if($row['usersNotifyAddEdit'] == 'E')
			{
				$this->session->setVar("mywikindx_NotifyEdit", TRUE);
				$this->session->delVar("mywikindx_NotifyAdd");
			}
			$this->session->setVar("mywikindx_NotifyThreshold", $row['usersNotifyThreshold']);
			$this->session->setVar("mywikindx_NotifyTimestamp", $row['usersNotifyTimestamp']);
			$this->session->setVar("mywikindx_NotifyDigestThreshold", $row['usersNotifyDigestThreshold']);
		}
		$pString .= \HTML\tdStart();
		$checked = $this->session->getVar("mywikindx_Notify") == 'N' ? TRUE : FALSE;
		$pString .= \HTML\p(\FORM\radioButton(FALSE, "Notify", "N", $checked) . "&nbsp;&nbsp;" .
			$this->messages->text("user", "notifyNone"));
		$checked = $this->session->getVar("mywikindx_Notify") == 'A' ? TRUE : FALSE;
		$pString .= \HTML\p(\FORM\radioButton(FALSE, "Notify", "A", $checked) . "&nbsp;&nbsp;" .
			$this->messages->text("user", "notifyAll"));
		$checked = $this->session->getVar("mywikindx_Notify") == 'M' ? TRUE : FALSE;
		$pString .= \HTML\p(\FORM\radioButton(FALSE, "Notify", "M", $checked) . "&nbsp;&nbsp;" .
			$this->messages->text("user", "notifyMyBib"));
		$checked = $this->session->getVar("mywikindx_Notify") == 'C' ? TRUE : FALSE;
		$pString .= \HTML\p(\FORM\radioButton(FALSE, "Notify", "C", $checked) . "&nbsp;&nbsp;" .
			$this->messages->text("user", "notifyMyCreator"));
		$add = $this->session->issetVar("mywikindx_NotifyAdd") ? 'CHECKED' : FALSE;
		$edit = $this->session->issetVar("mywikindx_NotifyEdit") ? 'CHECKED' : FALSE;
		$pString .= \HTML\p($this->messages->text("user", "notifyAdd") . ":&nbsp;&nbsp;" .
			\FORM\checkbox(FALSE, "NotifyAdd", $add) . BR .
			$this->messages->text("user", "notifyEdit") . ":&nbsp;&nbsp;" .
			\FORM\checkbox(FALSE, "NotifyEdit", $edit));
		$array = array(0 => $this->messages->text("user", "notifyImmediate"), 1 => 1, 7 => 7, 14 => 14, 28 => 28);
		$selected = $this->session->getVar('mywikindx_NotifyThreshold');
		if($selected)
			$pString .= \HTML\p(\FORM\selectedBoxValue($this->messages->text("user", "notifyThreshold"),
			"NotifyThreshold", $array, $selected, 5));
		else
			$pString .= \HTML\p(\FORM\selectFBoxValue($this->messages->text("user", "notifyThreshold"),
			"NotifyThreshold", $array, 5));
		$pString .= \HTML\p(\FORM\textInput($this->messages->text("user", "notifyDigestThreshold"),
			"DigestThreshold", $this->session->getVar('mywikindx_NotifyDigestThreshold'), 5, 255));
		$pString .= \HTML\tdEnd();
		$pString .= \HTML\trEnd();
		$pString .= \HTML\tableEnd();
		return $pString;
	}
// Set email notification
	public function notificationConfigEdit()
	{
		if(!array_key_exists('Notify', $this->vars) || !$this->vars['Notify'])
			$this->badInputLoad($this->errors->text("inputError", "missing"), 'notification');
		$updateArray = array('usersNotify' => $this->vars['Notify']);
		if(array_key_exists('NotifyAdd', $this->vars) && array_key_exists('NotifyEdit', $this->vars))
		{
			$this->session->setVar("mywikindx_NotifyAdd", TRUE);
			$this->session->setVar("mywikindx_NotifyEdit", TRUE);
			$updateArray['usersNotifyAddEdit'] = 'A';
		}
		else if(array_key_exists('NotifyAdd', $this->vars))
		{
			$this->session->setVar("mywikindx_NotifyAdd", TRUE);
			$this->session->delVar("mywikindx_NotifyEdit");
			$updateArray['usersNotifyAddEdit'] = 'N';
		}
		else if(array_key_exists('NotifyEdit', $this->vars))
		{
			$this->session->setVar("mywikindx_NotifyEdit", TRUE);
			$this->session->delVar("mywikindx_NotifyAdd");
			$updateArray['usersNotifyAddEdit'] = 'E';
		}
		else
		{
			$this->session->setVar("mywikindx_NotifyAdd", TRUE);
			$this->session->setVar("mywikindx_NotifyEdit", TRUE);
			$updateArray['usersNotifyAddEdit'] = 'A';
		}
		if(array_key_exists('NotifyThreshold', $this->vars))
		{
			$this->session->setVar("mywikindx_NotifyThreshold", $this->vars['NotifyThreshold']);
			$updateArray['usersNotifyThreshold'] = $this->vars['NotifyThreshold'];
		}
		if(array_key_exists('DigestThreshold', $this->vars))
		{
			$input = trim($this->vars['DigestThreshold']) + 0;
			if(is_int($input) && ($input > 0))
			{
				$this->session->setVar("mywikindx_NotifyDigestThreshold", trim($this->vars['DigestThreshold']));
				$updateArray['usersNotifyDigestThreshold'] = trim($this->vars['DigestThreshold']);
			}
			else
			{
				$this->session->setVar("mywikindx_NotifyDigestThreshold", 100);
				$updateArray['usersNotifyDigestThreshold'] = 100;
			}
		}
		$this->db->formatConditions(array('usersId' => $this->session->getVar('setup_UserId')));
		$this->db->update('users', $updateArray);
		$this->session->setVar("mywikindx_Notify", $this->vars['Notify']);
		$this->init(array($this->success->text("notify"), $this->vars['selectItem']));
		FACTORY_CLOSE::getInstance();
	}
// Display user groups config options
	public function userGroupsConfigDisplay()
	{
		$pString = $this->errorString;
		$pString .= \FORM\hidden("method", "userGroupsConfigEdit");
		$pString .= \HTML\tableStart('generalTable borderStyleSolid left');
		$pString .= \HTML\trStart();
		if(!$groups = $this->user->listUserGroups())
		{
			$pString .=  \HTML\td($this->messages->text("user", "noGroups"));
			$radios = \HTML\p(\FORM\radioButton(FALSE, "method", "createUserGroupInit", TRUE) . "&nbsp;&nbsp;" .
				$this->messages->text("user", "createGroup"), FALSE, "left");
		}
		else
		{
			$pString .= \HTML\td(\FORM\selectFBoxValue($this->messages->text("user", "groups"), "groupId", $groups, 5));
			if(!$this->session->getVar('mywikindx_group_radio'))
				$checked = TRUE;
			else
				$checked = $this->session->getVar('mywikindx_group_radio') == 'create' ? TRUE : FALSE;
			$radios = \HTML\p(\FORM\radioButton(FALSE, "method", "createUserGroupInit", $checked) .
				"&nbsp;&nbsp;" . $this->messages->text("user", "createGroup"), FALSE, "left");
			$checked = $this->session->getVar('mywikindx_group_radio') == 'edit' ? TRUE : FALSE;
			$radios .= \HTML\p(\FORM\radioButton(FALSE, "method", "editUserGroupInit", $checked) .
				"&nbsp;&nbsp;" . $this->messages->text("user", "editGroup"), FALSE, "left");
			$checked = $this->session->getVar('mywikindx_group_radio') == 'delete' ? TRUE : FALSE;
			$radios .= \HTML\p(\FORM\radioButton(FALSE, "method", "deleteUserGroupInit", $checked) .
				"&nbsp;&nbsp;" . $this->messages->text("user", "deleteGroup"), FALSE, "left");
		}
		$pString .= \HTML\td($radios);
		$pString .= \HTML\trEnd();
		$pString .= \HTML\tableEnd();
		$pString .= \HTML\trEnd();
		$pString .= \HTML\tableEnd();
		return $pString;
	}
// Display further user groups settings for creating
	public function createUserGroupInit($error = FALSE)
	{
		$tinymce = FACTORY_LOADTINYMCE::getInstance();
		$this->session->delVar('mywikindx_group_add');
		if(!$error)
		{
			$this->session->delVar('mywikindx_groupTitle');
			$this->session->delVar('mywikindx_groupDescription');
		}
		GLOBALS::setTplVar('heading', $this->messages->text("heading", "myWikindx",
			": " . $this->messages->text("user", "createGroup")));
		$this->session->delVar("mywikindx_groupLock");
		$pString = $error ? \HTML\p($error, "error", "center") : '';
		$pString .= '<script type="text/javascript" src="' . $this->config->WIKINDX_BASE_URL . '/core/modules/usersgroups/mywikindx.js"></script>';
		$pString .= \FORM\formHeader(FALSE);
		$pString .= \FORM\hidden("method", "createUserGroup");
		$pString .= $tinymce->loadBasicTextarea();
		$pString .= \HTML\tableStart();
		$pString .= \HTML\trStart();
		$sessVar = $this->session->issetVar("mywikindx_groupTitle") ?
			\HTML\dbToFormTidy($this->session->getVar("mywikindx_groupTitle")) : FALSE;
		$pString .= \HTML\td(\FORM\textInput($this->messages->text("user", "groupTitle"),
			"title", $sessVar, 50, 255) . " " . \HTML\span('*', 'required'));
		$pString .= \HTML\trEnd();
		$pString .= \HTML\tableEnd();
		$pString .= BR . "&nbsp;" . BR;
		$pString .= \HTML\tableStart();
		$pString .= \HTML\trStart();
		$sessVar = $this->session->issetVar("mywikindx_groupDescription") ?
			\HTML\dbToFormTidy($this->session->getVar("mywikindx_groupDescription")) : FALSE;
		$pString .= \HTML\td(\FORM\textAreaInput($this->messages->text("user", "groupDescription"),
			"description", $sessVar, 80, 10));
		$pString .= \HTML\trEnd();
		$pString .= \HTML\tableEnd();
		$users = $this->user->grabAll(TRUE);
// add 0 => IGNORE to $array
		$temp[0] = $this->messages->text("misc", "ignore");
		foreach($users as $key => $value)
		{
			if($key == $this->session->getVar('setup_UserId'))
				continue;
			$temp[$key] = $value;
		}
		$hint = \HTML\aBrowse('green', '', $this->messages->text("hint", "hint"), '#', "", $this->messages->text("hint", "multiples"));
		$pString .= \HTML\p(\FORM\selectFBoxValueMultiple($this->messages->text("user", "groupUserAdd"),
			"addUsers", $temp, 10) . BR . \HTML\span($hint, 'hint'));
		$jString = "onclick=\"javascript:sendMywikindxInput();return true;\"";
		$pString .= \HTML\p(\FORM\formSubmitButton($this->messages->text("submit", "Add"), FALSE, $jString));
		$pString .= \FORM\formEnd();
		$pString .= \FORM\formHeader("usersgroups_WIKINDXX_CORE&amp;method=init", "onsubmit=\"window.close();return true;\"");
		$pString .= \FORM\hidden("selectItem", "userGroups");
		$pString .= \HTML\p(\FORM\formSubmit($this->messages->text("submit", "Close")));
		$pString .= \FORM\formEnd();
		GLOBALS::addTplVar('content', $pString);
		FACTORY_CLOSENOMENU::getInstance();
	}
// Create the user group
	public function createUserGroup()
	{
		if(array_key_exists('description', $this->vars) && trim($this->vars['description']))
		{
			$description = json_decode($this->vars['description']);
			$this->session->setVar('mywikindx_groupDescription', $description);
			$fields[] = 'usergroupsDescription';
			$values[] = $description;
		}
		if(!$title = json_decode($this->vars['title']))
			$this->createUserGroupInit($this->createUserGroupInit($this->errors->text("inputError", "missing")));
		$this->session->setVar('mywikindx_groupTitle', $title);
		if($this->session->getVar("mywikindx_groupLock"))
			$this->badInputLoad($this->errors->text("done", "group"), 'userGroups');
		$this->checkUserGroupExists($title, FALSE);
		$userId = $this->session->getVar('setup_UserId');
		$fields[] = 'usergroupsTitle';
		$values[] = $title;
		$fields[] = 'usergroupsAdminId';
		$values[] = $userId;
		$this->db->insert('user_groups', $fields, $values);
		$groupId = $this->db->lastAutoId();
		$userIds[] = $userId;
		$users = json_decode($this->vars['addUsers']);
		foreach($users as $userId)
		{
			if(!$userId) // IGNORE
				continue;
			$userIds[] = $userId;
		}
// Insert new users
		foreach($userIds as $id)
			$this->db->insert('user_groups_users', array('usergroupsusersUserId', 'usergroupsusersGroupId'), array($id, $groupId));
		$this->session->setVar("mywikindx_groupLock", TRUE);
		$this->session->delVar('mywikindx_groupDescription');
		$this->session->delVar('mywikindx_groupTitle');
		$this->init(array($this->success->text("groupAdd"), 'userGroups'));
		FACTORY_CLOSE::getInstance();
	}
/**
* Does user group already exist?
* If $groupId, we're editing an existing user group.
*/
	private function checkUserGroupExists($title, $groupId = FALSE)
	{
		if($groupId)
			$this->db->formatConditions(array('usergroupsId' => $groupId), TRUE);
		$recordset = $this->db->select('user_groups', array('usergroupsTitle', 'usergroupsId'));
		while($row = $this->db->fetchRow($recordset))
		{
			if($title == $row['usergroupsTitle'])
				$this->badInputLoad($this->errors->text("inputError", "groupExists"), 'userGroups');
		}
	}
// Display further user groups settings for editing
	public function editUserGroupInit($error = FALSE)
	{
		$tinymce = FACTORY_LOADTINYMCE::getInstance();
		$this->session->delVar('mywikindx_group_edit');
		if(!$error)
		{
			$this->session->delVar('mywikindx_groupTitle');
			$this->session->delVar('mywikindx_groupDescription');
		}
		GLOBALS::setTplVar('heading', $this->messages->text("heading", "myWikindx",
			": " . $this->messages->text("user", "editGroup")));
		$this->session->delVar("mywikindx_groupLock");
		if(!array_key_exists('groupId', $this->vars) || !$this->vars['groupId'])
			$this->badInputLoad($this->errors->text("inputError", "missing"), 'userGroups');
		$this->checkValidUserGroup();
		$groupUsers = array();
		$pString = $error ? \HTML\p($error, "error", "center") : '';
		$pString .= '<script type="text/javascript" src="' . $this->config->WIKINDX_BASE_URL . '/core/modules/usersgroups/mywikindx.js"></script>';
		$pString .= \FORM\formHeader(FALSE);
		$pString .= \FORM\hidden("method", "editUserGroup");
		$pString .= \FORM\hidden("groupId", $this->vars['groupId']);
		$pString .= $tinymce->loadBasicTextarea();
		$pString .= \HTML\tableStart();
		$pString .= \HTML\trStart();
		if($this->session->issetVar("mywikindx_groupTitle"))
		{
			$title = \HTML\dbToFormTidy($this->session->getVar("mywikindx_groupTitle"));
			$description = \HTML\dbToFormTidy($this->session->getVar("mywikindx_groupDescription"));
		}
		else
		{
			$this->db->formatConditions(array('usergroupsId' => $this->vars['groupId']));
			$recordset = $this->db->select('user_groups', array('usergroupsTitle', 'usergroupsDescription'));
			$row = $this->db->fetchRow($recordset);
			$title = \HTML\dbToFormTidy($row['usergroupsTitle']);
			$description = \HTML\dbToFormTidy($row['usergroupsDescription']);
			$this->db->formatConditions(array('usergroupsusersGroupId' => $this->vars['groupId']));
			$this->db->leftJoin('users', 'usersId', 'usergroupsusersUserId');
			$recordset = $this->db->select('user_groups_users',
				array('usergroupsusersUserId', 'usersUsername', 'usersFullname', 'usersAdmin'));
// add 0 => IGNORE to $array
			$groupUsers[0] = $this->messages->text("misc", "ignore");
			while($row = $this->db->fetchRow($recordset))
			{
				if(!$row['usergroupsusersUserId'])
					continue;
				if($row['usergroupsusersUserId'] == $this->session->getVar('setup_UserId'))
					continue;
				$groupUsers[$row['usergroupsusersUserId']] = \HTML\dbToFormTidy($row['usersUsername']);
				if($row['usersFullname'])
					$groupUsers[$row['usergroupsusersUserId']] .= " (" . \HTML\dbToFormTidy($row['usersFullname']) . ")";
				if($row['usersAdmin'] == 'Y')
					$groupUsers[$row['usergroupsusersUserId']] .= " ADMIN";
			}
		}
		$pString .= \HTML\td(\FORM\textInput($this->messages->text("user", "groupTitle"), "title",
			$title, 50, 255) . " " . \HTML\span('*', 'required'));
		$pString .= \HTML\trEnd();
		$pString .= \HTML\tableEnd();
		$pString .= BR . "&nbsp;" . BR;
		$pString .= \HTML\tableStart();
		$pString .= \HTML\trStart();
		$pString .= \HTML\td(\FORM\textAreaInput($this->messages->text("user", "groupDescription"),
			"description", $description, 80, 10));
		$pString .= \HTML\trEnd();
		$pString .= \HTML\tableEnd();
		$users = $this->user->grabAll(TRUE);
// add 0 => IGNORE to $array
		$temp[0] = $this->messages->text("misc", "ignore");
		foreach($users as $key => $value)
		{
			if($key == $this->session->getVar('setup_UserId'))
				continue;
			if(array_key_exists($key, $groupUsers))
				continue;
			$temp[$key] = $value;
		}
		$pString .= BR . "&nbsp;" . BR;
		$pString .= \HTML\tableStart();
		$pString .= \HTML\trStart();
		$hint = \HTML\aBrowse('green', '', $this->messages->text("hint", "hint"), '#', "", $this->messages->text("hint", "multiples"));
		if(sizeof($groupUsers) > 1)
			$pString .= \HTML\td(\FORM\selectFBoxValueMultiple($this->messages->text("user",
			"groupUserDelete"), "deleteUsers", $groupUsers, 10) . BR . \HTML\span($hint, 'hint'));
		if(sizeof($temp) > 1)
			$pString .= \HTML\td(\FORM\selectFBoxValueMultiple($this->messages->text("user", "groupUserAdd"),
				"addUsers", $temp, 10) . BR . \HTML\span($hint, 'hint'));
		$pString .= \HTML\trEnd();
		$pString .= \HTML\tableEnd();
		
		$jString = "onclick=\"javascript:sendMywikindxInput();return true;\"";
		$pString .= \HTML\p(\FORM\formSubmitButton($this->messages->text("submit", "Edit"), FALSE, $jString));
		$pString .= \FORM\formEnd();
		$pString .= \FORM\formHeader("usersgroups_WIKINDXX_CORE&amp;method=init", "onsubmit=\"window.close();return true;\"");
		$pString .= \FORM\hidden("selectItem", "userGroups");
		$pString .= \HTML\p(\FORM\formSubmit($this->messages->text("submit", "Close")));
		$pString .= \FORM\formEnd();
		GLOBALS::addTplVar('content', $pString);
		FACTORY_CLOSENOMENU::getInstance();
	}
// edit a user group
	public function editUserGroup()
	{
		if($this->session->getVar("mywikindx_groupLock"))
			$this->badInputLoad($this->errors->text("done", "group"), 'userGroups');
		if(!array_key_exists('groupId', $this->vars) || !$this->vars['groupId'])
			$this->badInputLoad($this->errors->text("inputError", "missing"), 'userGroups');
		if(!$title = trim($this->vars['title']))
			$this->badInputLoad($this->errors->text("inputError", "missing"), 'userGroups');
		$title = json_decode($title);
			$this->session->setVar('mywikindx_groupTitle', $title);
		$this->checkValidUserGroup();
		$this->checkUserGroupExists($title, $this->vars['groupId']); // Check for title duplicate
		if($description = trim($this->vars['description']))
		{
			$description = json_decode($description);
			$this->session->setVar('mywikindx_groupDescription', $description);
			$updateArray['usergroupsDescription'] = $description;
		}
		else
		{
			$this->db->formatConditions(array('usergroupsId' => $this->vars['groupId']));
			$this->db->updateNull('user_groups', 'usergroupsDescription');
		}
		$updateArray['usergroupsTitle'] = $title;
		$this->db->formatConditions(array('usergroupsId' => $this->vars['groupId']));
		$this->db->update('user_groups', $updateArray);
// delete any users
		if(array_key_exists('deleteUsers', $this->vars))
		{
			$users = json_decode($this->vars['deleteUsers']);
			foreach($users as $userId) // to be deleted
			{
				if(!$userId) // 'IGNORE'
					continue;
				$this->db->formatConditions(array('usergroupsusersGroupId' => $this->vars['groupId']));
				$this->db->formatConditions(array('usergroupsusersUserId' => $userId));
				$this->db->delete('user_groups_users');
			}
		}
// Insert new users
		$addUsers = array();
		$users = json_decode($this->vars['addUsers']);
		foreach($users as $userId)
		{
			if(!$userId) // IGNORE
				continue;
			$addUsers[] = $userId;
		}
		foreach($addUsers as $id)
			$this->db->insert('user_groups_users', array('usergroupsusersUserId', 'usergroupsusersGroupId'),
				array($id, $this->vars['groupId']));
		$this->session->setVar("mywikindx_groupLock", TRUE);
		$this->session->delVar('mywikindx_groupDescription');
		$this->session->delVar('mywikindx_groupTitle');
		$this->init(array($this->success->text("groupEdit"), 'userGroups'));
		FACTORY_CLOSE::getInstance();
	}
// Display further user groups settings for deleting
	public function deleteUserGroupInit($error = FALSE)
	{
		if(!array_key_exists('groupId', $this->vars) || !$this->vars['groupId'])
			$this->badInputLoad($this->errors->text("inputError", "invalid"), 'userGroups');
		$this->checkValidUserGroup();
		GLOBALS::setTplVar('heading', $this->messages->text("heading", "myWikindx",
			": " . $this->messages->text("user", "deleteGroup")));
		$pString = $error ? \HTML\p($error, "error", "center") : '';
		$this->db->formatConditions(array('usergroupsId' => $this->vars['groupId']));
		$title = \HTML\strong($this->db->selectFirstField('user_groups', 'usergroupsTitle'));
		$pString .= \HTML\p($this->messages->text("user", "deleteConfirmGroup") . ":&nbsp;&nbsp;" .
			\HTML\dbToHtmlTidy($title));
		$pString .= \HTML\p($this->messages->text("user", "deleteGroup2"));
		$this->session->delVar('mywikindx_group_delete');
		$this->session->delVar("mywikindx_groupLock");
		$pString .= '<script type="text/javascript" src="' . $this->config->WIKINDX_BASE_URL . '/core/modules/usersgroups/mywikindx.js"></script>';
		$pString .= \FORM\formHeader(FALSE);
		$pString .= \FORM\hidden("method", "deleteUserGroup");
		$pString .= \FORM\hidden("groupId", $this->vars['groupId']);
		$jString = "onclick=\"javascript:sendMywikindxInput();return true;\"";
		$pString .= \HTML\p(\FORM\formSubmitButton($this->messages->text("submit", "Delete"), FALSE, $jString));
		$pString .= \FORM\formEnd();
		$pString .= \FORM\formHeader("usersgroups_WIKINDXX_CORE&amp;method=init", "onsubmit=\"window.close();return true;\"");
		$pString .= \FORM\hidden("selectItem", "userGroups");
		$pString .= \HTML\p(\FORM\formSubmit($this->messages->text("submit", "Close")));
		$pString .= \FORM\formEnd();
		GLOBALS::addTplVar('content', $pString);
		FACTORY_CLOSENOMENU::getInstance();
	}
// Delete user group
	function deleteUserGroup()
	{
		if(!array_key_exists('groupId', $this->vars) || !$this->vars['groupId'])
			$this->badInputLoad($this->errors->text("inputError", "invalid"), 'userGroups');
		$this->checkValidUserGroup();
// Get any bibliographyIds and delete those bibliographies
		$this->db->formatConditions(array('userbibliographyUserGroupId' => $this->vars['groupId']));
		$recordset = $this->db->select('user_bibliography', 'userbibliographyId');
		while($row = $this->db->fetchRow($recordset))
			$bibIds[] = $row['userbibliographyId'];
		if(isset($bibIds))
		{
			$this->db->formatConditionsOneField($bibIds, 'userbibliographyresourceBibliographyId');
			$this->db->delete('user_bibliography_resource');
		}
		$this->db->formatConditions(array('userbibliographyUserGroupId' => $this->vars['groupId']));
		$this->db->delete('user_bibliography');
// delete users from usergroup
		$this->db->formatConditions(array('usergroupsusersGroupId' => $this->vars['groupId']));
		$this->db->delete('user_groups_users');
// Delete usergroup
		$this->db->formatConditions(array('usergroupsId' => $this->vars['groupId']));
		$this->db->delete('user_groups');
		$this->init(array($this->success->text("groupDelete"), 'userGroups'));
		FACTORY_CLOSE::getInstance();
	}
// check this user can edit, delete and deleteFrom from user groups
	private function checkValidUserGroup()
	{
		$this->db->formatConditions(array('usergroupsId' => $this->vars['groupId']));
		$adminId = $this->db->selectFirstField('user_groups', 'usergroupsAdminId');
		if($this->session->getVar('setup_UserId') != $adminId)
			$this->badInputLoad($this->errors->text("inputError", "invalid"), 'userGroups');
	}
// Error handling
	private function badInputLoad($error, $item = FALSE)
	{
		if($item)
			$this->badInput->close($error, $this, array('init', $item));
		else
			$this->badInput->close($error, $this, 'init');
	}
}
