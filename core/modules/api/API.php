<?php
/**
 * WIKINDX : Bibliographic Management system.
 * @link https://wikindx.sourceforge.io/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2017 Mark Grimshaw-Aagaard <sirfragalot@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 */

/**
* \brief Provides interface for external programs and for Wikindx AJAX.
* Currently it is the "default" class that API functions get put into that
* don't yet fit into other classes.  Eventually, this class may be removed altogether.
*/
class API
{
	private $db;
	private $vars;
	private $config;
	private $icons;
	private $stmt;
	private $errors;
	private $messages;
	private $bibtex;
	private $bibStyle;
	private $stats;
	private $session;
	private $user;
	private $commonBib;
	private $badInput;
	private $common;
	private $abstract;
	private $note;
	private $url;

	public function __construct()
	{
		$this->db = FACTORY_DB::getInstance();
		$this->vars = GLOBALS::getVars();
		$this->config = FACTORY_CONFIG::getInstance();
		$this->icons = FACTORY_LOADICONS::getInstance();
		$this->stmt = FACTORY_SQLSTATEMENTS::getInstance();
		$this->errors = FACTORY_ERRORS::getInstance();
		$this->messages = FACTORY_MESSAGES::getInstance();
		$this->bibtex = FACTORY_EXPORTBIBTEX::getInstance();
		$this->bibStyle = FACTORY_BIBSTYLE::getInstance();
		$this->stats = FACTORY_STATISTICS::getInstance();
		$this->session = FACTORY_SESSION::getInstance();
		$this->user = FACTORY_USER::getInstance();
		$this->commonBib = FACTORY_BIBLIOGRAPHYCOMMON::getInstance();
		$this->badInput = FACTORY_BADINPUT::getInstance();
		$this->common = FACTORY_RESOURCECOMMON::getInstance();
		$this->url = FACTORY_URL::getInstance();
		include_once('core/modules/resource/RESOURCEABSTRACT.php');
		$this->abstract = new RESOURCEABSTRACT();
		include_once('core/modules/resource/RESOURCENOTE.php');
		$this->note = new RESOURCENOTE();
		include_once('core/modules/resource/RESOURCEMETA.php');
		$this->meta = new RESOURCEMETA();
		GLOBALS::setTplVar('heading', $this->messages->text("heading", "resources"));
	}
	public function fetchId()
	{
		if(!array_key_exists('bibtexKey', $this->vars) || !$this->vars['bibtexKey'])
			$this->badInput->close($this->errors->text("inputError", "missing"));

		$this->db->formatConditions(array('resourceBibtexKey' => $this->vars['bibtexKey']));
		$resultset = $this->db->select('resource', array('resourceId'));
		if(!$this->db->numRows($resultset))
			$this->badInput->close($this->messages->text("resources", "noResult"));
		$row = $this->db->fetchRow($resultset);

		GLOBALS::addTplVar('content', $row['resourceId']);
        FACTORY_CLOSERAW::getInstance();
	}
	public function getBibtex()
	{
		if(!array_key_exists('id', $this->vars) || !$this->vars['id'])
			$this->badInput->close($this->errors->text("inputError", "missing"));

		$this->stmt->listJoin();
		$this->db->formatConditions(array('resourceId' => $this->vars['id']));
		$resultset = $this->db->select('resource', $this->stmt->listFields());
		if(!$this->db->numRows($resultset))
			$this->badInput->close($this->messages->text("resources", "noResult"));
		$row = $this->db->fetchRow($resultset);
		$this->icons->setupIcons();

		$this->bibStyle->process($row);
		$body = $this->bibtex->export($row, $this->bibStyle->coinsCreators);

		GLOBALS::addTplVar('content', $body);
        FACTORY_CLOSERAW::getInstance();
	}
	private static function createBibtexRow( $varname, $val )
	{
		return "| $varname = $val". LF;
	}
}
