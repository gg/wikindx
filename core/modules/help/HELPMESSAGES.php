<?php
/**
 * WIKINDX : Bibliographic Management system.
 * @link https://wikindx.sourceforge.io/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2017 Mark Grimshaw-Aagaard <sirfragalot@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 */

/**
*	HELPMESSAGES class
*
*	Context help
*
*/
class HELPMESSAGES
{
private $vars;
private $help;
private $messages;
private $errors;
private $badInput;
private $icons;
private $session;

	public function __construct()
	{
		$this->vars = GLOBALS::getVars();
		$this->help = FACTORY_HELP::getInstance();
		$this->errors = FACTORY_ERRORS::getInstance();
		$this->badInput = FACTORY_BADINPUT::getInstance();
		$this->messages = FACTORY_MESSAGES::getInstance();

		$this->icons = FACTORY_LOADICONS::getInstance();
		$this->session = FACTORY_SESSION::getInstance();
		$this->icons->setupIcons('help');
		GLOBALS::setTplVar('heading', '');
	}
	public function init()
	{
		if(array_key_exists('message', $this->vars) && $this->vars['message'])
			GLOBALS::addTplVar('content', $this->help->text($this->vars['message']) . \HTML\p(\FORM\closePopup($this->messages->text("misc", "closePopup"))));
		else
		{
			$this->badInput->closeType = 'closePopup';
			$this->badInput->close($this->errors->text('inputError', 'missing'));
		}
		FACTORY_CLOSEPOPUP::getInstance();
	}
	public function createLink($message)
	{
		if(file_exists(WIKINDX_DIR_TEMPLATES . DIRECTORY_SEPARATOR . $this->session->getVar('setup_Template') . '/aside.txt'))
		{
			$jScript = "javascript:asideHelpToggle()";
			GLOBALS::addTplVar('asideHelp', $this->help->text($message));
		}
		else
			$jScript = "javascript:coreOpenPopup('index.php?action=help_HELPMESSAGES_CORE&amp;message=" . $message . "', 80)";
		return \HTML\a($this->icons->helpLink, $this->icons->help, $jScript);
	}
}
