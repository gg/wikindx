<?php
/**
 * WIKINDX : Bibliographic Management system.
 * @link https://wikindx.sourceforge.io/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2018-2019 Mark Grimshaw-Aagaard <sirfragalot@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 */

/*****
*	EMAIL class.
*****/
class EMAIL
{
private $db;
private $vars;
private $config;
private $smtp;
private $messages;
private $errors;
private $success;
private $session;
private $badInput;
private $title;
private $bibStyle;
private $user;
private $stmt;
private $res;
private $usersThreshold = array();
private $titles = array();
private $allAddedIds = array();
private $allEditedIds = array();

	public function __construct()
	{
		$this->db = FACTORY_DB::getInstance();
		$this->vars = GLOBALS::getVars();
		$this->config = FACTORY_CONFIG::getInstance();
		$this->smtp = FACTORY_MAIL::getInstance();
		$this->messages = FACTORY_MESSAGES::getInstance();
		$this->errors = FACTORY_ERRORS::getInstance();
		$this->success = FACTORY_SUCCESS::getInstance();
		$this->session = FACTORY_SESSION::getInstance();
		$this->badInput = FACTORY_BADINPUT::getInstance();
	}
/**
* Start functions for emailing users their preferences or 'my wikindx' details.
*/
	public function userEdit()
	{
// do nothing if email is not turned on
		if(!$this->config->WIKINDX_MAIL_SERVER)
			return TRUE;
		$link = $this->smtp->scriptPath();
		if(array_key_exists('email', $this->vars))
		{
			$email = $this->vars['email'];
			$subject = "WIKINDX Registration Confirmation";
			$message = $this->messages->text("user", "emailText3") . "\n\nWIKINDX:\t\t$link\n\nUSERNAME:\t\t" .
				trim($this->vars['uname']) . "\n". LF;
			$this->smtp->sendEmail($email, $subject, $message);
			return TRUE;
		}
		return FALSE;
	}
/**
* Email a user details of initial self-registration
*/
	public function register($hashKey, $email)
	{
// do nothing if email is not turned on
		if(!$this->config->WIKINDX_MAIL_SERVER)
			return TRUE;
		$scriptPath = $this->smtp->scriptPath();
		$subject = "WIKINDX Registration";
		$link = "$scriptPath" . "/index.php?action=usersgroups_REGISTER_CORE&method=registerConfirm&hashKey=$hashKey";
		$message = $this->messages->text("user", "emailText") . "\n". LF . $link . "\n". LF;
		return ($this->smtp->sendEmail($email, $subject, $message));
	}
/**
* Complete user self-registration
*/
	public function registerUserAdd($passwordShow = FALSE)
	{
// do nothing if email is not turned on
		if(!$this->config->WIKINDX_MAIL_SERVER)
			return TRUE;
		$link = $this->smtp->scriptPath();
		$email = $this->vars['email'];
		$subject = "WIKINDX Registration Confirmation";
		if($passwordShow)
			$message = $this->messages->text("user", "emailText2") . "\n\nWIKINDX:\t\t$link\n\nUSERNAME:\t\t" .
				trim($this->vars['username']) . "\n\nPASSWORD:\t\t" . trim($this->vars['password']) . "\n". LF;
		else
			$message = $this->messages->text("user", "emailText2") . "\n\nWIKINDX:\t\t$link\n\nUSERNAME:\t\t" .
				trim($this->vars['username']) . "\n". LF;
		if(!$this->smtp->sendEmail($email, $subject, $message))
			return FALSE;
// If needed, email admin about new user
		$co = FACTORY_CONFIGDBSTRUCTURE::getInstance();
		$email = $co->getOne('configEmailNewRegistrations');
		if($email && !$this->session->getVar('setup_Superadmin'))
		{
			$message = "A new user has registered for" . "\n\nWIKINDX:\t\t$link\n\nUSERNAME:\t\t" .
				trim($this->vars['username']) . "\n". LF;
			if($this->vars['fullname'])
				$message .= "FULLNAME:\t\t" . trim($this->vars['fullname']) . "\n". LF;
			if(!$this->smtp->sendEmail($email, $subject, $message))
				return FALSE;
		}
		return TRUE;
	}
	public function registerRequest()
	{
// do nothing if email is not turned on
		if(!$this->config->WIKINDX_MAIL_SERVER)
			return TRUE;
		$co = FACTORY_CONFIGDBSTRUCTURE::getInstance();
		$adminEmail = $co->getOne('configEmailNewRegistrations');
		if(!$adminEmail)
			return FALSE;
		$subject = "WIKINDX Registration Request";
		$message = $this->messages->text("user", "emailText4") . "\n". LF;

		return ($this->smtp->sendEmail($adminEmail, $subject, $message));
	}
/**
* Admin accepting/declining registration requests
*/
	public function registerRequestManage($registerIds)
	{
		$scriptPath = $this->smtp->scriptPath();
		foreach($registerIds as $id => $value)
		{
			$this->db->formatConditions(array('userregisterId' => $id));
			$this->db->formatConditions(array('userregisterConfirmed' => 'N'));
			$resultset = $this->db->select('user_register', array('userregisterEmail', 'userregisterHashKey'));
			$row = $this->db->fetchRow($resultset);
// Email user
			$email = $row['userregisterEmail'];
			$subject = "WIKINDX Registration Confirmation";
			if($value == 'accept')
			{
				$link = "$scriptPath" . "/index.php?action=usersgroups_REGISTER_CORE&method=registerConfirm&hashKey=" . $row['userregisterHashKey'];
				$message = $this->messages->text("user", "emailText") . "\n". LF . $link . "\n". LF;
			}
			else
				$message = $this->messages->text("user", "emailText5", " $scriptPath") . "\n". LF;
// do nothing if email is not turned on
			if($this->config->WIKINDX_MAIL_SERVER)
			{
				if(!$this->smtp->sendEmail($email, $subject, $message))
					return FALSE;
			}
// Delete those declined and confirm those accepted
			$this->db->formatConditions(array('userregisterId' => $id));
			if($value == 'accept')
				$this->db->update('user_register', array('userregisterConfirmed' => 'Y'));
			else
				$this->db->delete('user_register');
		}
		return TRUE;
	}
/**
* START functions for emailing a resource to a friend
* Called from index.php
*/
// Display form in pop-up for emailing a link to a friend
	public function emailFriendDisplay($error = FALSE)
	{
		if(array_key_exists('id', $this->vars))
			$hyperlink = $this->smtp->scriptPath() . "/index.php?action=resource_RESOURCEVIEW_CORE&id=" . $this->vars['id'];
		GLOBALS::setTplVar('heading', $this->messages->text("heading", "emailFriend"));
		$pString = $error ? \HTML\p($error, "error", "center") : FALSE;
		$pString .= \FORM\formHeader("email_EMAIL_CORE");
		$pString .= \FORM\hidden('method', 'emailFriend');
		$address = $this->session->getVar("emailFriend_Address");
		$subject = $this->session->getVar("emailFriend_Subject");
		if(isset($hyperlink))
			$text = "\n\n$hyperlink";
		else
			$text = $this->session->getVar("emailFriend_Text");
		$pString .= \HTML\p(\FORM\textInput($this->messages->text("misc", "emailFriendAddress"),
			"emailFriend_address", $address, 80) . BR .
			\HTML\span($this->messages->text('hint', 'emailFriendAddress'), 'hint'));
		$pString .= \HTML\p(\FORM\textInput($this->messages->text("misc", "emailFriendSubject"),
			"emailFriend_subject", $subject, 80));
		$pString .= \HTML\p(\FORM\textareaInput($this->messages->text("misc", "emailFriendText"),
			"emailFriend_text", $text, 80, 10));
		$pString .= \HTML\p(\FORM\formSubmit($this->messages->text("submit", "Email")), FALSE, "right");
		$pString .= \FORM\formEnd();
		GLOBALS::addTplVar('content', $pString);
		FACTORY_CLOSENOMENU::getInstance();
	}
// Send email to friend
	public function emailFriend()
	{
// do nothing if email is not turned on
		if(!$this->config->WIKINDX_MAIL_SERVER)
			return TRUE;
		list($addresses, $subject, $text) = $this->checkFriendInput();
		GLOBALS::setTplVar('heading', $this->messages->text("heading", "emailFriend"));
		if(!$this->smtp->sendEmail($addresses, $subject, $text))
			$this->badInput->close($this->errors->text('inputError', 'mail2'), $this, 'emailFriendDisplay');
		$this->session->clearArray('emailFriend');
		GLOBALS::addTplVar('content', $this->success->text('emailFriend'));
		FACTORY_CLOSENOMENU::getInstance();
	}
	private function checkFriendInput()
	{
		$address = $subject = $text = FALSE;
		if(array_key_exists('emailFriend_address', $this->vars))
			$address = trim($this->vars['emailFriend_address']);
		if(array_key_exists('emailFriend_subject', $this->vars))
			$subject = trim($this->vars['emailFriend_subject']);
		if(array_key_exists('emailFriend_text', $this->vars))
			$text = trim($this->vars['emailFriend_text']);
		$this->session->setVar('emailFriend_Address', $address);
		$this->session->setVar('emailFriend_Subject', $subject);
		$this->session->setVar('emailFriend_Text', $text);
		if(!$address || !$subject || !$text)
			$this->badInput->close($this->errors->text('inputError', 'missing'), $this, 'emailFriendDisplay');
		return array($address, $subject, $text);
	}
/**
* Emailing username::password to forgetful user
*/
	public function forgetProcess($username, $password)
	{
// do nothing if email is not turned on
		if(!$this->config->WIKINDX_MAIL_SERVER)
			return TRUE;
		$password = time();
		$link = $this->smtp->scriptPath();
		$email = trim($this->vars['email']);
		$subject = "Password Reset";
		$message = $this->messages->text("user", "forget9");
		$message .= "\n". LF . $this->title . ":\t\t\t\t$link";
		$message .= "\n\nUSERNAME:\t\t\t\t" . $username;
		$message .= "\n\nTEMPORARY PASSWORD:\t\t" . $password;
		$message .= "\n". LF;

		return ($this->smtp->sendEmail($email, $subject, $message));
	}
/**
* Email news items to users
*/
	public function news($title, $news)
	{
// do nothing if email is not turned on
		if(!$this->config->WIKINDX_MAIL_SERVER)
			return TRUE;
		$news = preg_replace('/\<br(\s*)?\/?\>/ui', CR.LF, $news);
		$news = preg_replace('/\<p(\s*)?\/?\>/ui', "\r\n\r". LF, $news);
		$news = html_entity_decode(\HTML\stripHtml(stripslashes($news)));
		$wikindxTitle = \HTML\stripHtml($this->config->WIKINDX_TITLE);
		$subject = "$wikindxTitle News";
		$message = "\n\n$title\n". LF;
		$message .= "$news\n". LF;
		$recordset = $this->db->select('users', 'usersEmail');
		while($row = $this->db->fetchRow($recordset))
		{
			if(!$row['usersEmail']) // This should only happen if superadmin has not entered email
				continue;
			$addresses[] = $row['usersEmail'];
		}
		if(!isset($addresses))
			return TRUE;

		return ($this->smtp->sendEmail(array_unique($addresses), $subject, $message));
	}
/**
* Notify users of resource additions and edits
*/
	public function notify($resourceId, $newResource = FALSE)
	{
// do nothing if admin does not allow notification or email is not turned on
		if(!$this->config->WIKINDX_MAIL_SERVER || !$this->session->getVar('setup_Notify'))
			return TRUE;
		$this->bibStyle = FACTORY_BIBSTYLE::getInstance();
		$this->bibStyle->output = 'plain';
		$this->user = FACTORY_USER::getInstance();
		$this->stmt = FACTORY_SQLSTATEMENTS::getInstance();
		$this->res = FACTORY_RESOURCECOMMON::getInstance();
		$subject = "Resource Notification";
		if(!$this->emailImmediate($resourceId, $newResource, $subject))
			return FALSE;
// Now deal with users with a set email threshold
		if(sizeof($this->usersThreshold) > 0)
		{
			if(!$this->emailThreshold($resourceId, $subject))
				return FALSE;
		}
		return TRUE; // success
	}
/**
* Email those with a notification threshold set
*/
	private function emailThreshold($resourceId, $subject)
	{
		$userId = $this->session->getVar('setup_UserId');
		$this->db->formatConditions(array('usersId' => $userId));
		$digestThreshold = $this->db->selectFirstField('users', 'usersNotifyDigestThreshold');
		$size = $this->grabResources($digestThreshold);
		if(!$size && empty($this->allAddedIds) && empty($this->allEditedIds))
			return TRUE; // nothing to do
		foreach($this->usersThreshold as $userId => $userArray)
		{
			if(!$userArray['email']) // This should only happen if superadmin has not entered email
				continue;
			if(!$size)
			{
				$message = $this->messages->text("user", "notifyMass4") . "\n\n\n". LF; // reset each time
				$notifyArray = array(); // reset each time
// User wants notification only on new resources
				if(!empty($this->allAddedIds) && ($userArray['notifyAddEdit'] == 'N'))
					$notifyArray = $this->grabTitlesThreshold($userArray, $this->allAddedIds);
// User wants notification only on edited resources
				else if(!empty($this->allEditedIds) && ($userArray['notifyAddEdit'] == 'E'))
					$notifyArray = $this->grabTitlesThreshold($userArray, $this->allEditedIds);
				if(empty($notifyArray) && $userArray['notify'] == 'A') // notify on all resources
// NB, if resource has not been edited, editedTimestamp is same as addedTimestamp
					$notifyArray = $this->grabTitlesThreshold($userArray, $this->allEditedIds);
// notify on resources in a user's bibliography
				else if(($userArray['notify'] == 'M'))
				{
					if(empty($notifyArray) && !empty($this->allEditedIds))
						$newArray = $this->allEditedIds;
					else if(!empty($notifyArray))
						$newArray = $notifyArray;
					else
						continue;
					$remainIds = array();
					$this->db->formatConditions(array('userbibliographyId' => $userId));
					$this->db->formatConditions(array('userbibliographyresourceResourceId' => $resourceId));
					$this->db->leftJoin('user_bibliography_resource', 'userbibliographyresourceBibliographyId', 'userbibliographyId');
					$recordset = $this->db->select('user_bibliography', 'userbibliographyId');
					if(!$this->db->numRows($recordset)) // This resource not in user's bibliography
						continue;
					while($row = $this->db->fetchRow($recordset))
					{
						$bibs = UTF8::mb_explode(',', $row['bibliography']);
						foreach($newArray as $id => $field)
						{
							if(array_search($id, $bibs) !== FALSE)
								$remainIds[$id] = $field;
						}
					}
					if(empty($notifyArray)) // $field is not a formatted title but is unixTimestamp from $this->allEditedIds
						$notifyArray = $this->grabTitlesThreshold($userArray, $remainIds);
					else
						$notifyArray = $remainIds;
				}
// notify if user is a creator of this resource and resourceId is not FALSE (e.g. notify from a mass bibliography import)
				else if($resourceId && ($userArray['notify'] == 'C'))
				{
					if(empty($notifyArray) && !empty($this->allEditedIds))
						$newArray = $this->allEditedIds;
					else if(!empty($notifyArray))
						$newArray = $notifyArray;
					else
						continue;
					$remainIds = array();
					$this->db->formatConditions(array('resourcecreatorResourceId' => $resourceId));
					$this->db->leftJoin('creator', $this->db->formatFields('creatorSameAs'),
						$this->db->tidyInput($userArray['creatorId']), FALSE);
					$recordset = $this->db->select('resource_creator', 'resourcecreatorResourceId', TRUE);
					if(!$this->db->numRows($recordset)) // This resource not in user's bibliography
						continue;
					while($row = $this->db->fetchRow($recordset))
					{
						foreach($newArray as $id => $field)
						{
							if($id = $row['resourcecreatorResourceId'])
								$remainIds[$id] = $field;
						}
					}
					if(empty($notifyArray)) // $field is not a formatted title but is unixTimestamp from $this->allEditedIds
						$notifyArray = $this->grabTitlesThreshold($userArray, $remainIds);
					else
						$notifyArray = $remainIds;
				}
				if(empty($notifyArray))
					continue;
// If more than xxx added resources, simply grab the number of added resources
				$size = sizeof($notifyArray);
			}
			if($size > $digestThreshold)
				$message = $this->messages->text("user", "notifyMass3", $size);
			else
			{
				if(!isset($notifyArray))
					continue;
				$message .= join("\n". LF, $notifyArray);
			}
			if(!$this->smtp->sendEmail($userArray['email'], $subject, $message))
				return FALSE;
// set this user's users.notifyTimestamp to current date
			$this->db->formatConditions(array('usersId' => $userId));
			$this->db->updateTimestamp('users', array('usersNotifyTimestamp' => 'CURRENT_TIMESTAMP'));
		}
		return TRUE;
	}
/**
* Deal with those requiring immediate notification
*/
	private function emailImmediate($resourceId, $newResource, $subject)
	{
		$this->earliestUserUnixTimestamp = $this->earliestNotifyUnixTimestamp = $this->greatestThreshold = FALSE;
		$userId = $this->session->getVar('setup_UserId');
		$this->db->formatConditions(array('usersId' => $userId));
		$digestThreshold = $this->db->selectFirstField('users', 'usersNotifyDigestThreshold');
// Are there any users wanting notification?
		$this->db->formatConditions(array('usersId' => $userId), TRUE); // 'TRUE' is NOT EQUAL
		$this->db->formatConditions(array('usersNotify' => 'N'), TRUE); // 'TRUE' is NOT EQUAL
		$recordset = $this->db->selectWithExceptions('users', array('usersId', 'usersEmail', 'usersNotify',
			'usersNotifyAddEdit', 'usersNotifyThreshold', 'usersIsCreator',
			array("UNIX_TIMESTAMP(usersNotifyTimestamp)" => 'usersUnixNotifyTimestamp'),
			array("UNIX_TIMESTAMP(usersTimestamp)" => 'usersUnixTimestamp')));
		if(!$this->db->numRows($recordset)) // nothing to do
			return TRUE;
		while($row = $this->db->fetchRow($recordset))
		{
			if($row['usersNotifyThreshold'] > 0)
			{
// Store greatest user notification threshold
				if(!$this->greatestThreshold || ($row['usersNotifyThreshold'] > $this->greatestThreshold))
					$this->greatestThreshold = $row['usersNotifyThreshold'];
// Store earliest user notification timestamp
				if(!$this->earliestNotifyUnixTimestamp || ($row['usersUnixNotifyTimestamp']
					< $this->earliestNotifyUnixTimestamp))
					$this->earliestNotifyUnixTimestamp = $row['usersUnixNotifyTimestamp'];
// Store earliest user notification timestamp
				if(!$this->earliestUserUnixTimestamp || ($row['usersUnixTimestamp'] < $this->earliestUserUnixTimestamp))
					$this->earliestUserUnixTimestamp = $row['usersUnixTimestamp'];
				$this->usersThreshold[$row['usersId']] = array(
					'email' => $row['usersEmail'],
					'notify' => $row['usersNotify'],
					'notifyAddEdit' => $row['usersNotifyAddEdit'],
					'notifyThreshold' => $row['usersNotifyThreshold'],
					'unixNotifyTimestamp' => $row['usersUnixNotifyTimestamp'],
					'unixTimestamp' => $row['usersUnixTimestamp'],
					'creatorId' => $row['usersIsCreator']
				);
			}
			else
				$users[$row['usersId']] = array(
					'email' => $row['usersEmail'],
					'notify' => $row['usersNotify'],
					'notifyAddEdit' => $row['usersNotifyAddEdit'],
					'creatorId' => $row['usersIsCreator']
				);
		}
		if(!isset($users)) // nothing to do
			return TRUE;
// Get this user's name (the user adding/editing a resource)
		$userAddEdit = $this->user->displayUserAddEditPlain($userId);
// Grab resource details if single
		if($resourceId)
		{
			list($title, $notifyMessage) = $this->formatTitle($resourceId, $userAddEdit, $digestThreshold);
			$message = $notifyMessage . "\n\n$title\n". LF;
		}
		else // mass import from bibliography
		{
			$message = $this->messages->text("user", "notifyMass1", $userAddEdit);
			$message .= ' ' . $this->messages->text("user", "notifyMass2");
		}
		$addresses = array();
		foreach($users as $userId => $user)
		{
// User wants notification only on new resources
			if(($user['notifyAddEdit'] == 'N') && !$newResource)
				continue;
// User wants notification only on edited resources
			if(($user['notifyAddEdit'] == 'E') && $newResource)
				continue;
			if(!$user['email']) // This should only happen if superadmin has not entered email
				continue;
// notify on resources in a user's bibliography (works for only a single resourceId)
			if($resourceId && ($user['notify'] == 'M') && !is_array($resourceId))
			{
				$this->db->formatConditions(array('userbibliographyId' => $userId));
				$this->db->formatConditions(array('userbibliographyresourceResourceId' => $resourceId));
				$this->db->leftJoin('user_bibliography_resource', 'userbibliographyresourceBibliographyId', 'userbibliographyId');
				$recordset2 = $this->db->select('user_bibliography', 'userbibliographyId');
				if(!$this->db->numRows($recordset2)) // This resource not in user's bibliography
					continue;
			}
// notify if user is a creator of this resource
			else if($resourceId && ($user['notify'] == 'C'))
			{
				$this->db->formatConditions(array('resourcecreatorResourceId' => $resourceId));
				$this->db->leftJoin('creator', $this->db->formatFields('creatorSameAs'), $this->db->tidyInput($user['creatorId']), FALSE);
				$recordset2 = $this->db->select('resource_creator', 'resourcecreatorResourceId', TRUE);
				if(!$this->db->numRows($recordset2)) // This resource not in user's bibliography
					continue;
			}
			$addresses[] = $user['email']; // add user to email recipients
		}
		if(empty($addresses))
			return TRUE;
		return $this->smtp->sendEmail($addresses, $subject, $message);
	}
/**
* grab titles from main list only if user threshold has been passed and resources have been added/edited since the user's last notification
*/
	private function grabTitlesThreshold($userArray, $idArray)
	{
// no. seconds in 1 day
		$day1secs = 86400;
		$userThreshold = $userArray['notifyThreshold'] * $day1secs;
		$now = time();
		$passed = (($now - $userArray['unixNotifyTimestamp']) > $userThreshold) ? TRUE : FALSE;
		$notifyArray = array();
		foreach($idArray as $id => $resourceTimestamp)
		{
			if(time())
			if(($resourceTimestamp > $userArray['unixNotifyTimestamp']) && ($passed))
				$notifyArray[$id] = $this->titles[$id];
		}
		return $notifyArray;
	}
/**
* Grab resources at greatest threshold following editing or addition.
* Resources must have NOW() - timestamp  that is less than the greatest notification threshold
*/
	private function grabResources($digestThreshold)
	{
		$size = 0;
		$this->db->formatConditions('UNIX_TIMESTAMP(' . $this->db->formatFields('resourcetimestampTimestampAdd') . ')>' .
			$this->db->tidyInput($this->earliestNotifyUnixTimestamp));
		$this->db->leftJoin('resource_timestamp', "resourcetimestampId", 'resourceId');
		$this->res->withUnixTimestamp = TRUE;
		$this->res->groupByResourceId = FALSE;
		$recordset = $this->res->getResource();
		$this->res->withUnixTimestamp = FALSE;
		$this->res->groupByResourceId = TRUE;
		if(($size = $this->db->numRows($recordset)) > $digestThreshold)
			return $size;
		while($row = $this->db->fetchRow($recordset))
		{
			if(!array_key_exists($row['resourceId'], $this->titles))
				$this->formatTitleThreshold($row);
			$this->allAddedIds[$row['resourceId']] = $row['addUnixTimestamp'];
		}
// grab greatest possible number of edited resources
		$this->db->formatConditions('UNIX_TIMESTAMP(' . $this->db->formatFields('resourcetimestampTimestamp') . ')>' .
			$this->db->tidyInput($this->earliestNotifyUnixTimestamp));
		$this->db->leftJoin('resource_timestamp', "resourcetimestampId", 'resourceId');
		$this->res->withUnixTimestamp = TRUE;
		$this->res->groupByResourceId = FALSE;
		$recordset = $this->res->getResource();
		$this->res->withUnixTimestamp = FALSE;
		$this->res->groupByResourceId = TRUE;
		if(($size = $this->db->numRows($recordset)) > $digestThreshold)
			return $size;
		while($row = $this->db->fetchRow($recordset))
		{
			if(!array_key_exists($row['resourceId'], $this->titles))
				$this->formatTitleThreshold($row);
			$this->allEditedIds[$row['resourceId']] = $row['editUnixTimestamp'];
		}
		return $size;
	}
/**
* Format the resource entry from the SQL row (threshold)
*/
	private function formatTitleThreshold($row)
	{
// Data stored in database as UTF-8
		$title = html_entity_decode(\HTML\stripHtml($this->bibStyle->process($row)));
		$this->titles[$row['resourceId']] = $title;
	}
/**
* Format the resource entry from the given resource ID.  $resourceIds is either a single ID or an array of IDs for mass imports
*/
	private function formatTitle($resourceId, $userAddEdit, $digestThreshold)
	{
		$title = '';
		if(is_array($resourceId)) // always an array coming from emailThreshold()
		{
// If more than xxx added resources, simply grab the number of added resources
			$size = sizeof($resourceId);
			if($size > $digestThreshold)
			{
				$notifyMessage = $this->messages->text("user", "notifyMass1", $userAddEdit);
				$notifyMessage .= ' ' . $this->messages->text("user", "notifyMass2", $size);
				$title = FALSE;
			}
			else
			{
				$recordset = $this->res->getResource($resourceId);
				while($row = $this->db->fetchRow($recordset))
					$this->titles[$row['resourceId']] = html_entity_decode(\HTML\stripHtml($this->bibStyle->process($row)));
				$title = join("\n". LF, $this->titles);
				$notifyMessage = $this->messages->text("user", "notifyMass1", $userAddEdit);
			}
		}
		else
		{
			$recordset = $this->res->getResource($resourceId);
			$row = $this->db->fetchRow($recordset);
			$title = html_entity_decode(\HTML\stripHtml($this->bibStyle->process($row)));
			$notifyMessage = $this->messages->text("user", "notify", $userAddEdit);
		}
		return array($title, $notifyMessage);
	}
}
