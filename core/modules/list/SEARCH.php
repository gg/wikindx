<?php
/**
 * WIKINDX : Bibliographic Management system.
 * @link https://wikindx.sourceforge.io/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2018-2019 Mark Grimshaw-Aagaard <sirfragalot@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 */

/**
*	SEARCH class
*
*	Search database
*
*/
class SEARCH
{
private $db;
private $vars;
private $stmt;
private $errors;
private $messages;
private $common;
private $config;
private $session;
private $keyword;
private $type;
private $category;
private $creator;
private $userTag;
private $publisher;
private $collection;
private $tag;
private $user;
private $input = array();
private $badInput;
private $parsePhrase;
private $languages = array();
private $commonBib;
private $metadata;
private $displayUserTags = FALSE;
private $displayQCs = FALSE;
private $displayPCs = FALSE;
private $displayMusings = FALSE;
private $displayIdeas = FALSE;
private $displayMKs = FALSE;
private $dbFields;
private $unionFragments = array();
private $ideas = array();
private $matchIds = array();
private $partials = array();
private $attachmentSearches = array();
private $validSearch = TRUE;
private $lastUnionResourceId;
private $unionResourceIds = array();
private $subQ;

	public function __construct()
	{
		$this->db = FACTORY_DB::getInstance();
		$this->vars = GLOBALS::getVars();
		$this->stmt = FACTORY_SQLSTATEMENTS::getInstance();
		$this->errors = FACTORY_ERRORS::getInstance();
		$this->messages = FACTORY_MESSAGES::getInstance();
		$this->common = FACTORY_LISTCOMMON::getInstance();
		$this->common->quickSearch = FALSE;
		$this->session = FACTORY_SESSION::getInstance();
		$this->type = FACTORY_TYPE::getInstance();
		$this->category = FACTORY_CATEGORY::getInstance();
		$this->keyword = FACTORY_KEYWORD::getInstance();
		$this->userTag = FACTORY_USERTAGS::getInstance();
		$this->creator = FACTORY_CREATOR::getInstance();
		$this->publisher = FACTORY_PUBLISHER::getInstance();
		$this->collection = FACTORY_COLLECTION::getInstance();
		$this->config = FACTORY_CONFIG::getInstance();
		$this->user = FACTORY_USER::getInstance();
		include_once("core/miscellaneous/TAG.php");
		$this->tag = new TAG();
		$this->badInput = FACTORY_BADINPUT::getInstance();
		$this->parsePhrase = FACTORY_PARSEPHRASE::getInstance();
		$this->commonBib = FACTORY_BIBLIOGRAPHYCOMMON::getInstance();
		$this->metadata = FACTORY_METADATA::getInstance();
		if(!$this->session->getVar('search_Order'))
			$this->session->setVar('search_Order', 'creator');
		$this->session->setVar('sql_LastOrder', $this->session->getVar('search_Order'));
		switch($this->session->getVar('search_Order'))
		{
			case 'title':
				break;
			case 'creator':
				break;
			case 'publisher':
				break;
			case 'year':
				break;
			case 'timestamp':
				break;
			default:
				$this->session->setVar('search_Order', 'creator');
		}
//		if(array_key_exists('action', $_GET) && ($_GET['action'] == 'curlFile'))
//			$this->curlFile();
	}
// display form options
	public function init($error = FALSE, $returnString = FALSE)
	{
///First check, do we have resources?
		if(!$this->common->resourcesExist())
			return;
		$this->input = $this->session->getArray('advancedSearch');
		$this->checkAvailableFields();
		include_once("core/modules/help/HELPMESSAGES.php");
		$help = new HELPMESSAGES();
		GLOBALS::setTplVar('help', $help->createLink('search'));
		if(!$returnString)
			GLOBALS::setTplVar('heading', $this->messages->text("heading", "search"));
		$this->session->delVar('mywikindx_PagingStart');
		$this->session->delVar('mywikindx_PagingStartAlpha');
		$this->session->setVar('advancedSearch_elementIndex', 1);

		$pString = '';
		$pString .= $error;
		$pString .= \HTML\p(\FORM\formHeader("list_SEARCH_CORE") . \FORM\hidden("method", "reset") . \FORM\formSubmit($this->messages->text("submit", "Reset")) . \FORM\formEnd());
		$pString .= \FORM\formHeader("list_SEARCH_CORE", "onsubmit=\"selectAll();return true;\"");
		$pString .= \FORM\hidden("method", "process");
		$pString .= \HTML\tableStart();
		$pString .= \HTML\trStart();
		$pString .= \HTML\td($this->firstDisplay());
		$pString .= \HTML\trEnd();
		$pString .= \HTML\tableEnd();

		$updateJSElementIndex = FALSE;
		if((!$this->session->getVar('setup_MetadataAllow')))
		{
			if(($this->session->getVar('setup_MetadataUserOnly')) && $this->session->getVar('setup_UserId'))
				$wordFields = array('title', 'note', 'abstract', 'quote', 'quoteComment', 'paraphrase', 'paraphraseComment', 'musing', 'idea');
			else
				$wordFields = array('title', 'note', 'abstract');
		}
		else
			$wordFields = array('title', 'note', 'abstract', 'quote', 'quoteComment', 'paraphrase', 'paraphraseComment', 'musing', 'idea');
		if((!$this->session->getVar("setup_FileViewLoggedOnOnly") || $this->session->getVar("setup_UserId")) &&
			($this->db->tableIsEmpty('resource_attachments') == 0)) // 0 means table is NOT empty
				array_splice($wordFields, 3, 0, "attachments");
		$subQ = $this->db->subQuery($this->db->selectNoExecute('resource_custom', 'resourcecustomCustomId'), FALSE, FALSE, TRUE);
		$this->db->formatConditions($this->db->formatFields('customId') . $this->db->inClause($subQ));
		$recordset = $this->db->select('custom', array('customId', 'customLabel', 'customSize'));

		while($row = $this->db->fetchRow($recordset))
		{
			if($row['customSize'] == 'S')
				$wordFields[] = 'Custom_S_' . $row['customId'];
			else
				$wordFields[] = 'Custom_L_' . $row['customId'];
		}

		// A possibility to add up to 50 search fields should be enough...
		for($i = 2; $i < 51; $i++)
		{
			if(array_key_exists("Field_$i", $this->input))
			{
				$noWords = FALSE;

				foreach($wordFields as $value)
				{
					if($value == $this->input["Field_$i"])
					{
						if(!array_key_exists("Word_$i", $this->input))
							$noWords = TRUE;
						break;
					}
				}

				if($noWords)
				{
					$pString .= \HTML\div("searchElement_$i", '');
					continue;
				}

				if(in_array($this->input["Field_$i"], array('type', 'category', 'subcategory', 'creator', 'keyword', 'metaKeyword', 'userTag', 'language', 'publisher', 'collection', 'tag', 'addedBy', 'editedBy'))
					&& !array_key_exists("Select_$i", $this->input))
				{
					$pString .= \HTML\div("searchElement_$i", '');
					continue;
				}
				else if(in_array($this->input["Field_$i"], array('publicationYear', 'access', 'maturityIndex'))
					&& !array_key_exists("Value1_$i", $this->input))
				{
					$pString .= \HTML\div("searchElement_$i", '');
					continue;
				}

				$div = \HTML\tableStart();
				$div .= \HTML\trStart();
				$div .= \HTML\td($this->addRemoveIcon($i, FALSE)); // add remove element icon
				if($this->input["Field_$i"] == 'idea')
					$buttons = \HTML\span('OR', 'small') . \FORM\hidden("advancedSearch_Button1_$i", 'OR');
				else
					$buttons = $this->makeRadioButtons1("advancedSearch_Button1_$i");
				$div .= \HTML\td(\HTML\div("searchElementButtons_$i", $buttons), 'left width5percent');
				$fields = $this->searchFields($i);
				$div .= \HTML\td($fields, 'left width15percent');
				$div .= \HTML\td(\HTML\div("searchElementContainer_$i", $this->createDivs($this->input["Field_$i"], $i)));
				$div .= \HTML\trEnd();
				$div .= \HTML\tableEnd();
				$pString .= \HTML\p(\HTML\div("searchElement_$i", $div));
				$this->session->setVar('advancedSearch_elementIndex', $i);
				$updateJSElementIndex = TRUE;
			}
			else
				$pString .= \HTML\div("searchElement_$i", '');
		}

		$pString .= \HTML\p(\HTML\div('searchElement_addIcon',
			$this->addRemoveIcon($this->session->getVar('advancedSearch_elementIndex'), TRUE, $updateJSElementIndex)));
		$pString .= \HTML\p($this->options());

		$pString .= \FORM\formEnd();

		if($returnString)
			return $pString; // cf FRONT.php or process() below.
		else
			GLOBALS::addTplVar('content', $pString);

		// Load at end because .js initialization needs various DIVs to be in the page before they are made invisible
		\AJAX\loadJavascript($this->config->WIKINDX_BASE_URL . '/core/modules/list/searchSelect.js');
	}
// Reset the form and clear the session
	public function reset()
	{
		$this->session->clearArray('advancedSearch');
		$this->session->saveState('advancedSearch');
		$this->init();
	}
// Display the first field of the search form
	private function firstDisplay()
	{
		return \HTML\div('searchElement_1', $this->wordSearch());
	}
// Create form fields for word searches on fields
	private function wordSearch($index = 1, $remove = FALSE)
	{
		$pString = \HTML\tableStart();
		$pString .= \HTML\trStart();
		if($remove)
			$pString .= \HTML\td($this->addRemoveIcon($index, FALSE)); // add remove element icon
		// Add radio buttons
		if($index > 1)
		{
			$buttons  = $this->makeRadioButtons1("advancedSearch_Button1_$index");
			$pString .= \HTML\td(\HTML\div("searchElementButtons_$index", $buttons), 'left width5percent');
		}
		$fields = $this->searchFields($index);
		$pString .= \HTML\td($fields, 'left width15percent');
		if(($index == 1) && array_key_exists("Field_1", $this->input))
			$pString .= \HTML\td(\HTML\div("searchElementContainer_1", $this->createDivs($this->input["Field_1"], 1)));
		else
			$pString .= \HTML\td(\HTML\div("searchElementContainer_$index", $this->wordDiv($index)));
		$pString .= \HTML\trEnd();
		$pString .= \HTML\tableEnd();
		return $pString;
	}
// Create radio buttons for AND and OR and NOT
	private function makeRadioButtons1($type)
	{
		if($this->session->getVar($type) == 'AND')
		{
			$pString = \HTML\span(\FORM\radioButton(FALSE, $type, 'OR') . " OR", "small") . BR;
			$pString .= \HTML\span(\FORM\radioButton(FALSE, $type, 'AND', TRUE) . " AND", "small") . BR;
			$pString .= \HTML\span(\FORM\radioButton(FALSE, $type, 'NOT') . " NOT", "small");
		}
		else if($this->session->getVar($type) == 'NOT')
		{
			$pString = \HTML\span(\FORM\radioButton(FALSE, $type, 'OR') . " OR", "small") . BR;
			$pString .= \HTML\span(\FORM\radioButton(FALSE, $type, 'AND') . " AND", "small") . BR;
			$pString .= \HTML\span(\FORM\radioButton(FALSE, $type, 'NOT', TRUE) . " NOT", "small");
		}
// Default
		else
		{
			$pString = \HTML\span(\FORM\radioButton(FALSE, $type, 'OR', TRUE) . " OR", "small") . BR;
			$pString .= \HTML\span(\FORM\radioButton(FALSE, $type, 'AND') . " AND", "small") . BR;
			$pString .= \HTML\span(\FORM\radioButton(FALSE, $type, 'NOT') . " NOT", "small");
		}
		return $pString;
	}
// Create radio buttons for AND and OR
	private function makeRadioButtons2($type)
	{
		if($this->session->getVar($type) == 'AND')
		{
			$pString = \HTML\span(\FORM\radioButton(FALSE, $type, 'OR') . " OR", "small") . BR;
			$pString .= \HTML\span(\FORM\radioButton(FALSE, $type, 'AND', TRUE) . " AND", "small");
		}
// Default
		else
		{
			$pString = \HTML\span(\FORM\radioButton(FALSE, $type, 'OR', TRUE) . " OR", "small") . BR;
			$pString .= \HTML\span(\FORM\radioButton(FALSE, $type, 'AND') . " AND", "small");
		}
		return $pString;
	}
/**
Search options
*/
	private function options()
	{
		$pString = \HTML\tableStart('generalTable borderStyleSolid');
		$pString .= \HTML\trStart();
		$pString .= \HTML\td($this->userBibs(), 'width10percent');
		$pString .= \HTML\td($this->displayOptions(), 'width10percent');
		$pString .= \HTML\td($this->common->displayOrder('advancedSearch'), 'width10percent');
		$pString .= \HTML\td(\FORM\formSubmit($this->messages->text("submit", "Search")), 'center top width5percent');
		$pString .= \HTML\td($this->displayTest(), 'width55percent');
		$pString .= \HTML\trEnd();
		$pString .= \HTML\tableEnd();
		return $pString;
	}
/**
Display test option
*/
	private function displayTest()
	{
		$jsonArray = array();
		$jsonArray[] = array(
			'startFunction' => "test",
			'script' =>"index.php?action=list_SEARCH_CORE&method=test",
			'targetDiv' => "advancedSearch_Test",
			);
		$testIcon = \AJAX\jActionIcon('view', 'onclick', $jsonArray);
		return $this->messages->text("search", "test") . ':&nbsp;&nbsp;' . $testIcon . \HTML\p(\HTML\div('advancedSearch_Test', ' '));
	}
/**
* Test search by displaying natural language search
*/
	public function test($search = FALSE, $input = array(), $bibIdArray = array(), $optionsArray = array())
	{
		$testArray = $bibIdArray = $optionsArray = $array = $tempIdeas = $ideas = array();
		if($search) // Doing an actual search rather than test
		{
			$longSpace = '    ';
			$shortSpace = '  ';
			$newLine = CR.LF;
			$array = $input;
			foreach($input as $key1 => $inputArray)
			{
				foreach($inputArray as $key2 => $value)
				{
					if($key2 == 'OriginalField')
						$array[$key1]['Field'] = $value;
					else if($key2 == 'Select')
						$array[$key1]['Select'] = join(',', $value);
				}
			}
		}
		else
		{
			$longSpace = '&nbsp;&nbsp;&nbsp;&nbsp;';
			$shortSpace = '&nbsp;&nbsp;';
			$newLine = BR;
			$vars = GLOBALS::getVars();
			$jArray = \AJAX\decode_jString($vars['ajaxReturn']);
			if(array_key_exists('advancedSearch_BibId', $jArray['elementFields']))
			{
				$bibIdArray = UTF8::mb_explode(',', $jArray['elementFields']['advancedSearch_BibId']);
				$optionsArray = UTF8::mb_explode(',', $jArray['elementFields']['advancedSearch_Options']);
			}
			unset($jArray['elementFields']['advancedSearch_BibId']);
			unset($jArray['elementFields']['advancedSearch_Options']);
			foreach($jArray['elementFields'] as $key => $value)
			{
				$split = UTF8::mb_explode('_', $key);
				$array[$split[2]][$split[1]] = $value;
			}
		}
		$arrayString = \HTML\em($this->messages->text("search", "naturalLanguage")) . $newLine;
		$temp = $bibIdArray;
		foreach($temp as $key => $value)
		{
			if(!trim($value))
				unset($bibIdArray[$key]);
		}
		$temp = $optionsArray;
		foreach($temp as $key => $value)
		{
			if(!trim($value))
				unset($optionsArray[$key]);
		}
		$tempArray = $array;
		foreach($array as $key1 => $value1)
		{
			foreach($value1 as $key2 => $value2)
			{
				if(($key2 == 'Field') && ($value2 == 'idea'))
				{
					$this->parsePhrase->idea = TRUE;
					if(($word = $this->parsePhrase->parse($value1, TRUE)) && array_key_exists('Word', $array[$key1]))
						$tempIdeas[$key1]['String'] = $word;
					else
					{
						unset($tempArray[$key1]);
						continue;
					}
					$tempIdeas[$key1]['Field'] = 'idea';
					$tempIdeas[$key1]['Button1'] = 'OR';
					unset($tempArray[$key1]);
				}
			}
		}
		$this->parsePhrase->idea = FALSE;
		$array = $tempArray;
		$ideas = $tempIdeas;
		foreach($tempIdeas as $key => $value)
			$ideas[$key] = '(' . str_replace('!WIKINDXFIELDWIKINDX!', \HTML\color($value['Field'], 'greenText'), $value['String']). ')';
		foreach($array as $key1 => $value1)
		{
			foreach($value1 as $key2 => $value2)
			{
				if($key2 == 'Word')
				{
					if($word = $this->parsePhrase->parse($value1, TRUE))
						$testArray[$key1]['String'] = $word;
				}
				else if($key2 == 'Select')
					$testArray[$key1]['Select'] = UTF8::mb_explode(',', $value2);
				else if($key2 == 'Field')
				{
					$split = UTF8::mb_explode('_', $value2);
					if(sizeof($split) == 3)
					{
						if(mb_strpos($split[0], 'Custom') !== FALSE)
						{
							$this->db->formatConditions(array('customId' => $split[2]));
							$customName = $this->db->selectFirstField('custom', 'customLabel');
							$testArray[$key1]['Field'] = \HTML\dbToHtmlTidy($customName);
						}
					}
					else
						$testArray[$key1]['Field'] = $value2;
				}
				else if($key2 == 'Button1')
					$testArray[$key1]['Button1'] = $value2;
				else if($key2 == 'Button2')
					$testArray[$key1]['Button2'] = $value2;
				else if($key2 == 'Comparison')
					$testArray[$key1]['Comparison'] = $value2;
				else if($key2 == 'Value1')
					$testArray[$key1]['Value1'] = trim($value2);
				else if($key2 == 'Value2')
					$testArray[$key1]['Value2'] = trim($value2);
			}
		}
		if(!empty($testArray) &&
			!array_key_exists('String', $testArray[$key1]) &&
			!array_key_exists('Select', $testArray[$key1]) &&
			!array_key_exists('Comparison', $testArray[$key1])
			)
			unset($testArray[$key1]);
		else if(!empty($testArray) && array_key_exists('Comparison', $testArray[$key1]))
		{
			if($testArray[$key1]['Comparison'] == 6) // i.e. <...<
			{
				if(!array_key_exists('Value2', $testArray[$key1]))
					unset($testArray[$key1]);
				else if(!is_numeric($testArray[$key1]['Value1']) || !is_numeric($testArray[$key1]['Value2']))
					unset($testArray[$key1]);
				else if(!$testArray[$key1]['Value1'] || !$testArray[$key1]['Value2'])
					unset($testArray[$key1]);
				else if($testArray[$key1]['Value1'] >= $testArray[$key1]['Value2'])
					unset($testArray[$key1]);
			}
			else if(!array_key_exists('Value1', $testArray[$key1]) || !$testArray[$key1]['Value1'] || !is_numeric($testArray[$key1]['Value1']))
				unset($testArray[$key1]);
		}
		$temp = $buttons = $final = array();
		foreach($testArray as $index => $value)
		{
			if(array_key_exists('Field', $value) && (count($value) == 1))
				continue;
			if(array_key_exists('String', $value))
				$temp[$index] = '(' . str_replace('!WIKINDXFIELDWIKINDX!', \HTML\color($value['Field'], 'greenText'), $value['String']). ')';
			else if(array_key_exists('Select', $value))
			{
				if(!array_key_exists('Button2', $value)) // e.g. publisher, collection etc. where each resource can have only one
					$value['Button2'] = 'OR';
				if(array_key_exists(0, $value['Select']) && !$value['Select'][0])
					unset($value['Select'][0]);
				if(empty($value['Select']))
					continue;
				$this->formatElement($value);
				$value['Select'] = array_map(array($this, "colorID"), $value['Select']);
				$sizeof = count($value['Select']);
				if($sizeof > 1)
				{
					if($value['Button2'] == 'OR')
						$temp[$index] = '(' . \HTML\color($value['Field'], 'greenText') .
							' IS ' . join(' ' . $value['Button2'] . ' ', $value['Select']) . ')';
					else // AND
						$temp[$index] = '(' . \HTML\color($value['Field'], 'greenText') .
							' INCLUDES ' . join(' ' . $value['Button2'] . ' ', $value['Select']) . ')';
				}
				else
				{
					if($shift = array_shift($value['Select']))
						$temp[$index] = '(' . \HTML\color($value['Field'], 'greenText') . ' IS ' . $shift . ')';
				}
			}
			else if(array_key_exists('Comparison', $value))
			{
				if(!array_key_exists('Value1', $value))
					continue;
				if($value['Comparison'] == 0)
					$temp[$index] = '(' . \HTML\color($value['Field'], 'greenText') . ' IS EQUAL TO ' .
						\HTML\color($value['Value1'], 'redText') . ')';
				else if($value['Comparison'] == 1)
					$temp[$index] = '(' . \HTML\color($value['Field'], 'greenText') . ' IS NOT EQUAL TO ' .
						\HTML\color($value['Value1'], 'redText') . ')';
				else if($value['Comparison'] == 2)
					$temp[$index] = '(' . \HTML\color($value['Field'], 'greenText') . ' IS LESS THAN ' .
						\HTML\color($value['Value1'], 'redText') . ')';
				else if($value['Comparison'] == 3)
					$temp[$index] = '(' . \HTML\color($value['Field'], 'greenText') . ' IS GREATER THAN ' .
						\HTML\color($value['Value1'], 'redText') . ')';
				if($value['Comparison'] == 4)
					$temp[$index] = '(' . \HTML\color($value['Field'], 'greenText') . ' IS LESS THAN OR EQUAL TO ' .
						\HTML\color($value['Value1'], 'redText') . ')';
				else if($value['Comparison'] == 5)
					$temp[$index] = '(' . \HTML\color($value['Field'], 'greenText') . ' IS GREATER THAN OR EQUAL TO ' .
						\HTML\color($value['Value1'], 'redText') . ')';
				else if($value['Comparison'] == 6)
				{
					if(!array_key_exists('Value2', $value))
						continue;
					$temp[$index] = '(' . \HTML\color($value['Field'], 'greenText') . ' IS MORE THAN ' .
						\HTML\color($value['Value1'], 'redText') . ' AND LESS THAN ' .
						\HTML\color($value['Value2'], 'redText') . ')';
				}
			}
			else
				continue;
			$buttons[$index] = array_key_exists('Button1', $value) ? $value['Button1'] : null;
		}
		$error = TRUE;
		if(!empty($temp))
		{
			ksort($temp);
			ksort($buttons);
// Each time an element begins with 'OR', close and begin new parentheses and use newlines if it is not the first element
			$count = 0;
			foreach($buttons as $index => $button)
			{
				if(!$count) // First element
					$final[$index] = '(' . $newLine . $longSpace . array_shift($temp);
				else if($button == 'OR')
					$final[$index] = $newLine . ')' . $newLine . 'OR' . $newLine . '(' . $newLine .
					$longSpace . array_shift($temp);
				else if($button == 'AND')
					$final[$index] = $newLine . $shortSpace . 'AND' . $newLine . $longSpace . array_shift($temp);
				else if($button == 'NOT')
					$final[$index] = $newLine . $shortSpace . 'NOT' . $newLine . $longSpace . array_shift($temp);
				$count++;
			}
			$final[$index] .= $newLine . ')';
// Now add any bibliography etc. options
			foreach($optionsArray as $value)
			{
				if(($value == 'ignore') || ($value == 'displayOnlyAttachment') || ($value == 'zipAttachment'))
					continue;
				$final[++$index] = $newLine . 'AND ' . \HTML\color($value, 'greenText');
			}
			foreach($bibIdArray as $value)
			{
				if(!$value)
					continue;
				$this->db->formatConditions(array('userbibliographyId' => $value));
				$row = $this->db->selectFirstRow('user_bibliography', 'userbibliographyTitle');
				$final[++$index] = $newLine . 'AND NOT IN ' .
					\HTML\color(\HTML\dbToHtmlTidy($row['userbibliographyTitle']), 'greenText') . ' bibliography';
			}
			$arrayString .= join(' ', $final);
			if($search)
			{
				if(!empty($ideas))
				{
					if(!empty($final))
						$or = $newLine . 'OR';
					else
						$or = FALSE;
					while($ideas)
						$finalIdeas[] = $or . $newLine . ' (' . $newLine . $longSpace . array_shift($ideas) . $newLine . ')';
					$arrayString .= join(' ', $finalIdeas);
				}
				$this->session->setVar('advancedSearch_listParams', $arrayString);
				return;
			}
			$div = \HTML\p($arrayString);
			$error = FALSE;
		}
		if(!empty($ideas))
		{
			if(!empty($final))
				$or = $newLine . 'OR';
			else
				$or = FALSE;
			while($ideas)
				$finalIdeas[] = $or . $newLine . ' (' . $newLine . $longSpace . array_shift($ideas) . $newLine . ')';
			$arrayString .= join(' ', $finalIdeas);
			if($search)
			{
				$this->session->setVar('advancedSearch_listParams', $arrayString);
				return;
			}
			$div = \HTML\p($arrayString);
			$error = FALSE;
		}
		if($error && !empty($this->attachmentSearches))
			return;
		else if($error)
			$this->testError();
		if(is_array(error_get_last()))
		{
// NB E_STRICT in PHP5 gives warning about use of GLOBALS below.  E_STRICT cannot be controlled through WIKINDX
			$error = error_get_last();
			$error = $error['message'];
			GLOBALS::addTplVar('content', \AJAX\encode_jArray(array('ERROR' => $error)));
		}
		else
		{   if (!isset($div)) $div = '';
			$jsonResponseArray = array('innerHTML' => "$div");
			GLOBALS::addTplVar('content', \AJAX\encode_jArray($jsonResponseArray));
		}
		FACTORY_CLOSERAW::getInstance();
	}
/**
Pull out real values behind IDs
*/
	private function formatElement(&$value)
	{
		if($value['Field'] == 'creator')
		{
			foreach($value['Select'] as $select)
				$temp[] = $this->formatCreators($select);
		}
		else if($value['Field'] == 'publisher')
		{
			foreach($value['Select'] as $select)
				$temp[] = $this->formatPublishers($select);
		}
		else if($value['Field'] == 'collection')
		{
			foreach($value['Select'] as $select)
				$temp[] = $this->formatCollections($select);
		}
		else if($value['Field'] == 'type')
			return;
		else if($value['Field'] == 'subcategory')
		{
			foreach($value['Select'] as $select)
				$temp[] = $this->formatSubcategories($select);
		}
		else if($value['Field'] == 'category')
		{
			foreach($value['Select'] as $select)
				$temp[] = $this->formatCategories($select);
		}
		else if(($value['Field'] == 'keyword') or ($value['Field'] == 'metaKeyword'))
		{
			foreach($value['Select'] as $select)
				$temp[] = $this->formatKeywords($select);
		}
		else if($value['Field'] == 'userTag')
		{
			foreach($value['Select'] as $select)
				$temp[] = $this->formatUsertags($select);
		}
		else if($value['Field'] == 'language')
		{
			foreach($value['Select'] as $select)
				$temp[] = $this->formatLanguages($select);
		}
		else if($value['Field'] == 'tag')
		{
			foreach($value['Select'] as $select)
				$temp[] = $this->formatTags($select);
		}
		else if(($value['Field'] == 'addedBy') or ($value['Field'] == 'editedBy'))
		{
			foreach($value['Select'] as $select)
				$temp[] = $this->formatUsernames($select);
		}
		$value['Select'] = $temp;
	}
/**
Test error
*/
	private function testError()
	{
		$div = $this->errors->text("inputError", "missing");
		$jsonResponseArray = array(
			'innerHTML' => "$div",
			);
		GLOBALS::addTplVar('content', \AJAX\encode_jArray($jsonResponseArray));
		FACTORY_CLOSERAW::getInstance();
	}
/**
* Callback function to color red each array element of a select box
*/
	private function colorID($element)
	{
		if(!trim($element))
			return '';
		return \HTML\color($element, 'redText');
	}
/**
Display attachment options
*/
	private function displayOptions()
	{
		$pString = \HTML\tableStart();
		$pString .= \HTML\trStart();
		if($this->session->getVar("setup_ReadOnly") && $this->session->getVar("setup_FileViewLoggedOnOnly"))
			$options = array();
		else if($this->db->tableIsEmpty('resource_attachments') == 1)
		{
			$options = array();
			}
		else
		{
			$options = array(
					'ignore'	=>	$this->messages->text("misc", "ignore"),
					'noAttachment'		=>	$this->messages->text('select', 'noAttachment'),
					'withAttachment'		=>	$this->messages->text('select', 'attachment'),
					'displayOnlyAttachment'	=>	$this->messages->text('select', 'displayAttachment'),
					'zipAttachment'	=>	$this->messages->text('select', 'displayAttachmentZip'),
				);
		}
		if(empty($options))
			$options['ignore'] = $this->messages->text("misc", "ignore");
		$options['withUrl'] = $this->messages->text('select', 'url');
		$options['withDoi'] = $this->messages->text('select', 'doi');
		$options['peerReviewed'] = $this->messages->text('select', 'displayPeerReviewed');
		$jsonArray[] = array(
			'startFunction' => "attachmentOptions",
			);
		$js = \AJAX\jActionForm('onchange', $jsonArray);
		if($selected = $this->session->getVar("advancedSearch_Options"))
		{
			$selected = unserialize(base64_decode($selected));
			if(($key = array_search('ignore', $selected)) !== FALSE)
				unset($selected[$key]);
			if(!empty($selected))
				$selectBox = \FORM\selectedBoxValueMultiple($this->messages->text('select', 'option'), "advancedSearch_Options",
					$options, $selected, 5, FALSE, $js);
			else
				$selectBox = \FORM\selectFBoxValueMultiple($this->messages->text('select', 'option'), "advancedSearch_Options",
					$options, 5, FALSE, $js);
		}
		else
			$selectBox = \FORM\selectFBoxValueMultiple($this->messages->text('select', 'option'), "advancedSearch_Options",
				$options, 5, FALSE, $js);
		$pString .= \HTML\td($selectBox);
		$pString .= \HTML\trEnd();
		$pString .= \HTML\tableEnd();
		return $pString;
	}
// Return select box of user bibliographies for this user if browsing the master bibliography
	private function userBibs()
	{
		$bibs = $this->commonBib->getUserBibs() + $this->commonBib->getGroupBibs();
		if($userBib = $this->session->getVar("mywikindx_Bibliography_use"))
			unset($bibs[$userBib]);
		if(empty($bibs))
			return "&nbsp;";
// add 0 => IGNORE to $array
		$temp[0] = $this->messages->text("misc", "ignore");
		foreach($bibs as $key => $value)
			$temp[$key] = $value;
		$selected = $this->session->getVar("advancedSearch_BibId");
		if($selected && array_key_exists($selected, $temp))
			$pString = \FORM\selectedBoxValue($this->messages->text("select", "notInUserBib"),
			"advancedSearch_BibId", $temp, $selected, 5);
		else
			$pString = \FORM\selectFBoxValue($this->messages->text("select", "notInUserBib"), "advancedSearch_BibId", $temp, 5);
		return $pString;
	}
/**
Word search div
*/
	private function wordDiv($index)
	{
		$pString = \HTML\tableStart();
		$pString .= \HTML\trStart();
		$word = array_key_exists("Word_$index", $this->input) ?
			htmlspecialchars(stripslashes($this->input["Word_$index"]), ENT_QUOTES | ENT_HTML5) : FALSE;
		$wordHint = BR . \HTML\span($this->messages->text("hint", "wordLogic"), 'hint');
		$checked = array_key_exists("Partial_$index", $this->input) ? 'CHECKED' : FALSE;
		$checked = BR . $this->messages->text("search", "partial") . ':&nbsp;&nbsp;' .
			\FORM\checkbox(FALSE, "advancedSearch_Partial_$index", $checked);
		$pString .= \HTML\td(\FORM\textInput($this->messages->text("search", "word"), "advancedSearch_Word_$index",
			$word, 40) . $wordHint . $checked);
		$pString .= \HTML\trEnd();
		$pString .= \HTML\tableEnd();
		return $pString;
	}
// Add or remove form element icon
	private function addRemoveIcon($index, $add = TRUE, $updateJSElementIndex = FALSE)
	{
		$jsonArray = array();
		if($add)
		{
			$jScript = "index.php?action=list_SEARCH_CORE&method=addElement";
			$startFunction = 'addElement';
			$elementIndex = $this->session->getVar('advancedSearch_elementIndex');
		}
		else
		{
			$jScript = "index.php?action=list_SEARCH_CORE&method=removeElement";
			$startFunction = 'removeElement';
			$elementIndex = $index;
		}
		$jsonArray[] = array(
			'startFunction' => "$startFunction",
			'script' => "$jScript",
			'elementIndex' => "$elementIndex",
			'updateJSElementIndex' =>	"$updateJSElementIndex",
			);
		if($add)
			return \AJAX\jActionIcon('add', 'onclick', $jsonArray);
		else
			return \AJAX\jActionIcon('remove', 'onclick', $jsonArray);
	}
// add form element
	public function addElement()
	{
		$vars = GLOBALS::getVars();
		$this->checkAvailableFields();
		$jArray = \AJAX\decode_jString($vars['ajaxReturn']);
		$div = \HTML\p($this->wordSearch($jArray['elementIndex'], TRUE));
		$this->session->setVar('advancedSearch_elementIndex', $jArray['elementIndex']);
		$jsonResponseArray = array(
			'innerHTML' => "$div",
			);
		if(is_array(error_get_last()))
		{
// NB E_STRICT in PHP5 gives warning about use of GLOBALS below.  E_STRICT cannot be controlled through WIKINDX
			$error = error_get_last();
			$error = $error['message'];
			GLOBALS::addTplVar('content', \AJAX\encode_jArray(array('ERROR' => $error)));
		}
		else
			GLOBALS::addTplVar('content', \AJAX\encode_jArray($jsonResponseArray));
		FACTORY_CLOSERAW::getInstance();
	}
/**
* remove form element
*/
	public function removeElement()
	{
		$vars = GLOBALS::getVars();
		$jArray = \AJAX\decode_jString($vars['ajaxReturn']);
		$this->session->delVar("advancedSearch_Field_" . $jArray['elementIndex']);
		$this->session->delVar("advancedSearch_Word_" . $jArray['elementIndex']);
		$this->session->delVar("advancedSearch_Partial_" . $jArray['elementIndex']);
		$this->session->delVar("advancedSearch_Button1_" . $jArray['elementIndex']);
		$this->session->delVar("advancedSearch_Button2_" . $jArray['elementIndex']);
		$this->session->delVar("advancedSearch_Select_" . $jArray['elementIndex']);
		$this->session->delVar("advancedSearch_Comparison_" . $jArray['elementIndex']);
		$this->session->delVar("advancedSearch_Value1_" . $jArray['elementIndex']);
		$this->session->delVar("advancedSearch_Value2_" . $jArray['elementIndex']);
		$div = ' ';
		$jsonResponseArray = array(
			'innerHTML' => $div,
			);
		if(is_array(error_get_last()))
		{
// NB E_STRICT in PHP5 gives warning about use of GLOBALS below.  E_STRICT cannot be controlled through WIKINDX
			$error = error_get_last();
			$error = $error['message'];
			GLOBALS::addTplVar('content', \AJAX\encode_jArray(array('ERROR' => $error)));
		}
		else
			GLOBALS::addTplVar('content', \AJAX\encode_jArray($jsonResponseArray));
		FACTORY_CLOSERAW::getInstance();
	}
/**
Make div selected box
*/
	private function makeSelectBox(&$array, $type, $index)
	{
		if(array_key_exists("Select_$index", $this->input))
		{
			$selected = array();
			$temp = unserialize(base64_decode($this->input["Select_$index"]));
			foreach($temp as $value)
			{
				if(!array_key_exists($value, $array)) // could be the case bibliography used for browsing has changed
					continue;
				$selected[$value] = $array[$value];
				unset($array[$value]);
			}
			if(!empty($selected))
				$selectBox = \FORM\selectFBoxValueMultiple(FALSE, "advancedSearch_Select_$index", $selected, 5);
			else
				$selectBox = \FORM\selectFBoxValueMultiple(FALSE, "advancedSearch_Select_$index", array(), 5);
		}
		else
			$selectBox = \FORM\selectFBoxValueMultiple(FALSE, "advancedSearch_Select_$index", array(), 5);
		if(($type == 'type') || ($type == 'publisher') || ($type == 'collection') || ($type == 'tag') || ($type == 'addedBy') || ($type == 'editedBy'))
			$buttons = \HTML\span('&nbsp;OR', 'small') . \FORM\hidden("advancedSearch_Button2_$index", 'OR');
		else
			$buttons = $this->makeRadioButtons2("advancedSearch_Button2_$index");
		$pString = \HTML\tableStart();
		$pString .= \HTML\trStart();
		$pString .= \HTML\td($this->messages->text("select", $type), '', 3);
		$pString .= \HTML\trEnd();
		$pString .= \HTML\trStart();
		$pString .= \HTML\td($selectBox, 'left width5percent');
		$pString .= \HTML\td($buttons, 'left');
		$pString .= \HTML\td('&nbsp;', 'width100percent'); // Filler to ensure buttons are flush against select box
		$pString .= \HTML\trEnd();
		$pString .= \HTML\tableEnd();
		return $pString;
	}
/**
Resource type div
*/
	private function typeDiv($index)
	{
		$types = $this->type->grabAll($this->session->getVar("mywikindx_Bibliography_use"), TRUE);
		$selectBox = $this->makeSelectBox($types, 'type', $index);
		natcasesort($types);
		$pString = \HTML\tableStart();
		$pString .= \HTML\trStart();
		$pString .= \HTML\td(\FORM\selectFBoxValueMultiple($this->messages->text("select", "availableType"),
			"types_$index", $types, 5), 'left width10percent');
		list($toRightImage, $toLeftImage) = $this->transferArrows("types_$index", "advancedSearch_Select_$index");
		$pString .= \HTML\td(\HTML\p('&nbsp;' . $toRightImage) . \HTML\p('&nbsp;' . $toLeftImage), 'left width10percent');
		$pString .= \HTML\td($selectBox);
		$pString .= \HTML\trEnd();
		$pString .= \HTML\tableEnd();
		return $pString;
	}
/**
Category div
*/
	private function categoryDiv($index)
	{
		$categories = $this->category->grabAll($this->session->getVar("mywikindx_Bibliography_use"), TRUE);
		$selectBox = $this->makeSelectBox($categories, 'category', $index);
		natcasesort($categories);
		$pString = \HTML\tableStart();
		$pString .= \HTML\trStart();
		$pString .= \HTML\td(\FORM\selectFBoxValueMultiple($this->messages->text("select", "availableCategory"),
			"categories_$index", $categories, 5), 'left width10percent');
		list($toRightImage, $toLeftImage) = $this->transferArrows("categories_$index", "advancedSearch_Select_$index");
		$pString .= \HTML\td(\HTML\p('&nbsp;' . $toRightImage) . \HTML\p('&nbsp;' . $toLeftImage), 'left width10percent');
		$pString .= \HTML\td($selectBox);
		$pString .= \HTML\trEnd();
		$pString .= \HTML\tableEnd();
		return $pString;
	}
/**
Subcategory div
*/
	private function subcategoryDiv($index)
	{
		$subcategories = $this->category->grabSubAll(TRUE, $this->session->getVar("mywikindx_Bibliography_use"), FALSE, TRUE);
		$selectBox = $this->makeSelectBox($subcategories, 'subcategory', $index);
		natcasesort($subcategories);
		$pString = \HTML\tableStart();
		$pString .= \HTML\trStart();
		$pString .= \HTML\td(\FORM\selectFBoxValueMultiple($this->messages->text("select", "availableSubcategory"),
			"subcategories_$index", $subcategories, 5), 'left width10percent');
		list($toRightImage, $toLeftImage) = $this->transferArrows("subcategories_$index", "advancedSearch_Select_$index");
		$pString .= \HTML\td(\HTML\p('&nbsp;' . $toRightImage) . \HTML\p('&nbsp;' . $toLeftImage), 'left width10percent');
		$pString .= \HTML\td($selectBox);
		$pString .= \HTML\trEnd();
		$pString .= \HTML\tableEnd();
		return $pString;
	}
/**
Creator div
*/
	private function creatorDiv($index)
	{
		$creators = $this->creator->grabAll($this->session->getVar("mywikindx_Bibliography_use"));
		$selectBox = $this->makeSelectBox($creators, 'creator', $index);
		natcasesort($creators);
		$pString = \HTML\tableStart();
		$pString .= \HTML\trStart();
		$pString .= \HTML\td(\FORM\selectFBoxValueMultiple($this->messages->text("select", "availableCreator"),
			"creators_$index", $creators, 5), 'left width10percent');
		list($toRightImage, $toLeftImage) = $this->transferArrows("creators_$index", "advancedSearch_Select_$index");
		$pString .= \HTML\td(\HTML\p('&nbsp;' . $toRightImage) . \HTML\p('&nbsp;' . $toLeftImage), 'left width10percent');
		$pString .= \HTML\td($selectBox);
		$pString .= \HTML\trEnd();
		$pString .= \HTML\tableEnd();
		return $pString;
	}
/**
Keyword div
*/
	private function keywordDiv($index)
	{
		$keywords = $this->keyword->grabAll($this->session->getVar("mywikindx_Bibliography_use"), 'resource');
		$selectBox = $this->makeSelectBox($keywords, 'keyword', $index);
		natcasesort($keywords);
		$pString = \HTML\tableStart();
		$pString .= \HTML\trStart();
		$pString .= \HTML\td(\FORM\selectFBoxValueMultiple($this->messages->text("select", "availableKeyword"),
			"keywords_$index", $keywords, 5), 'left width10percent');
		list($toRightImage, $toLeftImage) = $this->transferArrows("keywords_$index", "advancedSearch_Select_$index");
		$pString .= \HTML\td(\HTML\p('&nbsp;' . $toRightImage) . \HTML\p('&nbsp;' . $toLeftImage), 'left width10percent');
		$pString .= \HTML\td($selectBox);
		$pString .= \HTML\trEnd();
		$pString .= \HTML\tableEnd();
		return $pString;
	}
/**
Metadata Keyword div
*/
	private function metaKeywordDiv($index)
	{
		$metakeywords = $this->keyword->grabAll(TRUE, array('quote', 'paraphrase', 'quoteComment', 'paraphraseComment', 'musing', 'idea'));
		$selectBox = $this->makeSelectBox($metakeywords, 'metaKeyword', $index);
		natcasesort($metakeywords);
		$pString = \HTML\tableStart();
		$pString .= \HTML\trStart();
		$pString .= \HTML\td(\FORM\selectFBoxValueMultiple($this->messages->text("select", "availableMetaKeyword"),
			"metakeywords_$index", $metakeywords, 5), 'left width10percent');
		list($toRightImage, $toLeftImage) = $this->transferArrows("metakeywords_$index", "advancedSearch_Select_$index");
		$pString .= \HTML\td(\HTML\p('&nbsp;' . $toRightImage) . \HTML\p('&nbsp;' . $toLeftImage), 'left width10percent');
		$pString .= \HTML\td($selectBox);
		$pString .= \HTML\trEnd();
		$pString .= \HTML\tableEnd();
		return $pString;
	}
/**
Language div
*/
	private function languageDiv($index)
	{
		$this->grabLanguages();
		$selectBox = $this->makeSelectBox($this->languages, 'language', $index);
		natcasesort($this->languages);
		$pString = \HTML\tableStart();
		$pString .= \HTML\trStart();
		$pString .= \HTML\td(\FORM\selectFBoxValueMultiple($this->messages->text("select", "availableLanguage"),
			"languages_$index", $this->languages, 5), 'left width10percent');
		list($toRightImage, $toLeftImage) = $this->transferArrows("languages_$index", "advancedSearch_Select_$index");
		$pString .= \HTML\td(\HTML\p('&nbsp;' . $toRightImage) . \HTML\p('&nbsp;' . $toLeftImage), 'left width10percent');
		$pString .= \HTML\td($selectBox);
		$pString .= \HTML\trEnd();
		$pString .= \HTML\tableEnd();
		return $pString;
	}
/**
Publisher div
*/
	private function publisherDiv($index)
	{
		$publishers = $this->publisher->grabAll(FALSE, $this->session->getVar("mywikindx_Bibliography_use"));
		$selectBox = $this->makeSelectBox($publishers, 'publisher', $index);
		natcasesort($publishers);
		$pString = \HTML\tableStart();
		$pString .= \HTML\trStart();
		$pString .= \HTML\td(\FORM\selectFBoxValueMultiple($this->messages->text("select", "availablePublisher"),
			"publishers_$index", $publishers, 5), 'left width10percent');
		list($toRightImage, $toLeftImage) = $this->transferArrows("publishers_$index", "advancedSearch_Select_$index");
		$pString .= \HTML\td(\HTML\p('&nbsp;' . $toRightImage) . \HTML\p('&nbsp;' . $toLeftImage), 'left width10percent');
		$pString .= \HTML\td($selectBox);
		$pString .= \HTML\trEnd();
		$pString .= \HTML\tableEnd();
		return $pString;
	}
/**
Collection div
*/
	private function collectionDiv($index)
	{
		$collections = $this->collection->grabAll(FALSE, $this->session->getVar("mywikindx_Bibliography_use"));
		$selectBox = $this->makeSelectBox($collections, 'collection', $index);
		natcasesort($collections);
		$pString = \HTML\tableStart();
		$pString .= \HTML\trStart();
		$pString .= \HTML\td(\FORM\selectFBoxValueMultiple($this->messages->text("select", "availableCollection"),
			"collections_$index", $collections, 5), 'left width10percent');
		list($toRightImage, $toLeftImage) = $this->transferArrows("collections_$index", "advancedSearch_Select_$index");
		$pString .= \HTML\td(\HTML\p('&nbsp;' . $toRightImage) . \HTML\p('&nbsp;' . $toLeftImage), 'left width10percent');
		$pString .= \HTML\td($selectBox);
		$pString .= \HTML\trEnd();
		$pString .= \HTML\tableEnd();
		return $pString;
	}
/**
UserTag div
*/
	private function userTagDiv($index)
	{
		$userTags = $this->userTag->grabAll($this->session->getVar("mywikindx_Bibliography_use"), FALSE, TRUE);
		$selectBox = $this->makeSelectBox($userTags, 'userTag', $index);
		natcasesort($userTags);
		$pString = \HTML\tableStart();
		$pString .= \HTML\trStart();
		$pString .= \HTML\td(\FORM\selectFBoxValueMultiple($this->messages->text("select", "availableUserTag"),
			"userTags_$index", $userTags, 5), 'left width10percent');
		list($toRightImage, $toLeftImage) = $this->transferArrows("userTags_$index", "advancedSearch_Select_$index");
		$pString .= \HTML\td(\HTML\p('&nbsp;' . $toRightImage) . \HTML\p('&nbsp;' . $toLeftImage), 'left width10percent');
		$pString .= \HTML\td($selectBox);
		$pString .= \HTML\trEnd();
		$pString .= \HTML\tableEnd();
		return $pString;
	}
/**
Import Tag div
*/
	private function tagDiv($index)
	{
		$tags = $this->tag->grabAll();
		$selectBox = $this->makeSelectBox($tags, 'tag', $index);
		natcasesort($tags);
		$pString = \HTML\tableStart();
		$pString .= \HTML\trStart();
		$pString .= \HTML\td(\FORM\selectFBoxValueMultiple($this->messages->text("select", "availableTag"),
			"tags_$index", $tags, 5), 'left width10percent');
		list($toRightImage, $toLeftImage) = $this->transferArrows("tags_$index", "advancedSearch_Select_$index");
		$pString .= \HTML\td(\HTML\p('&nbsp;' . $toRightImage) . \HTML\p('&nbsp;' . $toLeftImage), 'left width10percent');
		$pString .= \HTML\td($selectBox);
		$pString .= \HTML\trEnd();
		$pString .= \HTML\tableEnd();
		return $pString;
	}
/**
AddedBy div
*/
	private function addedByDiv($index)
	{
		$users = $this->grabUsers('add');
		$selectBox = $this->makeSelectBox($users, 'addedBy', $index);
		natcasesort($users);
		$pString = \HTML\tableStart();
		$pString .= \HTML\trStart();
		$pString .= \HTML\td(\FORM\selectFBoxValueMultiple($this->messages->text("select", "availableAddedBy"),
			"addedBy_$index", $users, 5), 'left width10percent');
		list($toRightImage, $toLeftImage) = $this->transferArrows("addedBy_$index", "advancedSearch_Select_$index");
		$pString .= \HTML\td(\HTML\p('&nbsp;' . $toRightImage) . \HTML\p('&nbsp;' . $toLeftImage), 'left width10percent');
		$pString .= \HTML\td($selectBox);
		$pString .= \HTML\trEnd();
		$pString .= \HTML\tableEnd();
		return $pString;
	}
/**
EditedBy div
*/
	private function editedByDiv($index)
	{
		$users = $this->grabUsers('edit');
		$selectBox = $this->makeSelectBox($users, 'editedBy', $index);
		natcasesort($users);
		$pString = \HTML\tableStart();
		$pString .= \HTML\trStart();
		$pString .= \HTML\td(\FORM\selectFBoxValueMultiple($this->messages->text("select", "availableEditedBy"),
			"editedBy_$index", $users, 5), 'left width10percent');
		list($toRightImage, $toLeftImage) = $this->transferArrows("editedBy_$index", "advancedSearch_Select_$index");
		$pString .= \HTML\td(\HTML\p('&nbsp;' . $toRightImage) . \HTML\p('&nbsp;' . $toLeftImage), 'left width10percent');
		$pString .= \HTML\td($selectBox);
		$pString .= \HTML\trEnd();
		$pString .= \HTML\tableEnd();
		return $pString;
	}
/**
Value div for publicationYear, access, maturityIndex
*/
	private function valueDiv($index)
	{
		$comps = array('=', '!=', '<', '>', '<=', '>=', '<...<');
		$jsonArray = array();
		$jScript = "index.php?action=list_SEARCH_CORE&method=addComparisonValue";
		$jsonArray[] = array(
			'startFunction' => "addComparisonValue",
			'script' => "$jScript",
			'elementIndex' => "$index",
			'targetDiv' => "value2Container_$index",
			);
		$js = \AJAX\jActionForm('onclick', $jsonArray);
		$pString = \HTML\tableStart();
		$pString .= \HTML\trStart();
		if(array_key_exists("Comparison_$index", $this->input))
			$selectBox = \FORM\selectedBoxValue(FALSE, "advancedSearch_Comparison_$index", $comps, $this->input["Comparison_$index"], 7, FALSE, $js);
		else
			$selectBox = \FORM\selectFBoxValue(FALSE, "advancedSearch_Comparison_$index", $comps, 7, FALSE, $js);
		$pString .= \HTML\td($selectBox, 'left width10percent');
		if(array_key_exists("Value1_$index", $this->input))
			$textInput = \FORM\textInput(FALSE, "advancedSearch_Value1_$index", $this->input["Value1_$index"]);
		else
			$textInput = \FORM\textInput(FALSE, "advancedSearch_Value1_$index");
		$pString .= \HTML\td($textInput, 'left width10percent');
		if(array_key_exists("Value2_$index", $this->input))
			$pString .= \HTML\td(\HTML\div("value2Container_$index",
				\FORM\textInput(FALSE, "advancedSearch_Value2_$index", $this->input["Value2_$index"])), 'left width10percent');
		else
			$pString .= \HTML\td(\HTML\div("value2Container_$index", ' '), 'left width10percent'); // needed for '<...<'
		$pString .= \HTML\td('&nbsp;', 'width100percent'); // Filler to ensure buttons are flush against select box
		$pString .= \HTML\trEnd();
		$pString .= \HTML\tableEnd();
		return $pString;
	}
// Add a new text input for a second value for comparison
	public function addComparisonValue()
	{
		$vars = GLOBALS::getVars();
		$jArray = \AJAX\decode_jString($vars['ajaxReturn']);
		$index = $jArray['elementIndex'];
		if($jArray['execute'] === FALSE) // set to TRUE in searchSelect.js addComparisonValue() if option 6 of select box is selected
			$div = ' ';
		else
			$div = \FORM\textInput(FALSE, "advancedSearch_Value2_$index");
		$jsonResponseArray = array(
			'innerHTML' => "$div",
			);
		if(is_array(error_get_last()))
		{
// NB E_STRICT in PHP5 gives warning about use of GLOBALS below.  E_STRICT cannot be controlled through WIKINDX
			$error = error_get_last();
			$error = $error['message'];
			GLOBALS::addTplVar('content', \AJAX\encode_jArray(array('ERROR' => $error)));
		}
		else
			GLOBALS::addTplVar('content', \AJAX\encode_jArray($jsonResponseArray));
		FACTORY_CLOSERAW::getInstance();
	}
// get languages from database.
	private function grabLanguages()
	{
		$userBib = $this->session->getVar("mywikindx_Bibliography_use");
		if($userBib)
			$this->commonBib->userBibCondition('resourceLanguageResourceId');
		$this->db->orderBy('languageLanguage');
		$this->db->leftJoin('language', 'languageId', 'resourcelanguageLanguageId');
		$resultset = $this->db->select('resource_language', array('resourcelanguageLanguageId', 'languageLanguage'), TRUE);
		while($row = $this->db->fetchRow($resultset))
			$this->languages[$row['resourcelanguageLanguageId']] = $row['languageLanguage'];
	}
// grab users
	private function grabUsers($addEdit = 'add')
	{
		return $this->user->grabAll(TRUE, $this->session->getVar("mywikindx_Bibliography_use"), $addEdit);
	}
/**
* Make the transfer arrows to transfer fields between select boxes with onclick
*
* @return array (toRightImage, toLeftImage)
*/
	public function transferArrows($source, $target)
	{

		$jsonArrayS = $jsonArrayD = array();
		$jsonArrayS[] = array(
			'startFunction' => 'search_Transfer',
			'source' => "$source",
			'target' => "$target",
			);
		$toRightImage = \AJAX\jActionIcon('toRight', 'onclick', $jsonArrayS);
		$jsonArrayD[] = array(
			'startFunction' => 'search_Transfer',
			'source' => "$target",
			'target' => "$source",
			);
		$toLeftImage = \AJAX\jActionIcon('toLeft', 'onclick', $jsonArrayD);
		return array($toRightImage, $toLeftImage);
	}
// Check what search fields are available for this user in the database
/*
For comments, in the database resourcemetadataMetadataId references the resourcemetadataId field of the parent quote or paraphrase. So,
for searches within a bibliography, the SQL should be something like (here, pulling paraphrase comments against resources in bibliography ID 86
and with user ID 1):
SELECT COUNT(*) AS `count`, `resourcemetadataId`
FROM `WKX_resource_metadata`
LEFT JOIN `WKX_user_bibliography_resource` ON `userbibliographyresourceResourceId` = `resourcemetadataResourceId`
WHERE
(
	`resourcemetadataId` IN
	(
		SELECT `resourcemetadataMetadataId`
		FROM `WKX_resource_metadata`
		WHERE
			(CASE
				WHEN (`resourcemetadataPrivate` != 'N' AND `resourcemetadataPrivate` != 'Y' )
				THEN
				(
					SELECT `usergroupsusersId`
					FROM `WKX_user_groups_users`
					WHERE (`usergroupsusersUserId` = '1')
					AND (`usergroupsusersGroupId` = `resourcemetadataPrivate`)
				)
				END
				OR
				CASE WHEN (`resourcemetadataPrivate` = 'Y' )
				THEN
					(`resourcemetadataAddUserId` = '1')
				END
				OR
				CASE WHEN (`resourcemetadataPrivate` = 'N' )
				THEN
					('1')
				END)
			AND
			(`resourceMetadataType` = 'pc')
	)
)
AND (`userbibliographyresourceBibliographyId` = '86')
GROUP BY `resourcemetadataId`
*/
	private function checkAvailableFields()
	{
		$userId = $this->session->getVar('setup_UserId');
// userTags
		$this->db->formatConditions(array('userTagsUserId' => $userId));
		if(!empty($this->userTag->grabAll($this->session->getVar("mywikindx_Bibliography_use"), FALSE, TRUE)))
			$this->displayUserTags = TRUE;
		if((!$this->session->getVar('setup_MetadataAllow')))
		{
			if((!$this->session->getVar('setup_MetadataUserOnly')))
			{
				$this->displayIdeas = $this->displayQCs = $this->displayPCs = $this->displayMusings = $this->displayMKs = FALSE;
				return;
			}
		}
		$userBib = $this->session->getVar("mywikindx_Bibliography_use");
// for everything here, the user must be logged on
		if(!$userId)
			return;
// ideas (which are independent of resources). setCondition() returns FALSE if user is not logged on. ReadOnly users never see musings, ideas, or comments.
		if(!$this->metadata->setCondition('i'))
			return;
		$resultSet = $this->db->selectCount('resource_metadata', 'resourcemetadataId');
		if($this->db->fetchOne($resultSet))
			$this->displayIdeas = TRUE;
// quote comments
		if(!$this->metadata->setCondition('qc'))
			return;
		$subQ = $this->db->subQuery($this->db->selectNoExecute('resource_metadata', 'resourcemetadataMetadataId'), FALSE, FALSE, TRUE);
		$this->db->formatConditions($this->db->formatFields('resourcemetadataId') . $this->db->inClause($subQ));
		if($userBib)
			$this->commonBib->userBibCondition('resourcemetadataResourceId');
		$resultSet = $this->db->selectCount('resource_metadata', 'resourcemetadataId');
		if($this->db->fetchOne($resultSet))
			$this->displayQCs = TRUE;
// paraphrase comments
		if(!$this->metadata->setCondition('pc'))
			return;
		$subQ = $this->db->subQuery($this->db->selectNoExecute('resource_metadata', 'resourcemetadataMetadataId'), FALSE, FALSE, TRUE);
		$this->db->formatConditions($this->db->formatFields('resourcemetadataId') . $this->db->inClause($subQ));
		if($userBib)
			$this->commonBib->userBibCondition('resourcemetadataResourceId');
		$resultSet = $this->db->selectCount('resource_metadata', 'resourcemetadataId');
		if($this->db->fetchOne($resultSet))
			$this->displayPCs = TRUE;
// musings
		if(!$this->metadata->setCondition('m'))
			return;
		if($userBib)
			$this->commonBib->userBibCondition('resourcemetadataResourceId');
		$resultSet = $this->db->selectCount('resource_metadata', 'resourcemetadataId');
		if($this->db->fetchOne($resultSet))
			$this->displayMusings = TRUE;
// metadatakeywords (user restrictions if ideas and musings)
		if($userBib)
			$this->commonBib->userBibCondition('resourcekeywordResourceId');
		$this->db->formatConditions(array('resourcekeywordMetadataId' => 'IS NOT NULL'));
		$resultSet = $this->db->selectCount('resource_keyword', 'resourcekeywordMetadataId');
		if($this->db->fetchOne($resultSet))
			$this->displayMKs = TRUE;
		if(!$this->displayMKs)
		{
			if(!$this->metadata->setCondition('m'))
				return;
			$subQ = $this->db->subQuery($this->db->selectNoExecute('resource_keyword', 'resourcekeywordMetadataId'), FALSE, FALSE, TRUE);
			$this->db->formatConditions($this->db->formatFields('resourcemetadataId') . $this->db->inClause($subQ));
			if($userBib)
				$this->commonBib->userBibCondition('resourcemetadataResourceId');
			$resultSet = $this->db->selectCount('resource_metadata', 'resourcemetadataId');
			if($this->db->fetchOne($resultSet))
				$this->displayMKs = TRUE;
		}
		if(!$this->displayMKs)
		{
			if(!$this->metadata->setCondition('i'))
				return;
			$subQ = $this->db->subQuery($this->db->selectNoExecute('resource_keyword', 'resourcekeywordMetadataId'), FALSE, FALSE, TRUE);
			$this->db->formatConditions($this->db->formatFields('resourcemetadataId') . $this->db->inClause($subQ));
			if($userBib)
				$this->commonBib->userBibCondition('resourcemetadataResourceId');
			$resultSet = $this->db->selectCount('resource_metadata', 'resourcemetadataId');
			if($this->db->fetchOne($resultSet))
				$this->displayMKs = TRUE;
		}
	}
// Return array of database fields to perform the search on. This populates the first select box
	private function searchFields($index = 0)
	{
		$userBib = $this->session->getVar("mywikindx_Bibliography_use");
		$fields = array("title"	=>	$this->messages->text("search", "title"));
		if($userBib)
			$this->commonBib->userBibCondition('resourcetextId');
		$this->db->formatConditions(array('resourcetextNote' => 'IS NOT NULL'));
		$resultSet = $this->db->select('resource_text', 'resourcetextId');
		if($this->db->fetchOne($resultSet))
			$fields['note'] = $this->messages->text("search", "note");
		$this->db->formatConditions(array('resourcetextAbstract' => 'IS NOT NULL'));
		$resultSet = $this->db->select('resource_text', 'resourcetextId');
		if($this->db->fetchOne($resultSet))
			$fields['abstract'] = $this->messages->text("search", "abstract");
		if((!$this->session->getVar("setup_FileViewLoggedOnOnly") || $this->session->getVar("setup_UserId")) &&
			($this->db->tableIsEmpty('resource_attachments') == 0))
			$fields['attachments'] = $this->messages->text("search", "attachments");
		if((!$this->session->getVar('setup_MetadataAllow')))
		{
			if(($this->session->getVar('setup_MetadataUserOnly')) && $this->session->getVar('setup_UserId'))
			{
				if($userBib)
					$this->commonBib->userBibCondition('resourcemetadataResourceId');
				$this->db->formatConditions(array('resourcemetadataType' => 'q'));
				$resultSet = $this->db->select('resource_metadata', 'resourcemetadataId');
				if($this->db->fetchOne($resultSet))
					$fields['quote'] = $this->messages->text("search", "quote");
			}
		}
		else
		{
			if($userBib)
				$this->commonBib->userBibCondition('resourcemetadataResourceId');
			$this->db->formatConditions(array('resourcemetadataType' => 'q'));
			$resultSet = $this->db->select('resource_metadata', 'resourcemetadataId');
			if($this->db->fetchOne($resultSet))
				$fields['quote'] = $this->messages->text("search", "quote");
		}
		if($this->displayQCs)
			$fields['quoteComment'] = $this->messages->text("search", "quoteComment");
		if((!$this->session->getVar('setup_MetadataAllow')))
		{
			if(($this->session->getVar('setup_MetadataUserOnly')) && $this->session->getVar('setup_UserId'))
			{
				if($userBib)
					$this->commonBib->userBibCondition('resourcemetadataResourceId');
				$this->db->formatConditions(array('resourcemetadataType' => 'p'));
				$resultSet = $this->db->select('resource_metadata', 'resourcemetadataId');
				if($this->db->fetchOne($resultSet))
					$fields['paraphrase'] = $this->messages->text("search", "paraphrase");
			}
		}
		else
		{
			if($userBib)
				$this->commonBib->userBibCondition('resourcemetadataResourceId');
			$this->db->formatConditions(array('resourcemetadataType' => 'p'));
			$resultSet = $this->db->select('resource_metadata', 'resourcemetadataId');
			if($this->db->fetchOne($resultSet))
				$fields['paraphrase'] = $this->messages->text("search", "paraphrase");
		}
		if($this->displayPCs)
			$fields['paraphraseComment'] = $this->messages->text("search", "paraphraseComment");
		if($this->displayMusings)
			$fields['musing'] = $this->messages->text("search", "musing");
		if($this->displayIdeas)
			$fields['idea'] = $this->messages->text("search", "idea");
// Add any used custom fields
		$subQ = $this->db->subQuery($this->db->selectNoExecute('resource_custom', 'resourcecustomCustomId'), FALSE, FALSE, TRUE);
		$this->db->formatConditions($this->db->formatFields('customId') . $this->db->inClause($subQ));
		$recordset = $this->db->select('custom', array('customId', 'customLabel', 'customSize'));
		while($row = $this->db->fetchRow($recordset))
		{
			if($row['customSize'] == 'S')
				$fields['Custom_S_' . $row['customId']] = \HTML\dbToFormTidy($row['customLabel']);
			else
				$fields['Custom_L_' . $row['customId']] = \HTML\dbToFormTidy($row['customLabel']);
		}
		if($userBib)
			$this->commonBib->userBibCondition('resourcecreatorId');
		if($this->db->fetchOne($this->db->select('resource_creator', 'resourcecreatorId')))
			$fields['creator'] = $this->messages->text("search", "creator");
		if($userBib)
			$this->commonBib->userBibCondition('resourcemiscPublisher');
		if($this->db->fetchOne($this->db->select('resource_misc', 'resourcemiscPublisher')))
			$fields['publisher'] = $this->messages->text("search", "publisher");
		if($userBib)
			$this->commonBib->userBibCondition('resourcemiscCollection');
		if($this->db->fetchOne($this->db->select('resource_misc', 'resourcemiscCollection')))
			$fields['collection'] = $this->messages->text("search", "collection");
		$fields['type'] = $this->messages->text("search", "type");
		$fields['category'] = $this->messages->text("search", "category"); // All resources always belong to at least one category
		if($userBib)
			$this->commonBib->userBibCondition('resourcecategoryResourceId');
		$this->db->formatConditions(array('resourcecategorySubcategoryId' => 'IS NOT NULL'));
		if($this->db->fetchOne($this->db->select('resource_category', 'resourcecategorySubcategoryId')))
			$fields['subcategory'] = $this->messages->text("search", "subcategory");
		if($userBib)
			$this->commonBib->userBibCondition('resourcekeywordResourceId');
		$this->db->formatConditions(array('resourcekeywordResourceId' => 'IS NOT NULL'));
		if($this->db->fetchOne($this->db->select('resource_keyword', 'resourcekeywordId')))
			$fields['keyword'] = $this->messages->text("search", "keyword");
		if($this->displayMKs)
			$fields['metaKeyword'] = $this->messages->text("search", "metaKeyword");
		if($this->displayUserTags)
			$fields['userTag'] = $this->messages->text("search", "usertag");
		if($userBib)
			$this->commonBib->userBibCondition('resourcelanguageResourceId');
		if($this->db->fetchOne($this->db->select('resource_language', 'resourcelanguageId')))
			$fields['language'] = $this->messages->text("search", "language");
		if($this->db->fetchOne($this->db->selectCount('tag', 'tagId')))
			$fields['tag'] = $this->messages->text("search", "tag");
// If logged on and multiuser, display addedBy and editedBy options
		if($this->session->getVar("setup_UserId") && ($this->session->getVar("setup_MultiUser")))
		{
			if($userBib)
				$this->commonBib->userBibCondition('resourcemiscId');
			$this->db->formatConditions(array('resourcemiscAddUserIdResource' => 'IS NOT NULL'));
			if($this->db->fetchOne($this->db->select('resource_misc', 'resourcemiscAddUserIdResource')))
				$fields['addedBy'] = $this->messages->text("search", "addedBy");
			if($userBib)
				$this->commonBib->userBibCondition('resourcemiscId');
			$this->db->formatConditions(array('resourcemiscEditUserIdResource' => 'IS NOT NULL'));
			if($this->db->fetchOne($this->db->select('resource_misc', 'resourcemiscEditUserIdResource')))
				$fields['editedBy'] = $this->messages->text("search", "editedBy");
		}
		if($userBib)
			$this->commonBib->userBibCondition('resourceyearId');
		$this->db->formatConditions(array('resourceyearYear1' => 'IS NOT NULL'));
		if($this->db->fetchOne($this->db->select('resource_year', 'resourceyearYear1')))
			$fields['publicationYear'] = $this->messages->text("search", "publicationYear");
		if($userBib)
			$this->commonBib->userBibCondition('resourcemiscId');
			$this->db->formatConditions(array('resourcemiscAccesses' => 'IS NOT NULL'));
		if($this->db->fetchOne($this->db->select('resource_misc', 'resourcemiscAccesses')))
			$fields['access'] = $this->messages->text("search", "access");
		if($userBib)
			$this->commonBib->userBibCondition('resourcemiscId');
		if($this->db->fetchOne($this->db->select('resource_misc', 'resourcemiscMaturityIndex')))
			$fields['maturityIndex'] = $this->messages->text("search", "maturityIndex");
		$jsonArray = array();
		$jScript = "index.php?action=list_SEARCH_CORE&method=switchField";
		$triggerField = 'advancedSearch_Field_' . $index;
		$targetDiv = 'searchElementContainer_' . $index;
		$jsonArray[] = array(
			'startFunction' => 'triggerSearchParameter',
			'script' => "$jScript",
			'triggerField' => "$triggerField",
			'targetDiv' => "$targetDiv",
			'elementIndex' => "$index",
			);
		if($index > 1)
		{
			$jScript = "index.php?action=list_SEARCH_CORE&method=switchButtons";
			$triggerField = 'advancedSearch_Field_' . $index;
			$targetDiv = 'searchElementButtons_' . $index;
			$jsonArray[] = array(
				'startFunction' => 'triggerSearchParameter',
				'script' => "$jScript",
				'triggerField' => "$triggerField",
				'targetDiv' => "$targetDiv",
				'elementIndex' => "$index",
				);
		}
		$js = \AJAX\jActionForm('onchange', $jsonArray);
		if(array_key_exists("Field_$index", $this->input))
			return \FORM\selectedBoxValue($this->messages->text("search", "searchSelect"), "advancedSearch_Field_$index",
				$fields, $this->input["Field_$index"], 5, FALSE, $js);
		else
			return \FORM\selectFBoxValue($this->messages->text("search", "searchSelect"), "advancedSearch_Field_$index", $fields, 5, FALSE, $js);
	}
/**
Change OR/AND buttons for fields to unselectable OR if field == idea
*/
	public function switchButtons()
	{
		$vars = GLOBALS::getVars();
		$jArray = \AJAX\decode_jString($vars['ajaxReturn']);
		if($jArray['elementIndex'] == 1)
		{
			FACTORY_CLOSERAW::getInstance();
			die;
		}
		$i = $jArray['elementIndex'];
		if($jArray['field'] == 'idea')
			$div = \HTML\span('OR', 'small') . \FORM\hidden("advancedSearch_Button1_$i", 'OR');
		else
			$div = $this->makeRadioButtons1("advancedSearch_Button1_$i");
		$jsonResponseArray = array(
			'innerHTML' => "$div",
			);
		if(is_array(error_get_last()))
		{
// NB E_STRICT in PHP5 gives warning about use of GLOBALS below.  E_STRICT cannot be controlled through WIKINDX
			$error = error_get_last();
			$error = $error['message'];
			GLOBALS::addTplVar('content', \AJAX\encode_jArray(array('ERROR' => $error)));
		}
		else
			GLOBALS::addTplVar('content', \AJAX\encode_jArray($jsonResponseArray));
		FACTORY_CLOSERAW::getInstance();
	}
/**
Switch container fields depending on type of parameter selected in first field select box
*/
	public function switchField()
	{
		$vars = GLOBALS::getVars();
		$jArray = \AJAX\decode_jString($vars['ajaxReturn']);
		$div = $this->createDivs($jArray['field'], $jArray['elementIndex']);
		$jsonResponseArray = array(
			'innerHTML' => "$div",
			);
		if(is_array(error_get_last()))
		{
// NB E_STRICT in PHP5 gives warning about use of GLOBALS below.  E_STRICT cannot be controlled through WIKINDX
			$error = error_get_last();
			$error = $error['message'];
			GLOBALS::addTplVar('content', \AJAX\encode_jArray(array('ERROR' => $error)));
		}
		else
			GLOBALS::addTplVar('content', \AJAX\encode_jArray($jsonResponseArray));
		FACTORY_CLOSERAW::getInstance();
	}
/**
Create divs
*/
	private function createDivs($field, $index)
	{
		switch ($field)
		{
			case 'title':
			case 'note':
			case 'abstract':
			case 'attachments':
			case 'quote':
			case 'quoteComment':
			case 'paraphrase':
			case 'paraphraseComment':
			case 'idea':
				$div = $this->wordDiv($index);
				break;
			case 'category':
				$div = $this->categoryDiv($index);
				break;
			case 'type':
				$div = $this->typeDiv($index);
				break;
			case 'subcategory':
				$div = $this->subcategoryDiv($index);
				break;
			case 'creator':
				$div = $this->creatorDiv($index);
				break;
			case 'keyword':
				$div = $this->keywordDiv($index);
				break;
			case 'metaKeyword':
				$div = $this->metaKeywordDiv($index);
				break;
			case 'userTag':
				$div = $this->userTagDiv($index);
				break;
			case 'language':
				$div = $this->languageDiv($index);
				break;
			case 'publisher':
				$div = $this->publisherDiv($index);
				break;
			case 'collection':
				$div = $this->collectionDiv($index);
				break;
			case 'tag':
				$div = $this->tagDiv($index);
				break;
			case 'addedBy':
				$div = $this->addedByDiv($index);
				break;
			case 'editedBy':
				$div = $this->editedByDiv($index);
				break;
			case 'publicationYear':
			case 'access':
			case 'maturityIndex':
				$div = $this->valueDiv($index);
				break;
			default: // any custom tags
				$div = $this->wordDiv($index);
				break;
		}
		return $div;
	}
// relationships of search fields to database fields and tables (used when search is submitted)
	private function populateDbFields()
	{
		$this->dbFields = array(
		'type' => array('resourceType', 'resource', 'resourceId'),
		'title' => array('resourceTitleSort', 'resource', 'resourceId'),
		'note' => array('resourcetextNote', 'resource_text', 'resourcetextId'),
		'abstract' => array('resourcetextAbstract', 'resource_text', 'resourcetextId'),
		'quote' => array('resourcemetadataText', 'resource_metadata', 'resourcemetadataResourceId', 'q'),
		'paraphrase' => array('resourcemetadataText', 'resource_metadata', 'resourcemetadataResourceId', 'p'),
		'quoteComment' => array('resourcemetadataText', 'resource_metadata', 'resourcemetadataResourceId', 'qc'),
		'paraphraseComment' => array('resourcemetadataText', 'resource_metadata', 'resourcemetadataResourceId', 'pc'),
		'musing' => array('resourcemetadataText', 'resource_metadata', 'resourcemetadataResourceId', 'm'),
		'idea' => array('resourcemetadataText', 'resource_metadata'),
		'creator' => array('resourcecreatorCreatorId', 'resource_creator', 'resourcecreatorResourceId'),
		'publisher' => array('resourcemiscPublisher', 'resource_misc', 'resourcemiscId'),
		'collection' => array('resourcemiscCollection', 'resource_misc', 'resourcemiscId'),
		'category' => array('resourcecategoryCategoryId', 'resource_category', 'resourcecategoryResourceId'),
		'subcategory' => array('resourcecategorySubcategoryId', 'resource_category', 'resourcecategoryResourceId'),
		'keyword' => array('resourcekeywordKeywordId', 'resource_keyword', 'resourcekeywordResourceId'),
		'metaKeyword' => array('resourcekeywordKeywordId', 'resource_keyword', 'resourcemetadataResourceId'),
		'userTag' => array('resourceusertagsTagId', 'resource_user_tags', 'resourceusertagsResourceId'),
		'language' => array('resourcelanguageLanguageId', 'resource_language', 'resourcelanguageResourceId'),
		'tag' => array('resourcemiscTag', 'resource_misc', 'resourcemiscId'),
		'addedBy' => array('resourcemiscAddUserIdResource', 'resource_misc', 'resourcemiscId'),
		'editedBy' => array('resourcemiscEditUserIdResource', 'resource_misc', 'resourcemiscId'),
		'publicationYear' => array('resourceyearYear1', 'resource_year', 'resourceyearId'),
		'access' => array('resourcemiscAccesses', 'resource_misc', 'resourcemiscId'),
		'maturityIndex' => array('resourcemiscMaturityIndex', 'resource_misc', 'resourcemiscId'),
		);
// Add any used custom fields
		$subQ = $this->db->subQuery($this->db->selectNoExecute('resource_custom', 'resourcecustomCustomId'), FALSE, FALSE, TRUE);
		$this->db->formatConditions($this->db->formatFields('customId') . $this->db->inClause($subQ));
		$recordset = $this->db->select('custom', array('customId', 'customLabel', 'customSize'));
		while($row = $this->db->fetchRow($recordset))
		{
			$fieldPrefix = $row['customSize'] == 'S' ? 'S' : 'L';
			$this->dbFields['Custom_' . $fieldPrefix . '_' . $row['customId']] =
					array('resourcecustomShort', 'resource_custom', 'resourcecustomResourceId', $row['customId']);
		}
	}
/**
* For re-ordering or paging
*/
	public function reprocess()
	{
		$this->input = $this->session->getArray("advancedSearch");
		if(array_key_exists("search_Order", $this->vars) && $this->vars["search_Order"])
		{
			$this->input['Order'] = $this->vars["search_Order"];
			$this->session->setVar('search_Order', $this->input['Order']);
			$this->session->setVar('advancedSearch_Order', $this->input['Order']);
			$this->session->setVar('sql_LastOrder', $this->input['Order']);
			$this->session->setVar('search_AscDesc', $this->vars['search_AscDesc']);
			$this->session->setVar('advancedSearch_AscDesc', $this->vars['search_AscDesc']);
		}
		$this->process(TRUE);
	}
/*
Create the SQL fragments from the search fields that will make up the final SQL search statement
*/
	private function createSqlFragments($array)
	{
		ksort($array);
		$index = 0;
		$excludedIds = array();
		foreach($array as $key => $valueArray)
		{
			if(($valueArray['OriginalField'] != 'idea') && array_key_exists('Button1', $valueArray) && !$index)
				unset($valueArray['Button1']); // i.e. 'idea' was the first field searched on so there should be no button
			if(array_key_exists('String', $valueArray) && ($valueArray['OriginalField'] == 'idea'))
			{
				$this->ideas[$key] = $valueArray;
				continue;
			}
			else if(array_key_exists('String', $valueArray)) // 'String' already with field and formatted for SQL
				$this->createStringCondition($key, $valueArray);
			else if(array_key_exists('Select', $valueArray)) // 'Select' from a select box
				$this->createSelectCondition($key, $valueArray);
			else if(array_key_exists('Comparison', $valueArray)) // 'Comparison' of a value or two values
				$this->createComparisonCondition($key, $valueArray);
			if(!$this->validSearch)
				unset($this->unionFragments[$key]);
			$this->validSearch = TRUE;
			++$index;
		}
// check for attachment searches
/*
		$matchedIds = array();
		$temp = $this->matchIds;
		foreach($this->matchIds as $key1 => $array)
		{
			foreach($array as $key2 => $id)
			{
				if(!array_search($id, $matchedIds))
					unset($temp[$key1][$key2]);
				else
					$matchedIds[] = $id;
			}
		}
		$this->matchIds = $temp;
*/		if(!empty($this->unionFragments) && !empty($this->excludeIds))
		{
			$index = 0;
			foreach($this->unionFragments as $key => $uf) // append excluded resourceId conditions for attachment searches to unionFragments elements
			{
				foreach($this->excludeIds as $array)
				{
					foreach($array as $id)
					{
						if(array_search($id, $excludedIds) !== FALSE)
							continue;
						$condition = $this->db->and . $this->db->formatConditions(array($this->unionResourceIds[$index] => $id), TRUE, TRUE);
						$uf .= $condition;
						$excludedIds[] = $id;
					}
				}
				$this->unionFragments[$key] = $uf;
				++$index;
			}
		}
		$matchSearches = array();
		foreach($this->matchIds as $key1 => $array)
		{
			if(empty($array))
				continue;
			if(!array_key_exists('Button', $this->attachmentSearches[$key1]) || ($this->attachmentSearches[$key1]['Button'] == 'OR'))
			{
				$this->db->formatConditionsOneField($array, 'resourceId');
				$matchSearches[] = $this->db->queryNoExecute($this->db->selectNoExecute('resource', array(array('resourceId' => 'rId'))));
			}
			else if($this->attachmentSearches[$key1]['Button'] == 'AND')
			{
				if(empty($this->unionFragments))
				{
					$this->db->formatConditionsOneField($array, 'resourceId');
					$matchSearches[] = $this->db->queryNoExecute($this->db->selectNoExecute('resource', array(array('resourceId' => 'rId'))));
				}
				else
				{
					$index = 0;
					foreach($this->unionFragments as $key2 => $uf)
					{
						$condition = $this->db->and . $this->db->formatConditionsOneField($array, $this->unionResourceIds[$index],
							FALSE, TRUE, FALSE, FALSE, TRUE);
						$uf .= $condition;
						$this->unionFragments[$key2] = $uf;
						++$index;
					}
				}
			}
			else if($this->attachmentSearches[$key1]['Button'] == 'NOT')
			{
				$this->db->formatConditionsOneField($array, 'resourceId');
				$matchSearches[] = $this->db->queryNoExecute($this->db->selectNoExecute('resource', array(array('resourceId' => 'rId'))));
			}
		}
		foreach($matchSearches as $search)
			$this->unionFragments[] = $search;
		if(!empty($this->unionFragments))
		{
			$this->stmt->unions = $this->db->union($this->unionFragments);
			return TRUE;
		}
		return FALSE;
	}
// Create the SQL fragment for string queries
	private function createStringCondition($key, $valueArray)
	{
		$commentSubQ = FALSE;
		if(array_key_exists('Custom', $valueArray))
			$this->db->formatConditions(array('resourcecustomCustomId' => $this->dbFields[$valueArray['OriginalField']][3]));
		else if(($valueArray['OriginalField'] == 'quote') || ($valueArray['OriginalField'] == 'paraphrase'))
			$this->db->formatConditions(array('resourcemetadataType' => $this->dbFields[$valueArray['OriginalField']][3]));
		else if(($valueArray['OriginalField'] == 'musing') || ($valueArray['OriginalField'] == 'quoteComment') ||
			($valueArray['OriginalField'] == 'paraphraseComment'))
		{
			if(!$this->metadata->setCondition($this->dbFields[$valueArray['OriginalField']][3]))
				$this->validSearch = FALSE;
		}
		$this->db->formatConditions($valueArray['String']);
		if($this->validSearch && (($valueArray['OriginalField'] == 'quoteComment') || ($valueArray['OriginalField'] == 'paraphraseComment')))
		{
			$subQ = $this->db->queryNoExecute($this->db->selectNoExecute($this->dbFields[$valueArray['OriginalField']][1],
				'resourcemetadataMetadataId'));
			$this->db->formatConditions($this->db->formatFields('resourcemetadataId') . $this->db->inClause($subQ));
			$this->createOptionConditions($valueArray, 'resourcemetadataResourceId');
			$commentSubQ = $this->db->queryNoExecute($this->db->selectNoExecute($this->dbFields[$valueArray['OriginalField']][1],
				array(array($this->dbFields[$valueArray['OriginalField']][2] => 'rId'))));
		}
		if(!array_key_exists('Button1', $valueArray) or ($valueArray['Button1'] == 'OR')) // Start a new unionFragments element
		{
			$this->unionResourceIds[] = $this->lastUnionResourceId = $this->dbFields[$valueArray['OriginalField']][2];
// must let query be created in order to clear any conditions -- then, if necessary, we can remove from the unionFragments array
			if($commentSubQ)
				$this->unionFragments[$key] = $commentSubQ;
			else
			{
				$this->createOptionConditions($valueArray);
				$this->unionFragments[$key] = $this->db->queryNoExecute($this->db->selectNoExecute($this->dbFields[$valueArray['OriginalField']][1],
					array(array($this->dbFields[$valueArray['OriginalField']][2] => 'rId'))));
			}
		}
// Button1 == 'AND' or 'NOT' so use inClause() as a condition rather than union() and append to previous unionFragments array element
		else if(($valueArray['Button1'] == 'AND') || ($valueArray['Button1'] == 'NOT'))
		{
			$subQ = $this->db->queryNoExecute($this->db->selectNoExecute($this->dbFields[$valueArray['OriginalField']][1],
				$this->dbFields[$valueArray['OriginalField']][2]));
			$lastUFKey = key(array_slice($this->unionFragments, -1, 1, TRUE));
			if($commentSubQ)
			{
				if($valueArray['Button1'] == 'AND')
					$fc = $this->db->formatConditions($this->db->formatFields($this->lastUnionResourceId) .
						$this->db->inClause($commentSubQ), FALSE, TRUE);
				else
					$fc = $this->db->formatConditions($this->db->formatFields($this->lastUnionResourceId) .
						$this->db->inClause($commentSubQ, TRUE), FALSE, TRUE);
			}
			else
			{
				if($valueArray['Button1'] == 'AND')
					$fc = $this->db->formatConditions($this->db->formatFields($this->lastUnionResourceId) . $this->db->inClause($subQ), FALSE, TRUE);
				else
					$fc = $this->db->formatConditions($this->db->formatFields($this->lastUnionResourceId) . $this->db->inClause($subQ, TRUE), FALSE, TRUE);
			}
			$this->unionFragments[$lastUFKey] .= $fc;
			$this->validSearch = TRUE;
		}
	}
// Create the SQL fragment for select queries
	private function createSelectCondition($key, $valueArray)
	{
		$commentMK = FALSE;
// 'metaKeyword' => array('resourcekeywordKeywordId', 'resource_keyword', 'resourcemetadataResourceId'),
		if($valueArray['OriginalField'] == 'metaKeyword')
		{
			if($valueArray['Button2'] == 'OR')
				$this->db->formatConditionsOneField($valueArray['Select'], $this->dbFields[$valueArray['OriginalField']][0]);
			else
			{
				foreach($valueArray['Select'] as $select)
				{
					$this->db->formatConditions(array($this->dbFields[$valueArray['OriginalField']][0] => $select));
					$subQArray[] = $this->db->queryNoExecute($this->db->selectNoExecute($this->dbFields[$valueArray['OriginalField']][1],
						'resourcekeywordMetadataId'));
				}
				foreach($subQArray as $subQ)
					$this->db->formatConditions($this->db->formatFields('resourcekeywordMetadataId') . $this->db->inClause($subQ));
			}
			$this->db->formatConditions(array($this->dbFields[$valueArray['OriginalField']][2] => 'IS NOT NULL'));
			$subQ = $this->db->queryNoExecute($this->db->selectNoExecute($this->dbFields[$valueArray['OriginalField']][1],
				'resourcekeywordMetadataId'));
			$this->db->formatConditions($this->db->formatFields('resourcemetadataId') . $this->db->inClause($subQ));
			$this->createOptionConditions($valueArray, 'resourcemetadataResourceId');
			$commentMK = $this->db->queryNoExecute($this->db->selectNoExecute('resource_metadata',
				array(array($this->dbFields[$valueArray['OriginalField']][2] => 'rId'))));
		}
		else
		{
			if($valueArray['Button2'] == 'OR')
				$this->db->formatConditionsOneField($valueArray['Select'], $this->dbFields[$valueArray['OriginalField']][0], FALSE, TRUE, TRUE);
			else
			{
				foreach($valueArray['Select'] as $select)
				{
					$this->db->formatConditions(array($this->dbFields[$valueArray['OriginalField']][0] => $select));
					$subQArray[] = $this->db->queryNoExecute($this->db->selectNoExecute($this->dbFields[$valueArray['OriginalField']][1],
						$this->dbFields[$valueArray['OriginalField']][2]));
				}
				foreach($subQArray as $subQ)
					$fcArray[] = $this->db->formatFields($this->dbFields[$valueArray['OriginalField']][2]) . $this->db->inClause($subQ);
				$this->db->formatConditions(join(' ' . $this->db->and . ' ', $fcArray));
			}
		}
		if(!array_key_exists('Button1', $valueArray) or ($valueArray['Button1'] == 'OR')) // Start a new unionFragments element
		{
			$this->unionResourceIds[] = $this->lastUnionResourceId = $this->dbFields[$valueArray['OriginalField']][2];
// must let query be created in order to clear any conditions -- then, if necessary, we can remove from the unionFragments array
			if($commentMK)
				$this->unionFragments[$key] = $commentMK;
			else
			{
				$this->createOptionConditions($valueArray);
				$this->unionFragments[$key] = $this->db->queryNoExecute($this->db->selectNoExecute($this->dbFields[$valueArray['OriginalField']][1],
				array(array($this->dbFields[$valueArray['OriginalField']][2] => 'rId'))));
			}
		}
// Button1 == 'AND' or 'NOT' so use inClause() as a condition rather than union() and append to previous unionFragments array element
		else if(($valueArray['Button1'] == 'AND') || ($valueArray['Button1'] == 'NOT'))
		{
			if($valueArray['Button1'] == 'NOT')
				$this->db->formatConditions(array($this->dbFields[$valueArray['OriginalField']][2] => 'IS NOT NULL'));
			$subQ = $this->db->queryNoExecute($this->db->selectNoExecute($this->dbFields[$valueArray['OriginalField']][1],
				$this->dbFields[$valueArray['OriginalField']][2]));
			$lastUFKey = key(array_slice($this->unionFragments, -1, 1, TRUE));
			if($commentMK)
			{
				if($valueArray['Button1'] == 'AND')
					$fc = $this->db->formatConditions($this->db->formatFields($this->lastUnionResourceId) .
						$this->db->inClause($commentMK), FALSE, TRUE);
				else
					$fc = $this->db->formatConditions($this->db->formatFields($this->lastUnionResourceId) .
						$this->db->inClause($commentMK, TRUE), FALSE, TRUE);
			}
			else
			{
				if($valueArray['Button1'] == 'AND')
					$fc = $this->db->formatConditions($this->db->formatFields($this->lastUnionResourceId) . $this->db->inClause($subQ), FALSE, TRUE);
				else
					$fc = $this->db->formatConditions($this->db->formatFields($this->lastUnionResourceId) . $this->db->inClause($subQ, TRUE), FALSE, TRUE);
			}
			$this->unionFragments[$lastUFKey] .= $fc;
		}
	}
// Format comparisons for SQL conditions
	private function createComparisonCondition($key, $valueArray)
	{
		switch ($valueArray['Comparison'])
		{
			case 0: // '='
				$this->db->formatConditions(array($this->dbFields[$valueArray['OriginalField']][0] => $valueArray['Value1']), '=');
				break;
			case 1: // '!='
				$this->db->formatConditions(array($this->dbFields[$valueArray['OriginalField']][0] => $valueArray['Value1']), '!=');
				break;
			case 2: // '<'
				$this->db->formatConditions(array($this->dbFields[$valueArray['OriginalField']][0] => $valueArray['Value1']), '<');
				break;
			case 3: // '>'
				$this->db->formatConditions(array($this->dbFields[$valueArray['OriginalField']][0] => $valueArray['Value1']), '>');
				break;
			case 4: // '<='
				$this->db->formatConditions(array($this->dbFields[$valueArray['OriginalField']][0] => $valueArray['Value1']), '<=');
				break;
			case 5: // '>='
				$this->db->formatConditions(array($this->dbFields[$valueArray['OriginalField']][0] => $valueArray['Value1']), '>=');
				break;
			case 6: // '<...<'
				$this->db->formatConditions(array($this->dbFields[$valueArray['OriginalField']][0] => $valueArray['Value1']), '>');
				$this->db->formatConditions(array($this->dbFields[$valueArray['OriginalField']][0] => $valueArray['Value2']), '<');
				break;
			default: // shouldn't ever get here
				break;
		}
		if(!array_key_exists('Button1', $valueArray) or ($valueArray['Button1'] == 'OR')) // Start a new unionFragments element
		{
			$this->unionResourceIds[] = $this->lastUnionResourceId = $this->dbFields[$valueArray['OriginalField']][2];
			$this->createOptionConditions($valueArray);
// must let query be created in order to clear any conditions -- then, if necessary, we can remove from the unionFragments array
			$this->unionFragments[$key] = $this->db->queryNoExecute($this->db->selectNoExecute($this->dbFields[$valueArray['OriginalField']][1],
				array(array($this->dbFields[$valueArray['OriginalField']][2] => 'rId'))));
		}
// Button1 == 'AND' or 'NOT' so use inClause() as a condition rather than union() and append to previous unionFragments array element
		else if(($valueArray['Button1'] == 'AND') || ($valueArray['Button1'] == 'NOT'))
		{
			$subQ = $this->db->queryNoExecute($this->db->selectNoExecute($this->dbFields[$valueArray['OriginalField']][1],
				$this->dbFields[$valueArray['OriginalField']][2]));
			$lastUFKey = key(array_slice($this->unionFragments, -1, 1, TRUE));
			if($valueArray['Button1'] == 'AND')
				$fc = $this->db->formatConditions($this->db->formatFields($this->lastUnionResourceId) . $this->db->inClause($subQ), FALSE, TRUE);
			else
				$fc = $this->db->formatConditions($this->db->formatFields($this->lastUnionResourceId) . $this->db->inClause($subQ, TRUE), FALSE, TRUE);
			$this->unionFragments[$lastUFKey] .= $fc;
		}
	}
// add other conditions such DOI, URL etc.
	private function createOptionConditions($valueArray, $resourceId = FALSE)
	{
		$rId = $resourceId ? $resourceId : $this->lastUnionResourceId;
		$options = unserialize(base64_decode($this->session->getVar('advancedSearch_Options')));
		if(array_search('withDoi', $options) !== FALSE)
		{
			$this->db->formatConditions(array('resourceDoi' => ' IS NOT NULL'));
			if((!array_key_exists('Button1', $valueArray) or ($valueArray['Button1'] == 'OR'))
				&& ($valueArray['OriginalField'] != 'title') && ($valueArray['OriginalField'] != 'type'))
				$this->db->leftJoin('resource', 'resourceId', $rId);
		}
		if(array_search('peerReviewed', $options) !== FALSE)
		{
			$this->db->formatConditions(array('resourcemiscPeerReviewed' => 'Y'));
			if((!array_key_exists('Button1', $valueArray) or ($valueArray['Button1'] == 'OR'))
				&& ($valueArray['OriginalField'] != 'addedBy')
				&& ($valueArray['OriginalField'] != 'editedBy')
				&& ($valueArray['OriginalField'] != 'access')
				&& ($valueArray['OriginalField'] != 'maturityIndex'))
				$this->db->leftJoin('resource_misc', 'resourcemiscId', $rId);
		}
		if(array_search('withUrl', $options) !== FALSE)
		{
			$this->db->formatConditions(array('resourcetextUrls' => ' IS NOT NULL'));
			if((!array_key_exists('Button1', $valueArray) or ($valueArray['Button1'] == 'OR'))
				&& ($valueArray['OriginalField'] != 'note') && ($valueArray['OriginalField'] != 'abstract'))
				$this->db->leftJoin('resource_text', 'resourcetextId', $rId);
		}
	}
	public function process($reprocess = FALSE)
	{
/*		if(!array_key_exists('PagingStart', $this->vars))
		{
			if(array_key_exists('type', $this->vars) && ($this->vars['type'] == 'lastMulti'));
			else
				$this->session->delVar('setup_PagingTotal');
		}
*/		GLOBALS::setTplVar('heading', $this->messages->text("heading", "search"));
		if(!$reprocess)
		{
			$this->session->delVar('list_AllIds');
			$this->session->delVar('list_PagingAlphaLinks');
		}
		if(!$reprocess || ($this->session->getVar('setup_PagingStyle') == 'A'))
		{
			$this->session->delVar('sql_ListStmt');
			$this->session->delVar('advancedSearch_listParams');
		}
		$this->session->delVar('search_Highlight');
		$this->session->delVar('search_HighlightIdea');
		$this->stmt->listMethodAscDesc = 'advancedSearch_AscDesc';
		$this->stmt->listType = 'search';
		$queryString = 'action=list_SEARCH_CORE&method=reprocess';
		if(array_key_exists('type', $this->vars) && ($this->vars['type'] == 'lastMulti') && ($this->session->getVar('setup_PagingStyle') != 'A'))
		{
			$this->session->delVar('mywikindx_PagingStart');
			$this->pagingObject = FACTORY_PAGING::getInstance();
			$this->pagingObject->queryString = $queryString;
			$this->pagingObject->getPaging();
			$this->common->pagingObject = $this->pagingObject;
			$this->common->lastMulti('search');
			return;
		}
		if(!$reprocess)
			$this->checkInput();
		$this->populateDbFields();
		$array1 = $array2 = array();
		$options = unserialize(base64_decode($this->input['Options']));
		foreach($this->input as $key => $value)
		{
			$split = UTF8::mb_explode('_', $key);
			if(sizeof($split) != 2)
				continue;
			if($split[0] == 'Field')
			{
				if(($value == 'attachments'))
				{
					$field = 'Word_' . $split[1];
					$this->attachmentSearches[$split[1]]['Word'] = $this->input[$field];
					$this->attachmentSearches[$split[1]]['Field'] = 'attachment';
					$this->attachmentSearches[$split[1]]['OriginalField'] = 'attachment';
					$field = 'Partial_' . $split[1];
					if(array_key_exists($field, $this->input))
						$this->attachmentSearches[$split[1]]['Partial'] = $this->input[$field];
					$field = 'Button1_' . $split[1];
					if(array_key_exists($field, $this->input))
						$this->attachmentSearches[$split[1]]['Button'] = $this->attachmentSearches[$split[1]]['Button1'] = $this->input[$field];
					continue;
				}
				$array1[$split[1]]['Field'] = $this->dbFields[$value][0];
				$array1[$split[1]]['OriginalField'] = $value;
				if(mb_strpos($value, 'Custom_') === 0)
					$array1[$split[1]]['Custom'] = TRUE;
			}
			else
				$array1[$split[1]][$split[0]] = $value;
		}
		foreach($this->attachmentSearches as $key => $value)
		{
			if(array_key_exists($key, $array1))
				unset($array1[$key]);
		}
// do attachment searches
		if(!empty($this->attachmentSearches) && (array_search('noAttachment', $options) === FALSE))
			list($this->matchIds, $this->excludeIds) = $this->searchAttachments();
		foreach($array1 as $key1 => $value1)
		{
			foreach($value1 as $key2 => $value2)
			{
				if($key2 == 'Word')
				{
// some words (cf admin|configure menu) are filtered so this function may return FALSE if there is nothing to search on
					if($array2[$key1]['OriginalField'] == 'idea')
						$this->parsePhrase->idea = TRUE;
					else
						$this->parsePhrase->idea = FALSE;
					if(($word = $this->parsePhrase->parse($value1)) && $this->parsePhrase->validSearch)
						$array2[$key1]['String'] = $word;
					else
						$this->badInput->close($this->errors->text("inputError", "invalid"), $this, 'init');
					$array2[$key1]['Word'] = $value2;
				}
				else if($key2 == 'Select')
					$array2[$key1]['Select'] = unserialize(base64_decode($value2));
				else if($key2 == 'OriginalField')
					$array2[$key1]['OriginalField'] = $value2;
				else if($key2 == 'Field')
				{
					$array2[$key1]['Field'] = $value2;
					if((mb_strpos($value2, 'Custom') !== FALSE) && (!array_key_exists($value2, $this->dbFields)))
						$this->dbFields[$value2] = array('resourcecustomCustomId', 'resource_custom', 'resourcecustomResourceId');
				}
				else if($key2 == 'Button1')
					$array2[$key1]['Button1'] = $value2;
				else if($key2 == 'Button2')
					$array2[$key1]['Button2'] = $value2;
				else if($key2 == 'Comparison')
					$array2[$key1]['Comparison'] = $value2;
				else if($key2 == 'Value1')
					$array2[$key1]['Value1'] = trim($value2);
				else if($key2 == 'Value2')
					$array2[$key1]['Value2'] = trim($value2);
				else if($key2 == 'Custom')
					$array2[$key1]['Custom'] = $value2;
			}
			if(!array_key_exists('String', $array2[$key1]) &&
				!array_key_exists('Select', $array2[$key1]) &&
				!array_key_exists('Comparison', $array2[$key1])
				)
				unset($array2[$key1]);
			else if(array_key_exists('Comparison', $array2[$key1]))
			{
				if($array2[$key1]['Comparison'] == 6) // i.e. <...<
				{
					if(!array_key_exists('Value2', $array2[$key1]))
						unset($array2[$key1]);
					else if(!is_numeric($array2[$key1]['Value1']) || !is_numeric($array2[$key1]['Value2']))
						unset($array2[$key1]);
					else if(!$array2[$key1]['Value1'] || !$array2[$key1]['Value2'])
						unset($array2[$key1]);
					else if($array2[$key1]['Value1'] >= $array2[$key1]['Value2'])
						unset($array2[$key1]);
				}
				else if(!$array2[$key1]['Value1'] || !is_numeric($array2[$key1]['Value1']))
					unset($array2[$key1]);
			}
		}
		if(empty($array2) && empty($this->attachmentSearches))
		{
			GLOBALS::setTplVar('resourceListSearchForm',  FALSE);
			$this->badInput->close($this->errors->text("inputError", "invalid"), $this, 'init');
		}
		foreach($array2 as $key3 => $value3)
		{
			if(array_key_exists('String', $value3))
			{
				if($value3['Field'] == 'resourceTitleSort')
					$replace = $this->db->concat(array($this->db->formatFields('resourceNoSort'), $this->db->formatFields($value3['Field'])), ' ');
				else
					$replace = $this->db->formatFields($value3['Field']);
				$value3['String'] = str_replace('!WIKINDXFIELDWIKINDX!', $replace, $value3['String']);
				$array2[$key3] = $value3;
			}
		}
		if($this->createSqlFragments($array2))
			$sqlDummy = FALSE;
		else // create a dummy SQL query that will return no results in listcommon->display()
		{
			$this->db->formatConditions(array('resourceId' => 'IS NULL'));
			$this->db->leftJoin('resource', 'resourceId', 'resourcemiscId');
			$sqlDummy = $this->db->queryNoExecute($this->db->selectNoExecute('resource_misc', array('resourceId', 'resourcemiscId')));
		}
		if(array_key_exists('type', $this->vars) && ($this->vars['type'] == 'displayIdeas')) // from existing list, clicked on 'Ideas have been found'
			$this->unionFragments = array();
		if(!empty($this->ideas) && !empty($this->unionFragments))
		{
// Check this user is allowed to read the idea.
			foreach($this->ideas as $valueArray)
				$conditions[] = $valueArray['String'];
			$this->metadata->setCondition('i');
			$this->db->formatConditions(join($this->db->or, $conditions));
			$resultset = $this->db->select('resource_metadata', 'resourcemetadataId');
			if($this->db->numRows($resultset))
			{
				$this->common->ideasFound = TRUE;
				$this->session->setVar('sql_LastIdeaSearch', "index.php?action=list_SEARCH_CORE&method=reprocess&type=displayIdeas");
			}
			else
				$this->session->delVar('sql_LastIdeaSearch');
		}
		else if(!empty($this->ideas))
		{
			$this->searchIdeas();
			return;
		}
		else
			$this->session->delVar('sql_LastIdeaSearch');
		if($bibId = $this->session->getVar('advancedSearch_BibId'))
		{
			$bibIdArray[] = $bibId;
			$this->stmt->excludeBib($bibId, 'rId');
		}
		else
			$bibIdArray = array();
		foreach($this->attachmentSearches as $key1 => $value1)
		{
			foreach($value1 as $key2 => $value2)
			{
				if($key2 == 'Word')
					$array2[$key1]['Word'] = $value2;
				else if($key2 == 'OriginalField')
					$array2[$key1]['OriginalField'] = $value2;
				else if($key2 == 'Field')
				{
					$array2[$key1]['Field'] = $value2;
					if((mb_strpos($value2, 'Custom') !== FALSE) && (!array_key_exists($value2, $this->dbFields)))
						$this->dbFields[$value2] = array('resourcecustomCustomId', 'resource_custom', 'resourcecustomResourceId');
				}
				else if($key2 == 'Button1')
					$array2[$key1]['Button1'] = $value2;
			}
		}
		$this->test(TRUE, $array2, $bibIdArray, $options);
		$attach = FALSE;
/**
zipAttachment is dependent upon displayOnlyAttachment which is dependent upon withAttachment

Therefore, we can cascade . . .
*/
		$this->session->delVar('search_DisplayAttachment');
		$this->session->delVar('search_DisplayAttachmentZip');
		if(array_search('noAttachment', $options) !== FALSE)
			$attach = 'noAttachment';
		else if(array_search('withAttachment', $options) !== FALSE)
			$attach = 'withAttachment';
		$order = (array_search('displayOnlyAttachment', $options) !== FALSE) ? 'attachments' : $this->input['Order'];
		if($attach)
		{
			if($order == 'attachments') // displaying attachments only
			{
				$this->session->setVar('search_DisplayAttachment', TRUE);
				if(array_search('zipAttachment', $options) !== FALSE)
					$this->session->setVar('search_DisplayAttachmentZip', TRUE);
			}
		}
		if(!$sqlDummy)
		{
			if(!array_Key_exists('order', $this->input) && !array_Key_exists('Order', $this->input))
			{
				$this->session->setVar('search_Order', 'creator');
				$this->session->setVar('sql_LastOrder', 'creator');
				$this->session->setVar('search_AscDesc', $this->db->asc);
			}
			else
				$this->session->setVar('sql_LastOrder', $this->input['Order']);
// Turn on the 'add bookmark' menu item
			$this->session->setVar("bookmark_DisplayAdd", TRUE);
			$this->session->setVar('search_Order', $order);
			$subStmt = $this->setSubQuery($attach);
			$this->db->DisableFullGroupBySqlMode();
			$resourcesFound = $this->stmt->listSubQuery($this->session->getVar('search_Order'), $queryString, $subStmt, FALSE, $this->subQ);
			$this->db->EnableFullGroupBySqlMode();
			if(!$resourcesFound)
			{
				$this->common->noResources('search');
				return;
			}
		}
		$searchTerms = UTF8::mb_explode(",", $this->session->getVar('search_Highlight'));
		foreach($searchTerms as $term)
		{
			if(trim($term))
			{
				$term = preg_quote($term);
				$patterns[] = "/($term)(?=[^>]*(<|$))/ui";
			}
		}
		if(!isset($patterns))
		{
			$this->session->setVar('search_Patterns', base64_encode(serialize(array())));
			$this->common->patterns = FALSE;
		}
		else
		{
			$this->session->setVar('search_Patterns', base64_encode(serialize($patterns)));
			$this->common->patterns = $patterns;
		}
		$this->common->keepHighlight = TRUE;
		if($sqlDummy)
			$sql = $sqlDummy;
		else
			$sql = $this->stmt->listList($this->session->getVar('search_Order'), FALSE, $this->subQ);
		$this->common->display($sql, 'search');
// set the lastMulti session variable for quick return to this process.
		$this->session->setVar('sql_LastMulti', $queryString);
		$this->session->saveState(array('advancedSearch', 'sql', 'bookmark', 'list', 'setup'));
	}
// Set the subQuery
	private function setSubQuery($attach)
	{
		$unions = $this->db->union($this->unionFragments);
		$this->db->ascDesc = $this->session->getVar('search_AscDesc');
		if($attach == 'noAttachment')
		{
			$this->stmt->conditions[] = array('resourceattachmentsId' => ' IS NULL');
			$this->stmt->joins['resource_attachments'] = array('resourceattachmentsResourceId', 'rId');
		}
		else if($attach == 'withAttachment')
		{
			$this->stmt->conditions[] = (array('resourceattachmentsId' => ' IS NOT NULL'));
			$this->stmt->joins['resource_attachments'] = array('resourceattachmentsResourceId', 'rId');
		}
		switch($this->session->getVar('search_Order'))
		{
			case 'title':
				$this->stmt->useBib('rId');
				$this->stmt->quarantine(FALSE, 'rId');
				$this->stmt->joins['resource'] = array('resourceId', 'rId');
				$this->stmt->executeCondJoins();
				$this->db->groupBy(array('rId'));
				$this->subQ = $this->db->subQuery($unions, 'u', FALSE);
				$subQuery = $this->db->from . ' ' . $this->subQ;
				return $this->db->selectNoExecuteFromSubQuery(FALSE, array('rId'), $subQuery, FALSE, TRUE, TRUE);
			case 'creator':
				$this->stmt->useBib('rId');
				$this->stmt->quarantine(FALSE, 'rId');
				$this->stmt->joins['resource_creator'] = array('resourcecreatorResourceId', 'rId');
				$this->stmt->joins['creator'] = array('creatorId', 'resourcecreatorCreatorId');
				$this->stmt->executeCondJoins();
				$this->db->groupBy(array('rId'));
				$this->subQ = $this->db->subQuery($unions, 'u', FALSE);
				$subQuery = $this->db->from . ' ' . $this->subQ;
				return $this->db->selectNoExecuteFromSubQuery(FALSE, array('rId'), $subQuery, FALSE, TRUE, TRUE);
			case 'publisher':
				$this->stmt->useBib('rId');
				$this->stmt->quarantine(FALSE, 'rId', FALSE);
				$this->stmt->joins['resource_misc'] = array('resourcemiscId', 'rId');
				$this->stmt->joins['publisher'] = array('publisherId', 'resourcemiscPublisher');
				$this->stmt->executeCondJoins();
				$this->db->groupBy(array('rId'));
				$this->subQ = $this->db->subQuery($unions, 'u', FALSE);
				$subQuery = $this->db->from . ' ' . $this->subQ;
				return $this->db->selectNoExecuteFromSubQuery(FALSE, array('rId'), $subQuery, FALSE, TRUE, TRUE);
			case 'year':
				$this->stmt->useBib('rId');
				$this->stmt->quarantine(FALSE, 'rId');
				$this->stmt->joins['resource'] = array('resourceId', 'rId');
				$this->stmt->joins['resource_year'] = array('resourceyearId', 'rId');
				$this->stmt->executeCondJoins();
				$this->db->groupBy(array('rId'));
				$this->subQ = $this->db->subQuery($unions, 'u', FALSE);
				$subQuery = $this->db->from . ' ' . $this->subQ;
				return $this->db->selectNoExecuteFromSubQuery(FALSE, array('rId'), $subQuery, FALSE, TRUE, TRUE);
			case 'timestamp':
				$this->stmt->useBib('rId');
				$this->stmt->quarantine(FALSE, 'rId');
				$this->stmt->joins['resource_timestamp'] = array('resourcetimestampId', 'rId');
				$this->stmt->executeCondJoins();
				$this->db->groupBy(array('rId'));
				$this->subQ = $this->db->subQuery($unions, 'u', FALSE);
				$subQuery = $this->db->from . ' ' . $this->subQ;
				return $this->db->selectNoExecuteFromSubQuery(FALSE, array('rId'), $subQuery, FALSE, TRUE, TRUE);
			case 'attachments':
				$this->stmt->useBib('rId');
				$this->stmt->quarantine(FALSE, 'rId');
				$this->stmt->executeCondJoins();
				$this->db->groupBy(array('rId'));
				$this->subQ = $this->db->subQuery($unions, 'u', FALSE);
				$subQuery = $this->db->from . ' ' . $this->subQ;
				return $this->db->selectNoExecuteFromSubQuery(FALSE, array('rId'), $subQuery, FALSE, TRUE, TRUE);
			default:
				break;
		}
	}
// Search attachments for search words
	private function searchAttachments()
	{
		include_once("core/modules/list/FILETOTEXT.php");
		$ftt = new FILETOTEXT();
		$excludeIds = $matchIds = array();
		foreach($this->attachmentSearches as $key => $array)
		{
			$searchArray[$key] = $this->parsePhrase->parse($array, FALSE, TRUE, TRUE);
			if(array_key_exists('Partial', $array))
				$searchArray[$key]['Partial'] = 'on';
		}
		foreach($searchArray as $key => $arrays)
		{
			foreach($arrays as $wordKey => $wordArray)
			{
				if(is_array($wordArray))
				{
					$types[$key][$wordKey] = array_shift($wordArray);
					$phrases[$key][$wordKey] = array_shift($wordArray);
				}
			}
			if(array_key_exists('Partial', $arrays))
				$this->partials[$key] = TRUE;
			else
				$this->partials[$key] = FALSE;
		}
		$attachDir = \FACTORY_CONFIG::getInstance()->WIKINDX_ATTACHMENTS_DIR;
		$cacheDir = \FACTORY_CONFIG::getInstance()->WIKINDX_ATTACHMENTSCACHE_DIR;
		$mimeTypes = array(WIKINDX_MIMETYPE_PDF, WIKINDX_MIMETYPE_DOCX, WIKINDX_MIMETYPE_DOC);
		$this->db->formatConditionsOneField($mimeTypes, 'resourceattachmentsFileType');
		$resultset = $this->db->select('resource_attachments',
			array('resourceattachmentsResourceId', 'resourceattachmentsHashFilename', 'resourceattachmentsFileType', 'resourceattachmentsFileSize'));
		$attachments = $texts = array();
		while($row = $this->db->fetchRow($resultset))
		{
			$fileName = $attachDir . DIRECTORY_SEPARATOR . $row['resourceattachmentsHashFilename'];
			$fileNameCache = $cacheDir . DIRECTORY_SEPARATOR . $row['resourceattachmentsHashFilename'];
			if(!file_exists($fileName))
				continue;
			$attachments[] = $row;
// all attachments should be cached but check anyway
			if(file_exists($fileNameCache) && filemtime($fileNameCache) > filemtime($fileName))
			    $texts[$row['resourceattachmentsHashFilename']] = file_get_contents($fileNameCache);
		}
		foreach($attachments as $row)
		{
// NB Converting files to text takes time -- searches below are fast.
			if(!array_key_exists($row['resourceattachmentsHashFilename'], $texts) || $texts[$row['resourceattachmentsHashFilename']] == '')
				continue;
// For each search, we search on NOT and EXACTNOT first. If the search is true, then there is no need to do the other searches.
			foreach($types as $key1 => $typeArray)
			{
				foreach($typeArray as $key2 => $type)
				{
					if(($type == 'NOT') || ($type == 'exactNOT') ||
						(array_key_exists('Button', $this->attachmentSearches[$key1]) && ($this->attachmentSearches[$key1]['Button'] == 'NOT')))
					{
						$phrase = $phrases[$key1][$key2];
						if($this->partials[$key1])
						{
    					    // Escape the user input only for EXACT phrases
    					    // and unescape the jokers used by the advanced search syntax
    					    // 0 or more (lazy) - asterisk (*)
    					    // 1 character - question mark (?)
							if($type == 'exactNOT')
							{
								$pattern = UTF8::mb_explode("*", $phrase);
								foreach($pattern as $k1 => $p1)
								{
									$p1 = UTF8::mb_explode("?", $p1);

									foreach($p1 as $k2 => $p2)
									{
										$p2 = preg_quote($p2, '/');
									}
									$pattern[$k1] = implode(".?", $p1);
								}
								$pattern = implode(".*", $pattern);
							}
							else
								$pattern = preg_replace(array("/\*/", "/\?/"), array(".*", "."), $phrase);
							if(preg_match("/$pattern/iu", $texts[$row['resourceattachmentsHashFilename']]) === 1)
								$excludeIds[$key1][] = $row['resourceattachmentsResourceId'];
						}
						else
						{
    					    // Escape the user input
							if($type == 'exactNOT')
    					   	 $pattern = preg_quote($phrase, '/');
    					   	else
								$pattern = preg_replace(array("/\*/", "/\?/"), array(".*", "."), $phrase);
							if(preg_match("/\b$pattern\b/iu",$texts[$row['resourceattachmentsHashFilename']]) === 1)
								$excludeIds[$key1][] = $row['resourceattachmentsResourceId'];
						}
					}
				}
				foreach($typeArray as $key2 => $type) // for now, we ignore AND and OR etc. and their position. If a match is found, break the loop
				{
					if(($type == 'NOT') || ($type == 'exactNOT'))
						continue;
					$phrase = $phrases[$key1][$key2];
					if($this->partials[$key1])
					{
					    // Escape the user input only for EXACT phrases
					    // and unescape the jokers used by the advanced search syntax
					    // 0 or more (lazy) - asterisk (*)
					    // 1 character - question mark (?)
					    if(($type == 'exactAND') || ($type == 'exactOR'))
						{
							$pattern = UTF8::mb_explode("*", $phrase);
							foreach($pattern as $k1 => $p1)
							{
								$p1 = UTF8::mb_explode("?", $p1);

								foreach($p1 as $k2 => $p2)
								{
									$p2 = preg_quote($p2, '/');
								}
								$pattern[$k1] = implode(".?", $p1);
							}
							$pattern = implode(".*", $pattern);
						}
						else
							$pattern = preg_replace(array("/\*/", "/\?/"), array(".*", "."), $phrase);
						if(preg_match("/$pattern/iu", $texts[$row['resourceattachmentsHashFilename']]) === 1)
							$matchIds[$key1][] = $row['resourceattachmentsResourceId'];
					}
					else
					{
					    // Escape the user input if EXACT phrase
					    if(($type == 'exactAND') || ($type == 'exactOR'))
						    $pattern = preg_quote($phrase, '/');
						else // use as wildcards
							$pattern = preg_replace(array("/\*/", "/\?/"), array(".*", "."), $phrase);
						if(preg_match("/\b$pattern\b/iu", $texts[$row['resourceattachmentsHashFilename']]) === 1)
							$matchIds[$key1][] = $row['resourceattachmentsResourceId'];
					}
				}
			}
		}
		foreach($matchIds as $key => $array)
			$matchIds[$key] = array_unique($array);
		foreach($excludeIds as $key => $array)
			$excludeIds[$key] = array_unique($array);
// Remove duplicates
		$matchIds = $this->removeArrayDuplicates($matchIds);
		$excludeIds = $this->removeArrayDuplicates($excludeIds);
// An ID in $excludeIds cannot be anywhere in $matchIds
		$excludes = array();
		if(!empty($matchIds) && !empty($excludeIds))
		{
			foreach($excludeIds as $array)
			{
				foreach($array as $id)
					$excludes[] = $id;
			}
			$excludes = array_unique($excludes);
			$temp = $matchIds;
			foreach($temp as $key1 => $array)
			{
				foreach($array as $id)
				{
					if(($key2 = array_search($id, $excludes)) !== FALSE)
					{
						unset($matchIds[$key1][$key2]);
						if(empty($matchIds[$key1]))
							unset($matchIds[$key1]);
					}
				}
			}
		}
		return array($matchIds, $excludeIds, $this->partials);
	}
// Remove multidimensional array duplicates
	private function removeArrayDuplicates($input)
	{
		$temp1 = $input;
		$temp2 = array();
		foreach($temp1 as $key1 => $array)
		{
			foreach($array as $key2 => $id)
			{
				if(array_search($id, $temp2) !== FALSE)
				{
					unset($input[$key1][$key2]);
					if(empty($input[$key1]))
						unset($input[$key1]);
				}
				else
					$temp2[] = $id;
			}
		}
		return $input;
	}
// Search ideas for search words and display
	public function searchIdeas()
	{
		GLOBALS::setTplVar('heading', $this->messages->text("heading", "search"));
		$icons = FACTORY_LOADICONS::getInstance();
		$cite = FACTORY_CITE::getInstance();
		$userObj = FACTORY_USER::getInstance();
		$icons->setupIcons();
		$multiUser = $this->session->getVar('setup_MultiUser');
		$ideaList = array();
		$index = 0;
// get count statement and set queryString
		$pagingObject = FACTORY_PAGING::getInstance();
		if((!array_key_exists('PagingStart', $this->vars) || !$this->vars['PagingStart']))
			$this->session->delVar('mywikindx_PagingStart'); // might be set from last multi resource list display
		$queryString = "index.php?action=list_SEARCH_CORE&method=reprocess&type=displayIdeas";
// Check this user is allowed to read the idea.
		$this->metadata->setCondition('i');
		foreach($this->ideas as $valueArray)
			$conditions[] = $valueArray['String'];
		$this->db->formatConditions(join($this->db->or, $conditions));
		$countQuery = $this->db->selectCountDistinctField('resource_metadata', 'resourcemetadataId');
		$pagingObject->sqlTotal = $countQuery;
		$pagingObject->queryString = $queryString;
		$pagingObject->getPaging();
		$searchTerms = UTF8::mb_explode(",", $this->session->getVar('search_HighlightIdea'));
		foreach($searchTerms as $term)
		{
			if(trim($term))
				$patterns[] = "/($term)(?=[^>]*(<|$))/ui";
		}
// now get ideas
// Check this user is allowed to read the idea.
		$this->metadata->setCondition('i');
		$this->db->formatConditions(join($this->db->or, $conditions));
		$this->db->limit($this->session->getVar('setup_Paging'), $pagingObject->start);
		$resultset = $this->db->select('resource_metadata', array('resourcemetadataId', 'resourcemetadataTimestamp', 'resourcemetadataTimestampEdited',
			'resourcemetadataMetadataId', 'resourcemetadataText', 'resourcemetadataAddUserId', 'resourcemetadataPrivate'));
		if(!$this->db->numRows($resultset))
			$this->badInput->close($this->messages->text("select", "noIdeas"));
		while($row = $this->db->fetchRow($resultset))
		{
			if($multiUser)
			{
				list($user) = $userObj->displayUserAddEdit($row['resourcemetadataAddUserId'], FALSE, 'idea');
				if(!$row['resourcemetadataTimestampEdited'])
					$ideaList[$index]['user'] = $this->messages->text('hint', 'addedBy', $user . '&nbsp;' . $row['resourcemetadataTimestamp']);
				else
					$ideaList[$index]['user'] = $this->messages->text('hint', 'addedBy', $user . '&nbsp;' . $row['resourcemetadataTimestamp']) .
					',&nbsp;' . $this->messages->text('hint', 'editedBy', $user . '&nbsp;' . $row['resourcemetadataTimestampEdited']);
				GLOBALS::addTplVar('multiUser', TRUE);
			}
			$ideaList[$index]['links'] = $this->metadata->createLinks($row, TRUE);
			$data = preg_replace($patterns, \HTML\span("$1", "highlight"), $row['resourcemetadataText']);
			$ideaList[$index]['metadata'] = $cite->parseCitations(\HTML\dbToHtmlTidy($data), 'html');
			++$index;
		}
		$this->session->setVar('sql_LastIdeaSearch', $queryString);
		GLOBALS::addTplVar('ideaTemplate', TRUE);
		GLOBALS::addTplVar('ideaList', $ideaList);
		$this->common->pagingStyle($countQuery, FALSE, FALSE, $queryString);
	}
// write input to session
	private function writeSession()
	{
// First, write all input with 'advancedSearch_' prefix to session
		foreach($this->vars as $key => $value)
		{
			if(preg_match("/^advancedSearch_Word_/u", $key))
			{
				if(!trim($value))
					continue;
				$key = str_replace('advancedSearch_', '', $key);
				$temp[$key] = trim($value);
			}
			else if(preg_match("/^advancedSearch_Field_/u", $key))
			{
				$key = str_replace('advancedSearch_', '', $key);
				$temp[$key] = $value;
			}
			else if(preg_match("/^advancedSearch_Select_/u", $key))
			{
				$key = str_replace('advancedSearch_', '', $key);
				$temp[$key] = base64_encode(serialize($value));
			}
			else if(preg_match("/^advancedSearch_Comparison_/u", $key))
			{
				$key = str_replace('advancedSearch_', '', $key);
				$temp[$key] = $value;
			}
			else if(preg_match("/^advancedSearch_Value1_/u", $key))
			{
				$key = str_replace('advancedSearch_', '', $key);
				$temp[$key] = $value;
			}
			else if(preg_match("/^advancedSearch_Value2_/u", $key))
			{
				$key = str_replace('advancedSearch_', '', $key);
				$temp[$key] = $value;
			}
			else if(preg_match("/^advancedSearch_Button1_/u", $key))
			{
				$key = str_replace('advancedSearch_', '', $key);
				$temp[$key] = $value;
			}
			else if(preg_match("/^advancedSearch_Button2_/u", $key))
			{
				$key = str_replace('advancedSearch_', '', $key);
				$temp[$key] = $value;
			}
			else if(preg_match("/^advancedSearch_Partial_/u", $key))
			{
				$key = str_replace('advancedSearch_', '', $key);
				$temp[$key] = $value;
			}
		}
		if(array_key_exists('advancedSearch_BibId', $this->vars) && $this->vars['advancedSearch_BibId'])
			$temp['BibId'] = $this->vars['advancedSearch_BibId'];
		if(array_key_exists('advancedSearch_Options', $this->vars))
			$temp['Options'] = base64_encode(serialize($this->vars['advancedSearch_Options']));
		if(array_key_exists('advancedSearch_Order', $this->vars) && $this->vars['advancedSearch_Order'])
			$temp['Order'] = $this->vars['advancedSearch_Order'];
		if(array_key_exists('advancedSearch_AscDesc', $this->vars) && $this->vars['advancedSearch_AscDesc'])
			$temp['AscDesc'] = $this->vars['advancedSearch_AscDesc'];
		$this->session->clearArray("advancedSearch");
		if(!empty($temp))
		{
			$this->session->writeArray($temp, 'advancedSearch', TRUE);
			$this->session->setVar('search_Order', $temp['Order']);
			$this->session->setVar('search_AscDesc', $temp['AscDesc']);
		}
	}
// validate user input - method, word and field are required.
// Input comes either from form input or, when paging, from the session.
	private function checkInput()
	{
		$this->writeSession();
		$this->input = $this->session->getArray("advancedSearch");
		for($i = 1; $i <= 50; $i++)
		{
			if(!array_key_exists("Field_$i", $this->input))
				continue;
			if(array_key_exists("Comparison_$i", $this->input) && ($i == 1))
			{
				if($this->input["Comparison_$i"] == 6) // '<...<'
				{
					if(array_key_exists("Value1_$i", $this->input) && trim($this->input["Value1_$i"])
						&& array_key_exists("Value2_$i", $this->input) && trim($this->input["Value2_$i"]))
						continue;
					else
						$this->badInput->close($this->errors->text("inputError", "missing"), $this, 'init');
				}
				else
				{
					if(array_key_exists("Value1_$i", $this->input) && trim($this->input["Value1_$i"]))
						continue;
					else
						$this->badInput->close($this->errors->text("inputError", "missing"), $this, 'init');
				}
				continue;
			}
			if(array_key_exists("Word_$i", $this->input) && trim($this->input["Word_$i"]))
				continue;
			else if(array_key_exists("Select_$i", $this->input) && trim($this->input["Select_$i"]))
				continue;
			else if($i == 1)
				$this->badInput->close($this->errors->text("inputError", "missing"), $this, 'init');
		}
		$this->session->saveState('advancedSearch');
//$this->badInput->close(\HTML\p('still working on it . . .', 'error'), $this, 'init');
	}
/**
* Format creators returned from database
*
* @param int $id
*/
	private function formatCreators($id)
	{
		$this->db->formatConditions(array('creatorId' => $id));
		$row = $this->db->selectFirstRow('creator', array("creatorSurname", "creatorInitials", "creatorFirstname", "creatorPrefix"));
		if($row['creatorPrefix'])
			$name = $row['creatorPrefix'] . ' ' . $row['creatorSurname'];
		else
			$name = $row['creatorSurname'];
		if($row['creatorFirstname'])
		{
			$name .= ', ' . $row['creatorFirstname'] . ' ';
			if($row['creatorInitials'])
				$name .= ' ' . str_replace(' ', '.', $row['creatorInitials']) . '.';
		}
		else if($row['creatorInitials'])
			$name .= ', ' . str_replace(' ', '.', $row['creatorInitials']) . '.';
		return \HTML\dbToHtmlTidy(trim($name));
	}
/**
* Format publishers returned from database
*
* @param int $id
*/
	private function formatPublishers($id)
	{
		$this->db->formatConditions(array('publisherId' => $id));
		$row = $this->db->selectFirstRow('publisher', array("publisherName", "publisherLocation"));
		if($row['publisherLocation'])
			return \HTML\dbToHtmlTidy($row['publisherName'] .
			": " . $row['publisherLocation']);
		else
			return \HTML\dbToHtmlTidy($row['publisherName']);
	}
/**
* Format collections returned from database
*
* @param int $id
*/
	private function formatCollections($id)
	{
		$this->db->formatConditions(array('collectionId' => $id));
		$row = $this->db->selectFirstRow('collection', array("collectionTitle"));
		return \HTML\dbToHtmlTidy($row['collectionTitle']);
	}
/**
* Format subcategories returned from database
*
* @param int $id
*/
	private function formatSubcategories($id)
	{
		$this->db->formatConditions(array('subcategoryId' => $id));
		$row = $this->db->selectFirstRow('subcategory', array("subcategorySubcategory"));
		return \HTML\dbToHtmlTidy($row['subcategorySubcategory']);
	}
/**
* Format categories returned from database
*
* @param int $id
*/
	private function formatCategories($id)
	{
		$this->db->formatConditions(array('categoryId' => $id));
		$row = $this->db->selectFirstRow('category', array("categoryCategory"));
		return \HTML\dbToHtmlTidy($row['categoryCategory']);
	}
/**
* Format keywords returned from database
*
* @param int $id
*/
	private function formatKeywords($id)
	{
		$this->db->formatConditions(array('keywordId' => $id));
		$row = $this->db->selectFirstRow('keyword', array("keywordKeyword"));
		return \HTML\dbToHtmlTidy($row['keywordKeyword']);
	}
/**
* Format usertags returned from database
*
* @param int $id
*/
	private function formatUsertags($id)
	{
		$this->db->formatConditions(array('usertagsId' => $id));
		$row = $this->db->selectFirstRow('user_tags', array("usertagsTag"));
		return \HTML\dbToHtmlTidy($row['usertagsTag']);
	}
/**
* Format languages returned from database
*
* @param int $id
*/
	private function formatLanguages($id)
	{
		$this->db->formatConditions(array('languageId' => $id));
		$row = $this->db->selectFirstRow('language', array("languageLanguage"));
		return \HTML\dbToHtmlTidy($row['languageLanguage']);
	}
/**
* Format tags returned from database
*
* @param int $id
*/
	private function formatTags($id)
	{
		$this->db->formatConditions(array('tagId' => $id));
		$row = $this->db->selectFirstRow('tag', array("tagTag"));
		return \HTML\dbToHtmlTidy($row['tagTag']);
	}
/**
* Format usernames returned from database
*
* @param int $id
*/
	private function formatUsernames($id)
	{
		$this->db->formatConditions(array('usersId' => $id));
		$row = $this->db->selectFirstRow('users', array("usersUsername", "usersFullname"));
		if($row['usersFullname'])
			return \HTML\dbToHtmlTidy($row['usersUsername'] .
			' [' . $row['usersFullname'] . ']');
		else
			return \HTML\dbToHtmlTidy($row['usersUsername']);
	}
}
