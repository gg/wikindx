<?php
/**
 * WIKINDX : Bibliographic Management system.
 * @link https://wikindx.sourceforge.io/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2018 Mark Grimshaw-Aagaard <sirfragalot@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 */

/**
*	QUICKSEARCH class
*
*	Quickly search database
*
*/
class QUICKSEARCH
{
private $db;
private $vars;
private $stmt;
private $errors;
private $messages;
private $common;
private $session;
private $keyword;
private $input = array();
private $badInput;
private $parsePhrase;
public $words = '';
private $commonBib;
public $unions = array();
private $subQ;
public $insertCitation = FALSE; // TRUE if being called from INSERTCITATION

	public function __construct()
	{
		$this->session = FACTORY_SESSION::getInstance();
		$this->db = FACTORY_DB::getInstance();
		$this->vars = GLOBALS::getVars();
		$this->stmt = FACTORY_SQLSTATEMENTS::getInstance();
		$this->errors = FACTORY_ERRORS::getInstance();
		$this->messages = FACTORY_MESSAGES::getInstance();
		$this->common = FACTORY_LISTCOMMON::getInstance();
		$this->common->quickSearch = TRUE;
		$this->keyword = FACTORY_KEYWORD::getInstance();
		$this->badInput = FACTORY_BADINPUT::getInstance();
		$this->parsePhrase = FACTORY_PARSEPHRASE::getInstance();
		$this->commonBib = FACTORY_BIBLIOGRAPHYCOMMON::getInstance();
		switch($this->session->getVar('search_Order'))
		{
			case 'title':
				break;
			case 'creator':
				break;
			case 'publisher':
				break;
			case 'year':
				break;
			case 'timestamp':
				break;
			default:
				$this->session->setVar('search_Order', 'creator');
		}
// Turn on the 'add bookmark' menu item
		$this->session->setVar("bookmark_DisplayAdd", TRUE);
	}
// display form options. $word comes from modules/cite/INSERTCITATION.php
	public function init($error = FALSE, $tableBorder = FALSE, $returnString = FALSE, $word = FALSE)
	{
///First check, do we have resources?
		if(!$this->common->resourcesExist())
			return;
		include_once("core/modules/help/HELPMESSAGES.php");
		$help = new HELPMESSAGES();
		GLOBALS::setTplVar('help', $help->createLink('search'));
		if(!$returnString)
			GLOBALS::setTplVar('heading', $this->messages->text("heading", "search"));
		$this->session->delVar('mywikindx_PagingStart');
		$this->session->delVar('mywikindx_PagingStartAlpha');
		$pString = $error ? $error : FALSE;
		if(!$this->insertCitation)
			$pString .= \FORM\formHeader("list_QUICKSEARCH_CORE");
		else
			$pString .= \FORM\formHeaderVisibleAction("dialog.php", "searchInsertCitation");
		$pString .= \FORM\hidden("method", "process");
		if($tableBorder)
			$pString .= \HTML\tableStart('generalTable borderStyleSolid');
		else
			$pString .= \HTML\tableStart('center width50percent');
		$pString .= \HTML\trStart();
		$this->radioButtons = FALSE;
		if(!$word)
			$word = $this->session->issetVar("search_Word") ?
				htmlspecialchars(stripslashes($this->session->getVar("search_Word")), ENT_QUOTES | ENT_HTML5) : FALSE;
		$hint = BR . \HTML\span($this->messages->text("hint", "wordLogic"), 'hint');
		if(!$this->insertCitation)
			$pString .= \HTML\td(\FORM\textInput($this->messages->text("search", "word"), "search_Word",
				$word, 40) . $hint . '&nbsp;&nbsp;&nbsp;' . \FORM\formSubmit($this->messages->text("submit", "Search")), $tableBorder ? 'padding4px' : '');
		else
			$pString .= \HTML\td(\FORM\textInput($this->messages->text("search", "word"), "search_Word",
				$word, 60) . $hint . '&nbsp;&nbsp;&nbsp;' . \FORM\formSubmit($this->messages->text("submit", "Search")), $tableBorder ? 'padding4px' : '');
		$pString .= \HTML\trEnd();
		$pString .= \HTML\tableEnd();
		$pString .= \FORM\formEnd();
		if($returnString)
			return $pString; // cf FRONT.php or process() below.
		else
			GLOBALS::addTplVar('content', $pString);
	}
// Reset the form and clear the session
	public function reset()
	{
		$this->session->clearArray('search');
		$this->init();
	}
// parse the search word(s)
	private function parseWord()
	{
		$this->words = $this->parsePhrase->parse($this->input, FALSE, FALSE);
		if(!$this->words || !$this->parsePhrase->validSearch)
		{
			GLOBALS::setTplVar('resourceListSearchForm',  FALSE);
			$this->badInput->close($this->errors->text("inputError", "invalid"), $this, 'init');
		}
		$this->words = str_replace('!WIKINDXFIELDWIKINDX!', $this->db->formatFields('concatText'), $this->words);
	}
// create the subquery
	public function fieldSql()
	{
		$metadata = FACTORY_METADATA::getInstance();
		$metadataCond = $metadata->setCondition(FALSE, TRUE);
		$result = $this->db->formatFields('usertagsUserId') . $this->db->equal . '1';
		$userCond = $this->db->caseWhen('usertagsId', 'IS NOT NULL', $result, FALSE, FALSE);
		$concatArray[] = $this->db->concat(array($this->db->formatFields('resourceNoSort'), $this->db->formatFields('resourceTitleSort')), ' ');
		$concatArray[] = $this->db->groupConcat($this->db->formatFields('creatorSurname'), ' ', TRUE);
		$concatArray[] = $this->db->groupConcat($this->db->formatFields('keywordKeyword'), ' ', TRUE);
		$concatArray[] = $this->db->formatFields('resourcetextNote');
		$concatArray[] = $this->db->formatFields('resourcetextAbstract');
		$concatArray[] = $this->db->groupConcat($this->db->formatFields('resourcemetadataText'), ' ', TRUE);
		$concatArray[] = $this->db->groupConcat($this->db->formatFields('usertagsTag'), ' ', TRUE);
		$concatArray[] = $this->db->concat(array($this->db->formatFields('resourcecustomShort')), ' ');
		$concatArray[] = $this->db->concat(array($this->db->formatFields('resourcecustomLong')), ' ');
		$concat = $this->db->concat($concatArray, ' ');
		$this->db->leftJoin('resource_creator', 'resourcecreatorResourceId', 'resourceId');
		$this->db->leftJoin('creator', 'creatorId', 'resourcecreatorCreatorId');
		$this->db->leftJoin('resource_keyword', 'resourcekeywordResourceId', 'resourceId');
		$this->db->leftJoin('keyword', 'keywordId', 'resourcekeywordKeywordId');
		$this->db->leftJoin('resource_text', 'resourcetextId', 'resourceId');
		$this->db->leftJoinCondition('resource_metadata', $this->db->formatFields('resourcemetadataResourceId'),
			$this->db->formatFields('resourceId'), $metadataCond, FALSE, FALSE);
		$this->db->leftJoin('resource_user_tags', 'resourceusertagsResourceId', 'resourceId');
		$this->db->leftJoinCondition('user_tags', $this->db->formatFields('usertagsId'),
			$this->db->formatFields('resourceusertagsTagId'), $userCond, TRUE, FALSE);
		$this->db->leftJoin('resource_custom', 'resourcecustomResourceId', 'resourceId');
		$this->db->conditionSeparator = $this->db->or;
		$this->db->formatConditions(array('resourcecreatorResourceId' => ' IS NOT NULL ',
			'resourcekeywordResourceId' => ' IS NOT NULL ',
			'resourcetextId' => ' IS NOT NULL ',
			'resourcecustomResourceId' => ' IS NOT NULL'));
		$this->db->conditionSeparator = $this->db->and;
		$this->db->groupBy('resourceId');
		$this->unions = $this->db->queryNoExecute($this->db->selectNoExecute('resource',
			array($this->db->formatFields(array(array('resourceId' => 'rId'))),
			$this->db->formatFields(array(array($concat => 'concatText')), FALSE, FALSE)),
			FALSE, FALSE));
	}
/**
* For re-ordering or paging
*/
	public function reprocess()
	{
		$reprocess = TRUE;
		$this->input = $this->session->getArray("search");
		if(array_key_exists("search_Order", $this->vars) && $this->vars["search_Order"])
		{
			if(($this->session->getVar('search_Order') != $this->vars["search_Order"]) ||
				($this->session->getVar('search_AscDesc') != $this->vars['search_AscDesc']))
				$reprocess = FALSE;
			$this->input['order'] = $this->vars["search_Order"];
			$this->session->setVar('search_Order', $this->input['order']);
			$this->session->setVar('sql_LastOrder', $this->input['order']);
			$this->session->setVar('search_AscDesc', $this->vars['search_AscDesc']);
		}
		$this->process($reprocess);
	}
	public function process($reprocess = FALSE)
	{
		if(!$reprocess)
		{
			$this->session->delVar('list_AllIds');
			$this->session->delVar('list_PagingAlphaLinks');
		}
		if(!$reprocess || ($this->session->getVar('setup_PagingStyle') == 'A'))
		{
			$this->session->delVar('sql_ListStmt');
			$this->session->delVar('advancedSearch_listParams');
		}
		GLOBALS::setTplVar('heading', $this->messages->text("heading", "search"));
		$this->stmt->listMethodAscDesc = 'search_AscDesc';
		$this->stmt->listType = 'search';
		$queryString = 'action=list_QUICKSEARCH_CORE&method=reprocess';
		if(empty($this->input))
			$this->input = $this->checkInput();
		if(!array_Key_exists('order', $this->input) && !array_Key_exists('Order', $this->input))
		{
			$this->session->setVar('search_Order', 'creator');
			$this->session->setVar('sql_LastOrder', 'creator');
			$this->session->setVar('search_AscDesc', $this->db->asc);
		}
		else
			$this->session->setVar('sql_LastOrder', $this->input['Order']);
		$this->input['Partial'] = TRUE;
		GLOBALS::setTplVar('resourceListSearchForm',  $this->init(FALSE, TRUE, TRUE));
		if(!$reprocess || ($this->session->getVar('setup_PagingStyle') == 'A'))
		{
			$this->parseWord();
			$this->fieldSql();
			$subStmt = $this->setSubQuery();
			$this->db->DisableFullGroupBySqlMode();
			$resourcesFound = $this->stmt->listSubQuery($this->session->getVar('search_Order'), $queryString, $subStmt, FALSE, $this->subQ);
			$this->db->EnableFullGroupBySqlMode();
			if(!$resourcesFound)
			{
				$this->common->noResources('search');
				return;
			}
		}
		if(array_key_exists('type', $this->vars) && ($this->vars['type'] == 'lastMulti') && ($this->session->getVar('setup_PagingStyle') != 'A'))
		{
			$this->pagingObject = FACTORY_PAGING::getInstance();
			$this->pagingObject->queryString = $queryString;
			$this->pagingObject->getPaging();
			$this->common->pagingObject = $this->pagingObject;
			$this->common->lastMulti('search');
			return;
		}
		$searchTerms = UTF8::mb_explode(",", $this->session->getVar('search_Highlight'));
		$patterns = array();
		foreach($searchTerms as $term)
		{
			if(trim($term))
			{
				$term = preg_quote($term);
				$patterns[] = "/($term)(?=[^>]*(<|$))/ui";
			}
		}
		$this->common->patterns = $patterns;
		$this->session->setVar('search_Patterns', base64_encode(serialize($patterns)));
		$this->common->keepHighlight = TRUE;
		if(!$reprocess || ($this->session->getVar('setup_PagingStyle') == 'A'))
			$sql = $this->stmt->listList($this->session->getVar('search_Order'), FALSE, $this->subQ);
		else
		{
			$sql = $this->session->getVar('sql_ListStmt');
			$this->pagingObject = FACTORY_PAGING::getInstance();
			$this->pagingObject->queryString = $queryString;
			$this->pagingObject->getPaging();
			$this->common->pagingObject = $this->pagingObject;
			$limit = $this->db->limit($this->session->getVar('setup_Paging'), $this->pagingObject->start, TRUE); // "LIMIT $limitStart, $limit";
			$sql .= $limit;
		}
		$this->common->display($sql, 'search');
// set the lastMulti session variable for quick return to this process.
		$this->session->setVar('sql_LastMulti', $queryString);
		$this->session->saveState(array('search', 'sql', 'setup', 'bookmark', 'list'));
		$this->session->delVar('bookmarkRead');
	}
// write input to session
	private function writeSession()
	{
// First, write all input with 'search_' prefix to session
		foreach($this->vars as $key => $value)
		{
			if(preg_match("/^search_/u", $key))
			{
				$key = str_replace('search_', '', $key);
// Is this a multiple select box input?  If so, multiple choices are written to session as
// comma-delimited string (no spaces).
// Don't write any FALSE or '0' values.
				if(is_array($value))
				{
					if(!$value[0] || ($value[0] == $this->messages->text("misc", "ignore")))
						unset($value[0]);
					$value = implode(",", $value);
				}
				if(!trim($value))
					continue;
				$temp[$key] = trim($value);
			}
		}
		$this->session->clearArray("search");
		if(!empty($temp))
			$this->session->writeArray($temp, 'search');
	}
// validate user input - method, word and field are required.
// Input comes either from form input or, when paging, from the session.
	private function checkInput()
	{
		$this->writeSession();
		if((array_key_exists("search_Word", $this->vars) && !trim($this->vars["search_Word"]))
		|| !$this->session->getVar("search_Word"))
		{
			$this->badInput->close($this->errors->text("inputError", "missing"), $this, 'init');
		}
		return $this->session->getArray("search");
	}
// Set the subQuery
	private function setSubQuery()
	{
		$this->db->ascDesc = $this->session->getVar('search_AscDesc');
		$this->stmt->quarantine(FALSE, 'rId');
		$this->stmt->useBib('rId');
		$this->stmt->conditions[] = $this->words;
		switch($this->session->getVar('search_Order'))
		{
			case 'title':
				$this->stmt->joins['resource'] = array('resourceId', 'rId');
				$this->stmt->executeCondJoins();
				$this->db->groupBy(array('rId'));
				$this->subQ = $this->db->subQuery($this->unions, 't', FALSE);
				$subQuery = $this->db->from . ' ' . $this->subQ;
				return $this->db->selectNoExecuteFromSubQuery(FALSE, array('rId'), $subQuery, FALSE, TRUE, TRUE);
			case 'creator':
				$this->stmt->joins['resource_creator'] = array('resourcecreatorResourceId', 'rId');
				$this->stmt->joins['creator'] = array('creatorId', 'resourcecreatorCreatorId');
				$this->stmt->executeCondJoins();
				$this->db->groupBy(array('rId'));
				$this->subQ = $this->db->subQuery($this->unions, 't', FALSE);
				$subQuery = $this->db->from . ' ' . $this->subQ;
				return $this->db->selectNoExecuteFromSubQuery(FALSE, array('rId'), $subQuery, FALSE, TRUE, TRUE);
			case 'publisher':
				$this->stmt->joins['resource_misc'] = array('resourcemiscId', 'rId');
				$this->stmt->joins['publisher'] = array('publisherId', 'resourcemiscPublisher');
				$this->stmt->executeCondJoins();
				$this->subQ = $this->db->subQuery($this->unions, 't', FALSE);
				$subQuery = $this->db->from . ' ' . $this->subQ;
				return $this->db->selectNoExecuteFromSubQuery(FALSE, array('rId'), $subQuery, FALSE, TRUE, TRUE);
			case 'year':
				$this->stmt->joins['resource'] = array('resourceId', 'rId');
				$this->stmt->joins['resource_year'] = array('resourceyearId', 'rId');
				$this->stmt->executeCondJoins();
				$this->subQ = $this->db->subQuery($this->unions, 't', FALSE);
				$subQuery = $this->db->from . ' ' . $this->subQ;
				return $this->db->selectNoExecuteFromSubQuery(FALSE, array('rId'), $subQuery, FALSE, TRUE, TRUE);
			case 'timestamp':
				$this->stmt->joins['resource_timestamp'] = array('resourcetimestampId', 'rId');
				$this->stmt->executeCondJoins();
				$this->subQ = $this->db->subQuery($this->unions, 't', FALSE);
				$subQuery = $this->db->from . ' ' . $this->subQ;
				return $this->db->selectNoExecuteFromSubQuery(FALSE, array('rId'), $subQuery, FALSE, TRUE, TRUE);
			default:
				break;
		}
	}
}
