<?php
/**
 * WIKINDX : Bibliographic Management system.
 * @link https://wikindx.sourceforge.io/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2018-2019 Mark Grimshaw-Aagaard <sirfragalot@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 */

/**
* CONSTANTS
*
* VARIOUS WIKINDX constants
*
* @version	2
*
*	@package wikindx\core\startup
*	@author Mark Grimshaw-Aagaard <sirfragalot@users.sourceforge.net>
*
*/

/**
* CONSTANTS
*/
define('TAB', "\t");
define('CR', "\r");
define('LF', "\n");
define('BR', '<br>');
/**
* WIKINDX official name
*
* @name WIKINDX_NAME
*/
define('WIKINDX_NAME', 'WIKINDX');
/**
* WIKINDX version information
*
* @name WIKINDX_VERSION
*/
define('WIKINDX_VERSION', '5.8.3');
/**
* WIKINDX copyright
*
* @name WIKINDX_COPYRIGHT_YEAR
*/
define('WIKINDX_COPYRIGHT_YEAR', 2019);
/**
* WIKINDX SF url
*
* @name WIKINDX_URL
*/
define('WIKINDX_URL', 'https://wikindx.sourceforge.io');
/**
* Plugin compatibility -- x.x (usually matching the major WIKINDX version) which must be changed each time plugins require an
* upgrade to match the WIKINDX code. The plugin's $config->wikindxVersion must be equal to this value for the plugin to be compatible.
* 
* The check occurs in LOADEXTERNALMODULES.php
*
* @name WIKINDX_PLUGIN_VERSION
*/
define('WIKINDX_PLUGIN_VERSION', 5.8);
/**
* Max number of url in a sitemap (50000 max. allowed in the standard and 10 Mo max.)
*
* @name WIKINDX_SITEMAP_MAXSIZE
*/
define('WIKINDX_SITEMAP_MAXSIZE', 5000);
/**
* URL of the website sitemap
*
* @name WIKINDX_SITEMAP_PAGE
*/
define('WIKINDX_SITEMAP_PAGE', '/index.php?action=sitemap_SITEMAP_CORE');
/**
* Minimum required PHP version
*
* @name WIKINDX_PHP_VERSION_MIN
*/
define('WIKINDX_PHP_VERSION_MIN', '5.5.0');
/**
* Default charset
*
* @name WIKINDX_CHARSET
*/
define('WIKINDX_CHARSET', 'UTF-8');
/**
* Charset detection order for mbstring
*
* @name WIKINDX_CHARSET
*/
define('WIKINDX_MBSTRING_DETECT_ORDER', WIKINDX_CHARSET . ',ISO-8859-15,ISO-8859-1,ASCII');

// Debugging
/**
* Default values of config.php file
* We keep here to check them at load time
* NB: PHP 7 only can define a constant array
*/
define('WIKINDX_PHP_ERROR_REPORTING_DEFAULT', E_ALL);
define('WIKINDX_PHP_DISPLAY_ERRORS_DEFAULT', 'On');
define('WIKINDX_DEBUG_ERRORS_DEFAULT', FALSE);
define('WIKINDX_DEBUG_EMAIL_DEFAULT', '');
define('WIKINDX_DEBUG_SQL_DEFAULT', FALSE);
define('WIKINDX_DEBUG_SQLERROROUTPUT_DEFAULT', 'printSql');
define('WIKINDX_BYPASS_SMARTYCOMPILE_DEFAULT', FALSE);


// List of main directories
define('WIKINDX_DIR_ATTACHMENTS', 'attachments');
define('WIKINDX_DIR_ATTACHMENTSCACHE', 'attachments_cache');
define('WIKINDX_DIR_FILES', 'files');
define('WIKINDX_DIR_IMAGES', 'images');
define('WIKINDX_DIR_LANGUAGES', 'languages');
define('WIKINDX_DIR_PLUGINS', 'plugins');
define('WIKINDX_DIR_STYLES', 'styles');
define('WIKINDX_DIR_TEMPLATES', 'templates');
define('WIKINDX_DIR_STYLES_CACHE', 'styles' . DIRECTORY_SEPARATOR . 'CACHE');
define('WIKINDX_DIR_TEMPLATES_CACHE', 'tplcompilation');


// List of mime types used in the code base
define('WIKINDX_MIMETYPE_BIB', 'application/x-bibtex');
define('WIKINDX_MIMETYPE_DOC', 'application/msword');
define('WIKINDX_MIMETYPE_DOCX', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document');
define('WIKINDX_MIMETYPE_ENDNOTE', 'application/vnd.openxmlformats-officedocument.wordprocessingml.endnotes+xml');
define('WIKINDX_MIMETYPE_HTM', 'text/html');
define('WIKINDX_MIMETYPE_PDF', 'application/pdf');
define('WIKINDX_MIMETYPE_RIS', 'application/x-research-info-systems');
define('WIKINDX_MIMETYPE_RSS', 'application/rss+xml');
define('WIKINDX_MIMETYPE_RTF', 'application/rtf');
define('WIKINDX_MIMETYPE_TXT', 'text/plain');
define('WIKINDX_MIMETYPE_XML', 'application/xml');

define('WIKINDX_HTTP_CONTENT_TYPE_DEFAULT', WIKINDX_MIMETYPE_HTM);


define('WIKINDX_TITLE_DEFAULT', 'WIKINDX');
define('WIKINDX_CONTACTEMAIL_DEFAULT', '');
define('WIKINDX_DESCRIPTION_DEFAULT', 'My Wikindx');
define('WIKINDX_FILEDELETESECONDS_DEFAULT', 3600);
define('WIKINDX_PAGING_DEFAULT', 20);
define('WIKINDX_PAGINGMAXLINKS_DEFAULT', 11);
define('WIKINDX_STRINGLIMIT_DEFAULT', 40);


// Localisation
/** Default language */
define('WIKINDX_LANGUAGE_DEFAULT', 'en');

/** Gettext domain name of the core part */
define('WIKINDX_LANGUAGE_DOMAIN_DEFAULT', 'wikindx');

/** Default time zone (GMT) */
define('WIKINDX_TIMEZONE_DEFAULT', 'UTC');


// Divers
define('WIKINDX_STYLE_DEFAULT', 'APA');
define('WIKINDX_TEMPLATE_DEFAULT', 'default');
define('WIKINDX_MULTIUSER_DEFAULT', FALSE);
define('WIKINDX_USERREGISTRATION_DEFAULT', FALSE);
define('WIKINDX_USERREGISTRATIONMODERATE_DEFAULT', FALSE);
define('WIKINDX_NOTIFY_DEFAULT', FALSE);
define('WIKINDX_IMGWIDTHLIMIT_DEFAULT', 400);
define('WIKINDX_IMGHEIGHTLIMIT_DEFAULT', 400);
define('WIKINDX_FILEATTACH_DEFAULT', FALSE);
define('WIKINDX_VIEWFILELOGGEDONONLY_DEFAULT', FALSE);
define('WIKINDX_MAXPASTE_DEFAULT', 10);
define('WIKINDX_LASTCHANGES_DEFAULT', 10);
define('WIKINDX_LASTCHANGESTYPE_DEFAULT', 'number');
define('WIKINDX_LASTCHANGESDAYLIMIT_DEFAULT', 10);
define('WIKINDX_PAGINGTAGCLOUD_DEFAULT', 100);
define('WIKINDX_PAGINGSTYLE_DEFAULT', 'N');
define('WIKINDX_USEBIBTEXKEY_DEFAULT', 'N');
define('WIKINDX_USEWIKINDXKEY_DEFAULT', 'N');
define('WIKINDX_DISPLAYBIBTEXLINK_DEFAULT', 'N');
define('WIKINDX_DISPLAYCMSLINK_DEFAULT', 'N');
define('WIKINDX_TEMPLATEMENU_DEFAULT', 0);
define('WIKINDX_IMPORTBIB_DEFAULT', FALSE);
define('WIKINDX_EMAILNEWS_DEFAULT', FALSE);
define('WIKINDX_EMAILNEWREGISTRATIONS_DEFAULT', '');
define('WIKINDX_QUARANTINE_DEFAULT', FALSE);
define('WIKINDX_LISTLINK_DEFAULT', FALSE);
define('WIKINDX_EMAILSTATISTICS_DEFAULT', FALSE);
define('WIKINDX_STATISTICSCOMPILED_DEFAULT', '2018-01-01 01:01:01');
define('WIKINDX_METADATAALLOW_DEFAULT', TRUE);
define('WIKINDX_METADATAUSERONLY_DEFAULT', FALSE);
define('WIKINDX_NOSORT_DEFAULT', base64_encode(serialize(array('an', 'a', 'the', 'der', 'die', 'das', 'ein', 'eine', 'einer', 'eines',
	'le', 'la', 'las', 'il', 'les', 'une', 'un', 'una', 'uno', 'lo', 'los', 'i', 'gli', 'de', 'het', 'um', 'uma', 'o', 'os', 'as', 'den', 'det', 'en', 'et'))));
define('WIKINDX_SEARCHFILTER_DEFAULT', base64_encode(serialize(array('an', 'a', 'the', 'and', 'to'))));
define('WIKINDX_DENYREADONLY_DEFAULT', FALSE);
define('WIKINDX_READONLYACCESS_DEFAULT', TRUE);
define('WIKINDX_ORIGINATOREDITONLY_DEFAULT', FALSE);
define('WIKINDX_GLOBALEDIT_DEFAULT', FALSE);


// Database
define('WIKINDX_DB_HOST_DEFAULT', 'localhost');
define('WIKINDX_DB_DEFAULT', 'wikindx5');
define('WIKINDX_DB_USER_DEFAULT', 'wikindx');
define('WIKINDX_DB_PASSWORD_DEFAULT', 'wikindx');
define('WIKINDX_DB_TABLEPREFIX_DEFAULT', 'wkx_');
define('WIKINDX_DB_PERSISTENT_DEFAULT', TRUE);


// System
define('WIKINDX_RESTRICT_USERID_DEFAULT', FALSE);
define('WIKINDX_SESSION_PATH_CLEAR_DEFAULT', 10);
define('WIKINDX_SESSION_PATH_DEFAULT', FALSE);
define('WIKINDX_MEMORY_LIMIT_DEFAULT', '32M');
define('WIKINDX_MAX_WRITECHUNK_DEFAULT', 10000);
define('WIKINDX_MAX_EXECUTION_TIMEOUT_DEFAULT', FALSE);


// RSS feed
define('WIKINDX_RSS_ALLOW_DEFAULT', FALSE);
define('WIKINDX_RSS_LIMIT_DEFAULT', 10);
define('WIKINDX_RSS_PAGE', '/index.php?action=rss_RSS_CORE');
define('WIKINDX_RSS_BIBSTYLE_DEFAULT', WIKINDX_STYLE_DEFAULT);
define('WIKINDX_RSS_DISPLAY_DEFAULT', FALSE);
define('WIKINDX_RSS_TITLE_DEFAULT', 'WIKINDX');
define('WIKINDX_RSS_DESCRIPTION_DEFAULT', 'My Wikindx');


// LDAP
define('WIKINDX_LDAP_USE_DEFAULT', FALSE);
define('WIKINDX_LDAP_SERVER_DEFAULT', 'localhost');
define('WIKINDX_LDAP_DN_DEFAULT', FALSE);
define('WIKINDX_LDAP_PORT_DEFAULT', 389);
define('WIKINDX_LDAP_PROTOCOL_VERSION_DEFAULT', 3);


// Auth
define('WIKINDX_AUTHGATE_USE_DEFAULT', FALSE);
define('WIKINDX_AUTHGATE_MESSAGE_DEFAULT', FALSE);
define('WIKINDX_PASSWORDSIZE_DEFAULT', 6);
define('WIKINDX_PASSWORDSTRENGTH_DEFAULT', 'strong');


// Mail system
define('WIKINDX_MAIL_SERVER_DEFAULT', FALSE);
define('WIKINDX_MAIL_FROM_DEFAULT', WIKINDX_TITLE_DEFAULT);
define('WIKINDX_MAIL_REPLYTO_DEFAULT', 'noreply@noreply.org');
define('WIKINDX_MAIL_RETURN_PATH_DEFAULT', FALSE);
define('WIKINDX_MAIL_BACKEND_DEFAULT', 'smtp');
define('WIKINDX_MAIL_SMPATH_DEFAULT', '/usr/sbin/sendmail');
define('WIKINDX_MAIL_SMARGS_DEFAULT', '-i');
define('WIKINDX_MAIL_SMTPSERVER_DEFAULT', 'localhost');
define('WIKINDX_MAIL_SMTPPORT_DEFAULT', '25');
define('WIKINDX_MAIL_SMTPENCRYPT_DEFAULT', '');
define('WIKINDX_MAIL_SMTPPERSIST_DEFAULT', FALSE);
define('WIKINDX_MAIL_SMTPAUTH_DEFAULT', FALSE);
define('WIKINDX_MAIL_SMTPUSERNAME_DEFAULT', '');
define('WIKINDX_MAIL_SMTPPASSWORD_DEFAULT', '');
define('WIKINDX_GS_ALLOW_DEFAULT', FALSE);
define('WIKINDX_GS_ATTACHMENT_DEFAULT', FALSE);


// CMS API
define('WIKINDX_CMS_PAGE', '/index.php?action=cms_CMS_CORE');
define('WIKINDX_CMS_ALLOW_DEFAULT', FALSE);
define('WIKINDX_CMS_BIBSTYLE_DEFAULT', WIKINDX_STYLE_DEFAULT);
define('WIKINDX_CMS_SQL_DEFAULT', FALSE);
define('WIKINDX_CMS_DB_USER_DEFAULT', '');
define('WIKINDX_CMS_DB_PASSWORD_DEFAULT', '');


// Divers
define('WIKINDX_TAG_LOW_COLOUR_DEFAULT', 'a0a0a0');
define('WIKINDX_TAG_HIGH_COLOUR_DEFAULT', 'ff0000');
define('WIKINDX_TAG_LOW_SIZE_DEFAULT', 1);
define('WIKINDX_TAG_HIGH_SIZE_DEFAULT', 2);
define('WIKINDX_IMAGES_ALLOW_DEFAULT', FALSE);
define('WIKINDX_IMAGES_MAXSIZE_DEFAULT', 5);

// Statistics
define('WIKINDX_DISPLAY_STATISTICS_DEFAULT', FALSE);
define('WIKINDX_DISPLAY_USER_STATISTICS_DEFAULT', FALSE);
