<?php
/**
 * WIKINDX : Bibliographic Management system.
 * @link https://wikindx.sourceforge.io/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2018-2019 Mark Grimshaw-Aagaard <sirfragalot@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 */

/**
* CHECKUPDATEDATABASE
*
* Check only if the current Wikindx database need an update
*
* @version	1
*
* @package wikindx\core\startup
*
*/
class CHECKUPDATEDATABASE
{
/** object */
private $db;
/**
* CHECKUPDATEDATABASE
*/
	public function __construct()
	{
		$this->db = FACTORY_DB::getInstance();
	}
/**
* Check if the current Wikindx database need an upgrade
*
* @param boolean
*/
	public function needUpdate()
	{
	    // NB: tableConfigExists must be the first operation because
	    // it reads the db system catalog and can't fail if the db exists
	    if (!$this->tableConfigExists())
	        return TRUE;
	    else if (!$this->configurationExists())
	        return TRUE;
	    else if (!$this->isLastDatabaseVersion())
	        return TRUE;
	    else if (!$this->userExists())
	        return TRUE;
	    else
	        return FALSE;
	}
/**
* Check if 'config' table exists
*
* @return boolean
*/
	private function tableConfigExists()
	{
		return $this->db->tableExists('config');
	}
/**
* Check if 'config' table has been filled with a configuration
*
* @return boolean
*/
	private function configurationExists()
	{
		return !$this->db->tableIsEmpty('config');
	}
/**
* Check if 'users' table has been filled with a configuration
*
* @return boolean
*/
	private function userExists()
	{
		return !$this->db->tableIsEmpty('users');
	}
/**
* Check if the database version number is the same as source code version number
*
* @return boolean
*/
	private function isLastDatabaseVersion()
	{
	    $recordset = $this->db->queryNoError($this->db->selectNoExecute('database_summary', 'databasesummaryDbVersion'));

	    // The version field name have been modified
		if ($recordset !== FALSE)
	    {
	        $row = $this->db->fetchRow($recordset);
			$dbVersion = $row['databasesummaryDbVersion'];
	        unset($row);
		}
		else
		{
	        $recordset = $this->db->queryNoError($this->db->selectNoExecute('database_summary', 'dbVersion'));

	        if ($recordset !== FALSE)
	        {
    	        $row = $this->db->fetchRow($recordset);
    			$dbVersion = $row['dbVersion'];
    	        unset($row);
	        }
	        else
	            $dbVersion = 0;
		}
		$wV = explode('.', WIKINDX_VERSION);
		$wVersion = $wV[0] . '.' . $wV[1];
		return ($dbVersion == $wVersion);
	}
/**
* destruct method
*/
	public function __destruct()
	{
		$this->db = NULL;
	}
}
