<?php
/**
 * WIKINDX : Bibliographic Management system.
 * @link https://wikindx.sourceforge.io/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2019 Mark Grimshaw-Aagaard <sirfragalot@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
*/

/**
* HOUSEKEEPING
*
* Housekeeping tasks on startup
*
* @version	1
* @package wikindx\core\startup
* @author Mark Grimshaw-Aagaard <sirfragalot@users.sourceforge.net>
*/
class HOUSEKEEPING
{
/** object */
private $session;
/**
*	HOUSEKEEPING
*
* @param string $upgradeCompleted
*/
	public function __construct($upgradeCompleted)
	{
		$this->session = FACTORY_SESSION::getInstance();
		$this->statistics();
		if($this->session->getVar("setup_UserId") == 1) // superadmin logging on – caching requires the superadmin to click further
			$this->cacheAttachments($upgradeCompleted);
	}
/**
* Check if statistics need compiling and emailing out to registered users.
*/
	private function statistics()
	{
		$stats = FACTORY_STATISTICS::getInstance();
		$stats->compile();
	}
/**
* Check if any attachments need caching
*
* @param string $upgradeCompleted
*/
	public function cacheAttachments($upgradeCompleted)
	{
		$db = FACTORY_DB::getInstance();
		$messages = FACTORY_MESSAGES::getInstance();
		$count = 0;
		$attachDir = \FACTORY_CONFIG::getInstance()->WIKINDX_ATTACHMENTS_DIR;
		$cacheDir = \FACTORY_CONFIG::getInstance()->WIKINDX_ATTACHMENTSCACHE_DIR;
		$cacheDirFiles = scanDir($cacheDir);
		foreach($cacheDirFiles AS $key => $value)
		{
			if(($value == 'index.html') || ($value == 'ATTACHMENTS_CACHE') || (strpos($value, '.') === 0))
				unset($cacheDirFiles[$key]);
		}
		$this->session->setVar('cache_Attachments', sizeOf($cacheDirFiles));
		$mimeTypes = array(WIKINDX_MIMETYPE_PDF, WIKINDX_MIMETYPE_DOCX, WIKINDX_MIMETYPE_DOC);
		$db->formatConditionsOneField($mimeTypes, 'resourceattachmentsFileType');
		$resultset = $db->select('resource_attachments', array('resourceattachmentsHashFilename'));
		while($row = $db->fetchRow($resultset))
		{
			$f = $row['resourceattachmentsHashFilename'];
			$fileName = $attachDir . DIRECTORY_SEPARATOR . $f;
			$fileNameCache = $cacheDir . DIRECTORY_SEPARATOR . $f;
			if(!file_exists($fileName) || (file_exists($fileNameCache) && filemtime($fileNameCache) >= filemtime($fileName)))
				continue;
			++$count;
		}
		if($count)
		{
			$pString = \HTML\p($messages->text("misc", "attachmentCache1"));
			$pString .= \HTML\p($messages->text("misc", "attachmentCache2", $count));
			$lastCache = $this->session->getVar('cache_Attachments');
			if($lastCache)
				$pString .= \HTML\p($messages->text("misc", "attachmentCache3", $lastCache));
			$pString .= \FORM\formHeader("list_FILETOTEXT_CORE");
			$pString .= \FORM\hidden("method", "checkCache");
			if(function_exists('curl_multi_exec'))
			{
				if(!$this->session->getVar('cache_Attachments')) // At beginning
					$checked = 'CHECKED';
				else if($this->session->getVar('cache_Curl'))
					$checked = 'CHECKED';
				else
					$checked = FALSE;
				$pString .= \HTML\p(\FORM\checkbox($messages->text("misc", "attachmentCache4"), "cacheCurl", $checked));
			}
			$value = $this->session->getVar('cache_Limit');
			$pString .= \HTML\p($messages->text("misc", "attachmentCache5", \FORM\textInput(FALSE, "cacheLimit", $value, 3)));
			$pString .= \HTML\p(\FORM\formSubmit($this->messages->text("submit", "Cache")) . \FORM\formEnd());
			$pString .= \HTML\p(\HTML\a("skip", $messages->text("misc", "attachmentCache6"), htmlentities("index.php?action=skipCaching")));
			GLOBALS::addTplVar('content', $pString);
			FACTORY_CLOSENOMENU::getInstance(); // die
		}
		else
		{
		    if($upgradeCompleted == TRUE)
		    {
    			$success = FACTORY_SUCCESS::getInstance();
    			$message = $success->text("upgradeDB");
    			if(WIKINDX_VERSION >= 5.3)
	    			$message .= $success->text("upgradeDBv5.3");
			}
			else
				$message = '';
			include_once("core/display/FRONT.php");
			$front = new FRONT($message); // __construct() runs on autopilot
			FACTORY_CLOSE::getInstance();
		}
	}
}
