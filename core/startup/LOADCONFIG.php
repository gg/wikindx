<?php
/**
 * WIKINDX : Bibliographic Management system.
 * @link https://wikindx.sourceforge.io/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2017-2019 Mark Grimshaw-Aagaard <sirfragalot@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 */

/**
*	LOADCONFIG
*
*	Load config variables from the database
*
* @version	1
*
*	@package wikindx\core\startup
*	@author Mark Grimshaw-Aagaard <sirfragalot@users.sourceforge.net>
*
*/
class LOADCONFIG
{
/** object */
private $db;
/** object */
private $config;
/** object */
private $configDbStructure;

/**
*	LOADCONFIG
*/
	public function __construct()
	{
		$this->config = FACTORY_CONFIG::getInstance();
		$this->configDbStructure = FACTORY_CONFIGDBSTRUCTURE::getInstance();
		$this->db = FACTORY_DB::getInstance();
	}
/**
* Load various arrays into $this->config object
*/
	public function load()
	{
		$array= array();
		$resultSet = $this->db->select('config', '*');
		while($row = $this->db->fetchRow($resultSet))
		{
			if(array_key_exists($row['configName'], $this->configDbStructure->dbStructure) &&
			($value = $row[$this->configDbStructure->dbStructure[$row['configName']]]))
			{
				if(($value == 1) && ($this->configDbStructure->dbStructure[$row['configName']] == 'configBoolean'))
					$array[$row['configName']] = TRUE;
				else
				{
					if(($this->configDbStructure->dbStructure[$row['configName']] == 'configInt') ||
						($this->configDbStructure->dbStructure[$row['configName']] == 'configFloat'))
						$array[$row['configName']] = $value + 0; // cast to number from database string
					else
						$array[$row['configName']] = $value;
				}
			}
			else
				$array[$row['configName']] = FALSE;
			if(array_key_exists($row['configName'], $this->configDbStructure->configToConstant))
			{
				$configKey = $this->configDbStructure->configToConstant[$row['configName']];
				if(($row['configName'] == 'configNoSort') || ($row['configName'] == 'configSearchFilter') ||
					($row['configName'] == 'configDeactivateResourceTypes'))
					$this->config->{$configKey} = unserialize(base64_decode($array[$row['configName']]));
				else
					$this->config->{$configKey} = $array[$row['configName']];
			}
		}
		$this->checkConfigValidity();
		$this->configureErrorReporting();
	}
/**
* Check validity of configuration in database and amend where necessary (dying if required).
*
*/
	private function checkConfigValidity()
	{
		$dieMsgMissing = 'Missing configuration variable: ';
		if (!property_exists($this->config, 'WIKINDX_TITLE') || !$this->config->WIKINDX_TITLE)
			$this->config->WIKINDX_TITLE = WIKINDX_NAME;
// Set timezone (default if needed)
		if (!property_exists($this->config, 'WIKINDX_TIMEZONE'))
			$this->config->WIKINDX_TIMEZONE = WIKINDX_TIMEZONE_DEFAULT;
		elseif(!is_string($this->config->WIKINDX_TIMEZONE))
			die('WIKINDX_TIMEZONE must be a valid PHP time zone. See https://secure.php.net/manual/fr/timezones.php');
		elseif(trim($this->config->WIKINDX_TIMEZONE) == '')
			die('WIKINDX_TIMEZONE must be a valid PHP time zone. See https://secure.php.net/manual/fr/timezones.php');

		date_default_timezone_set($this->config->WIKINDX_TIMEZONE);

// Special userId (FALSE or userID from database). Used on the test wikindx to stop this write-enabled user changing login details
		if (!property_exists($this->config, 'WIKINDX_RESTRICT_USERID'))
			die($dieMsgMissing . 'WIKINDX_RESTRICT_USERID');
// Set resource type
		if (!property_exists($this->config, 'WIKINDX_DEACTIVATE_RESOURCE_TYPES'))
			die($dieMsgMissing . 'WIKINDX_DEACTIVATE_RESOURCE_TYPES');
		elseif(!is_array($this->config->WIKINDX_DEACTIVATE_RESOURCE_TYPES))
			die('WIKINDX_DEACTIVATE_RESOURCE_TYPES must be an array.');

// Enable / disable RSS feed
		if (!property_exists($this->config, 'WIKINDX_RSS_ALLOW'))
			die($dieMsgMissing . 'WIKINDX_RSS_ALLOW');
		elseif(!is_bool($this->config->WIKINDX_RSS_ALLOW))
			die('WIKINDX_RSS_ALLOW must be a boolean (TRUE / FALSE).');

// Set number of items in RSS feed
		if (!property_exists($this->config, 'WIKINDX_RSS_LIMIT'))
			$this->config->WIKINDX_RSS_LIMIT = WIKINDX_RSS_LIMIT_DEFAULT;
		elseif(!is_int($this->config->WIKINDX_RSS_LIMIT))
			die('WIKINDX_RSS_LIMIT must be > 0.');
		elseif($this->config->WIKINDX_RSS_ALLOW && $this->config->WIKINDX_RSS_LIMIT < 1)
			die('WIKINDX_RSS_LIMIT must be > 0.');

// Set bibliographic style of RSS feed
		if (!property_exists($this->config, 'WIKINDX_RSS_BIBSTYLE'))
			$this->config->WIKINDX_RSS_BIBSTYLE = WIKINDX_RSS_BIBSTYLE_DEFAULT;
		elseif(!is_string($this->config->WIKINDX_RSS_BIBSTYLE))
			die('WIKINDX_RSS_BIBSTYLE must be an existing bibliographic style name.');

// Set recents display in RSS feed
		if (!property_exists($this->config, 'WIKINDX_RSS_DISPLAY'))
			$this->config->WIKINDX_RSS_DISPLAY = WIKINDX_RSS_DISPLAY_DEFAULT;
		elseif(!is_bool($this->config->WIKINDX_RSS_DISPLAY))
			die('WIKINDX_RSS_DISPLAY must be a boolean (TRUE / FALSE).');

// Set RSS feed title
		if (!property_exists($this->config, 'WIKINDX_RSS_TITLE'))
			$this->config->WIKINDX_RSS_TITLE = WIKINDX_RSS_TITLE_DEFAULT;
		elseif(!is_string($this->config->WIKINDX_RSS_TITLE))
			die('WIKINDX_RSS_TITLE must be a string.');

// Set RSS feed description
		if (!property_exists($this->config, 'WIKINDX_RSS_DESCRIPTION'))
			$this->config->WIKINDX_RSS_DESCRIPTION = WIKINDX_RSS_DESCRIPTION_DEFAULT;
		elseif(!is_string($this->config->WIKINDX_RSS_DESCRIPTION))
			die('WIKINDX_RSS_DESCRIPTION must be a string.');

// Set LDAP configuration
		if (!property_exists($this->config, 'WIKINDX_LDAP_USE'))
			$this->config->WIKINDX_LDAP_USE = FALSE;
		elseif(!is_bool($this->config->WIKINDX_LDAP_USE))
			die('WIKINDX_LDAP_USE must be a boolean (TRUE / FALSE).');

// Set LDAP SERVER
		if (!property_exists($this->config, 'WIKINDX_LDAP_SERVER'))
			$this->config->WIKINDX_LDAP_SERVER = WIKINDX_LDAP_SERVER_DEFAULT;
		elseif(!is_string($this->config->WIKINDX_LDAP_SERVER))
			if($this->config->WIKINDX_LDAP_SERVER !== FALSE)
				die('WIKINDX_LDAP_SERVER must be a string or FALSE.');
		elseif(trim($this->config->WIKINDX_LDAP_SERVER == ''))
			$this->config->WIKINDX_LDAP_SERVER = WIKINDX_LDAP_SERVER_DEFAULT;

// Set LDAP DN
		if (!property_exists($this->config, 'WIKINDX_LDAP_DN'))
			$this->config->WIKINDX_LDAP_DN = WIKINDX_LDAP_DN_DEFAULT;
		elseif(!is_string($this->config->WIKINDX_LDAP_DN))
			if($this->config->WIKINDX_LDAP_DN !== FALSE)
				die('WIKINDX_LDAP_DN must be a string or FALSE.');
		elseif(trim($this->config->WIKINDX_LDAP_DN == ''))
			$this->config->WIKINDX_LDAP_DN = WIKINDX_LDAP_DN_DEFAULT;

// Set LDAP port
		if (!property_exists($this->config, 'WIKINDX_LDAP_PORT'))
			$this->config->WIKINDX_LDAP_PORT = WIKINDX_LDAP_PORT_DEFAULT;
		elseif(is_int($this->config->WIKINDX_LDAP_PORT))
			if($this->config->WIKINDX_LDAP_PORT < 0)
				die('WIKINDX_LDAP_PORT must be a positive integer.');

// Set LDAP Opt Protocol Version
		if (!property_exists($this->config, 'WIKINDX_LDAP_PROTOCOL_VERSION'))
			$this->config->WIKINDX_LDAP_PROTOCOL_VERSION = WIKINDX_LDAP_PROTOCOL_VERSION_DEFAULT;
		elseif(is_int($this->config->WIKINDX_LDAP_PROTOCOL_VERSION))
			if($this->config->WIKINDX_LDAP_PROTOCOL_VERSION < 0)
				die('WIKINDX_LDAP_PROTOCOL_VERSION must be a positive integer.');

// Set AUTHGATE configuration (or similar authentication gate)
		if (!property_exists($this->config, 'WIKINDX_AUTHGATE_USE'))
			$this->config->WIKINDX_AUTHGATE_USE = FALSE;
		elseif(!is_bool($this->config->WIKINDX_AUTHGATE_USE))
			die('WIKINDX_AUTHGATE_USE must be a boolean (TRUE / FALSE).');

// Set AUTHGATE message
		if (!property_exists($this->config, 'WIKINDX_AUTHGATE_MESSAGE'))
			$this->config->WIKINDX_AUTHGATE_MESSAGE = WIKINDX_AUTHGATE_MESSAGE_DEFAULT;
		elseif(!is_string($this->config->WIKINDX_AUTHGATE_MESSAGE))
			if($this->config->WIKINDX_AUTHGATE_MESSAGE !== FALSE)
				die('WIKINDX_AUTHGATE_MESSAGE must be a string or FALSE.');
		elseif(trim($this->config->WIKINDX_AUTHGATE_MESSAGE == ''))
			$this->config->WIKINDX_AUTHGATE_MESSAGE = WIKINDX_AUTHGATE_MESSAGE_DEFAULT;

// Set PASSWORD size
		if (!property_exists($this->config, 'WIKINDX_PASSWORDSIZE'))
			$this->config->WIKINDX_PASSWORDSIZE = WIKINDX_PASSWORDSIZE_DEFAULT;
		elseif(!is_int($this->config->WIKINDX_PASSWORDSIZE))
			if($this->config->WIKINDX_PASSWORDSIZE < 0)
				die('WIKINDX_PASSWORDSIZE must be a positive integer.');

// Set PASSWORD strength
		if (!property_exists($this->config, 'WIKINDX_PASSWORDSTRENGTH'))
			$this->config->WIKINDX_PASSWORDSTRENGTH = WIKINDX_PASSWORDSTRENGTH_DEFAULT;
		elseif(!is_string($this->config->WIKINDX_PASSWORDSTRENGTH))
			if($this->config->WIKINDX_PASSWORDSTRENGTH !== FALSE)
				die('WIKINDX_PASSWORDSTRENGTH must be a string or FALSE.');
		elseif(trim($this->config->WIKINDX_PASSWORDSTRENGTH == ''))
			$this->config->WIKINDX_PASSWORDSTRENGTH = WIKINDX_PASSWORDSTRENGTH_DEFAULT;

// Set mailer configuration
		if (!property_exists($this->config, 'WIKINDX_MAIL_SERVER'))
			die($dieMsgMissing . 'WIKINDX_MAIL_SERVER');
		elseif(!is_bool($this->config->WIKINDX_MAIL_SERVER))
			die('WIKINDX_MAIL_SERVER must be a boolean (TRUE / FALSE).');

// Set email from header
		if (!property_exists($this->config, 'WIKINDX_MAIL_FROM'))
			$this->config->WIKINDX_MAIL_FROM = WIKINDX_MAIL_FROM_DEFAULT;
		elseif(!is_string($this->config->WIKINDX_MAIL_FROM))
			if($this->config->WIKINDX_MAIL_FROM !== FALSE)
				die('WIKINDX_MAIL_FROM must be a string or FALSE.');
		elseif(trim($this->config->WIKINDX_MAIL_FROM == ''))
			$this->config->WIKINDX_MAIL_FROM = WIKINDX_MAIL_FROM_DEFAULT;

// Set email reply-to header
		if (!property_exists($this->config, 'WIKINDX_MAIL_REPLYTO'))
			$this->config->WIKINDX_MAIL_REPLYTO = WIKINDX_MAIL_REPLYTO_DEFAULT;
		elseif(!is_string($this->config->WIKINDX_MAIL_REPLYTO))
			if($this->config->WIKINDX_MAIL_REPLYTO !== FALSE)
				die('WIKINDX_MAIL_REPLYTO must be a string or FALSE.');
		elseif(trim($this->config->WIKINDX_MAIL_REPLYTO == ''))
			$this->config->WIKINDX_MAIL_REPLYTO = WIKINDX_MAIL_REPLYTO_DEFAULT;

// Set email path return header
		if (!property_exists($this->config, 'WIKINDX_MAIL_RETURN_PATH'))
			$this->config->WIKINDX_MAIL_RETURN_PATH = WIKINDX_MAIL_RETURN_PATH_DEFAULT;
		elseif(!is_string($this->config->WIKINDX_MAIL_RETURN_PATH))
			if($this->config->WIKINDX_MAIL_RETURN_PATH !== FALSE)
				die('WIKINDX_MAIL_RETURN_PATH must be a string or FALSE.');
		elseif(trim($this->config->WIKINDX_MAIL_RETURN_PATH == ''))
			$this->config->WIKINDX_MAIL_RETURN_PATH = WIKINDX_MAIL_RETURN_PATH_DEFAULT;

// Set MAIL backend
		if (!property_exists($this->config, 'WIKINDX_MAIL_BACKEND'))
			$this->config->WIKINDX_MAIL_BACKEND = WIKINDX_MAIL_BACKEND_DEFAULT;
		elseif(!is_string($this->config->WIKINDX_MAIL_BACKEND))
			die('WIKINDX_MAIL_BACKEND must be of this value: ' . join(', ', array('smtp', 'sendmail', 'mail')));
		elseif(!in_array($this->config->WIKINDX_MAIL_BACKEND, array('smtp', 'sendmail', 'mail')))
			die('WIKINDX_MAIL_BACKEND must be of this value: ' . join(', ', array('smtp', 'sendmail', 'mail')));
		elseif($this->config->WIKINDX_MAIL_BACKEND == 'mail' && !function_exists('mail'))
			die('Mail backend unavailable [WIKINDX_MAIL_BACKEND] : mail() function is disabled in the configuration of PHP.');

// Set sendmail path
		if (!property_exists($this->config, 'WIKINDX_MAIL_SMPATH'))
			$this->config->WIKINDX_MAIL_SMPATH = WIKINDX_MAIL_SMPATH_DEFAULT;
		elseif(!is_string($this->config->WIKINDX_MAIL_SMPATH))
			die('WIKINDX_MAIL_SMPATH must be a string.');

// Set sendmail optional parameters
		if (!property_exists($this->config, 'WIKINDX_MAIL_SMARGS'))
			$this->config->WIKINDX_MAIL_SMARGS = WIKINDX_MAIL_SMARGS_DEFAULT;
		elseif(!is_string($this->config->WIKINDX_MAIL_SMARGS))
			die('WIKINDX_MAIL_SMARGS must be a string.');

// Set smtp hostname
		if (!property_exists($this->config, 'WIKINDX_MAIL_SMTPSERVER'))
			$this->config->WIKINDX_MAIL_SMTPSERVER = WIKINDX_MAIL_SMTPSERVER_DEFAULT;
		elseif(!is_string($this->config->WIKINDX_MAIL_SMTPSERVER))
			die('WIKINDX_MAIL_SMTPSERVER must be a string.');

// Set smtp port
		if (!property_exists($this->config, 'WIKINDX_MAIL_SMTPPORT'))
			$this->config->WIKINDX_MAIL_SMTPPORT = WIKINDX_MAIL_SMTPPORT_DEFAULT;
		elseif(is_int($this->config->WIKINDX_MAIL_SMTPPORT))
			if($this->config->WIKINDX_MAIL_SMTPPORT < 0)
				die('WIKINDX_MAIL_SMTPPORT must be a positive integer.');

// Set smtp encryption
		if (!property_exists($this->config, 'WIKINDX_MAIL_SMTPENCRYPT'))
			$this->config->WIKINDX_MAIL_SMTPENCRYPT = WIKINDX_MAIL_SMTPENCRYPT_DEFAULT;
		else if(!$this->config->WIKINDX_MAIL_SMTPENCRYPT)
			$this->config->WIKINDX_MAIL_SMTPENCRYPT = '';
// No encryption stored in the database as 'none' Рensure this is an empty string
		else if($this->config->WIKINDX_MAIL_SMTPENCRYPT == 'none')
			$this->config->WIKINDX_MAIL_SMTPENCRYPT = '';
		elseif(!is_string($this->config->WIKINDX_MAIL_SMTPENCRYPT))
			die('WIKINDX_MAIL_SMTPENCRYPT must be of this value: ' . join(', ', array('tls', 'ssl', 'or an empty string')));
		elseif(!in_array($this->config->WIKINDX_MAIL_SMTPENCRYPT, array('', 'tls', 'ssl')))
			die('WIKINDX_MAIL_SMTPENCRYPT must be of this value: ' . join(', ', array('tls', 'ssl', 'or an empty string')));
		elseif ($this->config->WIKINDX_MAIL_SMTPENCRYPT == '')
		    $this->config->WIKINDX_MAIL_SMTPENCRYPT == FALSE;

// Set smtp persist
		if (!property_exists($this->config, 'WIKINDX_MAIL_SMTPPERSIST'))
			$this->config->WIKINDX_MAIL_SMTPPERSIST = WIKINDX_MAIL_SMTPPERSIST_DEFAULT;
		elseif(!is_bool($this->config->WIKINDX_MAIL_SMTPPERSIST))
			die('WIKINDX_MAIL_SMTPPERSIST must be a boolean (TRUE / FALSE).');

// Set smtp auth
		if (!property_exists($this->config, 'WIKINDX_MAIL_SMTPAUTH'))
			$this->config->WIKINDX_MAIL_SMTPAUTH = WIKINDX_MAIL_SMTPAUTH_DEFAULT;
		elseif(!is_bool($this->config->WIKINDX_MAIL_SMTPAUTH))
			die('WIKINDX_MAIL_SMTPAUTH must be a boolean (TRUE / FALSE).');

// Set smtp user
		if (!property_exists($this->config, 'WIKINDX_MAIL_SMTPUSERNAME'))
			$this->config->WIKINDX_MAIL_SMTPUSERNAME = WIKINDX_MAIL_SMTPUSERNAME_DEFAULT;
		elseif(!$this->config->WIKINDX_MAIL_SMTPUSERNAME)
			$this->config->WIKINDX_MAIL_SMTPUSERNAME = '';
		elseif(!is_string($this->config->WIKINDX_MAIL_SMTPUSERNAME))
			die('WIKINDX_MAIL_SMTPUSERNAME must be a string.');

// Set smtp password
		if (!property_exists($this->config, 'WIKINDX_MAIL_SMTPPASSWORD'))
			$this->config->WIKINDX_MAIL_SMTPPASSWORD = WIKINDX_MAIL_SMTPPASSWORD_DEFAULT;
		elseif(!$this->config->WIKINDX_MAIL_SMTPPASSWORD)
			$this->config->WIKINDX_MAIL_SMTPPASSWORD = '';
		elseif(!is_string($this->config->WIKINDX_MAIL_SMTPPASSWORD))
			die('WIKINDX_MAIL_SMTPPASSWORD must be a string.');

// Enable / disable GS
		if (!property_exists($this->config, 'WIKINDX_GS_ALLOW'))
			die($dieMsgMissing . 'WIKINDX_GS_ALLOW');
		elseif(!is_bool($this->config->WIKINDX_GS_ALLOW))
			die('WIKINDX_GS_ALLOW must be a boolean (TRUE / FALSE).');

// Enable / disable GS attachments
		if (!property_exists($this->config, 'WIKINDX_GS_ATTACHMENT'))
			$this->config->WIKINDX_GS_ATTACHMENT = WIKINDX_GS_ATTACHMENT_DEFAULT;
		elseif(!is_bool($this->config->WIKINDX_GS_ATTACHMENT))
			die('WIKINDX_GS_ATTACHMENT must be a boolean (TRUE / FALSE).');

// Enable / disable CMS
		if (!property_exists($this->config, 'WIKINDX_CMS_ALLOW'))
			die($dieMsgMissing . 'WIKINDX_CMS_ALLOW');
		elseif(!is_bool($this->config->WIKINDX_CMS_ALLOW))
			die('WIKINDX_CMS_ALLOW must be a boolean (TRUE / FALSE).');

// Set bibliographic style of CMS
		if (!property_exists($this->config, 'WIKINDX_CMS_BIBSTYLE'))
			if($this->config->WIKINDX_CMS_ALLOW)
				die($dieMsgMissing . 'WIKINDX_CMS_BIBSTYLE');
			else
				$this->config->WIKINDX_CMS_BIBSTYLE = WIKINDX_CMS_BIBSTYLE_DEFAULT;
		elseif(!is_string($this->config->WIKINDX_CMS_BIBSTYLE))
			die('WIKINDX_CMS_BIBSTYLE must an existing bibliographic style name.');

// Enable / disable CMS database access
		if (!property_exists($this->config, 'WIKINDX_CMS_SQL'))
			if($this->config->WIKINDX_CMS_ALLOW)
				die($dieMsgMissing . 'WIKINDX_CMS_SQL');
			else
				$this->config->WIKINDX_CMS_SQL = WIKINDX_CMS_SQL_DEFAULT;
		elseif(!is_bool($this->config->WIKINDX_CMS_SQL))
			die('WIKINDX_CMS_SQL must be a boolean (TRUE / FALSE).');

// Set CMS database user
		if (!property_exists($this->config, 'WIKINDX_CMS_DB_USER'))
		{
			if($this->config->WIKINDX_CMS_SQL)
				die($dieMsgMissing . 'WIKINDX_CMS_DB_USER');
			else
				$this->config->WIKINDX_CMS_DB_USER = WIKINDX_CMS_DB_USER_DEFAULT;
		}
		elseif(!$this->config->WIKINDX_CMS_DB_USER)
			$this->config->WIKINDX_CMS_DB_USER = '';
		elseif(!is_string($this->config->WIKINDX_CMS_DB_USER))
			die('WIKINDX_CMS_DB_USER must be a string.');

// Set CMS database password
		if (!property_exists($this->config, 'WIKINDX_CMS_DB_PASSWORD'))
		{
			if($this->config->WIKINDX_CMS_SQL)
				die($dieMsgMissing . 'WIKINDX_CMS_DB_PASSWORD');
			else
				$this->config->WIKINDX_CMS_DB_PASSWORD = WIKINDX_CMS_DB_PASSWORD_DEFAULT;
		}
		elseif(!$this->config->WIKINDX_CMS_DB_PASSWORD)
			$this->config->WIKINDX_CMS_DB_PASSWORD = '';
		elseif(!is_string($this->config->WIKINDX_CMS_DB_PASSWORD))
			die('WIKINDX_CMS_DB_PASSWORD must be a string.');

// Set Tag cloud low frequency color
		if (!property_exists($this->config, 'WIKINDX_TAG_LOW_COLOUR'))
			$this->config->WIKINDX_TAG_LOW_COLOUR = WIKINDX_TAG_LOW_COLOUR_DEFAULT;
		elseif(!is_string($this->config->WIKINDX_TAG_LOW_COLOUR))
			die('WIKINDX_TAG_LOW_COLOUR must be a valid HTML color (e.g. CCCCCC, gray).');

// Set Tag cloud high frequency color
		if (!property_exists($this->config, 'WIKINDX_TAG_HIGH_COLOUR'))
			$this->config->WIKINDX_TAG_HIGH_COLOUR = WIKINDX_TAG_HIGH_COLOUR_DEFAULT;
		elseif(!is_string($this->config->WIKINDX_TAG_HIGH_COLOUR))
			die('WIKINDX_TAG_HIGH_COLOUR must be a valid HTML color (e.g. FF0000, red).');

// Set Tag cloud low frequency size
		if (!property_exists($this->config, 'WIKINDX_TAG_LOW_SIZE'))
			$this->config->WIKINDX_TAG_LOW_SIZE = WIKINDX_TAG_LOW_SIZE_DEFAULT;
		elseif(!is_float($this->config->WIKINDX_TAG_LOW_SIZE) && !is_int($this->config->WIKINDX_TAG_LOW_SIZE))
			die('WIKINDX_TAG_LOW_SIZE must be a number.');

// Set Tag cloud low frequency size
		if (!property_exists($this->config, 'WIKINDX_TAG_HIGH_SIZE'))
			$this->config->WIKINDX_TAG_HIGH_SIZE = WIKINDX_TAG_HIGH_SIZE_DEFAULT;
		elseif(!is_float($this->config->WIKINDX_TAG_HIGH_SIZE) && !is_int($this->config->WIKINDX_TAG_HIGH_SIZE))
			die('WIKINDX_TAG_HIGH_SIZE must be a number.');

// Enable / disable images
		if (!property_exists($this->config, 'WIKINDX_IMAGES_ALLOW'))
			die($dieMsgMissing . 'WIKINDX_IMAGES_ALLOW');
		elseif(!is_bool($this->config->WIKINDX_IMAGES_ALLOW))
			die('WIKINDX_IMAGES_ALLOW must be a boolean (TRUE / FALSE).');

// Set image max size
		if (!property_exists($this->config, 'WIKINDX_IMAGES_MAXSIZE'))
			$this->config->WIKINDX_IMAGES_MAXSIZE = WIKINDX_IMAGES_MAXSIZE_DEFAULT;
		elseif(!is_int($this->config->WIKINDX_IMAGES_MAXSIZE))
			die('WIKINDX_IMAGES_MAXSIZE must be a positive integer (in MB).');

// Enable / disable Debug mode
		if (!property_exists($this->config, 'WIKINDX_DEBUG_ERRORS'))
			$this->config->WIKINDX_DEBUG_ERRORS = WIKINDX_DEBUG_ERRORS_DEFAULT;
		elseif(!is_bool($this->config->WIKINDX_DEBUG_ERRORS))
			die('WIKINDX_DEBUG_ERRORS must be a boolean (TRUE / FALSE).');
// Enable / disable email on Debug mode errors
		if (!property_exists($this->config, 'WIKINDX_DEBUG_EMAIL'))
			$this->config->WIKINDX_DEBUG_EMAIL = WIKINDX_DEBUG_EMAIL_DEFAULT;
		elseif(!is_bool($this->config->WIKINDX_DEBUG_EMAIL))
			die('WIKINDX_DEBUG_EMAIL must be a boolean (TRUE / FALSE).');

// Display / hide SQL code debug output
		if (!property_exists($this->config, 'WIKINDX_DEBUG_SQL'))
			$this->config->WIKINDX_DEBUG_SQL = WIKINDX_DEBUG_SQL_DEFAULT;
		elseif(!is_bool($this->config->WIKINDX_DEBUG_SQL))
			die('WIKINDX_DEBUG_SQL must be a boolean (TRUE / FALSE).');

// Enable / disable Smarty template compilation cache
		if (!property_exists($this->config, 'WIKINDX_BYPASS_SMARTYCOMPILE'))
			$this->config->WIKINDX_BYPASS_SMARTYCOMPILE = WIKINDX_BYPASS_SMARTYCOMPILE_DEFAULT;
		elseif(!is_bool($this->config->WIKINDX_BYPASS_SMARTYCOMPILE))
			die('WIKINDX_BYPASS_SMARTYCOMPILE must be a boolean (TRUE / FALSE).');

// Display statistics to read only users
		if (!property_exists($this->config, 'WIKINDX_DISPLAY_STATISTICS'))
			$this->config->WIKINDX_DISPLAY_STATISTICS = WIKINDX_DISPLAY_STATISTICS_DEFAULT;
		elseif(!is_bool($this->config->WIKINDX_DISPLAY_STATISTICS))
			die('WIKINDX_DISPLAY_STATISTICS must be a boolean (TRUE / FALSE).');

// Display user statistics to read only users
		if (!property_exists($this->config, 'WIKINDX_DISPLAY_USER_STATISTICS'))
			$this->config->WIKINDX_DISPLAY_USER_STATISTICS = WIKINDX_DISPLAY_USER_STATISTICS_DEFAULT;
		elseif(!is_bool($this->config->WIKINDX_DISPLAY_USER_STATISTICS))
			die('WIKINDX_DISPLAY_USER_STATISTICS must be a boolean (TRUE / FALSE).');
	}
	public function configureErrorReporting()
	{
		if($this->config->WIKINDX_DEBUG_ERRORS)
		{
			error_reporting(E_ALL);
			ini_set('display_errors', 'On');
		}
		else
		{
			// Disable all errors reports
			error_reporting(0);
			ini_set('display_errors', 'Off');
		}
	}
}
