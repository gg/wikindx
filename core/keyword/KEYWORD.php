<?php
/**
 * WIKINDX : Bibliographic Management system.
 * @link https://wikindx.sourceforge.io/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2017-2019 Mark Grimshaw-Aagaard <sirfragalot@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 */

/**
* KEYWORD common routines
*
* @version	1
*
*	@package wikindx\core\keyword
*	@author Mark Grimshaw-Aagaard <sirfragalot@users.sourceforge.net>
*
*/
class KEYWORD
{
/** object */
private $db;
/** object */
/** object */
private $messages;
/** object */
private $commonBib;

/**
*	KEYWORD class
*/
	public function __construct()
	{
		$this->db = FACTORY_DB::getInstance();

		$this->messages = FACTORY_MESSAGES::getInstance();
		$this->commonBib = FACTORY_BIBLIOGRAPHYCOMMON::getInstance();
	}
/**
* Get keyword names from WKX_keyword.
*
* @param boolean $userBib FALSE (default) or TRUE (return only resource types within current user bibliography)
* @param mixed $kType FALSE (default) or metadata type or metadata array ('quote', 'paraphrase' etc). Gather only keywords attached to metadata
* @param $typeArray FALSE (default) or limits keywords to those belonging to these types (only when table == 'resource_keyword')
* @return array id => keyword or FALSE
*/
	public function grabAll($userBib = FALSE, $kType = FALSE, $typeArray = FALSE)
	{
		$subQuery = FALSE;
		if(is_array($kType) && !empty($kType))
		{
			$unions = array();
			foreach($kType as $mType)
			{
				if(($mType == 'quote') || ($mType == 'quoteComment'))
				{
					$this->db->formatConditions(array('resourcekeywordMetadataId' => ' IS NOT NULL'));
					$this->db->formatConditions(array('resourcemetadataType' => 'q'));
					$this->db->leftJoin('resource_metadata', 'resourcemetadataId', 'resourcekeywordMetadataId');
					$unions[] = $this->db->selectNoExecute('resource_keyword', array(array('resourcekeywordKeywordId' => 'rkId')),
						TRUE, TRUE, TRUE);
				}
				else if(($mType == 'paraphrase') || ($mType == 'paraphraseComment'))
				{
					$this->db->formatConditions(array('resourcekeywordMetadataId' => ' IS NOT NULL'));
					$this->db->formatConditions(array('resourcemetadataType' => 'p'));
					$this->db->leftJoin('resource_metadata', 'resourcemetadataId', 'resourcekeywordMetadataId');
					$unions[] = $this->db->selectNoExecute('resource_keyword', array(array('resourcekeywordKeywordId' => 'rkId')),
						TRUE, TRUE, TRUE);
				}
				else if($mType == 'musing')
				{
					$this->db->formatConditions(array('resourcekeywordMetadataId' => ' IS NOT NULL'));
					$this->db->formatConditions(array('resourcemetadataType' => 'm'));
					$this->db->leftJoin('resource_metadata', 'resourcemetadataId', 'resourcekeywordMetadataId');
					$unions[] = $this->db->selectNoExecute('resource_keyword', array(array('resourcekeywordKeywordId' => 'rkId')),
						TRUE, TRUE, TRUE);
				}
				else if($mType == 'idea')
				{
					$this->db->formatConditions(array('resourcekeywordMetadataId' => ' IS NOT NULL'));
					$this->db->formatConditions(array('resourcemetadataType' => 'i'));
					$this->db->leftJoin('resource_metadata', 'resourcemetadataId', 'resourcekeywordMetadataId');
					$unions[] = $this->db->selectNoExecute('resource_keyword', array(array('resourcekeywordKeywordId' => 'rkId')),
						TRUE, TRUE, TRUE);
				}
			}
			if(!empty($unions))
				$subQuery = $this->db->subQuery($this->db->union($unions), 't');
			$id = 'resourcekeywordResourceId';
		}
		else if(!is_array($kType))
		{
			if($kType == 'quote')
			{
				if(!$userBib && is_array($keywords = $this->db->readCache('cacheQuoteKeywords')))
					return $keywords;
				$this->db->formatConditions($this->db->formatFields('resourcekeywordQuoteId') . ' IS NOT NULL');
				$id = 'resourceId';
			}
			else if($kType == 'paraphrase')
			{
				if(!$userBib && is_array($keywords = $this->db->readCache('cacheParaphraseKeywords')))
					return $keywords;
				$this->db->formatConditions($this->db->formatFields('resourcekeywordParaphraseId') . ' IS NOT NULL');
				$id = 'resourceId';
			}
			else if($kType == 'musing')
			{
				if(!$userBib && is_array($keywords = $this->db->readCache('cacheMusingKeywords')))
					return $keywords;
				$this->db->formatConditions($this->db->formatFields('resourcekeywordMusingId') . ' IS NOT NULL');
				$id = 'resourceId';
			}
			else if($kType == 'resource')
			{
				if(!$userBib && !is_array($typeArray) && is_array($keywords = $this->db->readCache('cacheResourceKeywords')))
					return $keywords;
				$this->db->formatConditions($this->db->formatFields('resourcekeywordResourceId') . ' IS NOT NULL');
				$id = 'resourcekeywordResourceId';
			}
			else // all keywords
			{
				if(!$userBib && !is_array($typeArray) && is_array($keywords = $this->db->readCache('cacheKeywords')))
					return $keywords;
				$id = 'resourcekeywordResourceId';
			}
		}
		else // all keywords
		{
			if(!$userBib && !is_array($typeArray) && is_array($keywords = $this->db->readCache('cacheKeywords')))
				return $keywords;
			$id = 'resourcekeywordResourceId';
		}
		if(is_array($typeArray) && !empty($typeArray) && $kType == 'resource')
			$this->db->formatConditionsOneField($typeArray, 'resourceType');
		if(!is_array($kType) && $kType)
		{
			$this->db->leftJoin('resource_keyword', 'resourcekeywordKeywordId', 'keywordId');
			$this->db->leftJoin('resource', 'resourceId', 'resourcekeywordResourceId');
		}
		$this->db->orderBy('keywordKeyword');
		if($subQuery)
		{
			$this->db->leftJoin('keyword', 'keywordId', 'rkId');
			$this->db->leftJoin('resource_keyword', 'resourcekeywordKeywordId', 'keywordId');
			if($userBib)
				$this->commonBib->userBibCondition($id);
			$recordset = $this->db->selectFromSubQuery(FALSE, array('keywordId', 'keywordKeyword', 'rkId'), $subQuery);
		}
		else
		{
			if($userBib)
				$this->commonBib->userBibCondition($id);
			$recordset = $this->db->select('keyword', array('keywordId', 'keywordKeyword'), TRUE);
		}
		while($row = $this->db->fetchRow($recordset))
			$keywords[$row['keywordId']] = \HTML\dbToFormTidy($row['keywordKeyword']);
		if(isset($keywords))
		{
// (re)create cache if listing for all types and no user bibliography
			if(!$userBib && !is_array($typeArray) && !is_array($kType))
			{
				if($kType == 'quote')
					$this->db->writeCache('cacheQuoteKeywords', $keywords);
				else if($kType == 'paraphrase')
					$this->db->writeCache('cacheParaphraseKeywords', $keywords);
				else if($kType == 'musing')
					$this->db->writeCache('cacheMusingKeywords', $keywords);
				else if($kType == 'resource')
					$this->db->writeCache('cacheResourceKeywords', $keywords);
				else
					$this->db->writeCache('cacheKeywords', $keywords);
			}
			return $keywords;
		}
		return FALSE;
	}
/**
* Does this keyword already exist in the database?
*
* @param string $keyword
return mixed Existing keyword ID of FALSE
*/
	public function checkExists($keyword)
	{
		$this->db->formatConditions($this->db->formatFields('keywordKeyword') . $this->db->like(FALSE, $keyword, FALSE));
		$resultset = $this->db->select('keyword', 'keywordId');
		if($this->db->numRows($resultset))
			return $this->db->fetchOne($resultset);
		return FALSE; // not found
	}
/**
* Remove resource-less keywords from keyword list
*/
	public function removeHanging()
	{
		$deleteIds = array();
		$this->db->formatConditions(array('resourcekeywordKeywordId' => ' IS NOT NULL'));
		$subStmt = $this->db->subQuery($this->db->selectNoExecute('resource_keyword', 'resourcekeywordKeywordId'), FALSE, FALSE, TRUE);
		$this->db->formatConditions($this->db->formatFields('keywordId'). $this->db->inClause($subStmt, TRUE));
		$recordset = $this->db->select('keyword', 'keywordId');
		while($row = $this->db->fetchRow($recordset))
			$deleteIds[] = $row['keywordId'];
		if(empty($deleteIds))
			return; // nothing to do
		if(!empty($deleteIds))
		{
			$this->db->formatConditionsOneField($deleteIds, 'keywordId');
			$this->db->delete("keyword");
// remove cache files for keywords
			$this->db->deleteCache('cacheResourceKeywords');
			$this->db->deleteCache('cacheMetadataKeywords');
			$this->db->deleteCache('cacheQuoteKeywords');
			$this->db->deleteCache('cacheParaphraseKeywords');
			$this->db->deleteCache('cacheMusingKeywords');
			$this->db->deleteCache('cacheKeywords');
		}
	}
/**
* Write names to keyword table and return keyword id array ready for adding to resource_keyword.resourcekeywordKeywordId fields.
*
* @param array $inputArray
* @param mixed $id Default is FALSE
* @todo Check if $id is actually needed at all
* @return mixed FALSE or array of keyword IDs
*/
	public function writeKeywordTable($inputArray, $id = FALSE)
	{
		$fields[] = "keywordKeyword";
		$addedKeyword = FALSE;
		$ids = $keywords = array();
		if(array_key_exists('keywords', $inputArray))
			$keywords = UTF8::mb_explode(',', $inputArray['keywords']);
		foreach($keywords as $keyword)
		{
			if(!$keyword = trim($keyword))
				continue;
			$values[0] = $keyword;
			if($id = $this->checkExists($keyword))
			{
				$ids[] = $id;
				foreach($fields as $field)
					$updateArray[$field] = array_shift($values);
				$this->db->formatConditions(array('keywordId' => $id));
				$this->db->update('keyword', $updateArray);
			}
// given keyword doesn't exist so now write to db
			else
			{
				$this->db->insert('keyword', $fields, $values);
				$ids[] = $this->db->lastAutoId();
				$addedKeyword = TRUE;
			}
		}
// merge two arrays to remove duplicate ids.
		if(array_key_exists('keyword_ids', $inputArray) && $inputArray['keyword_ids'])
			$ids = array_unique(array_merge($ids, UTF8::mb_explode(',', $inputArray['keyword_ids'])));
		if(empty($ids))
			return FALSE;
		if($addedKeyword)
		{
// remove cache files for keywords
			$this->db->deleteCache('cacheresourceKeywords');
			$this->db->deleteCache('cachemetadataKeywords');
			$this->db->deleteCache('cachequoteKeywords');
			$this->db->deleteCache('cacheparaphraseKeywords');
			$this->db->deleteCache('cachemusingKeywords');
			$this->db->deleteCache('cacheKeywords');
		}
// In case array key 0 ('IGNORE') is there, remove it
		if(array_search(0, $ids) === 0)
			unset($ids[0]);
		return $ids;
	}
/**
* Make the keyword select transfer arrows to transfer keywords between select boxes with onclick
*
* @return array (toRightImage, toLeftImage)
*/
	public function transferArrows()
	{

		$jsonArray = array();
		$jsonArray[] = array(
			'startFunction' => 'selectKeyword',
			);
		$toRightImage = \AJAX\jActionIcon('toRight', 'onclick', $jsonArray);
		$jsonArray = array();
		$jsonArray[] = array(
			'startFunction' => 'discardKeyword',
			);
		$toLeftImage = \AJAX\jActionIcon('toLeft', 'onclick', $jsonArray);
		return array($toRightImage, $toLeftImage);
	}
}
