<?php
/**
 * WIKINDX : Bibliographic Management system.
 * @link https://wikindx.sourceforge.io/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2018 Mark Grimshaw-Aagaard <sirfragalot@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 */

/**
* Load the WIKINDX display template system
*
* @version	1
*
*	@package wikindx\core\display
*	@author Mark Grimshaw-Aagaard <sirfragalot@users.sourceforge.net>
*
*/

class TEMPLATE
{
const DEFAULT_NAME = WIKINDX_TEMPLATE_DEFAULT;
const ROOT_DIR = 'templates';
const COMPILE_DIR = 'tplcompilation';
const CACHE_DIR = 'tplcache';

/** object */
public $tpl;
/** object */
private $config;
/** object */
private $errors;
/** object */
private $session;
/** string */
private $name;
/** string */
private $path;
/** array */
private $tplList = FALSE;

/**
* TEMPLATE
*/
	public function __construct()
	{
		include_once("core/display/Smarty/libs/Smarty.class.php");
		$this->tpl = new Smarty();

		// We never execute untrusted code, so we can disable this feature of Smarty
		$this->tpl->disableSecurity();
	}

/**
* Return the directory of the loaded template
*/
	public function getDirectory()
	{
		return $this->path;
	}

/**
* Clear all caches of Smarty
*/
	public function clearAllCache()
	{
		$this->clearCache();
		$this->clearCompileCache();
	}

/**
* Clear the entire template cache of Smarty
*/
	public function clearCache()
	{
		$this->tpl->clearAllCache();
	}

/**
* Clear all compilation directories of Smarty
*/
	public function clearCompileCache()
	{
		$this->tpl->clearCompiledTemplate();

		foreach (FILE\fileInDirToArray(self::COMPILE_DIR) as $ftpl)
		{
            @unlink(self::COMPILE_DIR . DIRECTORY_SEPARATOR . $ftpl);
		}
	}


/**
* Clear all compilation directories of Smarty
*
* @param string $dir
*/
	private function createDirectory($dir)
	{
		$this->errors = FACTORY_ERRORS::getInstance();

		if (!is_dir($dir)) {
			if (!file_exists($dir)) {
				if (!mkdir($dir)) {
					print $this->errors->text("file", "folder") . " " . $dir;
				}
			}
		}
	}

/**
* Called from CLOSE, this sets up the right template to use before the script is exited
*/
	public function loadTemplate()
	{
		$this->session = FACTORY_SESSION::getInstance();
		$co = FACTORY_CONFIGDBSTRUCTURE::getInstance();
		$tplArray = $this->loadDir();
		$this->name = $this->session->getVar("setup_Template");
		// Special case: during installation there is no template sets.
		if(!is_string($this->name))
			$this->name = self::DEFAULT_NAME;

		if (!array_key_exists($this->name, $tplArray)) {

			// At first install of a blank database
			if(!$this->name)
				$this->name = self::DEFAULT_NAME;

			if (!array_key_exists($this->name, $tplArray)) {
				$this->name = self::DEFAULT_NAME;
			}

			if(!$this->session->getVar('setup_ReadOnly'))
				$this->session->setVar("setup_Template", $this->name);
		}

		// template may have been disabled by admin
		if(!is_file(self::ROOT_DIR . DIRECTORY_SEPARATOR . $this->name . DIRECTORY_SEPARATOR . 'description.txt'))
		    $this->name = $co->getOne('configTemplate');

		// Configure (main) template path of Smarty
		// with the name of the template instead of using indice 0
		$this->path = self::ROOT_DIR . DIRECTORY_SEPARATOR . $this->name;
		$this->tpl->setTemplateDir(array($this->name => $this->path));

		// Register "menu" function of SmartyMenu plugin
		$this->tpl->registerDefaultPluginHandler(array($this, 'plugin_SmartyMenu_handler'));

		// Configure compilation options of Smarty
		$this->tpl->setCompileDir(self::COMPILE_DIR);
		$this->createDirectory(self::COMPILE_DIR);

		$this->tpl->setForceCompile($co->getOne('configBypassSmartyCompile'));

		// Configure cache options of Smarty
		// We use dynamic pages so certainly don't want caching!
		// However it is not impossible to cache the menu that now we have features to clear the cache.
		$this->tpl->setCacheDir(self::CACHE_DIR);
		// $this->createDirectory(self::CACHE_DIR); // Don't create it for the moment
		$this->tpl->setCaching(FALSE);

		if($fh = fopen($this->path . DIRECTORY_SEPARATOR . 'description.txt', "r"))
		{
			$index = 0;
			while(($line = fgets($fh)) !== FALSE)
			{
				if(!$index) // discard description line
				{
					++$index;
					continue;
				}
				else if($index == 1) // reduceMenuLevel
				{
					if($line == '1')
					{
						if($this->session->issetVar('setup_TemplateMenu'))
							$this->session->setVar('setup_ReduceMenuLevel', $this->session->getVar('setup_TemplateMenu'));
						else
							$this->session->setVar('setup_ReduceMenuLevel', 1);
					}
					else if($line == '1$') // the menu style cannot be overridden by the user
						$this->session->setVar('setup_ReduceMenuLevel', 1);
					else if($line == '2')
					{
						if($this->session->issetVar('setup_TemplateMenu'))
							$this->session->setVar('setup_ReduceMenuLevel', $this->session->getVar('setup_TemplateMenu'));
						else
							$this->session->setVar('setup_ReduceMenuLevel', 2);
					}
					else if($line == '2$') // the menu style cannot be overridden by the user
						$this->session->setVar('setup_ReduceMenuLevel', 2);
					else if($line == '0')
					{
						if($this->session->issetVar('setup_TemplateMenu'))
							$this->session->setVar('setup_ReduceMenuLevel', $this->session->getVar('setup_TemplateMenu'));
						else
							$this->session->setVar('setup_ReduceMenuLevel', FALSE);
					}
					else if($line == '0$') // the menu style cannot be overridden by the user
						$this->session->setVar('setup_ReduceMenuLevel', FALSE);
				}
				else if($index == 2) // pre text for menu items that were in a subSubmenu -- only valid for reduceMenuLevel == 1
					$this->session->setVar('setup_ReduceMenuLevelPretext', $line);
				++$index;
			}

			if($index == 1) // only one line to be read
			{
				if($this->session->issetVar('setup_TemplateMenu'))
					$this->session->setVar('setup_ReduceMenuLevel', $this->session->getVar('setup_TemplateMenu'));
				else
					$this->session->setVar('setup_ReduceMenuLevel', FALSE);
			}

			fclose($fh);
		}
	}

/**
* read WIKINDX_DIR_TEMPLATES directory for template preferences and check we have a sane environment
*
* @return array
*/

	public function loadDir()
	{
		// Use an internal array to "memoize" the list of installed templates
		// This function is called multiple times while its result is almost static but expensive due to disk access.
		if ($this->tplList === FALSE)
		{
			$array = array();

		    foreach (FILE\dirInDirToArray(self::ROOT_DIR) as $dir)
			{
				if(file_exists(self::ROOT_DIR . DIRECTORY_SEPARATOR . $dir . DIRECTORY_SEPARATOR . 'display.tpl'))
				{
					if(file_exists(self::ROOT_DIR . DIRECTORY_SEPARATOR . $dir . DIRECTORY_SEPARATOR . 'description.txt'))
					{
						// read one line
						$fh = fopen(self::ROOT_DIR . DIRECTORY_SEPARATOR . $dir . DIRECTORY_SEPARATOR . 'description.txt', 'r');
						if($string = fgets($fh)) $array[$dir] = $string;
						fclose($fh);
					}
				}
				// sort alphabetically on the key
				ksort($array);
			}
			$this->tplList = $array;
		}
		return $this->tplList;
	}

/**
* SmartyMenu Plugin Handler
*
* called when Smarty encounters an undefined tag during compilation
*
* @param string $name name of the undefined tag
* @param string $type tag type (e.g. Smarty::PLUGIN_FUNCTION, Smarty::PLUGIN_BLOCK, Smarty::PLUGIN_COMPILER, Smarty::PLUGIN_MODIFIER, Smarty::PLUGIN_MODIFIERCOMPILER)
* @param Smarty_Internal_Template $template template object
* @param string &$callback returned function name
* @param string &$script optional returned script filepath if function is external
* @param boolean &$cacheable true by default, set to false if plugin is not cachable (Smarty >= 3.1.8)
* @return boolean true if successful
*/
	public function plugin_SmartyMenu_handler($name, $type, $template, &$callback, &$script, &$cacheable)
	{
		switch ($type) {
			case Smarty::PLUGIN_FUNCTION:
				switch ($name) {
					case 'menu':
						$script = 'core/display/Smarty/SmartyMenu/function.menu.php';
						$callback = 'smarty_function_menu';
						return true;
				}
			case Smarty::PLUGIN_COMPILER:
				switch ($name) {
					case 'menu':
					$script = 'core/display/Smarty/SmartyMenu/function.menu.php';
					$callback = 'smarty_function_menu';
					return true;
				default:
					return false;
				}
			case Smarty::PLUGIN_BLOCK:
				switch ($name) {
					case 'menu':
					$script = 'core/display/Smarty/SmartyMenu/function.menu.php';
					$callback = 'smarty_function_menu';
					return true;
				default:
					return false;
				}
			default:
				return false;
		}
	}
}
