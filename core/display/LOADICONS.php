<?php
/**
 * WIKINDX : Bibliographic Management system.
 * @link https://wikindx.sourceforge.io/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2017-2019 Mark Grimshaw-Aagaard <sirfragalot@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 */

/**
* Load image icons used throughout WIKINDX
*
* @version	2
*
*	@package wikindx\core\display
*	@author Mark Grimshaw-Aagaard <sirfragalot@users.sourceforge.net>
*
*/
class LOADICONS
{
/** string */
private $templateDir;
/** string */
public $view;
/** string */
public $viewAttach;
/** string */
public $viewmeta;
/** string */
public $viewmetaAttach;
/** string */
public $edit;
/** string */
public $delete;
/** string */
public $bibtex;
/** string */
public $next;
/** string */
public $previous;
/** string */
public $cite;
/** string */
public $add;
/** string */
public $remove;
/** string */
public $toLeft;
/** string */
public $toRight;
/** string */
public $toBottom;
/** string */
public $toTop;
/** string */
public $quarantine;
/** string */
public $file;
/** string */
public $help;
/** string */
public $basketAdd;
/** string */
public $basketRemove;
/** string */
public $basketAddLink;
/** string */
public $basketRemoveLink;
/** string */
public $viewLink;
/** string */
public $addLink;
/** string */
public $removeLink;
/** string */
public $toLeftLink;
/** string */
public $toRightLink;
/** string */
public $toBottomLink;
/** string */
public $toTopLink;
/** string */
public $editLink;
/** string */
public $bibtexLink;
/** string */
public $deleleteLink;
/** string */
public $quarantineLink;
/** string */
public $helpLink;
/** array */
public $jscriptActions = array();
/** object */
private $messages;
/** boolean */
private $setupDone = FALSE;
/** boolean */
private $dirIcons = FALSE;

/**
* LOADICONS
*/
	public function __construct()
	{
		$session = FACTORY_SESSION::getInstance();
		if(!$this->templateDir = $session->getVar("setup_Template"))
		    $this->templateDir = "default";
		$this->messages = FACTORY_MESSAGES::getInstance();
// Sometimes a directory may have been removed but that language is still in the session or database preferences
// ... fall back to default and hope it's still there ;)
		if(is_dir(WIKINDX_DIR_TEMPLATES . DIRECTORY_SEPARATOR . $this->templateDir . "/icons"))
			$this->dirIcons = WIKINDX_DIR_TEMPLATES . DIRECTORY_SEPARATOR . $this->templateDir . "/icons";
		elseif(is_dir(WIKINDX_DIR_TEMPLATES . DIRECTORY_SEPARATOR . "default/icons"))
			$this->dirIcons = WIKINDX_DIR_TEMPLATES . DIRECTORY_SEPARATOR . "default/icons";
		else
			$this->dirIcons = FALSE;

		$this->dirIconsDefault = WIKINDX_DIR_TEMPLATES . DIRECTORY_SEPARATOR . "default/icons";

		$this->next = $this->messages->text("resources", "next");
		$this->previous = $this->messages->text("resources", "previous");
		$this->cite = $this->messages->text("cite", 'cite');
		$this->add = $this->messages->text("misc", "add");
		$this->edit = $this->messages->text("misc", "edit");
		$this->delete = $this->messages->text("misc", "delete");
		$this->bibtex = $this->messages->text("misc", "bibtex");
		$this->view = $this->messages->text("misc", "view");
		$this->viewmeta = $this->messages->text("misc", "view");
		$this->viewAttach = $this->messages->text("misc", "view");
		$this->viewmetaAttach = $this->messages->text("misc", "view");
		$this->remove = $this->messages->text("misc", "remove");
		$this->quarantine = $this->messages->text("misc", "quarantine");
		$this->help = $this->messages->text("misc", "help");
		$this->file = $this->messages->text("misc", "download");
		$this->basketAdd = $this->messages->text("resources", "basketAdd");
		$this->basketRemove = $this->messages->text("resources", "basketRemove");
	}
/**
* Get icon info
*
* @param string $input
*/
	private function getIconInfo($input)
	{
		// check we have both icons directory and icons themselves
		$exists = $input . "Exists";
		$this->{$exists} = FALSE;

		if($this->dirIcons !== FALSE)
		{
			$extensionFile = '';
			if(is_file($this->dirIcons . "/$input.png"))
				$extensionFile = 'png';
			elseif(is_file($this->dirIcons . "/$input.jpg"))
				$extensionFile = 'jpg';
			elseif(is_file($this->dirIcons . "/$input.jpeg"))
				$extensionFile = 'jpeg';
			elseif(is_file($this->dirIcons . "/$input.gif"))
				$extensionFile = 'gif';
			elseif(is_file($this->dirIcons . "/$input.svg"))
				$extensionFile = 'svg';
			elseif(is_file($this->dirIconsDefault . "/$input.png"))
				$icon = $this->dirIconsDefault . "/$input.png";
			elseif(is_file($this->dirIconsDefault . "/$input.jpg"))
				$icon = $this->dirIconsDefault . "/$input.jpg";
			elseif(is_file($this->dirIconsDefault . "/$input.jpeg"))
				$icon = $this->dirIconsDefault . "/$input.jpeg";
			elseif(is_file($this->dirIconsDefault . "/$input.gif"))
				$icon = $this->dirIconsDefault . "/$input.gif";
			elseif(is_file($this->dirIconsDefault . "/$input.svg"))
				$icon = $this->dirIconsDefault . "/$input.svg";

			if($extensionFile != '')
			{
				// get image size data
				$size = GetImageSize($this->dirIcons . "/$input." . $extensionFile);
				if(empty($size))
				{
				    $size[0] = "16";
				    $size[1] = "16";
				}

				if(!empty($this->jscriptActions) && array_key_exists($input, $this->jscriptActions))
					$js = $this->jscriptActions[$input];
				else
				    $js = '';

				$this->$input = \HTML\img(
				    $this->dirIcons . "/$input." . $extensionFile,
				    $size[0],
				    $size[1],
				    $this->$input,
				    '',
				    $js
				);
				$this->{$exists} = TRUE;
				$this->jscriptActions = array(); // reset immediately
			}
		}
	}
/**
* Set up icons for display.  If $input is given run only that icon setup.
*
* @param string $input Default is FALSE
*/
	public function setupIcons($input = FALSE)
	{
		if($this->setupDone)
			return;

		if($input)
		{
			// NB: This notation isn't a syntaxic error!
			$this->$input = $this->messages->text("misc", $input);
			$this->getIconInfo($input);
			$exists = $input . 'Exists';
			$link = $input . 'Link';
			$this->{$link} = $this->{$exists} ? "imgLink" : "link";

			return;
		}

		$this->getIconInfo('view');
		$this->getIconInfo('cite');
		$this->getIconInfo('add');
		$this->getIconInfo('remove');
		$this->getIconInfo('viewmeta');
		$this->getIconInfo('viewAttach');
		$this->getIconInfo('viewmetaAttach');
		$this->getIconInfo('edit');
		$this->getIconInfo('bibtex');
		$this->getIconInfo('delete');
		$this->getIconInfo('next');
		$this->getIconInfo('previous');
		$this->getIconInfo('file');
		$this->getIconInfo('toLeft');
		$this->getIconInfo('toRight');
		$this->getIconInfo('toBottom');
		$this->getIconInfo('toTop');
		$this->getIconInfo('quarantine');
		$this->getIconInfo('help');
		$this->getIconInfo('basketAdd');
		$this->getIconInfo('basketRemove');

		$this->viewLink = $this->viewExists ? "imgLink" : "link";
		$this->citeLink = $this->citeExists ? "imgLink" : "link";
		$this->addLink = $this->addExists ? "imgLink" : "link";
		$this->removeLink = $this->removeExists ? "imgLink" : "link";
		$this->editLink = $this->editExists ? "imgLink" : "link";
		$this->deleteLink = $this->editExists ? "imgLink" : "link";
		$this->bibtexLink = $this->bibtexExists ? "imgLink" : "link";
		$this->nextLink = $this->nextExists ? "imgLink" : "link";
		$this->previousLink = $this->previousExists ? "imgLink" : "link";
		$this->fileLink = $this->fileExists ? "imgLink" : "link";
		$this->toLeftLink = $this->toLeftExists ? "imgLink" : "link";
		$this->toRightLink = $this->toRightExists ? "imgLink" : "link";
		$this->toBottomLink = $this->toBottomExists ? "imgLink" : "link";
		$this->toTopLink = $this->toTopExists ? "imgLink" : "link";
		$this->helpLink = $this->helpExists ? "imgLink" : "link";
		$this->basketAddLink = $this->addExists ? "imgLink" : "link";
		$this->basketRemoveLink = $this->removeExists ? "imgLink" : "link";

		$this->setupDone = TRUE;
	}
/**
* Reset the icon initialisation routines
*/
	public function resetSetup()
	{
		$this->setupDone = FALSE;
	}

/**
* Return the HTML code of an image for the file type of the file name in argument
*
* @param string $file Path to an icon file
*/
	public function getIconForAFileExtension($file)
	{
		$ext = strtolower($file);
	    if(!array_key_exists('extension', pathinfo($ext)))
	    	$ext = FALSE;
	    else
		    $ext = pathinfo($ext)['extension'];
	    if(!$ext)
	    {
	    	if(is_file($this->dirIcons . "/file.png"))
	    		$icon = $this->dirIconsDefault . "/file.png";
	    	else
	    		$icon = "templates/default/icons/file.png";
	    }
		else if($this->dirIcons !== FALSE)
		{
			$input = "file_extension_$ext";
			if(is_file($this->dirIcons . "/$input.png"))
				$icon = $this->dirIcons . "/$input.png";
			elseif(is_file($this->dirIcons . "/$input.jpg"))
				$icon = $this->dirIcons . "/$input.jpg";
			elseif(is_file($this->dirIcons . "/$input.jpeg"))
				$icon = $this->dirIcons . "/$input.jpeg";
			elseif(is_file($this->dirIcons . "/$input.gif"))
				$icon = $this->dirIcons . "/$input.gif";
			elseif(is_file($this->dirIcons . "/$input.svg"))
				$icon = $this->dirIcons . "/$input.svg";
			elseif(is_file($this->dirIconsDefault . "/$input.png"))
				$icon = $this->dirIconsDefault . "/$input.png";
			elseif(is_file($this->dirIconsDefault . "/$input.jpg"))
				$icon = $this->dirIconsDefault . "/$input.jpg";
			elseif(is_file($this->dirIconsDefault . "/$input.jpeg"))
				$icon = $this->dirIconsDefault . "/$input.jpeg";
			elseif(is_file($this->dirIconsDefault . "/$input.gif"))
				$icon = $this->dirIconsDefault . "/$input.gif";
			elseif(is_file($this->dirIconsDefault . "/$input.svg"))
				$icon = $this->dirIconsDefault . "/$input.svg";
			else
				$icon = "templates/default/icons/file.png";
		}
		else // default fallback
	    	$icon = "templates/default/icons/file.png";
		$size = GetImageSize($icon);
		if(empty($size))
		{
		    $size[0] = "16";
		    $size[1] = "16";
		}

	    return \HTML\img(
		    $icon,
		    $size[0],
		    $size[1],
		    $file,
		    '',
		    ''
		);
	}
}
