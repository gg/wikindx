<?php
/**
 * WIKINDX : Bibliographic Management system.
 * @link https://wikindx.sourceforge.io/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2019 Stéphane Aulery <lkppo@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 */

/**
* Miscellaneous UTILS functions
*
* @version	1
*
*	@package wikindx\core\utils
*	@author Stéphane Aulery <lkppo@users.sourceforge.net>
*
*/
namespace UTILS
{
/**
* Format dates and times for localization (e.g. when viewing lists of resources in a multi-user wikindx 'Last edited by: xxx <date/time>)
*
* TODO: this function is extracted form the CONSTANTS.php file used for localisation.
* The date format does not depend on the language but rather on the uses of each country and the time zone.
* That's why this function has been isolated. However, operation is still not correct for the time zone.
* The comments were kept intact for later correction.
*
* @param $timestamp comes in from the database in the format 'YYYY-MM-DD HH:MM:SS' e.g. 2013-01-31 15:54:55
* @return string
*/
	function dateFormat($timestamp)
	{
// If you're happy with $timestamp's current format, all you need to do is uncomment this line:
//		return $timestamp;
// Otherwise. . .
		$unixTime = strtotime($timestamp);
// You have two choices for setting the time.  The first attempts to translate day and month names but will not
// work if the chosen locale is not available on your system.
// See
// http://www.php.net/manual/en/function.setlocale.php
// and
// http://www.php.net/manual/en/function.strftime.php
//
// The second is locale independent
// See:
// http://www.php.net/manual/en/function.date.php
//
// For the first option, set $dateFormat = 1; otherwise set $dateFormat = 2;
		$dateFormat = 2;
		if($dateFormat == 1)
		{
			$format = "%c"; // e.g. Tue Feb 5 00:45:10 2009 for February 5, 2009 at 12:45:10 AM
			$timestamp = strftime($format, $unixTime);
		}
		else if($dateFormat == 2)
		{
			$format = "%x %X"; // e.g. 02/05/09 03:59:16 for February 5, 2009 15:59:16
			$timestamp = date($format, $unixTime);
		}
		else
		{
		    // Keep the timestamp unformated
		}
		return $timestamp;
	}

/**
* Return the list of localized languages defined in /languages/languages_localized.json
*
* Each entry of the returned array is a language code (ll) or a language and its country (ll_CC) as key and a language name and country name as value.
* If the source file is not readeable only the reference language is listed.
*
* @return array
*/
	function getLocalizedLanguagesList()
	{
		$fallbackLocList = [WIKINDX_LANGUAGE_DEFAULT => codeISO639a1toName(WIKINDX_LANGUAGE_DEFAULT)];
		$msgfallback = "<p>As long as this error will not be corrected, only the <strong>" . $fallbackLocList[WIKINDX_LANGUAGE_DEFAULT] . " (" . WIKINDX_LANGUAGE_DEFAULT . ")</strong> language will be available.</p>";
		
		$path_languages_localized = implode(DIRECTORY_SEPARATOR, [WIKINDX_DIR_LANGUAGES, "languages_localized.json"]);
		
		if (!file_exists($path_languages_localized))
		{
		    echo "<p>The <strong>$path_languages_localized</strong> file doesn't exist.</p>" . $msgfallback;
		    return $fallbackLocList;
		}
		
		if (!is_readable($path_languages_localized))
		{
		    echo "<p>The <strong>$path_languages_localized</strong> file must be readable.</p>" . $msgfallback;
		    return $fallbackLocList;
		}
		
		$content_languages_localized = file_get_contents($path_languages_localized);
		if ($content_languages_localized === FALSE)
		{
		    echo "<p>Reading the <strong>$path_languages_localized</strong> file returned an error.</p>" . $msgfallback;
		    return $fallbackLocList;
		}
		
		$LocList = @json_decode($content_languages_localized, TRUE);
		if (json_last_error() != JSON_ERROR_NONE)
		{
		    echo "<p>Parsing the <strong>$path_languages_localized</strong> file returned an error:</p><p>" . json_last_error_msg() . ".</p>" . $msgfallback;
		    return $fallbackLocList;
		}
		
		asort($LocList);
		
		return $LocList;
	}
	
/**
* Return the name of a language defined by its ISO 639-1 (alpha-2) code.
*
* @see https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes
*
* @param $codeISO string
* @return string
*/
	function codeISO639a1toName($codeISO)
	{
	    $fallbackName = "";
	    
	    $path_languages_all = implode(DIRECTORY_SEPARATOR, [WIKINDX_DIR_LANGUAGES, "languages_all.json"]);
		
		if (!file_exists($path_languages_all))
		{
		    echo "<p>The <strong>$path_languages_all</strong> file doesn't exist.</p>";
		    return $fallbackName;
		}
		
		if (!is_readable($path_languages_all))
		{
		    echo "<p>The <strong>$path_languages_all</strong> file must be readable.</p>";
		    return $fallbackName;
		}
		
		$content_languages_all = file_get_contents($path_languages_all);
		if ($content_languages_all === FALSE)
		{
		    echo "<p>Reading the <strong>$path_languages_all</strong> file returned an error.</p>";
		    return $fallbackName;
		}
		
		$LocList = @json_decode($content_languages_all, TRUE);
		if (json_last_error() != JSON_ERROR_NONE)
		{
		    echo "<p>Parsing the <strong>$path_languages_all</strong> file returned an error:</p><p>" . json_last_error_msg() . ".</p>";
		    return $fallbackName;
		}
	    
	    return array_key_exists($codeISO, $LocList) ? $LocList[$codeISO] : $fallbackName;
	}
/**
* Return the BCP 47 code that matches the ISO 639-1 (alpha-2) code of a language.
*
* The BCP 47 code is used in the lang attribute of any HTML tag.
* The list of supported languages is simple enough to avoid having to encounter
* any particular case. Just apply the correct separator between the language code
* and the region code to find a viable equivalent.
*
* @see https://www.w3.org/International/questions/qa-html-language-declarations
* @see https://www.w3.org/International/questions/qa-choosing-language-tags
* @see https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes
*
* @param $codeISO string
* @return string
*/
	function codeISO639a1toBCP47($codeISO)
	{
	    return str_replace("_", "-", $codeISO);
	}
/**
* Load the gettext catalogs of the user preferred language.
*
* @return string
*/
    function load_locales()
    {
        $locale = \UTILS\determine_locale();
        putenv("LANG=$locale");
        setlocale(LC_ALL, $locale . ".utf-8");
        
        // WARNING: NEVER move this directory one level up because a bug under windows prevents the loading of mo files in that case.
        // It's crazy, but many hours lost are waiting for you if you go in that direction!
        $dirlocales = realpath(implode(DIRECTORY_SEPARATOR, [__DIR__, "..", "..", "languages"]));
        
        // List all available domains for the requested locale
        $files = \FILE\fileInDirToArray(implode(DIRECTORY_SEPARATOR, [$dirlocales, $locale, "LC_MESSAGES"]));
        
        $loading = FALSE;
        foreach ($files as $dir => $file)
        {
            if (mb_strtolower(mb_substr($file, - mb_strlen(".mo"))) == ".mo")
            {
                $domain = mb_strtolower(mb_substr($file, 0, mb_strlen($file) - mb_strlen(".mo")));
                bindtextdomain($domain, $dirlocales);
                bind_textdomain_codeset($domain, WIKINDX_CHARSET);
                $loading = TRUE;
            }
        }
        
        // If the previous locale contains a state suffix (e.g. en_GB) and no MO files have being loaded, try without suffix
        if (!$loading && strpos($locale, "_") !== FALSE)
        {
            $locale = mb_substr($locale, 0, strpos($locale, "_"));
            
            // List all available domains for the requested locale
            $files = \FILE\fileInDirToArray(implode(DIRECTORY_SEPARATOR, [$dirlocales, $locale, "LC_MESSAGES"]));
            
            foreach ($files as $dir => $file)
            {
                if (mb_strtolower(mb_substr($file, - mb_strlen(".mo"))) == ".mo")
                {
                    $domain = mb_strtolower(mb_substr($file, 0, mb_strlen($file) - mb_strlen(".mo")));
                    bindtextdomain($domain, $dirlocales);
                    bind_textdomain_codeset($domain, WIKINDX_CHARSET);
                }
            }
        }
        
        textdomain(WIKINDX_LANGUAGE_DOMAIN_DEFAULT);
    }
/**
* Determine of the user preferred language.
*
* This function builds a language priority stack. The first is the highest priority.
*
* If $force_code_lang is passed, this function will try to use this language first.
* 
* $param $force_code_lang ISO 639-1 (alpha-2) code of a language
* @return string
*/
    function determine_locale($force_code_lang = NULL)
    {
        $langPriorityStack = [];
        
        // 1. Forcing a language for special case or debugging
        $langPriorityStack[] = $force_code_lang;
        
        // 2. The preferred language of the user ("auto" disappears in filtering to make room for browser language)
        $session = \FACTORY_SESSION::getInstance();
        $langPriorityStack[] = $session->getVar("setup_Language", "auto");
        
        // 3. The preferred language of the user's browser
        if (isset($_SERVER["HTTP_ACCEPT_LANGUAGE"]))
        {
            $langs = explode(",", $_SERVER["HTTP_ACCEPT_LANGUAGE"]);
            
            // Remove the priority number after ";" and use the locale syntax for the country extension
            array_walk($langs, function (&$lang) { $lang = strtr(strtok($lang, ";"), ["-" => "_"]); });
            
            $langPriorityStack = array_merge($langPriorityStack, $langs);
        }
        
        // 4. The fallback language
        $langPriorityStack[] = WIKINDX_LANGUAGE_DEFAULT;
        
        // Of all the languages of the stack only keeps those that are actually available.
        $langPriorityStack = array_intersect($langPriorityStack, array_keys(getLocalizedLanguagesList()));
        
        // Extract the top priority language
        // NB. The index is not always 0 because the array is not reordered after filtering. 
        assert(count($langPriorityStack) > 0);
        foreach($langPriorityStack as $lang) {
            return $lang;
        }
    }
}
