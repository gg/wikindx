<?php
/**
 * WIKINDX : Bibliographic Management system.
 * @link https://wikindx.sourceforge.io/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2017 Mark Grimshaw-Aagaard <sirfragalot@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 */

session_start();
if(isset($_SESSION) && array_key_exists('wikindxBasePath', $_SESSION) && $_SESSION['wikindxBasePath'])
	chdir($_SESSION['wikindxBasePath']); // tinyMCE changes the phpbasepath
else
{
	$oldPath = dirname(__FILE__);
	$split = preg_split('/' . preg_quote(DIRECTORY_SEPARATOR, '/') . '/u', $oldPath);
	array_splice($split, -4); // get back to trunk
	$newPath = join(DIRECTORY_SEPARATOR, $split);
	chdir($newPath);
}

/**
* Import initial configuration and initialize the web server
*/
include_once("core/startup/WEBSERVERCONFIG.php");

include_once("core/modules/cite/INSERTCITATION.php");

GLOBALS::addTplVar('scripts', '<link href="css/wikindxCite.css" rel="stylesheet" type="text/css">');

$cite = new INSERTCITATION();
$cite->init();
