<?php
/**
 * WIKINDX : Bibliographic Management system.
 * @link https://wikindx.sourceforge.io/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2017 Mark Grimshaw-Aagaard <sirfragalot@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 */

/**
* Collection mapping
*
*	Provides mappings between resource types and collection types.  Allows for displaying only certain
*	collections appropriate to the resource type when entering a new resource.
*
* @version	1
*
*	@package wikindx\core\collection
*	@author Mark Grimshaw-Aagaard <sirfragalot@users.sourceforge.net>
*
*/
class COLLECTIONMAP
{
/** array */
public $collectionTypes;

/**
* COLLECTIONMAP
*/
	public function __construct()
	{
		$this->loadMap();
	}
/**
* Load collection map arrays
*
* @author Mark Grimshaw-Aagaard
* @version 2
*/
	private function loadMap()
	{
		$this->collectionTypes = array(
				'book_article'			=>	'book',
				'book_chapter'			=>	'book',
				'journal_article'		=>	'journal',
				'newspaper_article'		=>	'newspaper',
				'magazine_article'		=>	'magazine',
				'web_article'			=>	'web',
				'web_encyclopedia_article'			=>	'web',
				'conference_paper'		=>	'proceedings',
				'conference_poster'		=>	'proceedings',
				'proceedings_article'		=>	'proceedings',
				'thesis'			=>	'thesis',
				'manuscript'			=>	'manuscript',
				'music_track'			=>	'music',
				'miscellaneous_section'			=>	'miscellaneous',
			);
		$this->book = array(
				'resource'	=>	array(
					'field1'	=>	'seriesTitle',
					'field2'	=>	'edition',
					'field3'	=>	'seriesNumber',
					'transTitle'	=>	'title',
					'transSubtitle'	=>	'subtitle',
				),
				'resource_misc'	=>	array(
					'publisher'	=>	'publisher',
					'miscField1'	=>	'transPublisher',
					'miscField4'	=>	'numberOfVolumes',
					),
				'resource_year'	=>	array(
					'year1'		=>	'publicationYear',
					'year2'		=>	'reprintYear',
					'year3'		=>	'volumeYear',
					'year4'		=>	'transPublicationYear',
				),
				'resource_creator' =>	array(
					'creator2'	=>	'editor',
					'creator3'	=>	'translator',
					'creator4'	=>	'reviser',
					'creator5'	=>	'seriesEditor',
				),
// In addition to the collectionType, title and shortTitle of the collection, all these fields must match in order for a new collection to be judged to be the same as an existing one
				'match' => array('field1', 'field2', 'field3', 'publisher', 'year1'),
// Minimum fields for display of collection in selection box in addition to title and titleshort
				'display' => array('field2', 'year1'),
			);
		$this->journal = array(
				'resource'	=>	array(
//					'field1'	=>	'journalVolumeNumber',
//					'field2'	=>	'journalIssueNumber',
				),
				'resource_misc'	=>	array(
//					'miscField3'	=>	'month',
//					'miscField6'	=>	'endMonth',
					),
				'resource_year'	=>	array(
//					'year1'		=>	'publicationYear',
//					'year3'		=>	'endPublicationYear',
				),
				'resource_creator' =>	array(
				),
// In addition to the collectionType, title and shortTitle of the collection, all these fields must match in order for a new collection to be judged to be the same as an existing one
//				'match' => array('field1', 'field2', 'year1'),
				'match' => array(),
// Minimum fields for display of collection in selection box in addition to title and titleshort
//				'display' => array('field1', 'field2', 'year1'),
				'display' => array(),
			);
		$this->newspaper = array(
				'resource'	=>	array(
					'field2'	=>	'city',
				),
				'resource_misc'	=>	array(
					),
				'resource_year'	=>	array(
				),
				'resource_creator' =>	array(
				),
// In addition to the collectionType, title and shortTitle of the collection, all these fields must match in order for a new collection to be judged to be the same as an existing one
				'match' => array('field2'),
// Minimum fields for display of collection in selection box in addition to title and titleshort
				'display' => array('field2'),
			);
		$this->magazine = array(
				'resource'	=>	array(
				),
				'resource_misc'	=>	array(
					),
				'resource_year'	=>	array(
				),
				'resource_creator' =>	array(
				),
// In addition to the collectionType, title and shortTitle of the collection, all these fields must match in order for a new collection to be judged to be the same as an existing one
				'match' => array(), // empty array required
// Minimum fields for display of collection in selection box in addition to title and titleshort
				'display' => array(), // empty array required
			);
		$this->proceedings = array(
				'resource'	=>	array(
					'field1'	=>	'seriesTitle',
				),
				'resource_misc'	=>	array(
					),
				'resource_year'	=>	array(
//					'year3'		=>	'conferenceEndYear',
				),
				'resource_creator' =>	array(
				),
// In addition to the collectionType, title and shortTitle of the collection, all these fields must match in order for a new collection to be judged to be the same as an existing one
				'match' => array('field1'),
// Minimum fields for display of collection in selection box in addition to title and titleshort
				'display' => array('field1'),
			);
		$this->web = array(
				'resource'	=>	array(
//					'field1'	=>	'journalVolumeNumber',
//					'field2'	=>	'journalIssueNumber',
					),
				'resource_misc'	=>	array(
					'publisher'	=>	'publisher',
					),
				'resource_year'	=>	array(
//					'year1'		=>	'publicationYear',
				),
				'resource_creator' =>	array(
				),
// In addition to the collectionType, title and shortTitle of the collection, all these fields must match in order for a new collection to be judged to be the same as an existing one
//				'match' => array('field1', 'field2', 'year1'),
				'match' => array(),
// Minimum fields for display of collection in selection box in addition to title and titleshort
//				'display' => array('field1', 'field2', 'year1'),
				'display' => array(),
			);
		$this->web_encyclopedia = array(
				'resource'	=>	array(
					),
				'resource_misc'	=>	array(
					'publisher'	=>	'publisher',
					),
				'resource_year'	=>	array(
//					'year1'		=>	'publicationYear',
				),
				'resource_creator' =>	array(
				),
// In addition to the collectionType, title and shortTitle of the collection, all these fields must match in order for a new collection to be judged to be the same as an existing one
				'match' => array(),
// Minimum fields for display of collection in selection box in addition to title and titleshort
				'display' => array(),
			);
		$this->thesis = array(
				'resource'	=>	array(
//					'field3'	=>	'journalVolumeNumber',
//					'field4'	=>	'journalIssueNumber',
				),
				'resource_misc'	=>	array(
					),
				'resource_year'	=>	array(
//					'year2'		=>	'publicationYear',
				),
				'resource_creator' =>	array(
				),
// In addition to the collectionType, title and shortTitle of the collection, all these fields must match in order for a new collection to be judged to be the same as an existing one
//				'match' => array('field3', 'field4', 'year2'),
				'match' => array(),
// Minimum fields for display of collection in selection box in addition to title and titleshort
//				'display' => array('field3', 'field4', 'year2'),
				'display' => array(),
			);
		$this->music = array(
				'resource'	=>	array(
					'field2'	=>	'medium',
				),
				'resource_misc'	=>	array(
					'publisher'	=>	'publisher',
					),
				'resource_year'	=>	array(
					'year1'		=>	'publicationYear',
				),
				'resource_creator' =>	array(
				),
// In addition to the collectionType, title and shortTitle of the collection, all these fields must match in order for a new collection to be judged to be the same as an existing one
				'match' => array('publisher', 'year1'),
// Minimum fields for display of collection in selection box in addition to title and titleshort
				'display' => array('year1'),
			);
		$this->manuscript = array(
				'resource'	=>	array(
				),
				'resource_misc'	=>	array(
					),
				'resource_year'	=>	array(
				),
				'resource_creator' =>	array(
				),
// In addition to the collectionType, title and shortTitle of the collection, all these fields must match in order for a new collection to be judged to be the same as an existing one
				'match' => array(), // empty array required
// Minimum fields for display of collection in selection box in addition to title and titleshort
				'display' => array(), // empty array required
			);
		$this->miscellaneous = array(
				'resource'	=>	array(
					'field2'	=>	'medium',
				),
				'resource_misc'	=>	array(
					'publisher'	=>	'publisher',
					),
				'resource_year'	=>	array(
				),
				'resource_creator' =>	array(
				),
// In addition to the collectionType, title and shortTitle of the collection, all these fields must match in order for a new collection to be judged to be the same as an existing one
				'match' => array(), // empty array required
// Minimum fields for display of collection in selection box in addition to title and titleshort
				'display' => array(), // empty array required
			);
	}
}
