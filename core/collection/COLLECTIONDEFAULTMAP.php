<?php
/**
 * WIKINDX : Bibliographic Management system.
 * @link https://wikindx.sourceforge.io/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2017 Mark Grimshaw-Aagaard <sirfragalot@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 */

/**
* Collection mapping for the collectionDefault column in the collections table
*
*	Sets default settings for the collection that are common to all resources of that collection.
*
* @version	1
*
*	@package wikindx\core\collection
*	@author Mark Grimshaw-Aagaard <sirfragalot@users.sourceforge.net>
*
*/
class COLLECTIONDEFAULTMAP
{
/** array */
//public $collectionTypes;

/**
* COLLECTIONDEFAULTMAP
*/
	public function __construct()
	{
		$this->loadMap();
	}
/**
* Load collection map arrays
*
* @author Mark Grimshaw-Aagaard
* @version 2
*/
	private function loadMap()
	{
		$this->collectionTypes = array(
				'book_article'			=>	'book',
				'book_chapter'			=>	'book',
				'journal_article'		=>	'journal',
				'newspaper_article'		=>	'newspaper',
				'magazine_article'		=>	'magazine',
				'web_article'			=>	'web',
				'web_encyclopedia_article'			=>	'web',
				'conference_paper'		=>	'proceedings',
				'proceedings_article'		=>	'proceedings',
				'manuscript'			=>	'manuscript',
				'thesis'			=>	'thesis',
				'music_track'			=>	'music',
				'miscellaneous_section'			=>	'miscellaneous',
			);

/**
* Within each collection array, the outer array refers to the database table, the inner array has the key for the table column and a value
* to be found in the resources array of MESSAGES_xx.php
*/
		$this->book = array(
				'resource'	=>	array(
					'Field1'	=>	'seriesTitle',
					'Field2'	=>	'edition',
					'Field3'	=>	'seriesNumber',
					'Field4'	=>	'bookVolumeNumber',
					'TransTitle'	=>	'title',
					'TransSubtitle'	=>	'subtitle',
					'TransShortTitle' => 'shortTitle',
					'TransNoSort' => FALSE,
					'Isbn'	=>	'isbn',
					'Doi'	=>	'doi',
				),
				'resource_misc'	=>	array(
					'Publisher'	=>	'publisher',
					'Field1'	=>	'transPublisher',
					'Field4'	=>	'numberOfVolumes',
					'PeerReviewed'	=>	'peerReviewed',
					),
				'resource_year'	=>	array(
					'Year1'		=>	'publicationYear',
					'Year2'		=>	'reprintYear',
					'Year3'		=>	'volumeYear',
					'Year4'		=>	'transPublicationYear',
				),
				'resource_creator' =>	array(
					'2'	=>	'editor',
					'3'	=>	'translator',
					'4'	=>	'reviser',
					'5'	=>	'seriesEditor',
				),
			);
		$this->journal = array(
				'resource'	=>	array(
					'Isbn'	=>	'isbn',
				),
				'resource_misc'	=>	array(
					'Publisher'	=>	'publisher',
					'PeerReviewed'	=>	'peerReviewed',
					),
				'resource_year'	=>	array(
				),
				'resource_creator' =>	array(
				),
			);
		$this->newspaper = array(
				'resource'	=>	array(
					'Field2'	=>	'city',
					'Isbn'	=>	'isbn',
				),
				'resource_misc'	=>	array(
					),
				'resource_year'	=>	array(
				),
				'resource_creator' =>	array(
				),
			);
		$this->magazine = array(
				'resource'	=>	array(
					'Isbn'	=>	'isbn',
				),
				'resource_misc'	=>	array(
					),
				'resource_year'	=>	array(
				),
				'resource_creator' =>	array(
				),
			);
		$this->proceedings = array(
				'resource'	=>	array(
					'Field1'	=>	'seriesTitle',
					'Field3'	=>	'seriesNumber',
					'Field4'	=>	'proceedingsVolumeNumber',
					'Isbn'	=>	'isbn',
				),
				'resource_misc'	=>	array(
					'Field2'	=>	'startDay',
					'Field5'	=>	'endDay',
					'Field3'	=>	'startMonth',
					'Field6'	=>	'endMonth',
					'PeerReviewed' => 'peerReviewed',
					'Publisher' 	=>	'organizerId',
					'Field1'	=>	'publisherId',
					),
				'resource_year'	=>	array(
					'Year1'		=>	'publicationYear',
					'Year2'		=>	'startYear',
					'Year3'		=>	'endYear',
				),
				'resource_creator' =>	array(
					'2'	=>	'editor',
				),
			);
		$this->web = array(
				'resource'	=>	array(
					'Isbn'	=>	'isbn',
					),
				'resource_misc'	=>	array(
					'Publisher'	=>	'publisher',
					'PeerReviewed' => 'peerReviewed',
					),
				'resource_year'	=>	array(
				),
				'resource_creator' =>	array(
					'2'	=>	'editor',
				),
			);
		$this->music = array(
				'resource'	=>	array(
					'Field2'	=>	'medium',
					'Isbn'	=>	'isbn',
				),
				'resource_misc'	=>	array(
					'Publisher'	=>	'publisher',
					),
				'resource_year'	=>	array(
					'Year1'		=>	'publicationYear',
				),
				'resource_creator' =>	array(
				),
			);
		$this->thesis = array(
				'resource'	=>	array(
					'Isbn'	=>	'isbn',
				),
				'resource_misc'	=>	array(
					),
				'resource_year'	=>	array(
				),
				'resource_creator' =>	array(
				),
			);
		$this->manuscript = array(
				'resource'	=>	array(
					'Isbn'	=>	'isbn',
				),
				'resource_misc'	=>	array(
					),
				'resource_year'	=>	array(
				),
				'resource_creator' =>	array(
				),
			);
		$this->miscellaneous = array(
				'resource'	=>	array(
					'Field2'	=>	'medium',
					'Isbn'	=>	'isbn',
				),
				'resource_misc'	=>	array(
					'Publisher'	=>	'publisher',
					'PeerReviewed' => 'peerReviewed',
					),
				'resource_year'	=>	array(
					'Year1'		=>	'publicationYear',
				),
				'resource_creator' =>	array(
				),
			);
	}
}
