<?php
/**
 * WIKINDX : Bibliographic Management system.
 * @link https://wikindx.sourceforge.io/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2018-2019 Mark Grimshaw-Aagaard <sirfragalot@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 */

/**
* LISTCOMMON common functions for listing, searching, selecting etc. resources
*
* @version	1
*
*	@package wikindx\core\lists
*	@author Mark Grimshaw-Aagaard <sirfragalot@users.sourceforge.net>
*
*/
class LISTCOMMON
{
/** object */
private $db;
/** array */
private $vars;
/** object */
private $icons;
/** object */
private $config;
/** object */
private $session;
/** object */
private $messages;
/** object */
private $user;
/** object */
private $stats;
/** object */
public $pagingObject = FALSE;
/** object */
private $bibStyle;
/** boolean */
public $keepHighlight = FALSE;
/** string */
public $patterns = FALSE;
/** string */
public $navigate = 'list';
/** object */
private $commonBib;
/** object */
private $cite;
/** object */
private $resCommon;
/** boolean */
public $listQuarantined = FALSE;
/** boolean */
public $metadata = FALSE;
/** string */
public $metadataKeyword = FALSE;
/** array */
public $metadataText = array();
/** array */
public $metadataTextCite = array();
/** array */
public $metadataTextJoin = array();
/** array */
public $metadataTextCond = array();
/** boolean */
public $quickSearch = FALSE;
/** object */
private $languageClass;
/** string */
public $browse = FALSE;
/** boolean */
public $metadataPaging = FALSE;
/** booolean **/
public $ideasFound = FALSE;
/** array */
private $rows = array();


/**
* LISTCOMMON
*/
	public function __construct()
	{
		$this->db = FACTORY_DB::getInstance();
		$this->vars = GLOBALS::getVars();
		$this->icons = FACTORY_LOADICONS::getInstance();
		$this->config = FACTORY_CONFIG::getInstance();

		$this->session = FACTORY_SESSION::getInstance();
		$this->messages = FACTORY_MESSAGES::getInstance();
		$this->user = FACTORY_USER::getInstance();
		$this->stats = FACTORY_STATISTICS::getInstance();
		$this->bibStyle = FACTORY_BIBSTYLE::getInstance();

		$this->commonBib = FACTORY_BIBLIOGRAPHYCOMMON::getInstance();
		$this->cite = FACTORY_CITE::getInstance();
		$this->resCommon = FACTORY_RESOURCECOMMON::getInstance();
		$this->languageClass = FACTORY_CONSTANTS::getInstance();
		$this->icons->setupIcons();
		$this->stats->list = TRUE;
	}
/**
* Check there are resources to display
*
* @return boolean
*/
	public function resourcesExist()
	{
		$recordset = $this->db->select('database_summary', 'databaseSummaryTotalResources');
		if(!$this->db->fetchOne($recordset))
		{
			GLOBALS::addTplVar('content', $this->messages->text('misc', 'noResources'));
			return FALSE;
		}
		if($useBib = $this->session->getVar("mywikindx_Bibliography_use"))
		{
			$this->db->formatConditions(array('userbibliographyresourceBibliographyId' => $useBib));
			$this->db->formatConditions($this->db->formatFields('userbibliographyresourceResourceId') . $this->db->equal .
				$this->db->formatFields('resourceId'));
			$resultset = $this->db->select(array('resource', 'user_bibliography_resource'), 'resourceId');
			if(!$this->db->numRows($resultset))
			{
				GLOBALS::addTplVar('content', $this->messages->text('misc', 'noResourcesBib'));
				return FALSE;
			}
		}
		else
		{
			if($this->db->tableIsEmpty('resource'))
			{
				GLOBALS::addTplVar('content', $this->messages->text('misc', 'noResourcesBib'));
				return FALSE;
			}
		}
		return TRUE;
	}
/**
* Display a list from the lastMulti menu item
*
* @param string $listType (list, search etc)
*/
	public function lastMulti($listType = FALSE)
	{
		$this->session->delVar('mywikindx_PagingStart');
		$this->session->delVar('list_NextPreviousIds');
		$sql = $this->session->getVar('sql_ListStmt');
// set back to beginning
		$limit = $this->db->limit($this->session->getVar('setup_Paging'), $this->pagingObject->start, TRUE); // "LIMIT $limitStart, $limit";
		$this->display($sql . $limit, $listType);
		$this->session->saveState(array('list', 'sql', 'bookmark'));
		$this->session->setVar('list_SubQuery', $this->session->getVar('list_SubQueryMulti'));
	}
/**
* Produce a list of resources
*
* @param string $sql
* @param string $listType Default is FALSE
*/
	public function display($sql, $listType = FALSE)
	{
		$this->session->setVar("list_On", TRUE);
		if(!$this->keepHighlight)
			$this->session->delVar('search_Highlight');
		$this->bibStyle->bibformat->patterns = $this->patterns;
		if($this->session->getVar('setup_Listlink'))
			$this->bibStyle->linkUrl = FALSE;
		if($listType != 'cite')
			$this->session->setVar("bookmark_View", 'multi');
// $SQL can be FALSE if browsing a keyword that is not attached to resources but only to ideas.
		if(!$sql)
		{
			$this->noResources($listType);
			return TRUE;
		}
		$this->db->DisableFullGroupBySqlMode();
		if(($listType == 'front') || ($listType == 'cite'))
		{
			$recordset = $this->db->query($sql); // Don't mess up Last Multi by saving querystring
            // Restore SQL mode
		    $this->db->EnableFullGroupBySqlMode();
			if($listType == 'cite')
			{
				if(!$this->db->numRows($recordset))
					return FALSE;
			}
		}
		else
			$recordset = $this->db->query($sql, TRUE);
// Restore SQL mode
		$this->db->EnableFullGroupBySqlMode();
// Displaying only attachments?
		if($this->session->getVar($listType . '_DisplayAttachment'))
		{
			$this->listAttachments($recordset, $listType);
			$this->session->setVar('sql_DisplayAttachment', $listType . '_DisplayAttachment');
			return;
		}
		$this->session->delVar('sql_DisplayAttachment');
		$multiUserSwitch = ($this->session->getVar('setup_MultiUser'));
		if ($multiUserSwitch)
		    GLOBALS::addTplVar('multiUser', TRUE);
		$quarantineSwitch = ($this->session->getVar('setup_Quarantine'));
		$useDateFormatMethod = method_exists($this->languageClass, "dateFormat");
		//$citeRadioButtonFirst = TRUE;

		if($this->metadataKeyword)
		    $listMetadataMethod = 'listMetadata';
		else if(!empty($this->metadataText))
		    $listMetadataMethod = 'listMetadataText';
		else if($listType == 'cite' && !empty($this->metadataTextCite))
		    $listMetadataMethod = 'listMetadataText';
		else
		    $listMetadataMethod = '';

		$resourceList = array();
		$resources = array();
		$resIds = array();
		while($row = $this->db->fetchRow($recordset))
		{
		    // will be the case if ideas have been found through a keyword
			if(!$row['resourceId'])
				continue;

			// Don't return twice the same resource
			if (array_key_exists($row['resourceId'], $resources) !== FALSE)
				continue;

			$this->rows[$row['resourceId']] = $row;

			if ($listMetadataMethod != '')
			{
			    $mArray = $this->{$listMetadataMethod}($row['resourceId']);
    			if(!empty($mArray))
    			{
    				$resourceList[$row['resourceId']]['metadata'] = $mArray;
    				unset($mArray);
    			}
			}

			// e.g. from the TinyMCE insert cite button of resource metadata
			if($listType != 'cite')
			{
    			if($quarantineSwitch && ($row['resourcemiscQuarantine'] == 'Y'))
    				$resourceList[$row['resourceId']]['quarantine'] = $this->icons->quarantine;

    			if($multiUserSwitch)
    			{
    				$resourceList[$row['resourceId']]['user'] = $this->user->displayUserAddEdit($row);
    				$resourceList[$row['resourceId']]['maturity'] = $row['resourcemiscMaturityIndex'] ?
    					"&nbsp;" . $this->messages->text("misc", "matIndex") .
    					"&nbsp;" . $row['resourcemiscMaturityIndex'] . "/10" . BR
    					: FALSE;
    			}

    			if($useDateFormatMethod)
    				$resourceList[$row['resourceId']]['timestamp'] = \UTILS\dateFormat($row['resourcetimestampTimestamp']);
    			else
    			    $resourceList[$row['resourceId']]['timestamp'] = $row['resourcetimestampTimestamp'];
			}

			// Although quotes and paraphrases are not useful in all cases,
			// we add them to force the count in the subsequent procedure.
			$resources[$row['resourceId']] = array('quotes' => $row['resourcesummaryQuotes'], 'paraphrases' => $row['resourcesummaryParaphrases']);
			$resIds[] = $row['resourceId'];
			unset($row);
		}

		if(count($resources) > 0)
		{
			$this->session->setVar("list_NextPreviousIds", base64_encode(serialize($resIds)));
    		$this->formatResources($listType, $resourceList, $resources);
    		$this->createLinks($listType, $resourceList, $resources);

			if(!$this->listQuarantined && ($listType != 'cite'))
			{
				if($this->pagingObject)
					$this->displayListInfo($listType, TRUE);
			}

		    // Templates expect list ordered from 0,
		    // so we renumber from zero
			GLOBALS::setTplVar('resourceList', array_values($resourceList));
			unset($resourceList);
		}
		else
			$this->noResources($listType);
		$this->rows = NULL;
	    unset($resources);
	    unset($resourceList);
		return TRUE;
	}
/**
* Tidy display when there are no resources
*
* @param string $listType
* @return boolean
*/
	public function noResources($listType)
	{
		$this->session->delVar("list_AllIds");
		$this->session->delVar("list_NextPreviousIds");
		if($this->pagingObject && ($listType != 'cite'))
			$this->displayListInfo($listType, FALSE);
		else // from SEARCH.php if only ideas are searched on
		{
			$this->pagingObject = FACTORY_PAGINGALPHA::getInstance();
			$this->displayListInfo($listType, FALSE);
		}
		return TRUE;
	}
/**
* Get metadata for this resource when browsing a keyword
*
* @param int $resourceId
* @return array
*/
	private function listMetadata($resourceId)
	{
		$array = array();
// quotes
		$this->db->formatConditions(array('resourcekeywordKeywordId' => $this->metadataKeyword));
		$this->db->formatConditions(array('resourcekeywordMetadataId' => ' IS NOT NULL'));
		$this->db->formatConditions(array('resourcemetadataResourceId' => $resourceId));
		$this->db->leftJoin('resource_metadata', 'resourcemetadataId', 'resourcekeywordMetadataId');
		$resultset = $this->db->select('resource_keyword', 'resourcemetadataText');
		while($row = $this->db->fetchRow($resultset))
			$array[] = $this->cite->parseCitations(\HTML\dbToHtmlTidy($row['resourcemetadataText']), 'htmlNoBib', FALSE);
		return $array;
	}
/**
* Get metadata for this resource when selecting or searching metadata
*
* @param int $resourceId
* @return array
*/
	private function listMetadataText($resourceId)
	{
		$array = array();
		if(!empty($this->metadataText)) // i.e. not called from the word processor so no need for radio buttons on quotes, comments etc.
		{
			$cite = FALSE;
			$cycleArray = $this->metadataText;
		}
		else
		{
			$cite = TRUE;
			$cycleArray = $this->metadataTextCite;
		}
		foreach($cycleArray as $sql)
		{
			$sql = str_replace('RESID', $this->db->tidyInput($resourceId), $sql);
			$resultset = $this->db->query($sql);
			while($row = $this->db->fetchRow($resultset))
			{
				if($cite)
					$array[] = \FORM\radioButton(FALSE, 'cite', $resourceId . '_' .
					base64_encode(\HTML\dbToTinyMCE($row['text']))) .
					'&nbsp;' .
					$this->resCommon->doHighlight($this->cite->parseCitations(\HTML\dbToHtmlTidy($row['text']), 'htmlNoBib', FALSE));
				else
					$array[] = $this->resCommon->doHighlight($this->cite->parseCitations(\HTML\dbToHtmlTidy($row['text']), 'htmlNoBib', FALSE));
			}
		}
		return $array;
	}
/**
* list only attachments
*
* @param object $recordset
* @param string $listType
*/
	private function listAttachments($recordset, $listType)
	{
// Are only logged on users allowed to view this file and is this user logged on?
		if($this->session->getVar("setup_FileViewLoggedOnOnly") && !$this->session->getVar("setup_UserId"))
			$this->displayListInfo($listType, FALSE);
		include_once("core/miscellaneous/ATTACHMENT.php");
		$attachments = new ATTACHMENT();
		$files = array();
		$zip = $this->session->getVar($listType . '_DisplayAttachmentZip') ? TRUE : FALSE;
		while($row = $this->db->fetchRow($recordset))
		{
			if($zip)
				$files[$row['resourceattachmentsFileName']] = $row['resourceattachmentsHashFilename'];
			else
			{
				$files[] = $attachments->makeLink($row, TRUE, FALSE);
				$ids[] = \HTML\a($this->icons->viewLink, $this->icons->view, "index.php?action=resource_RESOURCEVIEW_CORE" .
					htmlentities("&id=" . $row['resourceattachmentsResourceId']));
			}
		}

		if(empty($files))
		{
			$this->displayListInfo($listType, FALSE);
			return;
		}
		if($zip) // zip the files
		{
			$path = $this->config->WIKINDX_ATTACHMENTS_DIR;
			if(!$file = FILE\zip($files, $path))
			{
				$errors = FACTORY_ERRORS::getInstance();
				$badInput = FACTORY_BADINPUT::getInstance();
				$badInput->close($errors->text("file", "write"));
			}
			$path = $this->config->WIKINDX_FILES_DIR;
			$link[] = \HTML\a("link", 'ZIP', $path . DIRECTORY_SEPARATOR . $file, "_blank");
			GLOBALS::addTplVar('fileList', $link);
			return;
		}
		$this->displayListInfo($listType);
		GLOBALS::addTplVar('fileList', $files);
		GLOBALS::addTplVar('fileListIds', $ids);
	}
/**
* Format resource according to bibliographic style when viewing a list, search results or a results prior to inserting a citation link into metadata
*
* @param string $listType Type of list to format
* @param array $resourceList Reference to $resourceList
* @param array $resources Reference to $resources
*/
	private function formatResources($listType, &$resourceList, &$resources)
	{
	    $resultSet = $this->getCreators(array_keys($resources));

		$creators = array();

		while($cRow = $this->db->fetchRow($resultSet))
		{
			$creators[$cRow['resourcecreatorResourceId']][$cRow['resourcecreatorRole']][] = $cRow['creatorId'];
			$array = array(
					'surname' => $cRow['surname'],
					'firstname' => $cRow['firstname'],
					'initials' => $cRow['initials'],
					'prefix' => $cRow['prefix'],
					'creatorId' => $cRow['creatorId'],
				);
			$this->bibStyle->creators[$cRow['creatorId']] = array_map(array($this->bibStyle, "removeSlashes"), $array);
		}

		foreach($this->rows as $rId => $row)
		{
			if(empty($creators) || !array_key_exists($rId, $creators) || empty($creators[$rId]))
			{
				for($index = 1; $index <=5; $index++)
					$row["creator$index"] = ''; // need empty fields for BIBSTYLE
			}
			else
			{
				for($index = 1; $index <=5; $index++)
				{
					if(array_key_exists($index, $creators[$rId]))
						$row["creator$index"] = join(',', $creators[$rId][$index]);
					else
						$row["creator$index"] = '';
				}
			}

			$resourceList[$row['resourceId']]['resource'] = $this->bibStyle->process($row, FALSE, FALSE);
		}
	}
/**
* Get SQL resultset for creator details before formatting resources
*
* @param array $resourceIds
* @return object SQL resultset
*/
	private function getCreators($resourceIds)
	{
		$this->db->formatConditionsOneField($resourceIds, 'resourcecreatorResourceId');
		$this->db->leftJoin('creator', 'creatorId', 'resourcecreatorCreatorId');
		$this->db->orderBy('resourcecreatorResourceId', TRUE, FALSE);
		$this->db->ascDesc = $this->db->asc;
		$this->db->orderBy('resourcecreatorRole', TRUE, FALSE);
		$this->db->orderBy('resourcecreatorOrder', TRUE, FALSE);
		return $this->db->select('resource_creator', array('resourcecreatorResourceId', array('creatorSurname' => 'surname'),
			array('creatorFirstname' => 'firstname'), array('creatorInitials' => 'initials'), array('creatorPrefix' => 'prefix'),
			'creatorId', 'resourcecreatorRole'));
	}
/**
* Create links for viewing, editing deleting etc. resources
*
* @param string $listType Type of list to format
* @param array $resourceList Reference to $resourceList
* @param array $resources
*/
	private function createLinks($listType, &$resourceList, $resources)
	{
	    if ($listType == 'cite')
	    {
		    $citeRadioButtonFirst = TRUE;
		    foreach($resourceList as $resourceId => $resourceArray)
		    {
			    $resourceList[$resourceId]['links']['checkbox'] = \FORM\radioButton(FALSE, 'cite', $resourceId, $citeRadioButtonFirst);
			    $citeRadioButtonFirst = FALSE;
			}
		}
		else
		{
    		$write = $this->session->getVar('setup_Write');
    		$superAdmin = $this->session->getVar('setup_Superadmin');
    		$userId = $this->session->getVar('setup_UserId');
    		$attachments = $musings = array();
    		$edit = FALSE;
    // Check if these resources have attachments and display view icons accordingly. Also, calculate the resource's popularity index
    		if($this->session->getVar("setup_UserId") || !$this->session->getVar("setup_FileViewLoggedOnOnly"))
    		{
    			$mdr = $this->stats->getMaxDownloadRatio();
    			$mar = $this->stats->getMaxAccessRatio();
    			// avoid division by 0
    			if(!$mdr) $mdr = 1;
    			if(!$mar) $mar = 1;
    // build inner SELECT statement
    			$dr = $this->db->dateDiffRatio('resourceattachmentsDownloads', 'resourceattachmentsTimestamp', 'downloadRatio', 'AVG', 0);
    			$this->db->groupBy('resourceattachmentsResourceId');
    			$raId = $this->db->formatFields('resourceattachmentsResourceId');
    			$innerSubQ = $this->db->selectNoExecute('resource_attachments', array($raId, $dr), FALSE, FALSE, TRUE);
    			$innerSubQ = $this->db->subQuery($innerSubQ, 't1');
    // build middle SELECT statement
    			$middleSubQ = $this->db->selectNoExecuteFromSubQuery(FALSE, array('resourceattachmentsResourceId', 'downloadRatio'), $innerSubQ);
    			$middleSubQ = $this->db->subQuery($middleSubQ, 't2', FALSE);
    // build outer SELECT statement
    			$pi = $this->db->dateDiffRatio('resourcemiscAccesses', 'resourcetimestampTimestampAdd', FALSE, '', 0);
    			$pi = $this->db->round('((' . $pi . ") / $mar) * 0.25 + ((" . $this->db->formatFields('downloadRatio') . " / $mdr) * 0.75)", 'popIndex', 2);
    			$this->db->formatConditionsOneField(array_keys($resources), 'resourcemiscId');
    			$this->db->leftJoin('resource_timestamp', 'resourcetimestampId', 'resourcemiscId');
    			$this->db->leftJoinSubQuery($middleSubQ, 't2.resourceattachmentsResourceId', 'resource_misc.resourcemiscId');
    			$this->db->groupBy(array('resourcemiscId', 't2.resourceattachmentsResourceId', 'resourcemiscAccesses', 'resourcetimestampTimestampAdd', 't2.downloadRatio'));
    			$resultSet = $this->db->select('resource_misc', array('resourcemiscId', $pi, 'downloadRatio', 'resourceattachmentsResourceId'), FALSE, FALSE);
    			while($row = $this->db->fetchRow($resultSet))
    			{
    				if($row['resourceattachmentsResourceId'])
    					$attachments[$row['resourcemiscId']] = TRUE;
    				if($row['popIndex'])
    					$popIndex = $row['popIndex'] * 100;
    				else
    					$popIndex = 0;
    				$resourceList[$row['resourcemiscId']]['popIndex'] = $this->messages->text("misc", "popIndex", $popIndex);
    			}
    		}
    // Check if these resources have metadata and display view icons accordingly
    		$this->db->formatConditionsOneField(array_keys($resources), 'resourcemetadataResourceId');
    		$this->db->formatConditionsOneField(array('q', 'p', 'm'), 'resourcemetadataType');
    		$resultSet = $this->db->select('resource_metadata', array('resourcemetadataPrivate', 'resourcemetadataAddUserId', 
    			'resourcemetadataResourceId'));
    		while($row = $this->db->fetchRow($resultSet))
    		{
    			if(($row['resourcemetadataPrivate'] == 'N') || ($userId == $row['resourcemetadataAddUserId']))
    				$musings[$row['resourcemetadataResourceId']] = TRUE;
    		}

    		$isHyperlinked = ($this->session->getVar('setup_Listlink'));

    		foreach($resources as $resourceId => $resourceArray)
    		{
    			if($resourceArray['quotes'] || $resourceArray['paraphrases'] || array_key_exists($resourceId, $musings))
    			{
    				if(array_key_exists($resourceId, $attachments))
    					$view = $this->icons->viewmetaAttach;
    				else
	    				$view = $this->icons->viewmeta;
    			}
    			else if(array_key_exists($resourceId, $attachments))
    				$view = $this->icons->viewAttach;
    			else
    				$view = $this->icons->view;
    			if ($isHyperlinked)
    			{
    				$resourceLink = "index.php?action=resource_RESOURCEVIEW_CORE" . htmlentities("&id=" . $resourceId);
    				$resourceList[$resourceId]['resource'] =
    					\HTML\a('rLink', $resourceList[$resourceId]['resource'], $resourceLink);
    			}
        		if(($this->pagingObject && $this->session->getVar('setup_Write')) || ($listType != 'front'))
        			$resourceList[$resourceId]['links']['checkbox'] = \FORM\checkBox(FALSE, "bib_" . $resourceId);

    			if($write && !$this->config->WIKINDX_ORIGINATOR_EDITONLY)
    			{
    				$resourceList[$resourceId]['links']['edit'] = \HTML\a($this->icons->editLink, $this->icons->edit,
    					"index.php?action=resource_RESOURCEFORM_CORE&amp;type=edit" . htmlentities("&id=" . $resourceId));
    				if($row['resourcemiscAddUserIdResource'] == $userId)
    					$resourceList[$resourceId]['links']['delete'] = \HTML\a($this->icons->deleteLink, $this->icons->delete,
    						"index.php?action=admin_DELETERESOURCE_CORE" . htmlentities('&function=deleteResourceConfirm&navigate=' .
    						$this->navigate . '&resource_id=' . $resourceId));
    				$edit = TRUE;
    			}
    			else if($write && ($row['resourcemiscAddUserIdResource'] == $userId))
    			{
    				$resourceList[$resourceId]['links']['edit'] = \HTML\a($this->icons->editLink, $this->icons->edit,
    					"index.php?action=resource_RESOURCEFORM_CORE&amp;type=edit" . htmlentities("&id=" . $resourceId));
    				$resourceList[$resourceId]['links']['delete'] = \HTML\a($this->icons->deleteLink, $this->icons->delete,
    					"index.php?action=admin_DELETERESOURCE_CORE" . htmlentities('&function=deleteResourceConfirm&navigate=' .
    					$this->navigate . '&resource_id=' . $resourceId));
    				$edit = TRUE;
    			}
    			if($superAdmin)
    			{
    				if(!$edit)
    					$resourceList[$resourceId]['links']['edit'] = \HTML\a($this->icons->editLink, $this->icons->edit,
    					"index.php?action=resource_RESOURCEFORM_CORE&amp;type=edit" . htmlentities("&id=" . $resourceId));
    				$resourceList[$resourceId]['links']['delete'] = \HTML\a($this->icons->deleteLink, $this->icons->delete,
    					"index.php?action=admin_DELETERESOURCE_CORE" . htmlentities('&function=deleteResourceConfirm&navigate=' .
    					$this->navigate . '&resource_id=' . $resourceId));
    			}
    // display CMS link if required
    // link is actually a JavaScript call
    			if($this->session->getVar('setup_DisplayCmsLink') && $this->config->WIKINDX_CMS_ALLOW)
    				$resourceList[$resourceId]['links']['cms'] = \HTML\a('cmsLink', "CMS:&nbsp;" . $resourceId,
    					"javascript:coreOpenPopup('index.php?action=cms_CMS_CORE&amp;method=display" . "&amp;id=" . $resourceId . "',
    					90)");
    // display bibtex link if required
    // link is actually a JavaScript call
    			if($this->session->getVar('setup_DisplayBibtexLink'))
    				$resourceList[$resourceId]['links']['bibtex'] = \HTML\a($this->icons->bibtexLink, $this->icons->bibtex,
    					"javascript:coreOpenPopup('index.php?action=resource_VIEWBIBTEX_CORE&amp;method=display" .
    					"&amp;id=" . $resourceId . "', 90)");
    			// Display a resource
    			$resourceList[$resourceId]['links']['view'] = \HTML\a($this->icons->viewLink, $view, "index.php?action=resource_RESOURCEVIEW_CORE" .
    				htmlentities("&id=" . $resourceId));
    		}

    		unset($resources);
		}
	}
/**
* Print radio buttons ascending, descending for ordering
*
* @param string $type
* @return string
*/
	public function displayAscDesc($type)
	{
		if($ascDesc = trim($this->session->getVar($type . "_AscDesc")))
		{
			if($ascDesc == 'ASC')
				return \FORM\radioButton(FALSE, $type . "_AscDesc", 'ASC', TRUE) .
					$this->messages->text("list", "ascending") .
					BR . \FORM\radioButton(FALSE, $type . "_AscDesc", 'DESC') .
					$this->messages->text("list", "descending");
			else
				return \FORM\radioButton(FALSE, $type . "_AscDesc", 'ASC') .
					$this->messages->text("list", "ascending") .
					BR . \FORM\radioButton(FALSE, $type . "_AscDesc", 'DESC', TRUE) .
					$this->messages->text("list", "descending");
		}
		else
			return \FORM\radioButton(FALSE, $type . "_AscDesc", 'ASC', TRUE) .
				$this->messages->text("list", "ascending") .
				BR .\FORM\radioButton(FALSE, $type . "_AscDesc", 'DESC') .
				$this->messages->text("list", "descending");
	}
/**
* Set the paging object if paging is alphabetic or not
*
* @param string $sql
* @param string $listType
* @param string $order
* @param string $queryString
* @param boolean $conditions Array of conditions to SQL (default is FALSE)
* @param boolean $joins Array of table joins to SQL (array(table => array(rightField, leftField)) (Default is FALSE)
* @param boolean $conditionsOneField
* @param string $table default is 'resource'
* @param string $subQ Optional SQL subquery for input to COUNT operations - default is FALSE
*/
	public function pagingStyle($sql, $listType, $order, $queryString,
		$conditions = FALSE, $joins = FALSE, $conditionsOneField = FALSE, $table = 'resource', $subQ = FALSE)
	{
		if(($this->session->getVar('setup_PagingStyle') == 'A') &&
			(($order == 'title') || ($order == 'creator') || ($order == 'attachments')))
		{
			$this->pagingObject = FACTORY_PAGINGALPHA::getInstance();
			if($this->metadataPaging)
				$this->pagingObject->metadata = TRUE;
			$this->pagingObject->listType = $listType;
			$this->pagingObject->order = $order;
			$this->pagingObject->queryString = $queryString;
			$this->pagingObject->getPaging($conditions, $joins, $conditionsOneField, $table, $subQ);
		}
		else
		{
			$this->pagingObject = FACTORY_PAGING::getInstance();
			$this->pagingObject->queryString = $queryString;
			$this->pagingObject->getPaging();
		}
	}
/**
* Check for user bibliographies
*
* @return array (usingBib, bibUserId, bibs)
*/
	private function getUserBib()
	{
		$usingBib = $bibUserId = FALSE;
		$bibs = array();
		$uBibs = $this->commonBib->getUserBibs();
		$gBibs = $this->commonBib->getGroupBibs();
		$bibs = array_merge($uBibs, $gBibs);
		$useBib = $this->session->getVar('mywikindx_Bibliography_use');
		if($useBib)
		{
			$this->db->formatConditions(array('userbibliographyId' => $useBib));
			$recordset = $this->db->select('user_bibliography', array('userbibliographyTitle', 'userbibliographyUserId'));
			$row = $this->db->fetchRow($recordset);
			$usingBib = stripslashes($row['userbibliographyTitle']);
			$bibUserId = $row['userbibliographyUserId'];
			if(array_key_exists($useBib, $bibs))
				unset($bibs[$useBib]); // Remove the currently used one from the list
		}
		return array($usingBib, $bibUserId, $bibs);
	}
/**
* Display list information and userBib, category and keyword select box to add items to
*
* @param string $listType
* @param boolean $resourcesExist
*/
	private function displayListInfo($listType, $resourcesExist = TRUE)
	{
		list($usingBib, $bibUserId, $bibs) = $this->getUserBib();
		if($usingBib)
			$linksInfo['info'] = $this->pagingObject->linksInfo($usingBib);
		else
			$linksInfo['info'] = $this->pagingObject->linksInfo();
		$linksInfo['params'] = $this->listParams($listType);
		if($this->ideasFound)
		{
			if($listType == 'search')
				$linksInfo['info'] .= '&nbsp;' . \HTML\a('link', $this->messages->text('search', 'ideasFound'), "index.php?action=list_SEARCH_CORE" .
					htmlentities("&method=reprocess&type=displayIdeas"));
			else
				$linksInfo['info'] .= '&nbsp;' . \HTML\a('link', $this->messages->text('search', 'ideasFound'), "index.php?action=ideas_IDEAS_CORE" .
					htmlentities("&method=" . 'keywordIdeaList') . htmlentities("&resourcekeywordKeywordId=" . $this->metadataKeyword));
		}
		if(!$resourcesExist)
		{
			GLOBALS::setTplVar('resourceListInfo', $linksInfo);
		    unset($linksInfo);
			return;
		}
		if(!$this->session->getVar($listType . '_DisplayAttachment'))
		{
			$linksInfo['selectformheader'] = \FORM\formHeaderName('list_LISTADDTO_CORE', 'formSortingAddingListInfo', FALSE);
			$linksInfo['selectformfooter'] = \FORM\formEnd();
			$linksInfo['select'] = $this->createAddToBox($bibUserId, $bibs, $listType);
			if($listType == 'list')
			{
				if($this->session->getVar('list_SomeResources'))
					$formHeader = 'list_LISTSOMERESOURCES_CORE';
				else
					$formHeader = 'list_LISTRESOURCES_CORE';
				$linksInfo['reorder'] =
					\FORM\hidden("method", "reorder") . $this->displayOrder($listType, TRUE) .
					BR . \FORM\formSubmit($this->messages->text("submit", "Proceed"), 'Submit', "onclick=\"document.forms['formSortingAddingListInfo'].elements['action'].value='$formHeader'\"");
			}
			else if($listType == 'basket')
				$linksInfo['reorder'] =
					\FORM\hidden("method", "reorder") . $this->displayOrder('basket', TRUE) .
					BR . \FORM\formSubmit($this->messages->text("submit", "Proceed"), 'Submit', "onclick=\"document.forms['formSortingAddingListInfo'].elements['action'].value='basket_BASKET_CORE'\"");
			else if($listType == 'search')
			{
				if($this->quickSearch)
					$linksInfo['reorder'] =
						\FORM\hidden("method", "reprocess") . $this->displayOrder($listType, TRUE) .
						BR . \FORM\formSubmit($this->messages->text("submit", "Proceed"), 'Submit', "onclick=\"document.forms['formSortingAddingListInfo'].elements['action'].value='list_QUICKSEARCH_CORE'\"");
				else
					$linksInfo['reorder'] =
						\FORM\hidden("method", "reprocess") . $this->displayOrder($listType, TRUE) .
						BR . \FORM\formSubmit($this->messages->text("submit", "Proceed"), 'Submit', "onclick=\"document.forms['formSortingAddingListInfo'].elements['action'].value='list_SEARCH_CORE'\"");
			}
		}
// display CMS link if required
// link is actually a JavaScript call
		if($this->session->getVar('setup_DisplayCmsLink') && $this->config->WIKINDX_CMS_ALLOW && $this->config->WIKINDX_CMS_SQL)
			$linksInfo['cms'] = \HTML\a('cmsLink', "CMS",
				"javascript:coreOpenPopup('index.php?action=cms_CMS_CORE&amp;method=displayList" . "', 90)");
		GLOBALS::setTplVar('resourceListInfo', $linksInfo);
		unset($linksInfo);
	}
/**
* Create select box allowing users to add to categories, keywords etc.
*
* @param int $bibUserId
* @param array $bibs
* @param string $listType
* @return string
*/
	private function createAddToBox($bibUserId, $bibs, $listType)
	{
		if($this->session->getVar('setup_Write'))
			$array[1] = $this->messages->text("resources", "organize");
		if(!empty($bibs))
			$array[0] = $this->messages->text("resources", "addToBib");
		if($this->session->getVar('setup_UserId') && ($this->session->getVar('setup_UserId') == $bibUserId))
			$array[3] = $this->messages->text('resources', 'deleteFromBib');
		else if($this->session->getVar("resourceSelectedTo") == '3') // previous operation was 'deleteFromBib'
			$this->session->delVar("resourceSelectedTo");
		if($listType == 'basket')
			$array[8] = $this->messages->text('resources', 'basketRemove');
		else
			$array[7] = $this->messages->text('resources', 'basketAdd');
		if($this->session->getVar('setup_Superadmin'))
			$array[4] = $this->messages->text('resources', 'deleteResource');
		$array[9] = $this->messages->text('resources', 'exportCoins1');
		if(!isset($array))
			return FALSE;
		$t = \HTML\tableStart('right');
		$t .= \HTML\trStart('right');
		$sessVar = $this->session->getVar("resourceSelectedTo");
/*		if($this->session->getVar('setup_PagingStyle') == 'A')
			$radios = $this->messages->text("resources", "selectCheck") . '&nbsp;' . \FORM\radioButton(FALSE, 'selectWhat', 'checked') .
			BR .
			$this->messages->text("resources", "selectDisplay") . '&nbsp;' . \FORM\radioButton(FALSE, 'selectWhat', 'display', TRUE);
		else
*/			$radios = $this->messages->text("resources", "selectCheck") . '&nbsp;' . \FORM\radioButton(FALSE, 'selectWhat', 'checked', TRUE) .
			BR .
			$this->messages->text("resources", "selectDisplay") . '&nbsp;' . \FORM\radioButton(FALSE, 'selectWhat', 'display') .
			BR .
			$this->messages->text("resources", "selectAll") . '&nbsp;' . \FORM\radioButton(FALSE, 'selectWhat', 'all');

		\FORM\checkbox(FALSE, "selectWhat", FALSE);
		if($sessVar !== FALSE)
			$select = \FORM\selectedBoxValue(FALSE, "resourceSelectedTo", $array, $sessVar, 1);
		else
			$select = \FORM\selectFBoxValue(FALSE, "resourceSelectedTo", $array, 1);
		$t .= \HTML\td($select, FALSE, 'right');
		$t .= \HTML\trEnd();
		$t .= \HTML\trStart('right');
		$tr = \HTML\tableStart('right');
		$tr .= \HTML\trStart('right');
		$tr .= \HTML\td($radios, FALSE, 'right');
		$tr .= \HTML\td(\FORM\formSubmit($this->messages->text("submit", "Proceed"), 'Submit', "onclick=\"document.forms['formSortingAddingListInfo'].elements['action'].value='list_LISTADDTO_CORE';document.forms['formSortingAddingListInfo'].elements['method'].value='init';\""), 'right bottom width1percent');
		$tr .= \HTML\trEnd();
		$tr .= \HTML\tableEnd();
		$t .= \HTML\td($tr);
		$t .= \HTML\trEnd();
		$t .= \HTML\tableEnd();
		return $t;
	}
/**
* Ordering options for select and quicksearch
*
* @param string $type
* @param boolean $reorder Default is FALSE
* @return string
*/
	public function displayOrder($type, $reorder = FALSE)
	{
		if(($type == 'list') && !$this->browse)
			$order = array(
					"creator" => $this->messages->text("list", "creator"),
					"title" => $this->messages->text("list", "title"),
					"publisher" => $this->messages->text("list", "publisher"),
					"year" => $this->messages->text("list", "year"),
					"timestamp" => $this->messages->text("list", "timestamp"),
					"popularityIndex" => $this->messages->text("list", "popularity"),
					"viewsIndex" => $this->messages->text("list", "views"),
					"downloadsIndex" => $this->messages->text("list", "downloads"),
					"maturityIndex" => $this->messages->text("list", "maturity"),
				);
		else
			$order = array(
					"creator" => $this->messages->text("list", "creator"),
					"title" => $this->messages->text("list", "title"),
					"publisher" => $this->messages->text("list", "publisher"),
					"year" => $this->messages->text("list", "year"),
					"timestamp" => $this->messages->text("list", "timestamp"),
				);
		if($type == 'basket')
			$type = 'list';
		if(!$reorder)
			$size = '5';
		else
			$size = '2';
		if($selected = $this->session->getVar($type . "_Order"))
			$pString = \FORM\selectedBoxValue($this->messages->text("list", "order"),
			$type . "_Order", $order, $selected, 1);
		else
			$pString = \FORM\selectFBoxValue($this->messages->text("list", "order"),
			$type . "_Order", $order, 1);
		if(!$reorder)
			$pString .= \HTML\p($this->displayAscDesc($type));
		else
			$pString .= BR . $this->displayAscDesc($type);
		return $pString;
	}
/**
* Display some information about the search/select/list parameters
*
* @param string $listType
* @return string
*/
	private function listParams($listType)
	{
		$strings = array();
// Bookmarked multi view?
		if($this->session->getVar('bookmark_MultiView'))
		{
			$strings = unserialize(base64_decode($this->session->getVar('sql_ListParams')));
			if(!is_array($strings) && $strings) // From advanced search
				return \HTML\aBrowse('green', '1em', $this->messages->text('listParams', 'listParams'), '#',
					"", \HTML\dbToHtmlPopupTidy(\HTML\dbToHtmlTidy($strings))) . BR;
			if(empty($strings))
				return FALSE;
			$this->session->delVar('bookmark_MultiView');
			return $this->messages->text('listParams', 'listParams') . BR . join(BR, $strings);
		}
		if(array_key_exists('statistics', $this->vars) && ($this->vars['statistics'] == 'Type'))
			$strings[] = $this->messages->text('listParams', 'type') . ':&nbsp;&nbsp;' . $this->vars['id'];
		else if($id = $this->session->getVar($listType . "_Type"))
		{
			$ids = UTF8::mb_explode(',', $id);
			if(sizeof($ids) > 1)
				$strings[] = $this->messages->text('listParams', 'type') . ':&nbsp;&nbsp;' .
					\HTML\em($this->messages->text('listParams', 'listParamMultiple'));
			else
				$strings[] = $this->messages->text('listParams', 'type') . ':&nbsp;&nbsp;' . $this->messages->text('resourceType', $id);
		}
		if($listType == 'select')
		{
			if($id = $this->session->getVar($listType . '_Tag'))
			{
				$ids = UTF8::mb_explode(',', $id);
				if(sizeof($ids) > 1)
					$strings[] = $this->messages->text('listParams', 'tag') . ':&nbsp;&nbsp;' .
						\HTML\em($this->messages->text('listParams', 'listParamMultiple'));
				else
				{
					$this->db->formatConditions(array('tagId' => $id));
					$strings[] = $this->messages->text('listParams', 'tag') . ':&nbsp;&nbsp;' .
						\HTML\dbToHtmlTidy($this->db->selectFirstField('tag', 'tagTag'));
				}
			}
			if($id = $this->session->getVar($listType . "_attachment"))
				$strings[] = $this->messages->text('listParams', 'attachment');
		}
		if(($listType == 'listCategory') || ($id = $this->session->getVar($listType . '_Category')))
		{
			if($listType == 'listCategory')
				$id = array_key_exists("id", $this->vars) ?
					$this->vars["id"] : $this->session->getVar("list_Ids");
			$cats = UTF8::mb_explode(',', $id);
			if(sizeof($cats) > 1)
				$strings[] = $this->messages->text('listParams', 'category') . ':&nbsp;&nbsp;' .
					\HTML\em($this->messages->text('listParams', 'listParamMultiple'));
			else
			{
				$this->db->formatConditions(array('categoryId' => $id));
				$strings[] = $this->messages->text('listParams', 'category') . ':&nbsp;&nbsp;' .
					\HTML\dbToHtmlTidy($this->db->selectFirstField('category', 'categoryCategory'));
			}
		}
		if(($listType == 'listSubcategory') || ($id = $this->session->getVar($listType . '_Subcategory')))
		{
			if($listType == 'listSubcategory')
				$id = array_key_exists("id", $this->vars) ?
					$this->vars["id"] : $this->session->getVar("list_Ids");
			$cats = UTF8::mb_explode(',', $id);
			if(sizeof($cats) > 1)
				$strings[] = $this->messages->text('listParams', 'subcategory') . ':&nbsp;&nbsp;' .
					\HTML\em($this->messages->text('listParams', 'listParamMultiple'));
			else
			{
				$this->db->formatConditions(array('subcategoryId' => $id));
				$strings[] = $this->messages->text('listParams', 'subcategory') . ':&nbsp;&nbsp;' .
					\HTML\dbToHtmlTidy($this->db->selectFirstField('subcategory', 'subcategorySubcategory'));
			}
		}
		if(($listType == 'listUserTag') || ($id = $this->session->getVar($listType . '_UserTag')))
		{
			if($listType == 'listUserTag')
				$id = array_key_exists("id", $this->vars) ?
					$this->vars["id"] : $this->session->getVar("list_Ids");
			$cats = UTF8::mb_explode(',', $id);
			if(sizeof($cats) > 1)
				$strings[] = $this->messages->text('listParams', 'userTag') . ':&nbsp;&nbsp;' .
					\HTML\em($this->messages->text('listParams', 'listParamMultiple'));
			else
			{
				$this->db->formatConditions(array('usertagsId' => $id));
				$strings[] = $this->messages->text('listParams', 'userTag') . ':&nbsp;&nbsp;' .
					\HTML\dbToHtmlTidy($this->db->selectFirstField('user_tags', 'usertagsTag'));
			}
		}
		if(($listType == 'listCollection') || ($id = $this->session->getVar($listType . '_Collection')))
		{
			if($listType == 'listCollection')
				$id = array_key_exists("id", $this->vars) ?
					$this->vars["id"] : $this->session->getVar("list_Ids");
			$cats = UTF8::mb_explode(',', $id);
			if(sizeof($cats) > 1)
				$strings[] = $this->messages->text('listParams', 'collection') . ':&nbsp;&nbsp;' .
					\HTML\em($this->messages->text('listParams', 'listParamMultiple'));
			else
			{
				$this->db->formatConditions(array('collectionId' => $id));
				$strings[] = $this->messages->text('listParams', 'collection') . ':&nbsp;&nbsp;' .
					\HTML\dbToHtmlTidy($this->db->selectFirstField('collection', 'collectionTitle'));
			}
		}
		if(($listType == 'listPublisher') || ($id = $this->session->getVar($listType . '_Publisher')))
		{
			if($listType == 'listPublisher')
				$id = array_key_exists("id", $this->vars) ?
					$this->vars["id"] : $this->session->getVar("list_Ids");
			$cats = UTF8::mb_explode(',', $id);
			if(sizeof($cats) > 1)
				$strings[] = $this->messages->text('listParams', 'publisher') . ':&nbsp;&nbsp;' .
					\HTML\em($this->messages->text('listParams', 'listParamMultiple'));
			else
			{
				$this->db->formatConditions(array('publisherId' => $id));
				$recordset = $this->db->select('publisher', array('publisherName', 'publisherLocation'));
				$row = $this->db->fetchRow($recordset);
				if($row['publisherLocation'])
					$loc = ' (' . stripslashes($row['publisherLocation']) . ')';
				else
					$loc = FALSE;
				$publisher = stripslashes($row['publisherName']) . $loc;
				$strings[] = $this->messages->text('listParams', 'publisher') . ':&nbsp;&nbsp;' .
					\HTML\dbToHtmlTidy($publisher);
			}
		}
		if(($listType == 'listKeyword') || ($id = $this->session->getVar($listType . '_Keyword')))
		{
			if($listType == 'listKeyword')
				$id = array_key_exists("id", $this->vars) ?
					$this->vars["id"] : $this->session->getVar("list_Ids");
			$ids = UTF8::mb_explode(',', $id);
			if(sizeof($ids) > 1)
				$strings[] = $this->messages->text('listParams', 'keyword') . ':&nbsp;&nbsp;' .
					\HTML\em($this->messages->text('listParams', 'listParamMultiple'));
			else
			{
				$this->db->formatConditions(array('keywordId' => $id));
				$strings[] = $this->messages->text('listParams', 'keyword') . ':&nbsp;&nbsp;' .
					\HTML\dbToHtmlTidy($this->db->selectFirstField('keyword', 'keywordKeyword'));
			}
		}
		if($id = $this->session->getVar($listType . '_Language'))
		{
			$ids = UTF8::mb_explode(',', $id);
			if(sizeof($ids) > 1)
				$strings[] = $this->messages->text('listParams', 'language') . ':&nbsp;&nbsp;' .
					\HTML\em($this->messages->text('listParams', 'listParamMultiple'));
			else
			{
				$this->db->formatConditions(array('languageId' => $id));
				$recordset = $this->db->select('language', 'languageLanguage');
				$strings[] = $this->messages->text('listParams', 'language') . ':&nbsp;&nbsp;' .
					\HTML\dbToHtmlTidy($this->db->selectFirstField('language', 'languageLanguage'));
			}
		}
		if(($listType == 'listCreator') || ($listType == 'select'))
		{
			if($id = $this->session->getVar($listType . '_BibId'))
			{
				$this->db->formatConditions(array('userbibliographyId' => $id));
				$strings[] = $this->messages->text('listParams', 'notInUserBib') . ':&nbsp;&nbsp;' .
					\HTML\dbToHtmlTidy($this->db->selectFirstField('user_bibliography', 'userbibliographyTitle'));
			}
			if(($listType == 'listCreator') || ($id = $this->session->getVar($listType . '_Creator')))
			{
				if($listType == 'listCreator')
					$id = array_key_exists("id", $this->vars) ?
					$this->vars["id"] : $this->session->getVar("list_Ids");
				$ids = UTF8::mb_explode(',', $id);
				if(sizeof($ids) > 1)
					$strings[] = $this->messages->text('listParams', 'creator') . ':&nbsp;&nbsp;' .
						\HTML\em($this->messages->text('listParams', 'listParamMultiple'));
				else
				{
					$this->db->formatConditions(array('creatorId' => $id));
					$recordset = $this->db->select('creator', array('creatorPrefix', 'creatorSurname'));
					$row = $this->db->fetchRow($recordset);
					$name = $row['creatorPrefix'] ? $row['creatorPrefix'] . ' ' .
						$row['creatorSurname'] : $row['creatorSurname'];
					$strings[] = $this->messages->text('listParams', 'creator') . ':&nbsp;&nbsp;' .
						\HTML\dbToHtmlTidy($name);
				}
			}
		}
		if($listType == 'search')
		{
			if($param = $this->session->getVar('advancedSearch_listParams'))
				return \HTML\aBrowse('green', '1em', $this->messages->text('listParams', 'listParams'), '#',
					"", \HTML\dbToHtmlPopupTidy(\HTML\dbToHtmlTidy($param))) . BR;
			else
			{
				if($id = $this->session->getVar($listType . '_Field'))
				{
					$ids = UTF8::mb_explode(',', $id);
					if(sizeof($ids) > 1)
						$strings[] = $this->messages->text('listParams', 'field') . ':&nbsp;&nbsp;' .
							\HTML\em($this->messages->text('listParams', 'listParamMultiple'));
					else
					{
						if(mb_strpos($id, 'Custom_') === 0)
						{
							$customField = UTF8::mb_explode('_', $id);
							$this->db->formatConditions(array('customId' => $customField[2]));
							$id = $this->db->selectFirstField('custom', 'customLabel');
						}
						else
							$id = $this->messages->text("search", $id);
						$strings[] = $this->messages->text('listParams', 'field') . ':&nbsp;&nbsp;' . $id;
					}
				}
				if($id = $this->session->getVar($listType . '_Word'))
				{
					if($this->session->getVar($listType . '_Partial') == 'on')
						$id .= "&nbsp;(" . $this->messages->text('listParams', 'partial') . ")";
					$strings[] = $this->messages->text('listParams', 'word') . ':&nbsp;&nbsp;' . \HTML\dbToHtmlTidy($id);
				}
			}
		}
		if($listType == 'list')
		{
			if(array_key_exists('id', $this->vars))
				$id = $this->vars['id'];
			else if(array_key_exists('method', $this->vars) && ($this->vars['method'] == 'processAdd'))
				$id = $this->vars['list_AddedBy'];
			else if(array_key_exists('method', $this->vars) && ($this->vars['method'] == 'processEdit'))
				$id = $this->vars['list_EditedBy'];
			else if(array_key_exists('method', $this->vars) && ($this->vars['method'] == 'processGeneral'))
				$id = FALSE;
			else if(array_key_exists('department', $this->vars))
				$id = base64_decode($this->vars['department']);
			else if(array_key_exists('institution', $this->vars))
				$id = base64_decode($this->vars['institution']);
			if(!$id)
				$strings[] = $this->messages->text('listParams', 'listAll');
			else if(array_key_exists('method', $this->vars) && ($this->vars['method'] == 'typeProcess'))
				$strings[] = $this->messages->text('listParams', 'type') . ':&nbsp;&nbsp;' . $this->messages->text('resourceType', $id);
			else if(array_key_exists('method', $this->vars) && ($this->vars['method'] == 'creatorProcess'))
			{
				if(array_key_exists('department', $this->vars))
					$strings[] = $this->messages->text('listParams', 'department') . ':&nbsp;&nbsp;' . $id;
				else if(array_key_exists('institution', $this->vars))
					$strings[] = $this->messages->text('listParams', 'institution') . ':&nbsp;&nbsp;' . $id;
				else
				{
					$this->db->formatConditions(array('creatorId' => $id));
					$id = $this->db->selectFirstField('creator', 'creatorSurname');
					$strings[] = $this->messages->text('listParams', 'creator') . ':&nbsp;&nbsp;' . \HTML\dbToHtmlTidy($id);
				}
			}
			else if(array_key_exists('method', $this->vars) && ($this->vars['method'] == 'citeProcessCreator'))
				$strings[] = $this->messages->text('listParams', 'cited');
			else if(array_key_exists('method', $this->vars) && ($this->vars['method'] == 'collectionProcess'))
			{
				$this->db->formatConditions(array('collectionId' => $id));
				$id = $this->db->selectFirstField('collection', 'collectionTitle');
				$strings[] = $this->messages->text('listParams', 'collection') . ':&nbsp;&nbsp;' . \HTML\dbToHtmlTidy($id);
			}
			else if(array_key_exists('method', $this->vars) && ($this->vars['method'] == 'publisherProcess'))
			{
				$this->db->formatConditions(array('publisherId' => $id));
				$row = $this->db->selectFirstRow('publisher', array('publisherName', 'publisherLocation'));
				if($row['publisherLocation'])
					$id = $row['publisherName'] . '(' . $row['publisherLocation'] . ')';
				else
					$id = $row['publisherName'];
				$strings[] = $this->messages->text('listParams', 'publisher') . ':&nbsp;&nbsp;' . \HTML\dbToHtmlTidy($id);
			}
			else if(array_key_exists('method', $this->vars) && ($this->vars['method'] == 'yearProcess'))
				$strings[] = $this->messages->text('listParams', 'year') . ':&nbsp;&nbsp;' . base64_decode($id);
			else if(array_key_exists('method', $this->vars) && ($this->vars['method'] == 'keywordProcess'))
			{
				$this->db->formatConditions(array('keywordId' => $id));
				$id = $this->db->selectFirstField('keyword', 'keywordKeyword');
				$strings[] = $this->messages->text('listParams', 'keyword') . ':&nbsp;&nbsp;' . \HTML\dbToHtmlTidy($id);
			}
			else if(array_key_exists('method', $this->vars) && ($this->vars['method'] == 'categoryProcess'))
			{
				$this->db->formatConditions(array('categoryId' => $id));
				$id = $this->db->selectFirstField('category', 'categoryCategory');
				$strings[] = $this->messages->text('listParams', 'category') . ':&nbsp;&nbsp;' . \HTML\dbToHtmlTidy($id);
			}
			else if(array_key_exists('method', $this->vars) && ($this->vars['method'] == 'subcategoryProcess'))
			{
				$this->db->formatConditions(array('subcategoryId' => $id));
				$id = $this->db->selectFirstField('subcategory', 'subcategorySubcategory');
				$strings[] = $this->messages->text('listParams', 'subcategory') . ':&nbsp;&nbsp;' . \HTML\dbToHtmlTidy($id);
			}
			else if(array_key_exists('method', $this->vars) && ($this->vars['method'] == 'languageProcess'))
			{
				$this->db->formatConditions(array('languageId' => $id));
				$id = $this->db->selectFirstField('language', 'languageLanguage');
				$strings[] = $this->messages->text('listParams', 'language') . ':&nbsp;&nbsp;' . \HTML\dbToHtmlTidy($id);
			}
			else if(array_key_exists('method', $this->vars) && ($this->vars['method'] == 'bibliographyProcess'))
			{
				$this->db->formatConditions(array('userbibliographyId' => $id));
				$id = $this->db->selectFirstField('user_bibliography', 'userbibliographyTitle');
				$strings[] = $this->messages->text('listParams', 'bibliography') . ':&nbsp;&nbsp;' . \HTML\dbToHtmlTidy($id);
			}
			else if(array_key_exists('method', $this->vars) && ($this->vars['method'] == 'processAdd'))
			{
				$this->db->formatConditions(array('usersId' => $id));
				$row = $this->db->selectFirstRow('users', array('usersFullname', 'usersUsername'));
				$id = $row['usersFullname'] ? $row['usersFullname'] : $row['usersUsername'];
				$strings[] = $this->messages->text('listParams', 'addedBy', \HTML\dbToHtmlTidy($id));
			}
			else if(array_key_exists('method', $this->vars) && ($this->vars['method'] == 'processEdit'))
			{
				$this->db->formatConditions(array('usersId' => $id));
				$row = $this->db->selectFirstRow('users', array('usersFullname', 'usersUsername'));
				$id = $row['usersFullname'] ? $row['usersFullname'] : $row['usersUsername'];
				$strings[] = $this->messages->text('listParams', 'editedBy', \HTML\dbToHtmlTidy($id));
			}
			else if(array_key_exists('method', $this->vars) && ($this->vars['method'] == 'usertagProcess'))
			{
				$this->db->formatConditions(array('usertagsId' => $id));
				$id = $this->db->selectFirstField('user_tags', 'usertagsTag');
				$strings[] = $this->messages->text('listParams', 'userTag') . ':&nbsp;&nbsp;' . \HTML\dbToHtmlTidy($id);
			}
		}
		if(empty($strings))
		{
			$this->session->delVar('sql_ListParams');
			return FALSE;
		}
		$this->session->setVar('sql_ListParams', base64_encode(serialize($strings)));
		return $this->messages->text('listParams', 'listParams') . BR . join(BR, $strings);
	}
}
