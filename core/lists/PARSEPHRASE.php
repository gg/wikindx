<?php
/**
 * WIKINDX : Bibliographic Management system.
 * @link https://wikindx.sourceforge.io/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2018-2019 Mark Grimshaw-Aagaard <sirfragalot@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 */

/**
* PARSEPHRASE -- break up a search phrase into component parts such as 'exact phrase', AND, OR and NOT
*
* @version	1
*
*	@package wikindx\core\lists
*	@author Mark Grimshaw-Aagaard <sirfragalot@users.sourceforge.net>
*
*/
class PARSEPHRASE
{
/** object */
private $db;
/** object */
private $session;
/** object */
private $config;
/** object */
private $errors;
/** array */
private $stringArray;
/** array */
private $NOTfragments;
/** booolean */
private $fulltextSearch;
/** boolean */
public $idea = FALSE;
/** array */
private $useRegex;
/** boolean */
public $validSearch = TRUE;

/**
* PARSEPHRASE
*/
	public function __construct()
	{
		$this->session = FACTORY_SESSION::getInstance();
		$this->db = FACTORY_DB::getInstance();
		$this->config = FACTORY_CONFIG::getInstance();
		$this->errors = FACTORY_ERRORS::getInstance();
	}
/**
* Parse input and create SQL condition
*
* @param array $input $input['Word'] is the input string from the search form
* @param boolean $test if TRUE formats the string for the natural language section of SEARCH.php. Default is FALSE
* @param boolean $stringSearch if TRUE formats the string for searching attachments in SEARCH.php. Default is FALSE
* @param boolean $fulltextSearch if TRUE manages the use of wildcards for fulltext (attachment) searches
* @return mixed FALSE|string
*/
	public function parse($input, $test = FALSE, $stringSearch = FALSE, $fulltextSearch = FALSE)
	{
		$this->stringArray = array();
		$this->NOTfragments = array();
		$this->useRegex = array();
		$this->fulltextSearch = $fulltextSearch;
// Check for malformed or empty $phrase
		if(!array_Key_exists('Word', $input))
		{
			$this->validSearch = FALSE;
			return ' 1 = 0 ';
		}
// check for equal no. double quotes
		if(substr_count($input['Word'], '"') % 2) // i.e. odd number
		{
			$this->validSearch = FALSE;
			return \HTML\color($this->errors->text("inputError", "invalid"), 'redText');
		}
// check for valid use of wildcards
		if(substr_count($input['Word'], '**') || substr_count($input['Word'], '??'))
		{
			$this->validSearch = FALSE;
			return \HTML\color($this->errors->text("inputError", "invalid"), 'redText');
		}
		$phrase = str_replace('"', 'WIKINDXDOUBLEQUOTEWIKINDX', $input['Word']);
		$phrase = trim(stripslashes($phrase));
		if(!$phrase || !$this->malformedString($phrase))
		{
			$this->validSearch = FALSE;
			return \HTML\color($this->errors->text("inputError", "invalid"), 'redText');
		}
// remove all punctuation (keep wildcard characters, apostrophe and dash for names such as Grimshaw-Aagaard and D'Eath)
		$phrase = preg_replace('/[^\p{L}\p{N}\s\*\?\-\']/u', '', $phrase);
		$phrase = str_replace('WIKINDXDOUBLEQUOTEWIKINDX', '"', $phrase);
		if(!$phrase = $this->tidySearch($phrase))
		{
			$this->validSearch = FALSE;
			return \HTML\color($this->errors->text("inputError", "invalid"), 'redText');
		}
// split up search string on single spaces -- NB, multiple spaces will result in empty elements
		$fragments = $this->splitSpaces(trim($phrase));
// loop through $fragments sorting into 'exact phrase', AND fragments, NOT fragments and OR fragments
		$this->sortFragments($fragments);
		$this->removeNot_and_FilterWords();
		if(empty($this->stringArray))
		{
			$this->validSearch = FALSE;
			return \HTML\color($this->errors->text("inputError", "invalid"), 'redText');
		}
		if(is_bool($stringSearch) && $stringSearch)
			return $this->stringArray;
		return $this->createCondition($input, $test);
	}
/**
* Create the condition clause and store search highlighting in the session
*
* @param array $input
* @param boolean $test If TRUE, we are just testing the parsing
* @return string
*/
	private function createCondition($input, $test)
	{
		$count = 0;
		$lastType = FALSE;
		$conditions = $searchHighlight = $conditionsJoin = $clauseOr = array();
		foreach($this->stringArray as $wordArray)
		{
// temporarily prepend '!WIKINDXFIELDWIKINDX!' which will later be replaced by the actual field name to search on.
// Each type that equals AND means we must bracket until the next AND or the end of the search -- thus including any
// exact phrases and OR statements in the bracket.  NOT statements are treated as AND statements.
// We always start the search with an AND clause -- parsePhrase()
// does not allow the search word phrase to start with OR or NOT.
			$type = array_shift($wordArray);
			$highlightSearch = $search = array_shift($wordArray);
			$search = addslashes($search);
// As of v4.2.2 (Sept 2013), 'Partial' is set TRUE in QUICKSEARCH
			if((array_key_exists('Partial', $input) && array_key_exists($count, $this->useRegex) && !$this->useRegex[$count])
			&&
				(($type == 'AND') || ($type == 'OR') || ($type == 'NOT')))
			{
				if($type == 'NOT')
				{
					if(!$test)
						$conditions[] = '!WIKINDXFIELDWIKINDX! ' . $this->db->like('%', $search, '%', TRUE);
					else
					{
						$search = \HTML\color('?' . $search . '?', 'redtext');
						if($count)
							$conditions[] = '!WIKINDXFIELDWIKINDX! ' . 'DOES NOT CONTAIN ' . $search;
						else
							$conditions[] = '!WIKINDXFIELDWIKINDX! ' . 'DOES NOT CONTAIN ' . $search;
					}
					$lastType = 'NOT';
					$conditionsJoin[] = 'AND';
				}
				else if($type == 'OR')
				{
					if($lastType == 'NOT')
					{
						if(!$test)
							$clauseOr[] = '!WIKINDXFIELDWIKINDX! ' . $this->db->like('%', $search, '%', TRUE);
						else
						{
							$search = \HTML\color('?' . $search . '?', 'redtext');
							if($count)
								$clauseOr[] = '!WIKINDXFIELDWIKINDX! ' . 'DOES NOT CONTAIN ' . $search;
							else
								$clauseOr[] = '!WIKINDXFIELDWIKINDX! ' . 'DOES NOT CONTAIN ' . $search;
						}
					}
					else
					{
						if(!$test)
							$clauseOr[] = '!WIKINDXFIELDWIKINDX! ' . $this->db->like('%', $search, '%');
						else
						{
							$search = \HTML\color('?' . $search . '?', 'redtext');
							if($count)
								$clauseOr[] = '!WIKINDXFIELDWIKINDX! ' . 'CONTAINS ' . $search;
							else
								$clauseOr[] = '!WIKINDXFIELDWIKINDX! ' . 'CONTAINS ' . $search;
						}
					}
					$lastType = 'OR';
				}
				else if($type == 'AND')
				{
					if(!$test)
						$conditions[] = '!WIKINDXFIELDWIKINDX! ' . $this->db->like('%', $search, '%');
					else
					{
						$search = \HTML\color('?' . $search . '?', 'redtext');
						if($count)
							$conditions[] = '!WIKINDXFIELDWIKINDX! ' . 'CONTAINS ' . $search;
						else
							$conditions[] = '!WIKINDXFIELDWIKINDX! ' . 'CONTAINS ' . $search;
					}
					$lastType = 'AND';
					$conditionsJoin[] = 'AND';
				}
			}
			else
			{
				if(($type == 'NOT') || ($type == 'exactNOT'))
				{
					if(!$test)
						$conditions[] = '!WIKINDXFIELDWIKINDX! ' . $this->db->regexp('[[:<:]]', $search, '[[:>:]]', TRUE);
					else
					{
						if($count)
						{
							if($type == 'exactNOT')
							{
								$search = \HTML\color('"' . $search . '"', 'redtext');
								$conditions[] = '!WIKINDXFIELDWIKINDX! ' . 'DOES NOT CONTAIN ' . $search;
							}
							else
								$conditions[] = '!WIKINDXFIELDWIKINDX! ' . 'DOES NOT CONTAIN ' . \HTML\color($search, 'redtext');
						}
						else
						{
							if($type == 'exactNOT')
							{
								$search = \HTML\color('"' . $search . '"', 'redtext');
								$conditions[] = '!WIKINDXFIELDWIKINDX! ' . 'DOES NOT CONTAIN ' . $search;
							}
							else
								$conditions[] = '!WIKINDXFIELDWIKINDX! ' . 'DOES NOT CONTAIN ' . \HTML\color($search, 'redtext');
						}
					}
					$lastType = 'NOT';
					$conditionsJoin[] = 'AND';
				}
				else if(($type == 'OR') || ($type == 'exactOR'))
				{
					if($lastType == 'NOT')
					{
						if(!$test)
							$clauseOr[] = '!WIKINDXFIELDWIKINDX! ' . $this->db->regexp('[[:<:]]', $search, '[[:>:]]', TRUE);
						else
						{
							if($count)
							{
								if($type == 'exactOR')
								{
									$search = \HTML\color('"' . $search . '"', 'redtext');
									$clauseOr[] = '!WIKINDXFIELDWIKINDX! ' . 'DOES NOT CONTAIN ' . $search;
								}
								else
									$clauseOr[] = '!WIKINDXFIELDWIKINDX! ' . 'DOES NOT CONTAIN ' . \HTML\color($search, 'redtext');
							}
							else
							{
								if($type == 'exactOR')
								{
									$search = \HTML\color('"' . $search . '"', 'redtext');
									$clauseOr[] = '!WIKINDXFIELDWIKINDX! ' . 'DOES NOT CONTAIN ' . $search;
								}
								else
									$clauseOr[] = '!WIKINDXFIELDWIKINDX! ' . 'DOES NOT CONTAIN ' . \HTML\color($search, 'redtext');
							}
						}
					}
					else
					{
						if(!$test)
							$clauseOr[] = '!WIKINDXFIELDWIKINDX! ' . $this->db->regexp('[[:<:]]', $search, '[[:>:]]');
						else
						{
							if($count)
							{
								if($type == 'exactOR')
								{
									$search = \HTML\color('"' . $search . '"', 'redtext');
									$clauseOr[] = '!WIKINDXFIELDWIKINDX! ' . 'CONTAINS ' . $search;
								}
								else
									$clauseOr[] = '!WIKINDXFIELDWIKINDX! ' . 'CONTAINS ' . \HTML\color($search, 'redtext');
							}
							else
							{
								if($type == 'exactOR')
								{
									$search = \HTML\color('"' . $search . '"', 'redtext');
									$clauseOr[] = '!WIKINDXFIELDWIKINDX! ' . 'CONTAINS ' . $search;
								}
								else
									$clauseOr[] = '!WIKINDXFIELDWIKINDX! ' . 'CONTAINS ' . \HTML\color($search, 'redtext');
							}
						}
					}
					$lastType = 'OR';
				}
				else if(($type == 'AND') || ($type == 'exactAND'))
				{
					if(!$test)
						$conditions[] = '!WIKINDXFIELDWIKINDX! ' . $this->db->regexp('[[:<:]]', $search, '[[:>:]]');
					else
					{
						if($count)
						{
							if($type == 'exactAND')
							{
								$search = \HTML\color('"' . $search . '"', 'redtext');
								$conditions[] = '!WIKINDXFIELDWIKINDX! ' . 'CONTAINS ' . $search;
							}
							else
								$conditions[] = '!WIKINDXFIELDWIKINDX! ' . 'CONTAINS ' . \HTML\color($search, 'redtext');
						}
						else
						{
							if($type == 'exactAND')
							{
								$search = \HTML\color('"' . $search . '"', 'redtext');
								$conditions[] = '!WIKINDXFIELDWIKINDX! ' . 'CONTAINS ' . $search;
							}
							else
								$conditions[] = '!WIKINDXFIELDWIKINDX! ' . 'CONTAINS ' . \HTML\color($search, 'redtext');
						}
					}
					$lastType = 'AND';
					$conditionsJoin[] = 'AND';
				}
			}
			if(!empty($clauseOr))
			{
				if(!empty($conditions))
					$conditions[] = $this->db->delimit(array_pop($conditions) . $this->db->or .
						join($this->db->or, $clauseOr), 'parentheses');
				else
					$conditions[] = $this->db->delimit(join($this->db->or, $clauseOr), 'parentheses');
				$clauseOr = array();
			}
			++$count;
			if($lastType != 'NOT')
				$searchHighlight[] = $highlightSearch;
		}
		if(!$this->idea)
		{
			$storedHighlight = UTF8::mb_explode(',', $this->session->getVar("search_Highlight"));
			$searchHighlight = array_unique(array_merge($storedHighlight, $searchHighlight));
			$this->session->setVar("search_Highlight", implode(",", $searchHighlight));
		}
		else
		{
			$storedHighlight = UTF8::mb_explode(',', $this->session->getVar("search_HighlightIdea"));
			$searchHighlight = array_unique(array_merge($storedHighlight, $searchHighlight));
			$this->session->setVar("search_HighlightIdea", implode(",", $searchHighlight));
		}
		$sizeof = count($conditions);
		foreach($conditions as $condition)
		{
			$sizeof--;
			if(!empty($conditionsJoin))
			{
				if(array_shift($conditionsJoin) == 'AND')
				{
					if($sizeof == 0) // last loop through
						$stringArray[] = $condition;
					else
						$stringArray[] = $condition . ' ' . $this->db->and;
				}
			}
			else
				$stringArray[] = $condition;
		}
		return join(' ', $stringArray);
	}
/**
* loop through $fragments sorting into 'exact phrase', AND fragments, NOT fragments and OR fragments
*
* @param array $fragments
*/
	private function sortFragments($fragments)
	{
		$searchArrayIndex = 0;
		while(!empty($fragments))
		{
			$fragment = array_shift($fragments);
// one word in quotes?
			if((mb_strpos($fragment, '"') === 0) && (mb_strpos($fragment, '"', 1) === mb_strlen($fragment) - 1))
			{
				$this->stringArray[$searchArrayIndex]['type'] = 'exactOR'; // treat this word as an OR clause
				$this->stringArray[$searchArrayIndex]['string'] = trim($fragment, '"');
			}
// AND keyword
			else if(($fragment === 'AND') && (sizeOf($fragments)))
			{
				$this->stringArray[$searchArrayIndex]['type'] = 'AND';
				$this->tidyFragment($fragment, $fragments, $searchArrayIndex, 'AND');
			}
// OR keyword
			else if(($fragment === 'OR') && (sizeOf($fragments)))
			{
				$this->stringArray[$searchArrayIndex]['type'] = 'OR';
				$this->tidyFragment($fragment, $fragments, $searchArrayIndex, 'OR');
			}
// NOT keyword
			else if(($fragment === 'NOT') && (sizeOf($fragments)))
			{
				$this->stringArray[$searchArrayIndex]['type'] = 'NOT';
				$this->tidyFragment($fragment, $fragments, $searchArrayIndex, 'NOT');
			}
// start of exact phrase search?
			else if(mb_strpos($fragment, '"') === 0)
			{
				$this->stringArray[$searchArrayIndex]['type'] = 'exactOR';
				$this->extractExactPhrase($fragment, $fragments, $searchArrayIndex);
			}
// everthing else, treat as OR <word> -- default behaviour
			else
			{
				$this->stringArray[$searchArrayIndex]['type'] = 'OR';
				$this->tidyFragment($fragment, $fragments, $searchArrayIndex, 'noKeyword');
			}
			if(!array_key_exists('string', $this->stringArray[$searchArrayIndex]) || !$this->stringArray[$searchArrayIndex]['string'])
				unset($this->stringArray[$searchArrayIndex]);
			++$searchArrayIndex;
		}
// first phrase must be an AND phrase
		if($this->stringArray[0]['type'] == 'OR')
			$this->stringArray[0]['type'] = 'AND';
	}
/**
* For all AND, OR or NOT fragments, tidy the input so we have something usable
*
* @param string $fragment
* @param array $fragments Reference to array
* @param int $searchArrayIndex
* @param string $type Default is FALSE
*/
	private function tidyFragment($fragment, &$fragments, $searchArrayIndex, $type = FALSE)
	{
		if(sizeOf($fragments) && ($type != 'noKeyword'))
		{
			do
				$fragment = array_shift($fragments);
				while(!$fragment);
		}
// one word in quotes?
		if((mb_strpos($fragment, '"') === 0) && (mb_strpos($fragment, '"', 1) === mb_strlen($fragment) - 1))
		{
			if($type == 'noKeyword')
				$type = 'AND';
			$this->stringArray[$searchArrayIndex]['type'] = 'exact' . $type;
			$this->stringArray[$searchArrayIndex]['string'] = trim($fragment, '"');
		}
		else if(mb_strpos($fragment, '"') === 0) // keyword is followed by exact phrase
		{
			if($type == 'noKeyword')
				$type = 'AND';
			$this->stringArray[$searchArrayIndex]['type'] = 'exact' . $type;
			$this->extractExactPhrase($fragment, $fragments, $searchArrayIndex);
		}
		else
		{
			if($fragment = preg_replace("/[^\pL\pN\p{Zs}'-][!\*\?]/u", '', $fragment)) // remove useless characters (leave wildcards)
			{
// convert to MySQL wildcards: ? = '_', * = '%' if required
				if(!$this->fulltextSearch) //leave '? and '*' for attachment searches
				{
// NB. For wildcards, we always use regex
					$this->stringArray[$searchArrayIndex]['string'] = preg_replace(array("/\*/u", "/\?/u"),
						array("[[:alnum:]]*", "[[:alnum:]]?"), $fragment, -1, $count);
					if($count)
						$this->useRegex[$searchArrayIndex] = TRUE;
					else
						$this->useRegex[$searchArrayIndex] = FALSE;
				}
				else
					$this->stringArray[$searchArrayIndex]['string'] = $fragment;
			}
			if($type == 'NOT')
				$this->NOTfragments[] = $fragment;
		}
	}
/**
* extract all fragment elements until end of exact phrase is found
*
* @param string $fragment
* @param array $fragments Reference to array
* @param int $searchArrayIndex
*/
	private function extractExactPhrase($fragment, &$fragments, $searchArrayIndex)
	{
		$this->stringArray[$searchArrayIndex]['string'] = ltrim($fragment, '"'); // start of exact phrase
		$extracted = FALSE;
		while(!$extracted)
		{
			$fragment = array_shift($fragments);
			if(!$fragment) // extra space -- must keep for exact phrase
				$this->stringArray[$searchArrayIndex]['string'] .= ' ' . $fragment;
			if(mb_strpos($fragment, '"') === mb_strlen($fragment) - 1) // end of exact phrase
			{
				$this->stringArray[$searchArrayIndex]['string'] .= ' ' . rtrim($fragment, '"');
				$extracted = TRUE;
			}
			else
				$this->stringArray[$searchArrayIndex]['string'] .= ' ' . $fragment;
		}
	}
/**
* split up search string on single spaces
*
* @param string $phrase
*/
	private function splitSpaces($phrase)
	{
		return UTF8::mb_explode(' ', $phrase);
	}
/**
* check for malformed search strings -- i.e. odd no. of " chars
*
* @param string $phrase
* @return boolean
*/
	private function malformedString($phrase)
	{
// odd no. of " chars
		if(mb_substr_count($phrase, '"') % 2)
			return FALSE;
// Reserved word
		if(mb_strpos($phrase, '!WIKINDXFIELDWIKINDX!') !== FALSE)
			return FALSE;
// discard empty exact phrases and phrases where the final quote is not followed by a space
		else if(mb_substr_count($phrase, '"'))
		{
			preg_match_all('/"/u', $phrase, $matches, PREG_OFFSET_CAPTURE);
			$count = 0;
			foreach($matches as $array)
			{
				foreach($array as $key => $null)
				{
					$offset = $array[$key][1] + 1;
					if($offset == mb_strlen($phrase)) // reached end of search phrase
						return TRUE;
// 2nd of a pair of quotes not at the end of the phrase and not followed by space
					if(($count % 2) && (mb_substr($phrase, $offset, 1) != ' '))
						return FALSE;
					++$count;
				}
			}
		}
		return TRUE;
	}
/**
* Return words governed by AND, OR or NOT
*
* @param string $phrase
* @return string
*/
	private function tidySearch($phrase)
	{
		if(preg_match('/^AND(.*)|^OR(.*)|(.*)AND$|(.*)OR$|(.*)NOT$/u', $phrase, $matches))
			return array_pop($matches);
		return $phrase;
	}
/**
* Remove any NOT words that are listed as AND or OR and any words to be filtered
*/
	private function removeNot_and_FilterWords()
	{
	    if (is_array($this->stringArray))
	    {
	        if (count($this->stringArray) > 0)
	        {
        		$delete = array();
        		foreach($this->stringArray as $index => $array)
        		{
        			if((($array['type'] == 'AND') || ($array['type'] == 'OR') || ($array['type'] == 'NOT'))
        				&&
        				(array_search(mb_strtolower($array['string']), $this->config->WIKINDX_SEARCHFILTER) !== FALSE))
        			{
        				$delete[] = $index;
        				continue;
        			}
        			foreach($this->NOTfragments as $not)
        			{
        				if((($array['type'] == 'AND') || ($array['type'] == 'OR')) &&
        					(mb_strtolower($array['string']) == mb_strtolower($not)))
        					$delete[] = $index;
        			}
        		}
        		foreach($delete as $index)
        			unset($this->stringArray[$index]);
    		}
		}
	}
/**
* Neatly print the elements of the quoteFragments array -- for debugging purposes only
*
* @param array $fragments
*/
	private function printFragments($fragments)
	{
		foreach($fragments as $key => $value)
		{
			if(!$value)
				$value = 'EXTRASPACE';
			print "$key: $value" . BR;
		}
	}
}
