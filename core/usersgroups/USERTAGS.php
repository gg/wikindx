<?php
/**
 * WIKINDX : Bibliographic Management system.
 * @link https://wikindx.sourceforge.io/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2017 Mark Grimshaw-Aagaard <sirfragalot@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 */

/**
* USERTAGS
*
* Common methods for handling user tags.
*
* @version	1
*
*	@package wikindx\core\usergroups
*	@author Mark Grimshaw-Aagaard <sirfragalot@users.sourceforge.net>
*
*/
class USERTAGS
{
/** object */
private $db;
/** object */
/** object */
private $session;
/** object */
private $commonBib;

/**
*	USERTAGS
*/
	public function __construct()
	{
		$this->db = FACTORY_DB::getInstance();

		$this->session = FACTORY_SESSION::getInstance();
		$this->commonBib = FACTORY_BIBLIOGRAPHYCOMMON::getInstance();
	}
/**
* grab all current usertags for this user as assoc. array
*
* @param boolean $userBib Default is FALSE
* @param array $typeArray Default is FALSE
* @param boolean $populated Default is FALSE
* @return array
*/
	public function grabAll($userBib = FALSE, $typeArray = FALSE, $populated = FALSE)
	{
		$this->db->leftJoin('resource_user_tags', 'resourceusertagsTagId', 'usertagsId');
		if($userBib)
			$this->commonBib->userBibCondition('resourceusertagsResourceId');
		if(is_array($typeArray) && !empty($typeArray))
		{
			$this->db->leftJoin('resource', 'resourceId', 'resourceusertagsResourceId');
			foreach($typeArray as $type)
				$conditions[] = $type;
			$this->db->formatConditionsOneField($conditions, 'resourceType');
		}
		$this->db->formatConditions(array('usertagsUserId' => $this->session->getVar('setup_UserId')));
		if($populated)
		{
			$this->db->groupBy(array('usertagsId', 'usertagsTag', 'resourceusertagsTagId'), TRUE, $this->db->count('resourceusertagsTagId') . '>0');
			$resultset = $this->db->selectCounts('user_tags', 'resourceusertagsTagId', array('usertagsId', 'usertagsTag'), FALSE, FALSE);
		}
		else
			$resultset = $this->db->select('user_tags', array('usertagsId', 'usertagsTag'));
		$array = array();
		while($row = $this->db->fetchRow($resultset))
			$array[$row['usertagsId']] = \HTML\dbToFormTidy($row['usertagsTag']);
		return $array;
	}
/**
* Does this usertag already exist in the database?
*
* @param string $usertag
* @return mixed FALSE|usertag ID
*/
	public function checkExists($usertag)
	{
		$this->db->formatConditions(array('usertagsUserId' => $this->session->getVar('setup_UserId')));
		$this->db->formatConditions($this->db->formatFields('usertagsTag') . $this->db->like(FALSE, $usertag, FALSE));
		$resultset = $this->db->select('user_tags', 'usertagsId');
		if($this->db->numRows($resultset))
			return $this->db->fetchOne($resultset);
		else
		    return FALSE; // not found
	}
/**
* Make the usertag select transfer arrows to transfer usertags between select boxes with onclick
*
* @return array (toRightImage, toLeftImage)
*/
	public function transferArrows()
	{

		$jsonArray = array();
		$jsonArray[] = array(
			'startFunction' => 'selectUserTag',
			);
		$toRightImage = \AJAX\jActionIcon('toRight', 'onclick', $jsonArray);
		$jsonArray = array();
		$jsonArray[] = array(
			'startFunction' => 'discardUserTag',
			);
		$toLeftImage = \AJAX\jActionIcon('toLeft', 'onclick', $jsonArray);
		return array($toRightImage, $toLeftImage);
	}
}
