<?php
/**
 * WIKINDX : Bibliographic Management system.
 * @link https://wikindx.sourceforge.io/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2017 Mark Grimshaw-Aagaard <sirfragalot@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 */

/**
* TAG
*
* An import may be tagged so that the resources in it may be tracked in later list and delete operations.
*
* @version	1
*
*	@package wikindx\core\miscellaneous
*	@author Mark Grimshaw-Aagaard <sirfragalot@users.sourceforge.net>
*
*/
class TAG
{
/** object */
private $db;
/** object */

/**
*	TAG class
*/
	public function __construct()
	{
		$this->db = FACTORY_DB::getInstance();

	}
/**
* Get tags from tag table.
*
* @return mixed FALSE|associative array of id => group.
*/
	public function grabAll()
	{
		$this->db->orderBy('tagTag');
		$recordset = $this->db->select('tag', array('tagId', 'tagTag'));
		while($row = $this->db->fetchRow($recordset))
			$tags[$row['tagId']] = \HTML\dbToFormTidy($row['tagTag']);
		if(isset($tags))
			return $tags;
		else
		    return FALSE;
	}
/**
* Does this tag already exist in the database?
*
* @param string $tag
* @return mixed FALSE|tag ID
*/
	public function checkExists($tag)
	{
		$this->db->formatConditions(array('tagTag' => $tag));
		$resultset = $this->db->select('tag', 'tagId');
		if($this->db->numRows($resultset))
			return $this->db->fetchOne($resultset);
		else
		    return FALSE; // not found
	}
}
