<?php
/*
Copyright Stéphane Aulery, 2019

<lkppo@users.sourceforge.net>

Ce logiciel est un programme informatique servant à préparer le code de
wikindx pour sa publication officiel.

Ce logiciel est régi par la licence CeCILL-B soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-B telle que diffusée par le CEA, le CNRS et l'INRIA
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme, le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard l'attention de l'utilisateur est attirée sur les risques
associés au chargement, à l'utilisation, à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant
donné sa spécificité de logiciel libre, qui peut le rendre complexe à
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant des connaissances informatiques approfondies. Les
utilisateurs sont donc invités à charger et tester l'adéquation du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
pris connaissance de la licence CeCILL-B, et que vous en avez accepté les
termes.
*/

include_once("core/startup/CONSTANTS.php");
include_once("core/file/FILE.php");

///////////////////////////////////////////////////////////////////////
/// Configuration
///////////////////////////////////////////////////////////////////////

$dirroot = __DIR__;
$dirsrc = implode(DIRECTORY_SEPARATOR, [__DIR__, "docs"]);

///////////////////////////////////////////////////////////////////////
/// MAIN
///////////////////////////////////////////////////////////////////////

echo "\n";
echo "Building markdown documentation\n";

$listDocFile = array(
    $dirsrc => FILE\fileInDirToArray($dirsrc),
);

foreach ($listDocFile as $dir => $aFile)
{
    foreach ($aFile as $file)
    {
        $fsrc = $dir . DIRECTORY_SEPARATOR . $file;
        $fdst = $dir . DIRECTORY_SEPARATOR . basename($file, ".md") . ".htm";
        chdir($dir);

        if (is_file($fsrc) && matchExtension($file, ".md"))
        {
            echo " - $file\n";
            exec("pandoc --verbose --self-contained --number-sections --toc --data-dir=\"$dir\" --from=markdown --to=html5 --output=\"$fdst\" \"$fsrc\"");
        }
    }
}


///////////////////////////////////////////////////////////////////////
/// Library
///////////////////////////////////////////////////////////////////////

function matchExtension($filename, $ext)
{
    return (mb_strtolower(mb_substr($filename, - mb_strlen($ext))) == $ext);
}
