-- 
-- WIKINDX : Bibliographic Management system.
-- @link https://wikindx.sourceforge.io/ The WIKINDX SourceForge project
-- @author The WIKINDX Team
-- @copyright 2018 Stéphane Aulery <lkppo@users.sourceforge.net>
-- @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
-- 
-- Initially set all users to default settings
-- Same for read-only users in config table
-- 

SET NAMES latin1;
SET CHARACTER SET latin1;

UPDATE %%WIKINDX_DB_TABLEPREFIX%%users
SET
	usersStyle = 'APA',
	usersLanguage = 'en',
	usersTemplate = 'default',
	usersCookie = 'N';

UPDATE %%WIKINDX_DB_TABLEPREFIX%%config
SET
	configStyle = 'APA',
	configLanguage = 'en',
	configTemplate = 'default';
