-- 
-- WIKINDX : Bibliographic Management system.
-- @link https://wikindx.sourceforge.io/ The WIKINDX SourceForge project
-- @author The WIKINDX Team
-- @copyright 2018 Stéphane Aulery <lkppo@users.sourceforge.net>
-- @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
-- 
-- Remove deprecated language options for CMS and RSS
-- 
-- https://mathiasbynens.be/notes/mysql-utf8mb4

DELETE FROM %%WIKINDX_DB_TABLEPREFIX%%config
WHERE configName IN ('configCmsLanguage', 'configRssLanguage');
