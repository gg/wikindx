-- 
-- WIKINDX : Bibliographic Management system.
-- @link https://wikindx.sourceforge.io/ The WIKINDX SourceForge project
-- @author The WIKINDX Team
-- @copyright 2018 Stéphane Aulery <lkppo@users.sourceforge.net>
-- @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
-- 
-- Add columns
-- 

ALTER TABLE %%WIKINDX_DB_TABLEPREFIX%%resource_keyword ADD COLUMN `resourcekeywordMetadataId` INT(11) DEFAULT NULL;
ALTER TABLE %%WIKINDX_DB_TABLEPREFIX%%keyword          ADD COLUMN `keywordGlossary` TEXT DEFAULT NULL;
ALTER TABLE %%WIKINDX_DB_TABLEPREFIX%%collection       ADD COLUMN `collectionDefault` LONGTEXT DEFAULT NULL;
ALTER TABLE %%WIKINDX_DB_TABLEPREFIX%%users            ADD COLUMN `usersNotifyDigestThreshold` INT(11) DEFAULT 100;

ALTER TABLE %%WIKINDX_DB_TABLEPREFIX%%resource_custom  ADD INDEX  `resourcecustomResourceId` (resourcecustomResourceId);