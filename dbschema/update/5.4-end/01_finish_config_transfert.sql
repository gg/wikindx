-- 
-- WIKINDX : Bibliographic Management system.
-- @link https://wikindx.sourceforge.io/ The WIKINDX SourceForge project
-- @author The WIKINDX Team
-- @copyright 2018 Stéphane Aulery <lkppo@users.sourceforge.net>
-- @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
-- 
-- Finish transfers of config table
-- 

DROP TABLE IF EXISTS %%WIKINDX_DB_TABLEPREFIX%%config;

ALTER TABLE %%WIKINDX_DB_TABLEPREFIX%%configtemp RENAME `%%WIKINDX_DB_TABLEPREFIX%%config`;

ALTER TABLE %%WIKINDX_DB_TABLEPREFIX%%config ADD INDEX `configName` (`configName`(768));
