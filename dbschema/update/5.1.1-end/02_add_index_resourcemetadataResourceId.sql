-- 
-- WIKINDX : Bibliographic Management system.
-- @link https://wikindx.sourceforge.io/ The WIKINDX SourceForge project
-- @author The WIKINDX Team
-- @copyright 2018 Stéphane Aulery <lkppo@users.sourceforge.net>
-- @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
-- 
-- Add index resourcemetadataResourceId
-- 

ALTER TABLE %%WIKINDX_DB_TABLEPREFIX%%user_bibliography ADD INDEX `userbibliographyTitle` (`userbibliographyTitle`(768));
ALTER TABLE %%WIKINDX_DB_TABLEPREFIX%%resource_metadata ADD INDEX `resourcemetadataResourceId` (resourcemetadataResourceId);
