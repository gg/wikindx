<?php
/**
 * WIKINDX : Bibliographic Management system.
 * @link https://wikindx.sourceforge.io/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2018 Mark Grimshaw-Aagaard <sirfragalot@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 */

/**
* RSS
*
* RSS feed
*
* @version	3
*
*	@package wikindx
*	@author Mark Grimshaw-Aagaard <sirfragalot@users.sourceforge.net>
*/

include_once("core/startup/CONSTANTS.php");

function preserve_qs()
{
    if (empty($_SERVER['QUERY_STRING']) && mb_strpos($_SERVER['REQUEST_URI'], '?') === FALSE) {
        return '';
    }
    return '&' . $_SERVER['QUERY_STRING'];
}

// This code had moved to core/modules/rss/RSS.php
// to use the current module loading scheme.
// Keep this page only to not break external links
// LkpPo, 20180802
// HTTP/1.0 301 Moved Permanently
header('Location: ' . WIKINDX_RSS_PAGE . preserve_qs(), TRUE, 301);

exit();
