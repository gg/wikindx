<?php
/**
* Handle updates and new files for plugins, styles, languages, and templates.
*
* WIKINDX : Bibliographic Management system.
* @link https://wikindx.sourceforge.io/ The WIKINDX SourceForge project
* @author The WIKINDX Team
* @copyright 2018–2019 Mark Grimshaw-Aagaard <sirfragalot@users.sourceforge.net>
* @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
*/ 

// check type of method required
    if (array_key_exists('method', $_GET) && ($_GET['method'] == 'reportPlugins'))
    	reportPlugins();
   	else if (array_key_exists('method', $_GET) && ($_GET['method'] == 'getServerReadme') && array_key_exists('path', $_GET))
    	getServerReadme($_GET['path']);
    else if (array_key_exists('method', $_GET) && ($_GET['method'] == 'reportStyles'))
    	reportStyles();
    else if (array_key_exists('method', $_GET) && ($_GET['method'] == 'reportTemplates'))
    	reportTemplates();
    else if (array_key_exists('method', $_GET) && ($_GET['method'] == 'reportLanguages'))
    	reportLanguages();
    else if (array_key_exists('method', $_GET) && ($_GET['method'] == 'zipDirectory') && array_key_exists('path', $_GET))
    	zipDirectory();
    else
    	print json_encode('ERROR: No method found');

exit();

/**
* Return the readme file
*/
	function getServerReadme($path)
	{
		$string = '';
		$file = $path . '/README.txt';
		if(file_exists($file))
			$string = file_get_contents($file);
		else
		{
			$file = $path . '/README';
			if(file_exists($file))
				$string = file_get_contents($file);
		}
		if(!$string)
			print json_encode("ERROR: No README or README is empty");
		else
			print json_encode(array($string));
	}


/**
* Report on any updates for or new languages
*/
	function reportLanguages()
	{
// read plugins directory
		$languageList = array();
		$topFolder = 'languages';
		if($handle = opendir($topFolder))
		{
			$topFolder .= '/';
			while(FALSE !== ($dir = readdir($handle)))
			{
				if(($dir != '.') && ($dir != '..') && is_dir($topFolder . $dir))
				{
					$filePath = $topFolder . $dir . '/' . 'description.txt';
					if(file_exists($filePath))
					{
						$fh = fopen($filePath, "r");
						$languageList[$dir]['name'] = fgets($fh);
						fclose($fh);
					}
					else
						$languageList[$dir]['name'] = $dir;
					$array = getHighestFileTimestamp($topFolder . $dir . '/');
					$languageList[$dir]['timestamp'] = $array;
				}
			}
			closedir($handle);
		}
		else
			print json_encode("ERROR: not opened");
		if(!empty($languageList))
			print json_encode($languageList);
		else
			print json_encode("ERROR: no languages on the server");
	}
/**
* Report on any updates for or new templates
*/
	function reportTemplates()
	{
// read plugins directory
		$templateList = array();
		$topFolder = 'templates';
		if($handle = opendir($topFolder))
		{
			$topFolder .= '/';
			while(FALSE !== ($dir = readdir($handle)))
			{
				if(($dir != '.') && ($dir != '..') && is_dir($topFolder . $dir))
				{
					$filePath = $topFolder . $dir . '/' . 'description.txt';
					if(file_exists($filePath))
					{
						$fh = fopen($filePath, "r");
						$templateList[$dir]['name'] = fgets($fh);
						fclose($fh);
					}
					else
						$templateList[$dir]['name'] = $dir;
					$array = getHighestFileTimestamp($topFolder . $dir . '/');
					$templateList[$dir]['timestamp'] = $array;
				}
			}
			closedir($handle);
		}
		else
			print json_encode("not opened");
		if(!empty($templateList))
			print json_encode($templateList);
		else
			print json_encode("no templates on the server");
	}
/**
* Report on any updates for or new styles
*/
	function reportStyles()
	{
// read plugins directory
		$styleList = array();
		$topFolder = 'styles';
		if($handle = opendir($topFolder))
		{
			$topFolder .= '/';
			while(FALSE !== ($dir = readdir($handle)))
			{
				if(($dir != '.') && ($dir != '..') && is_dir($topFolder . $dir))
				{
					$xml = $topFolder . $dir . '/' . mb_strtoupper($dir) . '.xml';
					$readme = $topFolder . $dir . '/README.txt';
					if(file_exists($xml) && file_exists($readme)) // Appears to be valid style
					{
					    $fh = fopen($xml, "r");
						while(!feof($fh))
						{
							$line = stream_get_line($fh, 1000000, "". LF);
							if(preg_match("/<description>(.*)<\\/description>/Uui", $line, $matches))
							{
								$array[mb_strtoupper($dir)] = $matches[1];
								$styleList[$dir]['name'] = $matches[1];
								break;
							}
						}
						fclose($fh);
						if(!array_key_exists('name', $styleList[$dir]))
							$styleList[$dir]['name'] = $dir;
						$array = getHighestFileTimestamp($topFolder . $dir . '/');
						$styleList[$dir]['timestamp'] = $array;
					}
				}
			}
			closedir($handle);
		}
		else
			print json_encode("ERROR: not opened");
		if(!empty($styleList))
			print json_encode($styleList);
		else
			print json_encode("ERROR: no styles on the server");
	}
/**
* Report on any updates for or new plugins
*/
	function reportPlugins()
	{
// read plugins directory
		$pluginList = array();
		$topFolder = 'plugins';
		if($handle = opendir($topFolder))
		{
			$topFolder .= '/';
			while(FALSE !== ($dir = readdir($handle)))
			{
				if(($dir != '.') && ($dir != '..') && is_dir($topFolder . $dir))
				{
					$index = $topFolder . $dir . '/index.php';
					$config = $topFolder . $dir . '/config.php';
					$type = $topFolder . $dir . '/plugintype.txt';
					if(file_exists($index) && file_exists($config) && file_exists($type)) // Appears to be valid plugin
					{
						$filePath = $topFolder . $dir . '/' . 'description.txt';
						if(file_exists($filePath))
						{
							$fh = fopen($filePath, "r");
							$pluginList[$dir]['name'] = fgets($fh);
							fclose($fh);
						}
						else
							$pluginList[$dir]['name'] = $dir;
						$array = getHighestFileTimestamp($topFolder . $dir . '/');
						$pluginList[$dir]['timestamp'] = $array;
						include_once($config);
						$className = $dir . "_CONFIG";
						$class = new $className();
						$pluginList[$dir]['wikindxVersion'] = $class->wikindxVersion;
						$classVars = get_object_vars($class);
// Grab class properties (NB only public properties can be checked but all properties in config.php are likely to be public
						foreach($classVars as $property => $value)
							$pluginList[$dir]['configProperties'][$property] = $property;
					}
				}
			}
			closedir($handle);
		}
		else
			print json_encode("ERROR: not opened");
		if(!empty($pluginList))
			print json_encode($pluginList);
		else
			print json_encode("ERROR: no plugins on the server");
	}
	function getAllFiles($directory, $recursive = TRUE) 
	{
    	$result = array();
    	$handle = opendir($directory);
    	while(FALSE !== ($datei = readdir($handle)))
    	{
       		if (($datei != '.') && ($datei != '..'))
  			{
				$file = $directory . $datei;
            	if (is_dir($file))
            	{
            		if($recursive)
                 		$result = array_merge($result, getAllFiles($file . '/'));
            	}
            	else
              		$result[] = $file;
    		}
		}
		closedir($handle);
		return $result;
	}

	function getHighestFileTimestamp($directory, $recursive = TRUE)
	{
		$allFiles = getAllFiles($directory, $recursive);
		$highestKnown = 0;
		foreach($allFiles as $value)
		{
			$currentValue = filemtime($value);
			if($currentValue > $highestKnown)
			{
				$highestKnown = $currentValue;
				$newestFile = $value;
			}
     	}
     	return array($newestFile => $highestKnown);
	}
/**
* Zip a directory and return a pathname
*
* @param array $files unqualified filenames (key is label of file, value is filename on disk)
* @param string $path file path
* @return mixed unqualified SHA1'ed filename of zip or FALSE if failure
*/
	function zipDirectory()
	{
		if(!class_exists('ZipArchive'))
		{
			print json_encode("ERROR: ZipArchive class does not exist on the server");
			return;
		}
		$sourceDirectory  = $_GET['path'] . '/';
		$zipFile = uniqid() . '.zip';
		$zip = new ZipArchive();
// Snippet pinched from http://megarush.net/compress-a-folder-containing-sub-folders-and-files/
		if($zip->open($zipFile, ZIPARCHIVE::CREATE) !== TRUE)
		{
			print json_encode("ERROR: Unable to create ZIP file");
			return;
		}
		$source = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($sourceDirectory));
		foreach($source as $f => $value)
		{
			if(!$zip->addFile(realpath($f), $f))
			{
				print json_encode("ERROR: Unable to add file: $f");
				return;
			}
		}
		$zip->close();
		print json_encode(array($zipFile));
	}
